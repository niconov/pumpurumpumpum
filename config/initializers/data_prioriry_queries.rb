qs = [
  'бумага для выпечки',
  'пакеты фасовочные',
  'пленка пищевая',
  'мешки для мусора',
  'вилки одноразовые',
  'стаканы бумажные'
]

Rails.application.configure do
  config.after_initialize do
		qs.map do |query|
		  pup = Deliveries::PriorityQuery.find_or_create_by query: query
		  pup.pages = 5
		  pup.save
		end
  end
end