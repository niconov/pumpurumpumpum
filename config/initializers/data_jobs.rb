jobs = [
    {
        job_name: 'WbUpdateAdvertsStatsJob',
        name: 'Реклама/Статистика: обновление',
        period_minute: 15,
        only_one: false,
        only_admin: false,
        queue: 'adverts'
    },
    {
        job_name: 'WbUpdatePricesJob',
        name: 'Цены: обновление',
        period_minute: 5,
        only_one: false,
        only_admin: false,
        queue: 'nms'
    },
    {
        job_name: 'WbUpdateNmStatJob',
        name: 'Номенклатура: конверсии',
        period_minute: 15,
        only_one: false,
        only_admin: false,
        queue: 'nms'
    },
    {
        job_name: 'WbUpdateAdvertsJob',
        name: 'Реклама: обновление',
        period_minute: 5,
        only_one: false,
        only_admin: false,
        queue: 'adverts'
    },
    {
        job_name: 'WbUpdateAdvertClusterJob',
        name: 'Реклама: кластеры',
        period_minute: 25,
        only_one: false,
        only_admin: false,
        queue: 'adverts'
    },

    

    {
        job_name: 'WbUpdateAdvertAutoStatJob',
        name: 'Реклама: статистика по ключам АРК',
        period_minute: 60,
        only_one: false,
        only_admin: false,
        queue: 'adverts'
    },
    {
        job_name: 'WbUpdateOrdersJob',
        name: 'Заказы: обновление',
        period_minute: 5,
        only_one: false,
        only_admin: false,
        queue: 'sales'
    },
    {
        job_name: 'WbUpdateNmJob',
        name: 'Номенклатура: обновление',
        period_minute: 5,
        only_one: false,
        only_admin: false,
        queue: 'nms'
    },
    {
        job_name: 'WbAutoresponderJob',
        name: 'Автоответчик',
        period_minute: 3,
        only_one: false,
        only_admin: false,
        queue: 'autoresponder'
    },

    {
        job_name: 'WbUpdatePositionsByQueryJob',
        name: 'Позиции по запросам: обновление',
        period_minute: 10,
        only_one: false,
        only_admin: false,
        queue: 'parsing'
    },
    {
        job_name: 'WbUpdateIncomesJob',
        name: 'Поступления: обновление',
        period_minute: 30,
        only_one: false,
        only_admin: false,
        queue: 'stocks'
    },
    {
        job_name: 'WbUpdateStocksJob',
        name: 'Остатки: обновление',
        period_minute: 5,
        only_one: false,
        only_admin: false,
        queue: 'stocks'
    },
    {
        job_name: 'WbUpdateStockHistoryJob',
        name: 'Остатки: история',
        period_minute: 60,
        only_one: false,
        only_admin: false,
        queue: 'stocks'
    },


    {
        job_name: 'WbUpdateSaleReportJob',
        name: 'Финансовые отчеты',
        period_minute: 60,
        only_one: false,
        only_admin: false,
        queue: 'sales'
    },
    {
        job_name: 'StatsUpdateOrderByDistrictStatsJob',
        name: 'Статистика заказов по округам',
        period_minute: 1440,
        only_one: false,
        only_admin: false,
        queue: 'stats'
    },
    {
        job_name: 'StatsUpdateOrderDailyStatsJob',
        name: 'Статистика заказов по дням',
        period_minute: 1440,
        only_one: true,
        only_admin: true,
        queue: 'stats'
    },

    
    {
        job_name: 'WbUpdateWarehousePriorityJob',
        name: 'Приоритет складов',
        period_minute: 30,
        only_one: true,
        only_admin: true,
        queue: 'parsing'
    },
    {
        job_name: 'WbAdvertBudgetReplenishJob',
        name: 'Пополнение бюджета РК',
        period_minute: 30,
        only_one: false,
        only_admin: false,
        queue: 'adverts'
    },

    {
        job_name: 'QueryCleanerJob',
        name: 'Очистка очереди',
        period_minute: 1,
        only_one: true,
        only_admin: true,
        queue: 'system'
    },

    {
        job_name: 'WbTariffUpdateJob',
        name: 'Тарифы',
        period_minute: 60,
        only_one: true,
        only_admin: true,
        queue: 'tariffs'
    },
    

    
]

Rails.application.configure do
  config.after_initialize do
    jobs.map do |i|
      t = ScheduleTemplate.find_or_create_by(job_name: i[:job_name])
      t.job_name = i[:job_name]
      t.name = i[:name]
      t.period_minute = i[:period_minute]
      t.only_admin = i[:only_admin]
      t.only_one = i[:only_one]
      t.queue = i[:queue]
      t.save
    end
  end
end




