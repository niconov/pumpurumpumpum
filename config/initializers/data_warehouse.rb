data = {
    "id": "json-rpc_10",
    "jsonrpc": "2.0",
    "result": {
        "warehouses": [
            {
                "id": 119798,
                "displayName": "220вольт склад",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 124583,
                "displayName": "FBS Европа",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 124093,
                "displayName": "FBS Екатеринбург 48ч",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 124094,
                "displayName": "FBS Екатеринбург 72ч",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 124095,
                "displayName": "FBS Екатеринбург 96ч",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 123820,
                "displayName": "FBS Казань 48ч",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 123821,
                "displayName": "FBS Казань 72ч",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 123822,
                "displayName": "FBS Казань 96ч",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 124584,
                "displayName": "FBS Китай",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 124096,
                "displayName": "FBS Краснодар 48ч",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 124097,
                "displayName": "FBS Краснодар 72ч",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 124098,
                "displayName": "FBS Краснодар 96ч",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 127445,
                "displayName": "FBS Краснодар Магнит №1",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 127447,
                "displayName": "FBS Краснодар Магнит №2",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 132344,
                "displayName": "FBS Краснодар Шипы и Розы",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 127456,
                "displayName": "FBS Магнит №10 Гидрострой",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 127457,
                "displayName": "FBS Магнит №11 Рэфил",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 127459,
                "displayName": "FBS Магнит №12 Красных Па",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 127463,
                "displayName": "FBS Магнит №13 Театральны",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 127464,
                "displayName": "FBS Магнит №14 Леваневски",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 127465,
                "displayName": "FBS Магнит №15 Архат",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 132150,
                "displayName": "FBS Магнит №16 Хёрбранц",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 132035,
                "displayName": "FBS Магнит №17 Раздув",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 132036,
                "displayName": "FBS Магнит №18 Байронизм",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 127449,
                "displayName": "FBS Магнит №3 Ницшеанец",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 127450,
                "displayName": "FBS Магнит №4 Домино",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 127451,
                "displayName": "FBS Магнит №5 Рубероидный",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 127452,
                "displayName": "FBS Магнит №6 Зиповский",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 127453,
                "displayName": "FBS Магнит №7 Зеленый",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 127454,
                "displayName": "FBS Магнит №8 Мореттини",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 127455,
                "displayName": "FBS Магнит №9 Балака",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 133618,
                "displayName": "FBS Москва AMF 4",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 132709,
                "displayName": "FBS Москва Гребнев Илья 4",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 132597,
                "displayName": "FBS Москва Джема 24ч",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 132291,
                "displayName": "FBS Москва Курьер 48",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 132994,
                "displayName": "FBS Москва Курьер КБТ 48",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 132807,
                "displayName": "FBS Москва ПроЛофт  5 дне",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 133348,
                "displayName": "FBS Москва Русский икорны",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 133347,
                "displayName": "FBS Москва Русский икорны",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 132318,
                "displayName": "FBS Москва Своими силами ",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 132321,
                "displayName": "FBS Москва Своими силами ",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 132320,
                "displayName": "FBS Москва Своими силами ",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 132012,
                "displayName": "FBS Москва Склад Экспресс",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 131999,
                "displayName": "FBS Москва Склад Экспресс",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 125186,
                "displayName": "FBS Москва Фуд 12ч",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 126667,
                "displayName": "FBS Москва Цветы 12ч",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 126680,
                "displayName": "FBS Москва Экспресс 24/7",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 126679,
                "displayName": "FBS Москва Экспресс 9-18",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 126675,
                "displayName": "FBS Москва Экспресс-доста",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 126674,
                "displayName": "FBS Москва Экспресс-доста",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 127014,
                "displayName": "FBS Москва Экспресс-доста",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 126670,
                "displayName": "FBS Москва Экспресс-доста",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 126676,
                "displayName": "FBS Москва Экспресс-доста",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 124099,
                "displayName": "FBS Новосибирск 48ч",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 124100,
                "displayName": "FBS Новосибирск 72ч",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 124101,
                "displayName": "FBS Новосибирск 96ч",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 122590,
                "displayName": "FBS Очаково 48ч",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 122591,
                "displayName": "FBS Очаково 72ч",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 122592,
                "displayName": "FBS Очаково 96ч",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 117480,
                "displayName": "FBS ПВЗ Электросталь",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 123816,
                "displayName": "FBS СПБ 48ч",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 123817,
                "displayName": "FBS СПБ 72ч",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 123818,
                "displayName": "FBS СПБ 96ч",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 132473,
                "displayName": "FBS Санкт-Петербург Импер",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 132294,
                "displayName": "FBS Санкт-Петербург Семиц",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 132453,
                "displayName": "FBS Санкт-Петербург Спири",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 132015,
                "displayName": "FBS Санкт-Петербург Холод",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 132061,
                "displayName": "FBS Санкт-Петербург Экспр",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 132060,
                "displayName": "FBS Санкт-Петербург Экспр",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 125611,
                "displayName": "FBS Фарма",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 127466,
                "displayName": "FBS Холодильник КГТ",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 119781,
                "displayName": "JoyJoy",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 174,
                "displayName": "old Казань",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 1785,
                "displayName": "Алматы 8 мкр РЦ",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 218987,
                "displayName": "Алматы Атакент",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 1772,
                "displayName": "Ангарск 272 квартал РЦ",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 1786,
                "displayName": "Артем Интернациональная Р",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 1812,
                "displayName": "Архангельск Революции РЦ",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 204939,
                "displayName": "Астана",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 1724,
                "displayName": "Астана Жумабаева РЦ",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 6153,
                "displayName": "Белгород",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 206236,
                "displayName": "Белые Столбы",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 205859,
                "displayName": "Бишкек",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 1864,
                "displayName": "Братск Индустриальный РЦ",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 6146,
                "displayName": "Брянск",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 1789,
                "displayName": "Брянск Ленина РЦ",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 1790,
                "displayName": "Великий Новгород Десятинн",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 302856,
                "displayName": "Видное",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 117413,
                "displayName": "Виртуальный склад Кари",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 1791,
                "displayName": "Владивосток Тухачевского",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 115398,
                "displayName": "Владикавказ - old",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 1775,
                "displayName": "Владимир Матросова РЦ",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 217678,
                "displayName": "Внуково КБТ",
                "isActive": true,
                "allowKgt": true,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 578,
                "displayName": "Волгоград",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 117498,
                "displayName": "Воронеж - old",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 1723,
                "displayName": "Воронеж Фестивальный РЦ",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 133355,
                "displayName": "Воронеж улица Урывского 6",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 210515,
                "displayName": "Вёшки",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 300461,
                "displayName": "Гомель 2",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 1769,
                "displayName": "Гомель Шилова РЦ",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 208941,
                "displayName": "Домодедово",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 116433,
                "displayName": "Домодедово",
                "isActive": false,
                "allowKgt": true,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 209513,
                "displayName": "Домодедово КБТ",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 1733,
                "displayName": "Екатеринбург - Испытателей 14г",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 300571,
                "displayName": "Екатеринбург - Перспективный 12/2",
                "isActive": true,
                "allowKgt": true,
                "isTransitActive": true,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 1725,
                "displayName": "Екатеринбург Горнистов РЦ",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 1793,
                "displayName": "Елизово Вилюйская РЦ",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 207726,
                "displayName": "Жуковский",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 1782,
                "displayName": "Ижевск Гагарина РЦ",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 117986,
                "displayName": "Казань",
                "isActive": true,
                "allowKgt": true,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 206844,
                "displayName": "Калининград",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 1810,
                "displayName": "Кандалакша Советская РЦ",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 1796,
                "displayName": "Караганда Бухар Жырау РЦ",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 303295,
                "displayName": "Клин",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 507,
                "displayName": "Коледино",
                "isActive": true,
                "allowKgt": true,
                "isTransitActive": true,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 120602,
                "displayName": "Коледино КБТ",
                "isActive": false,
                "allowKgt": true,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 3158,
                "displayName": "Коледино склад",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 132170,
                "displayName": "Краснодар (Грузовая доставка)",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 130744,
                "displayName": "Краснодар (Тихорецкая)",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 1716,
                "displayName": "Краснодар Почтовая РЦ",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 1699,
                "displayName": "Краснодар Старый",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 6145,
                "displayName": "Красноярск",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 37,
                "displayName": "Красноярск Семафорная РЦ",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 205985,
                "displayName": "Крыловская",
                "isActive": true,
                "allowKgt": true,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 115577,
                "displayName": "Крёкшино",
                "isActive": false,
                "allowKgt": true,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 124731,
                "displayName": "Крёкшино КБТ",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 302335,
                "displayName": "Кузнецк",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 6151,
                "displayName": "Курск",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 1797,
                "displayName": "Кыштым Возмездия РЦ",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 6143,
                "displayName": "Липецк",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 1798,
                "displayName": "Магадан Советская РЦ",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 1799,
                "displayName": "Махачкала Акушинского РЦ",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 211622,
                "displayName": "Минск",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": true,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 572,
                "displayName": "Минск",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 571,
                "displayName": "Минск Колодищи РЦ",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 1800,
                "displayName": "Мирный Комсомольская РЦ",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 1097,
                "displayName": "Могилев",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 133084,
                "displayName": "Москва Джема 24 КГТ",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 143772,
                "displayName": "Москва Издательские решен",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 135238,
                "displayName": "Москва Москва Своими сила",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 132829,
                "displayName": "Москва ПроЛофт КГТ",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 132870,
                "displayName": "Москва Своими силами Моск",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 132871,
                "displayName": "Москва Своими силами Моск",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 132869,
                "displayName": "Москва Своими силами Моск",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 133917,
                "displayName": "Москва Экомаркет 2ч",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 135243,
                "displayName": "Москва Экспресс-доставка ",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 1678,
                "displayName": "Мурманск Промышленная РЦ",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 117700,
                "displayName": "Н.Новгород Чаадаева1",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 208277,
                "displayName": "Невинномысск",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 1227,
                "displayName": "Нижний Новгород",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 6158,
                "displayName": "Николо-Хованское",
                "isActive": false,
                "allowKgt": true,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 686,
                "displayName": "Новосибирск",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": true,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 223,
                "displayName": "Новосибирск Петухова РЦ",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 118301,
                "displayName": "Ногинск",
                "isActive": false,
                "allowKgt": true,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 218210,
                "displayName": "Обухово",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": true,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 1729,
                "displayName": "Омск 2-я Солнечная РЦ",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 1686,
                "displayName": "Орел Северная РЦ",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 106476,
                "displayName": "Оренбург",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 1801,
                "displayName": "П-Камчатский Лукашевского",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 1824,
                "displayName": "Пенза Аустрина РЦ",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 1672,
                "displayName": "Пермь Сорокинская РЦ",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 117501,
                "displayName": "Подольск",
                "isActive": true,
                "allowKgt": true,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 218623,
                "displayName": "Подольск 3",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 301229,
                "displayName": "Подольск 4",
                "isActive": true,
                "allowKgt": true,
                "isTransitActive": true,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 207743,
                "displayName": "Пушкино",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": true,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 119400,
                "displayName": "Пушкино",
                "isActive": false,
                "allowKgt": true,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 114813,
                "displayName": "Пятигорск old",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 1814,
                "displayName": "Пятигорск Ермолова РЦ",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 300168,
                "displayName": "Радумля",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 300169,
                "displayName": "Радумля КБТ",
                "isActive": true,
                "allowKgt": true,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 131,
                "displayName": "Ростов-на-Дону",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 301760,
                "displayName": "Рязань (Тюшевское)",
                "isActive": true,
                "allowKgt": true,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 1921,
                "displayName": "С-П Черниговская РЦ",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 161812,
                "displayName": "СПБ Шушары КБТ",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 206298,
                "displayName": "СЦ Абакан",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 300862,
                "displayName": "СЦ Абакан 2",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 214951,
                "displayName": "СЦ Артем",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 209207,
                "displayName": "СЦ Архангельск",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 6148,
                "displayName": "СЦ Архангельск old",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 169872,
                "displayName": "СЦ Астрахань",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 215020,
                "displayName": "СЦ Байсерке",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 115649,
                "displayName": "СЦ Балашиха",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 172430,
                "displayName": "СЦ Барнаул",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 205228,
                "displayName": "СЦ Белая Дача",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 210557,
                "displayName": "СЦ Белогорск",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 216476,
                "displayName": "СЦ Бишкек",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 300363,
                "displayName": "СЦ Брест",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 116471,
                "displayName": "СЦ Бронницы",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 172940,
                "displayName": "СЦ Брянск",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 158751,
                "displayName": "СЦ Владикавказ",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 144649,
                "displayName": "СЦ Владимир",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 117392,
                "displayName": "СЦ Владимир old",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 210127,
                "displayName": "СЦ Внуково",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 6144,
                "displayName": "СЦ Волгоград",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 301516,
                "displayName": "СЦ Волгоград 2",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 203631,
                "displayName": "СЦ Вологда",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 300219,
                "displayName": "СЦ Вологда 2",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 211415,
                "displayName": "СЦ Воронеж",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 117059,
                "displayName": "СЦ Гомель",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 116338,
                "displayName": "СЦ Дзержинский",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 118400,
                "displayName": "СЦ Екатеринбург",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 117423,
                "displayName": "СЦ Екатеринбург 1",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 117424,
                "displayName": "СЦ Екатеринбург 2",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 211644,
                "displayName": "СЦ Екатеринбург 2 (Альпинистов)",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 204387,
                "displayName": "СЦ Ереван район Эребуни",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 218402,
                "displayName": "СЦ Иваново",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 203632,
                "displayName": "СЦ Иваново (до 03.05.23)",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 218628,
                "displayName": "СЦ Ижевск",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 158140,
                "displayName": "СЦ Ижевск (до 29.05)",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 131643,
                "displayName": "СЦ Иркутск",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 6147,
                "displayName": "СЦ Иркутск old",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 117779,
                "displayName": "СЦ Казань",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 120719,
                "displayName": "СЦ Казань 1",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 117442,
                "displayName": "СЦ Калуга",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 213849,
                "displayName": "СЦ Кемерово",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 205205,
                "displayName": "СЦ Киров",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 117443,
                "displayName": "СЦ Коледино К1",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 117445,
                "displayName": "СЦ Коледино К2",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 117446,
                "displayName": "СЦ Коледино К3",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 117447,
                "displayName": "СЦ Коледино К4",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 117448,
                "displayName": "СЦ Коледино К5",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 117449,
                "displayName": "СЦ Коледино К6",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 117450,
                "displayName": "СЦ Коледино К7",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 117451,
                "displayName": "СЦ Коледино К8",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 154371,
                "displayName": "СЦ Комсомольская",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 6159,
                "displayName": "СЦ Красногорск",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 117427,
                "displayName": "СЦ Краснодар 1",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 117428,
                "displayName": "СЦ Краснодар 2",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 117429,
                "displayName": "СЦ Краснодар 3",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 117430,
                "displayName": "СЦ Краснодар 4",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 118365,
                "displayName": "СЦ Красноярск Полиго",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 117433,
                "displayName": "СЦ Крёкшино 1",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 140302,
                "displayName": "СЦ Курск",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 156814,
                "displayName": "СЦ Курьяновская",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 160030,
                "displayName": "СЦ Липецк",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 117289,
                "displayName": "СЦ Лобня",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 209211,
                "displayName": "СЦ Махачкала",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 117393,
                "displayName": "СЦ Минск",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 121700,
                "displayName": "СЦ Минск 2",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 205349,
                "displayName": "СЦ Мурманск",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 115650,
                "displayName": "СЦ Мытищи",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 204952,
                "displayName": "СЦ Набережные Челны",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 118535,
                "displayName": "СЦ Нижний Новгород",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 6140,
                "displayName": "СЦ Нижний Новгород old",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 3140,
                "displayName": "СЦ Нижний Новгород не исп",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 211470,
                "displayName": "СЦ Нижний Тагил",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 141637,
                "displayName": "СЦ Новокосино",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 206708,
                "displayName": "СЦ Новокузнецк",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 117419,
                "displayName": "СЦ Новосибирск 1 old",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 118839,
                "displayName": "СЦ Новосибирск old",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 161520,
                "displayName": "СЦ Новосибирск Пасечная",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 118105,
                "displayName": "СЦ Обнинск",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 168458,
                "displayName": "СЦ Омск",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 6152,
                "displayName": "СЦ Омск - old",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 118026,
                "displayName": "СЦ Омск 2-я Казахста",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 206319,
                "displayName": "СЦ Оренбург",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 119408,
                "displayName": "СЦ Очаково",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 218732,
                "displayName": "СЦ Ош",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 147019,
                "displayName": "СЦ Пермь",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 216566,
                "displayName": "СЦ Пермь 2",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 208647,
                "displayName": "СЦ Печатники",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 117432,
                "displayName": "СЦ Подольск Б5",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 124716,
                "displayName": "СЦ Подрезково",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 209209,
                "displayName": "СЦ Псков",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 158311,
                "displayName": "СЦ Пятигорск",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 301920,
                "displayName": "СЦ Пятигорск (Этока)",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 218616,
                "displayName": "СЦ Ростов-на-Дону",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 133533,
                "displayName": "СЦ Ростов-на-Дону old",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 118019,
                "displayName": "СЦ Ростов-на-Дону old",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 6156,
                "displayName": "СЦ Рязань",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 117414,
                "displayName": "СЦ СПБ1",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 117230,
                "displayName": "СЦ Самара",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 117734,
                "displayName": "СЦ Санкт-Петербург Ю",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 158929,
                "displayName": "СЦ Саратов",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 303189,
                "displayName": "СЦ Семей",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 169537,
                "displayName": "СЦ Серов",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 144154,
                "displayName": "СЦ Симферополь",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 210937,
                "displayName": "СЦ Симферополь 2",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 6149,
                "displayName": "СЦ Симферополь old",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 117497,
                "displayName": "СЦ Смоленск",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 207803,
                "displayName": "СЦ Смоленск 2",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 117462,
                "displayName": "СЦ Солнечногорск",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 209596,
                "displayName": "СЦ Солнцево",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 161003,
                "displayName": "СЦ Сургут",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 117231,
                "displayName": "СЦ Сургут old",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 209208,
                "displayName": "СЦ Сыктывкар",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 117866,
                "displayName": "СЦ Тамбов",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 218636,
                "displayName": "СЦ Ташкент",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 117456,
                "displayName": "СЦ Тверь",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 204615,
                "displayName": "СЦ Томск",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 117401,
                "displayName": "СЦ Тула",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 117819,
                "displayName": "СЦ Тюмень",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 205104,
                "displayName": "СЦ Ульяновск",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 6150,
                "displayName": "СЦ Ульяновск old",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 300711,
                "displayName": "СЦ Уральск",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 149445,
                "displayName": "СЦ Уфа",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 6155,
                "displayName": "СЦ Уфа old",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 218644,
                "displayName": "СЦ Хабаровск",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 203799,
                "displayName": "СЦ Чебоксары",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 218916,
                "displayName": "СЦ Чебоксары 2",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 132508,
                "displayName": "СЦ Челябинск",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 218225,
                "displayName": "СЦ Челябинск 2",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": true,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 117493,
                "displayName": "СЦ Челябинск old",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 117721,
                "displayName": "СЦ Чита",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 218674,
                "displayName": "СЦ Чита 2",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 207022,
                "displayName": "СЦ Чёрная Грязь",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 218698,
                "displayName": "СЦ Шымкент",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 121703,
                "displayName": "СЦ Электросталь 4",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 158328,
                "displayName": "СЦ Южные Ворота",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 207404,
                "displayName": "СЦ Ярославль",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 6154,
                "displayName": "СЦ Ярославль old",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 502,
                "displayName": "Самара",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 68,
                "displayName": "Санкт-Петербург",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 2737,
                "displayName": "Санкт-Петербург (Уткина Заводь)",
                "isActive": true,
                "allowKgt": true,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 159402,
                "displayName": "Санкт-Петербург (Шушары)",
                "isActive": true,
                "allowKgt": true,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 132043,
                "displayName": "Санкт-Петербург КБТ (не используем)",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 133171,
                "displayName": "Санкт-Петербург Своими си",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 133172,
                "displayName": "Санкт-Петербург Своими си",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 133173,
                "displayName": "Санкт-Петербург Своими си",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 133174,
                "displayName": "Санкт-Петербург Своими си",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 133176,
                "displayName": "Санкт-Петербург Своими си",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 133175,
                "displayName": "Санкт-Петербург Своими си",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 117544,
                "displayName": "Санкт-Петербург Север",
                "isActive": false,
                "allowKgt": true,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 1680,
                "displayName": "Саратов Депутатская РЦ",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 119070,
                "displayName": "Север-Авто-М",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 6157,
                "displayName": "Склад Варшава",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 118106,
                "displayName": "Склад Диваны24",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 115574,
                "displayName": "Склад Климовск",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 119204,
                "displayName": "Склад Чехов",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 125238,
                "displayName": "Склад пос-ка СПБ КБТ 48ч",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 125239,
                "displayName": "Склад пос-ка СПБ КБТ 72ч",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 125240,
                "displayName": "Склад пос-ка СПБ КБТ 96ч",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 122316,
                "displayName": "Склад пост Алматы 48ч",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 122491,
                "displayName": "Склад пост Алматы 72ч",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 122492,
                "displayName": "Склад пост Алматы 96ч",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 121631,
                "displayName": "Склад пост Минск 48ч",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 122466,
                "displayName": "Склад пост Минск 72ч",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 122467,
                "displayName": "Склад пост Минск 96ч",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 122496,
                "displayName": "Склад пост Нурсултан 72ч",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 122498,
                "displayName": "Склад пост Нурсултан 96ч",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 119261,
                "displayName": "Склад поставщика",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 122252,
                "displayName": "Склад поставщика 72 часа ",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 122256,
                "displayName": "Склад поставщика 96 часов",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 117673,
                "displayName": "Склад поставщика КБТ 48ч",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 122258,
                "displayName": "Склад поставщика КБТ 72 ч",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 122259,
                "displayName": "Склад поставщика КБТ 96 ч",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 74,
                "displayName": "Сургут Сосновая РЦ",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 217081,
                "displayName": "Сц Брянск 2",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 122495,
                "displayName": "Сц Нурсултан",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 302445,
                "displayName": "Сынково",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 115651,
                "displayName": "Тамбов",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 1681,
                "displayName": "Тамбов Бастионная РЦ",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 1774,
                "displayName": "Тверь Коминтерна РЦ",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 206348,
                "displayName": "Тула",
                "isActive": true,
                "allowKgt": true,
                "isTransitActive": true,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 303024,
                "displayName": "Улан-Удэ, Ботаническая ",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 1803,
                "displayName": "Уральск Громовой РЦ",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 302222,
                "displayName": "Уфа, Зубово",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 1193,
                "displayName": "Хабаровск",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 119742,
                "displayName": "Холл.ру Питер",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 1804,
                "displayName": "Челябинск Дзержинского РЦ",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 206968,
                "displayName": "Чехов 1, Новоселки вл 11 стр 2",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 210001,
                "displayName": "Чехов 2, Новоселки вл 11 стр 7",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 300864,
                "displayName": "Шелепаново",
                "isActive": true,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 120762,
                "displayName": "Электросталь",
                "isActive": true,
                "allowKgt": true,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 121709,
                "displayName": "Электросталь КБТ",
                "isActive": true,
                "allowKgt": true,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 93,
                "displayName": "Южно-Сахалинск Ленина 123",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            },
            {
                "id": 1771,
                "displayName": "Ярославль",
                "isActive": false,
                "allowKgt": false,
                "isTransitActive": false,
                "isTransitOnly": false,
                "address": nil,
                "noPassEqueueAllowed": false
            }
        ]
    }
}


Rails.application.configure do
  config.after_initialize do

		data[:result][:warehouses].each do |wh| 
			unallowed = wh[:displayName].downcase.include?('сц') or wh[:displayName].downcase.include?('рц')
			if wh[:isActive] and wh[:isTransitOnly] == false and unallowed == false
				name_mapped = Warehouse::Warehouse.name_mapping wh[:displayName]
				wh_exist = Warehouse::Warehouse.find_by name: name_mapped
				if not wh_exist
                    ap "'#{name_mapped}': Создаем новый склад (initializers data_warehouse.rb)"
                    ap wh
					wh_exist = Warehouse::Warehouse.create name: name_mapped
				end
				wh_exist.warehouse_web_id = wh[:id]
				wh_exist.allow_kgt = wh[:allowKgt]
				unless wh_exist.save
					ap wh_exist.errors
				end
			end
		end

	end
end