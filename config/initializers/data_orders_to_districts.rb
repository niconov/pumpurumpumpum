regions_gpt = [
	{"region": "Чуйская область", "country": "Кыргызстан"},
	{"region": "Брестская область", "country": "Беларусь"},
	{"region": "город республиканского подчинения Бишкек", "country": "Кыргызстан"},
	{"region": "область Улытау", "country": "Казахстан"},
	{"region": "Мангистауская область", "country": "Казахстан"},
	{"region": "Гехаркуникская область", "country": "Армения"},
	{"region": "Алматинская область", "country": "Казахстан"},
	{"region": "Лорийская область", "country": "Армения"},
	{"region": "Могилёвская область", "country": "Беларусь"},
	{"region": "Актюбинская область", "country": "Казахстан"},
	{"region": "Шымкент", "country": "Казахстан"},
	{"region": "Жамбылская область", "country": "Казахстан"},
	{"region": "Минская область", "country": "Беларусь"},
	{"region": "Армавирская область", "country": "Армения"},
	{"region": "Тавушская область", "country": "Армения"},
	{"region": "Ереван", "country": "Армения"},
	{"region": "область Жетысу", "country": "Казахстан"},
	{"region": "Гродненская область", "country": "Беларусь"},
	{"region": "Карагандинская область", "country": "Казахстан"},
	{"region": "Северо-Казахстанская область", "country": "Казахстан"},
	{"region": "Ширакская область", "country": "Армения"},
	{"region": "Гомельская область", "country": "Беларусь"},
	{"region": "Котайкская область", "country": "Армения"},
	{"region": "Вайоцдзорская область", "country": "Армения"},
	{"region": "Алматы", "country": "Казахстан"},
	{"region": "Восточно-Казахстанская область", "country": "Казахстан"},
	{"region": "Западно-Казахстанская область", "country": "Казахстан"},
	{"region": "Сюникская область", "country": "Армения"},
	{"region": "Араратская область", "country": "Армения"},
	{"region": "Костанайская область", "country": "Казахстан"},
	{"region": "Витебская область", "country": "Беларусь"},
	{"region": "город республиканского значения Нур-Султан", "country": "Казахстан"},
	{"region": "Ташкент", "country": "Узбекистан"},
	{"region": "Кызылординская область", "country": "Казахстан"},
	{"region": "Павлодарская область", "country": "Казахстан"},
	{"region": "Арагацотнская область", "country": "Армения"},
	{"region": "город республиканского значения Астана", "country": "Казахстан"},
	{"region": "Атырауская область", "country": "Казахстан"},
	{"region": "Самаркандская область", "country": "Узбекистан"},
	{"region": "Минск", "country": "Беларусь"},
	{"region": "Акмолинская область", "country": "Казахстан"},
	{"region": "город республиканского значения Байконур", "country": "Казахстан"},
	{"region": "область Абай", "country": "Казахстан"},
	{"region": "город республиканского подчинения Ош", "country": "Кыргызстан"}
]

# Rails.application.configure do
#   config.after_initialize do
# 		regions = Order::Order.where(district_id: [1, '',nil]).where.not(region: nil).select(:region).distinct.pluck(:region)
# 		# ap regions
# 		regions.map do |region|
# 			region_gpt = regions_gpt.find {|i| i[:region] == region}
# 			unless region_gpt
# 				ap "Не найден регион: '#{region}'"
# 			else
# 				country_gpt = region_gpt[:country].downcase
# 				if country_gpt != 'россия'
# 					# ap region_gpt
# 					# ap country_gpt
# 					district = District.find_by name: country_gpt
# 					# ap district
# 					Order::Order.where(district_id: ['',nil]).where(region: region, district_id: nil).update_all(country: country_gpt, district_id: district.id)
# 					# select(:region).distinct.pluck(:region)
# 					# ap region_gpt
# 					# ap region
# 					# ap Order::Order.where(region: region).where.not(district_id: ['', nil]).all.as_json
# 				end
# 			end
# 		end
# 		regions = Order::Order.select(:region).where(district_id: 1).distinct.pluck(:region)
# 		regions.map do |i|
# 			orders = Order::Order.where(region: i).where.not(district_id: 1).all
# 			if orders.count > 1
# 				district = orders.last.district
# 				district_id = orders.last.district_id
# 				country = orders.last.country
# 				opts = {
# 					district: district,
# 					district_id: district_id,
# 					country: country,
# 				}
# 				ap Order::Order.select(:region).where(district_id: 1, region: i).update_all(opts)
# 			end
# 		end
#   end
# end


#  regions = [
# 	{"region": "Джалал-Абадская область", "district": "киргизия"},
# 	{"region": "Ростовская область", "district": "южный"},
# 	{"region": "Республика Ингушетия", "district": "северо-кавказский"},
# 	{"region": "Архангельская область", "district": "северо-западный"},
# 	{"region": "Чуйская область", "district": "кыргызстан"},
# 	{"region": "Туркестанская область", "district": "казахстан"},
# 	{"region": "город республиканского подчинения Бишкек", "district": ""},
# 	{"region": "Брестская область", "district": "беларусь"},
# 	{"region": "область Улытау", "district": "казахстан"},
# 	{"region": "Мангистауская область", "district": "казахстан"},
# 	{"region": "Севастополь", "district": ""},
# 	{"region": "Республика Башкортостан", "district": ""},
# 	{"region": "Костромская область", "district": ""},
# 	{"region": "Липецкая область", "district": ""},
# 	{"region": "Алтайский край", "district": ""},
# 	{"region": "Алматинская область", "district": ""},
# 	{"region": "Гехаркуникская область", "district": ""},
# 	{"region": "Воронежская область", "district": ""},
# 	{"region": "Камчатский край", "district": ""},
# 	{"region": "Лорийская область", "district": ""},
# 	{"region": "Ханты-Мансийский автономный округ", "district": ""},
# 	{"region": "Омская область", "district": ""},
# 	{"region": "Новгородская область", "district": ""},
# 	{"region": "Могилёвская область", "district": ""},
# 	{"region": "Рязанская область", "district": ""},
# 	{"region": "Республика Северная Осетия — Алания", "district": ""},
# 	{"region": "Актюбинская область", "district": ""},
# 	{"region": "Смоленская область", "district": ""},
# 	{"region": "Республика Коми", "district": ""},
# 	{"region": "Шымкент", "district": ""},
# 	{"region": "Свердловская область", "district": ""},
# 	{"region": "Жамбылская область", "district": ""},
# 	{"region": "Минская область", "district": ""},
# 	{"region": "Чувашская Республика", "district": ""},
# 	{"region": "Владимирская область", "district": ""},
# 	{"region": "Армавирская область", "district": ""},
# 	{"region": "Брянская область", "district": ""},
# 	{"region": "Республика Тыва", "district": ""},
# 	{"region": "Кировская область", "district": ""},
# 	{"region": "Ставропольский край", "district": ""},
# 	{"region": "Карачаево-Черкесская Республика", "district": ""},
# 	{"region": "Ленинградская область", "district": ""},
# 	{"region": "Нижегородская область", "district": ""},
# 	{"region": "Тавушская область", "district": ""},
# 	{"region": "Ереван", "district": ""},
# 	{"region": "область Жетысу", "district": ""},
# 	{"region": "Томская область", "district": ""},
# 	{"region": "Республика Калмыкия", "district": ""},
# 	{"region": "", "district": ""},
# 	{"region": "Республика Крым", "district": ""},
# 	{"region": "Красноярский край", "district": ""},
# 	{"region": "Карагандинская область", "district": ""},
# 	{"region": "Гродненская область", "district": ""},
# 	{"region": "Республика Карелия", "district": ""},
# 	{"region": "Северо-Казахстанская область", "district": ""},
# 	{"region": "Ширакская область", "district": ""},
# 	{"region": "Калининградская область", "district": ""},
# 	{"region": "Саратовская область", "district": ""},
# 	{"region": "Гомельская область", "district": ""},
# 	{"region": "Волгоградская область", "district": ""},
# 	{"region": "Вологодская область", "district": ""},
# 	{"region": "Москва", "district": ""},
# 	{"region": "Котайкская область", "district": ""},
# 	{"region": "Республика Мордовия", "district": ""},
# 	{"region": "Еврейская автономная область", "district": ""},
# 	{"region": "Мурманская область", "district": ""},
# 	{"region": "Забайкальский край", "district": ""},
# 	{"region": "Иркутская область", "district": ""},
# 	{"region": "Ульяновская область", "district": ""},
# 	{"region": "Московская область", "district": ""},
# 	{"region": "Вайоцдзорская область", "district": ""},
# 	{"region": "Оренбургская область", "district": ""},
# 	{"region": "Сахалинская область", "district": ""},
# 	{"region": "Орловская область", "district": ""},
# 	{"region": "Республика Марий Эл", "district": ""},
# 	{"region": "Ташкентская область", "district": ""},
# 	{"region": "федеральная территория Сириус", "district": ""},
# 	{"region": "Удмуртская Республика", "district": ""},
# 	{"region": "Самарская область", "district": ""},
# 	{"region": "Алматы", "district": ""},
# 	{"region": "Восточно-Казахстанская область", "district": ""},
# 	{"region": "Западно-Казахстанская область", "district": ""},
# 	{"region": "Республика Дагестан", "district": ""},
# 	{"region": "Сюникская область", "district": ""},
# 	{"region": "Араратская область", "district": ""},
# 	{"region": "Белгородская область", "district": ""},
# 	{"region": "Иссык-Кульская область", "district": ""},
# 	{"region": "Челябинская область", "district": ""},
# 	{"region": "Кемеровская область", "district": ""},
# 	{"region": "Республика Татарстан", "district": ""},
# 	{"region": "Костанайская область", "district": ""},
# 	{"region": "Краснодарский край", "district": ""},
# 	{"region": "Республика Каракалпакстан", "district": ""},
# 	{"region": "Республика Саха (Якутия)", "district": ""},
# 	{"region": "Приморский край", "district": ""},
# 	{"region": "Тюменская область", "district": ""},
# 	{"region": "Республика Алтай", "district": ""},
# 	{"region": "Витебская область", "district": ""},
# 	{"region": "Магаданская область", "district": ""},
# 	{"region": "Республика Хакасия", "district": ""},
# 	{"region": "город республиканского значения Нур-Султан", "district": ""},
# 	{"region": "Хабаровский край", "district": ""},
# 	{"region": "Ташкент", "district": ""},
# 	{"region": "Пензенская область", "district": ""},
# 	{"region": "Ямало-Ненецкий автономный округ", "district": ""},
# 	{"region": "Ненецкий автономный округ", "district": ""},
# 	{"region": "Чукотский автономный округ", "district": ""},
# 	{"region": "Тульская область", "district": ""},
# 	{"region": "Новосибирская область", "district": ""},
# 	{"region": "Республика Бурятия", "district": ""},
# 	{"region": "Ярославская область", "district": ""},
# 	{"region": "Ивановская область", "district": ""},
# 	{"region": "Кызылординская область", "district": ""},
# 	{"region": "Павлодарская область", "district": ""},
# 	{"region": "Арагацотнская область", "district": ""},
# 	{"region": "Республика Адыгея", "district": ""},
# 	{"region": "город республиканского значения Астана", "district": ""},
# 	{"region": "Атырауская область", "district": ""},
# 	{"region": "Самаркандская область", "district": ""},
# 	{"region": "Минск", "district": ""},
# 	{"region": "Чеченская Республика", "district": ""},
# 	{"region": "Кабардино-Балкарская Республика", "district": ""},
# 	{"region": "Пермский край", "district": ""},
# 	{"region": "Астраханская область", "district": ""},
# 	{"region": "Тверская область", "district": ""},
# 	{"region": "Псковская область", "district": ""},
# 	{"region": "Акмолинская область", "district": ""},
# 	{"region": "Амурская область", "district": ""},
# 	{"region": "Тамбовская область", "district": ""},
# 	{"region": "Санкт-Петербург", "district": ""},
# 	{"region": "Курганская область", "district": ""},
# 	{"region": "город республиканского значения Байконур", "district": ""},
# 	{"region": "область Абай", "district": ""},
# 	{"region": "Калужская область", "district": ""},
# 	{"region": "город республиканского подчинения Ош", "district": ""},
# 	{"region": "Курская область", "district": ""}
# ] 

#  ap regions