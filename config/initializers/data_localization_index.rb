data = {
    "data": {
        "indexes": [
            {
                "index": 0.5,
                "left": 95,
                "right": 100
            },
            {
                "index": 0.6,
                "left": 90,
                "right": 94.99
            },
            {
                "index": 0.65,
                "left": 85,
                "right": 89.99
            },
            {
                "index": 0.7,
                "left": 80,
                "right": 84.99
            },
            {
                "index": 0.75,
                "left": 75,
                "right": 79.99
            },
            {
                "index": 0.8,
                "left": 70,
                "right": 74.99
            },
            {
                "index": 0.85,
                "left": 65,
                "right": 69.99
            },
            {
                "index": 0.9,
                "left": 60,
                "right": 64.99
            },
            {
                "index": 0.95,
                "left": 55,
                "right": 59.99
            },
            {
                "index": 1,
                "left": 50,
                "right": 54.99
            },
            {
                "index": 1,
                "left": 45,
                "right": 49.99
            },
            {
                "index": 1,
                "left": 40,
                "right": 44.99
            },
            {
                "index": 1,
                "left": 35,
                "right": 39.99
            },
            {
                "index": 1.06,
                "left": 30,
                "right": 34.99
            },
            {
                "index": 1.1,
                "left": 25,
                "right": 29.99
            },
            {
                "index": 1.14,
                "left": 20,
                "right": 24.99
            },
            {
                "index": 1.18,
                "left": 15,
                "right": 19.99
            },
            {
                "index": 1.2,
                "left": 10,
                "right": 14.99
            },
            {
                "index": 1.22,
                "left": 5,
                "right": 9.99
            },
            {
                "index": 1.25,
                "left": 0,
                "right": 4.99
            }
        ]
    },
    "error": false,
    "errorText": "",
    "additionalErrors": nil
}

# Rails.application.configure do
#   config.after_initialize do
# 		Tariff::Localization.all.destroy_all
#   	data[:data][:indexes].map do |idx|
# 		  Tariff::Localization.create index: idx[:idx], left: idx[:left], right: idx[:right]
#   	end
#   end
# end