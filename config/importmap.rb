# Pin npm packages by running ./bin/importmap

pin "application", preload: true
pin "foundation", preload: true
pin "tailwind", preload: true
pin "javascripts/tailwind.js", preload: true
pin "@hotwired/turbo-rails", to: "turbo.min.js", preload: true
pin "@hotwired/stimulus", to: "@hotwired--stimulus.js" # @3.2.2
pin "@hotwired/stimulus-loading", to: "stimulus-loading.js", preload: true
pin_all_from "app/javascript/controllers", under: "controllers"
# pin "vue" # @3.4.3
# pin "@vue/reactivity", to: "@vue--reactivity.js" # @3.2.29
# pin "@vue/runtime-core", to: "@vue--runtime-core.js" # @3.2.29
# pin "@vue/runtime-dom", to: "@vue--runtime-dom.js" # @3.2.29
# pin "@vue/shared", to: "@vue--shared.js" # @3.2.29
pin "@headlessui/vue", to: "@headlessui--vue.js" # @1.7.16
pin "tailwindcss-stimulus-components" # @4.0.4
pin "chartkick", to: "chartkick.js"
pin "Chart.bundle", to: "Chart.bundle.js"