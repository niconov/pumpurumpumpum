Rails.application.routes.draw do
  namespace :sale do 
    resources :reports
  end
  
  namespace :warehouse do
    get 'warehouse/index'
  end
  namespace :deliveries do
    get 'supply_schema/schema'
    get 'supply_schema/stats'
    get 'priorities/index'
    resources :scheduleds
    resources :scheduled_items
  end

  namespace :tariff do
    get 'tariffs/index'
    get 'tariff/index'
  end

  namespace :task_manager do
    resources :tasks
    resources :statuses
    resources :categories
  end

  resources :app_global_constants
  resources :sales_operations
  resources :sales
  resources :autoresponsers

  require 'sidekiq_unique_jobs/web'
  mount Sidekiq::Web => '/sidekiq', at: "/sidekiq"

  # Rails.application.routes.draw do
  resources :sale_reports
  namespace :sale do
    get 'sale_reports/csv/:id', to: 'reports#report_csv', as: 'report_csv'
  end
  patch 'nms/update_price/:id', to: 'nms#update_price', as: 'update_price'

  namespace :warehouse do
    get 'warehouse/index'
  end
  namespace :deliveries do
    get 'supply_schema/schema'
    get 'priorities/index'
    resources :scheduleds
    resources :scheduled_items
  end


  namespace :nm do
    resources :nm_customer_discounts
    resources :nm_customer_discounts
    resources :nm_comperitors
    resources :nm_group_competitors
    resources :nm_group_queries
    resources :nms
    patch 'nms/update_price/:id', to: 'nms#update_price', as: 'update_price'
    post '/competitor_action/:id', to: 'nms#competitor_action', as: 'nm_competitor_action'
    post 'nm_group_queries/:id/query_item_action', to: 'nm_group_queries#query_item_action', as: 'nm_group_queries_item_action'
    get 'nm_group_queries/update_nm_positions/:nm_id', to: 'nm_group_queries#update_nm_positions', as: 'nm_group_queries_update_nm_positions'

  end
  resources :app_global_constants
  resources :sales_operations
  resources :sales
  resources :autoresponsers
  # resources :promotion_managers
  #   mount Sidekiq::Web => "/sidekiq"
  #   get "job" => "job#index"
  #   get "job/email" => "job#email"
  #   get "job/post" => "job#delayed_post"
  #   get "job/long" => "job#long"
  #   get "job/crash" => "job#crash"
  #   get "job/bulk" => "job#bulk"
  # end

  namespace :income do 
    post "income/update_box_type/:id", to: "income#update_box_type", as: "box_update"
    resources :income
    get "planned", to: "planned#index"
    get "planned/archived", to: "planned#archived"
    get "planned/show/:id", to: "planned#show", as: "planned_show"
    get "planned/delete/:id", to: "planned#delete", as: "planned_delete"
    get "planned/create", to: "planned#create", as: "planned_create"
    post "planned/update/:income_id", to: "planned#update", as: "planned_update"
  end
  namespace :project do 
    resources :project_roles
    resources :projects
    post "projects/role/change", to: "projects#role_user_on_projects_action", as: "projects_role_user_on_projects_action"
    post "projects/set_as_current_project", to: "projects#set_as_current_project", as: "set_as_current_project"
    post "projects/settings/change", to: "projects#user_settings_update", as: "user_settings_update"

  end
  
  resources :delivery_methods

  resources :comissions
  namespace :advert do 
    resources :adverts

    get 'adverts/:id/advert_update_api', to: 'adverts#update_from_api', as: 'advert_update_api'
    get 'adverts/:id/start', to: 'adverts#start', as: 'advert_start'
    # get 'adverts/:id/stop', to: 'adverts#stop', as: 'advert_stop'
    get 'adverts/:id/pause', to: 'adverts#pause', as: 'advert_pause'
    post 'advert/adverts/set_cpm/:id', to: 'adverts#set_cpm', as: 'set_cpm'
    post 'advert/adverts/set_daily_budget_max/:id', to: 'adverts#set_daily_budget_max', as: 'set_daily_budget_max'
    get 'adverts/:id/:advert_action', to: 'adverts#advert_action', as: 'advert_action'
    post 'adverts/:id/cluster/set_forced_state/:cluster_id', to: 'adverts#cluster_set_forced_state', as: 'cluster_set_forced_state'
    get 'adverts/:id/keywords/flush_excluded_keyword', to: 'adverts#flush_excluded_keyword', as: 'flush_excluded_keyword'
    get 'adverts/:id/keywords/clear_bad_keywords', to: 'adverts#clear_bad_keywords', as: 'clear_bad_keywords'

    get '/advert/render_async_cluster_stats/:id/:cluster_id', to: "adverts#render_async_cluster_stats", as: 'render_async_cluster_stats'
    #  to 'advert#render_partial_advert_clusters_async' as 'render_partial_advert_clusters_async'
    
    resources :promotion_managers
    get 'promotion_manager/:id/setup', to: 'promotion_managers#setup', as: 'promotion_manager_setup'
    get 'promotion_manager/setup_all', to: 'promotion_managers#setup_all', as: 'promotion_manager_setup_all'
    get 'promotion_manager/:id/set_status/:status', to: 'promotion_managers#set_status', as: 'promotion_manager_set_status'
  end

  resources :events
  resources :orders
  devise_for :users, controllers: { sessions: 'users/sessions', registrations: 'users/registrations', "passwords": 'users/passwords'}
  # devise_for :users, :as => :user_session
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Reveal health status on /up that returns 200 if the app boots with no exceptions, otherwise 500.
  # Can be used by load balancers and uptime monitors to verify that the app is live.
  get "up" => "rails/health#show", as: :rails_health_check

  # Defines the root path route ("/")
  root "dashboard#index", layouts: 'layouts'


  get "dashboard/stocks_history", to: "dashboard#stocks_history", layouts: 'layouts'
  get "dashboard/daily", to: "dashboard#daily", layouts: 'layouts'
  get "dashboard/trading", to: "dashboard#trading", layouts: 'layouts'
  get "dashboard/warehouse_balance", to: "dashboard#warehouse_balance", layouts: 'layouts'
  get "dashboard/stocks_planning", to: "dashboard#stocks_planning", layouts: 'layouts'
  get "dashboard/logistics_planning", to: "dashboard#logistics_planning", layouts: 'layouts'
  get "dashboard/sales_stats_by_district", to: "dashboard#sales_stats_by_district", layouts: 'layouts'
  get "dashboard/profit_by_current_stocks", to: "dashboard#profit_by_current_stocks", layouts: 'layouts'
  get "dashboard/nm_positions", to: "dashboard#nm_positions", layouts: 'layouts'
  get "dashboard/con_anal", to: "dashboard#con_anal", layouts: 'layouts'
  get "dashboard/weekly_orders", to: "dashboard#weekly_orders", layouts: 'layouts'
  get "dashboard/stocks_by_incomes", to: "dashboard#stocks_by_incomes"
  get "dashboard/nm/stats", to: "dashboard#nm_stats"



  get "scheduler/main", to: "scheduler#main", layouts: 'layouts'
  get "scheduler/task/:task_action/:id", to: "scheduler#task_action", layouts: 'layouts',  as: 'scheduler_task_action'

  # get "nms_queries/index", to: "nm_group_query#index", layouts: 'layouts'


  get "regions/regions", to: "regions#regions", layouts: 'layouts'
  get "regions/districts", to: "regions#districts", layouts: 'layouts'
  get "regions/district_warehouses", to: "regions#district_warehouses", layouts: 'layouts'

  
  #post 'nm_group_queries/:id/query_item_action', to: 'nm_group_queries#query_item_action', as: 'nm_group_queries_item_action'


  namespace :tariff do 
    resources :tariffs
    get "tariffs_active/index", to: "tariffs_active#index", layouts: 'layouts'
    get "tariffs_active/set/:id", to: "tariffs_active#set", layouts: 'layouts', as: 'tariffs_active_set'
    get "tariffs_active/unset/:id", to: "tariffs_active#unset", layouts: 'layouts', as: 'tariffs_active_unset'
  end


  get "pricing/index", to: "pricing#index", as: 'pricings'
  get "pricing/warehouses", to: "pricing#warehouses", as: 'pricing_wh'

  get "pricing/update_price/:nm_id", to: "pricing#update_price_item", as: 'pricings_update_prices_item'
  get "pricing/update_prices_all", to: "pricing#update_prices_all", as: 'pricings_update_prices_all'
  get "pricing/graph_prices_vs_sales/:nm_id", to: "pricing#graph_prices_vs_sales", as: 'pricing_price_vs_sales'
  get "pricing/graph_prices_vs_sales_data/:nm_id", to: "pricing#graph_prices_vs_sales_data", as: 'graph_prices_vs_sales_data'
  
  
end














  # 
