require_relative "boot"

require "rails/all"
require "sidekiq/web"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)


module WbLocal
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 7.1
    config.i18n.default_locale = 'ru'
    # Rails.autoloaders.log!


    # Please, add to the `ignore` list any other `lib` subdirectories that do
    # not contain `.rb` files, or that should not be reloaded or eager loaded.
    # Common ones are `templates`, `generators`, or `middleware`, for example.
    config.autoload_lib(ignore: %w(assets tasks))
    # config.autoload_paths += Dir["#{config.root}/app/lib/**/"] 

    # config.autoload_paths << Rails.root.join('app', 'models', 'advert', 'concerns')

    config.time_zone = 'Moscow'
    Time.zone = 'Moscow'
    # config.active_record.default_timezone = :utc
    config.active_record.default_timezone = :local
    Groupdate.time_zone = "Moscow"

    config.hosts << "pumpurumpumpum.ru"
    config.active_job.queue_adapter = :sidekiq
    config.active_record.query_log_tags_enabled = true

    %w( logistics nm_to_refactoring  projects misc geo  sales_and_orders tools).map do |path|
      config.autoload_paths << "#{Rails.root}/app/models/#{path}"
      # config.autoload_paths << "#{Rails.root}/app/controllers/#{path}"
    end
    # config.autoload_paths += Dir[Rails.root.join('app', 'controllers', '**/')]
    # config.autoload_paths += Dir[Rails.root.join('app', 'models', '**/')]
    # config.autoload_paths += Dir[Rails.root.join('app', 'models', 'misc/**')]

        
    # Rails.logger.level = 0
    config.log_level = :error
    config.wb_job_configured = false
    config.logger = Logger.new(STDOUT)
    # config.time_zone = 'Eastern Time (US & Canada)'


    # Configuration for the application, engines, and railties goes here.
    #
    # config.eager_load_paths << Rails.root.join("extras")
  end
end

WillPaginate.per_page = 10

# Telegram.bot == Telegram.bots[:default]
# ap Rails.application.credentials.tg_token

Telegram.bots_config = { default: Rails.application.credentials.tg_token }

