# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.1].define(version: 2024_02_22_183231) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "advert_adverts", force: :cascade do |t|
    t.integer "project_id"
    t.integer "advert_id"
    t.string "name"
    t.integer "type_id"
    t.integer "max_cpm"
    t.integer "min_place"
    t.integer "bid_current"
    t.string "search_query"
    t.integer "param_value"
    t.string "param_name"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.json "response_raw"
    t.float "cpm"
    t.float "budget"
    t.boolean "is_low_budget", default: true
    t.boolean "manager_enabled", default: false
    t.integer "budget_max", default: 0
    t.integer "status"
    t.integer "balance_payment_sum_on_period", default: 0
    t.datetime "balance_payment_period", default: "2024-01-08 11:48:55"
  end

  create_table "advert_cluster_keywords", force: :cascade do |t|
    t.string "keyword"
  end

  create_table "advert_clusters", force: :cascade do |t|
    t.integer "advert_id"
    t.string "cluster_name"
    t.boolean "excluded"
    t.integer "project_id"
    t.integer "forced_state", default: 0
    t.integer "clicks", default: 0
    t.integer "views", default: 0
    t.float "ctr"
    t.float "sum"
    t.index ["project_id", "advert_id", "cluster_name"], name: "idx_on_project_id_advert_id_cluster_name_501e08092a", unique: true
  end

  create_table "advert_costs", force: :cascade do |t|
    t.integer "update_num"
    t.date "update_time"
    t.float "update_sum"
    t.integer "advert_id"
    t.string "camp_name"
    t.integer "advert_type"
    t.integer "advert_status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "payment_type"
  end

  create_table "advert_params", force: :cascade do |t|
    t.integer "advert_id"
    t.integer "nm_id"
    t.boolean "carousel"
    t.boolean "recom"
    t.boolean "booster"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "advert_payments", force: :cascade do |t|
    t.integer "payment_id"
    t.date "date"
    t.float "sum"
    t.integer "payment_type"
    t.integer "status_id"
    t.string "card_status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "advert_promotion_managers", force: :cascade do |t|
    t.integer "nm_id"
    t.integer "advert_id"
    t.float "budget_daily"
    t.boolean "enabled"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "project_id"
  end

  create_table "advert_stats", force: :cascade do |t|
    t.float "views"
    t.float "clicks"
    t.float "ctr"
    t.float "cpc"
    t.float "sum"
    t.float "atbs"
    t.float "orders"
    t.float "cr"
    t.float "shks"
    t.float "sum_price"
    t.integer "nm_id"
    t.datetime "period"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "advert_id"
    t.integer "project_id"
  end

  create_table "advert_stats_auto_items", force: :cascade do |t|
    t.string "keyword"
    t.integer "views"
    t.integer "clicks"
    t.decimal "sum", precision: 15, scale: 2
    t.datetime "period"
    t.integer "advert_id"
    t.integer "project_id"
    t.float "ctr", default: 0.0
    t.index ["project_id", "advert_id", "period", "keyword"], name: "idx_on_project_id_advert_id_period_keyword_a440b50b22", unique: true
  end

  create_table "advert_stats_autos", force: :cascade do |t|
    t.string "keyword"
    t.integer "advert_id"
    t.integer "views", default: 0
    t.integer "clicks", default: 0
    t.float "ctr", default: 0.0
    t.datetime "updated_at", null: false
    t.integer "project_id"
    t.integer "keyword_id"
    t.date "period"
    t.float "sum"
    t.index ["project_id", "advert_id", "period", "keyword_id"], name: "idx_on_project_id_advert_id_period_keyword_id_72f4d8f243", unique: true
  end

  create_table "advert_statuses", force: :cascade do |t|
    t.string "advert_id"
    t.integer "state"
    t.integer "price"
    t.integer "place"
    t.integer "priority"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "app_global_constants", force: :cascade do |t|
    t.string "name"
    t.string "value"
    t.string "value_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "autoresponsers", force: :cascade do |t|
    t.text "text"
    t.integer "rating"
    t.integer "project_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "comissions", force: :cascade do |t|
    t.string "category"
    t.string "subject"
    t.float "fbo"
    t.float "fbs"
    t.float "dbs"
  end

  create_table "countries_and_districts", force: :cascade do |t|
    t.string "country"
    t.string "district"
    t.string "region"
  end

  create_table "deliveries", force: :cascade do |t|
    t.integer "warehouse_id"
    t.string "warehouse"
    t.string "region"
    t.float "price"
  end

  create_table "deliveries_pickup_points", force: :cascade do |t|
    t.string "regions"
    t.string "dest"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.integer "district_id"
  end

  create_table "deliveries_priority_queries", force: :cascade do |t|
    t.string "query"
    t.integer "pages"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "deliveries_scheduled_items", force: :cascade do |t|
    t.integer "nm_id"
    t.integer "qty"
    t.integer "deliveries_sheduled_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "deliveries_scheduleds", force: :cascade do |t|
    t.datetime "period"
    t.integer "warehouse_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "deliveries_supply_schemas", force: :cascade do |t|
    t.integer "nm_id"
    t.integer "project_id"
    t.integer "count", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "district_id"
  end

  create_table "delivery_methods", force: :cascade do |t|
    t.date "period"
    t.string "name"
    t.integer "tariff_id"
    t.float "price"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "project_id"
    t.integer "time_delivery", default: 0
  end

  create_table "districts", force: :cascade do |t|
    t.string "name"
    t.integer "order_level", default: 0
    t.string "name_short"
  end

  create_table "events", force: :cascade do |t|
    t.string "subject"
    t.string "text"
    t.integer "project_id"
    t.datetime "date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "priority"
    t.integer "subject_id"
    t.boolean "viewed", default: false
    t.string "status"
  end

  create_table "income_incomes", force: :cascade do |t|
    t.integer "income_id"
    t.string "number"
    t.datetime "date"
    t.datetime "last_change_date"
    t.string "supplier_article"
    t.string "tech_size"
    t.string "barcode"
    t.integer "quantity"
    t.float "total_price"
    t.datetime "date_close"
    t.string "warehouse_name"
    t.integer "nm_id"
    t.string "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "project_id"
    t.string "box_type"
  end

  create_table "income_planned_heads", force: :cascade do |t|
    t.integer "warehouse_id"
    t.integer "project_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "is_deleted", default: false
    t.time "deleted_at"
    t.date "period", default: "2024-01-16"
  end

  create_table "income_planned_items", force: :cascade do |t|
    t.integer "income_planned_id"
    t.integer "nm_id"
    t.integer "quantity"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "localization_index", force: :cascade do |t|
    t.float "left"
    t.float "right"
    t.float "index"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "locations", force: :cascade do |t|
    t.string "country"
    t.string "district_name"
    t.string "district_fullname"
    t.string "region"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "nm_competitor_connectors", force: :cascade do |t|
    t.integer "nm_id"
    t.integer "nm_competitor_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "nm_competitors", force: :cascade do |t|
    t.integer "nm_group_competitor_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "query_id"
    t.integer "nm_id"
    t.json "raw"
    t.json "raw_card"
    t.float "price_rrc"
    t.float "price"
    t.float "price_with_spp"
    t.integer "position"
  end

  create_table "nm_customer_discounts", force: :cascade do |t|
    t.integer "spp"
    t.float "bound_left"
    t.float "bound_right"
    t.integer "nm_group_query_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "project_id"
  end

  create_table "nm_group_competitors", force: :cascade do |t|
    t.integer "nm_id"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "project_id"
  end

  create_table "nm_group_queries", force: :cascade do |t|
    t.integer "project_id"
    t.string "name"
    t.string "query"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "nm_group_query_items", force: :cascade do |t|
    t.integer "nm_group_query_id"
    t.integer "nm_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "nm_nms", force: :cascade do |t|
    t.boolean "prohibited"
    t.string "supplier_article"
    t.string "barcode"
    t.string "imt_id"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.float "volume"
    t.string "object"
    t.float "dim_wb_a"
    t.float "dim_wb_b"
    t.float "dim_wb_c"
    t.float "dim_wb_m"
    t.string "skus"
    t.integer "box_per_pallet"
    t.integer "unit_per_box"
    t.integer "unit_per_pallet"
    t.float "unit_mass"
    t.float "pallet_mass"
    t.float "box_mass"
    t.float "cost"
    t.float "cost_log_in"
    t.float "cost_log_out"
    t.float "cost_package"
    t.float "cost_packaging"
    t.integer "obj_id"
    t.float "ros_min"
    t.float "roi_min"
    t.integer "nm_id"
    t.integer "project_id"
    t.integer "spp"
  end

  create_table "nm_positions", force: :cascade do |t|
    t.integer "nm_id"
    t.integer "dist"
    t.integer "time1"
    t.integer "time2"
    t.datetime "period"
    t.integer "cpm"
    t.integer "position"
    t.integer "position_no_advert"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "promo"
    t.float "price_fictive"
    t.float "price"
    t.float "sale"
  end

  create_table "nm_stats", force: :cascade do |t|
    t.datetime "period"
    t.integer "nm_id"
    t.string "vendor_code"
    t.string "brand_name"
    t.string "object_name"
    t.integer "object__id"
    t.integer "open_card_count"
    t.integer "add_to_cart_count"
    t.integer "orders_count"
    t.float "orders_sum_rub"
    t.integer "buyouts_count"
    t.float "buyouts_sum_rub"
    t.integer "cancel_count"
    t.float "cancel_sum_rub"
    t.float "avg_price_rub"
    t.integer "avg_orders_count_per_day"
    t.float "add_to_cart_percent"
    t.float "cart_to_order_percent"
    t.float "buyouts_percent"
    t.integer "stocks_mp"
    t.integer "stocks_wb"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "project_id"
  end

  create_table "order_daily_stats", force: :cascade do |t|
    t.date "period"
    t.integer "nm_id"
    t.integer "sum"
    t.integer "quantity"
    t.integer "price"
    t.integer "project_id"
    t.index ["project_id", "period", "nm_id"], name: "index_order_daily_stats_on_project_id_and_period_and_nm_id", unique: true
  end

  create_table "order_district_stats", force: :cascade do |t|
    t.date "period"
    t.integer "nm_id"
    t.integer "sum"
    t.integer "quantity"
    t.integer "price"
    t.integer "project_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "district_id"
    t.index ["project_id", "period", "nm_id", "district_id"], name: "idx_on_project_id_period_nm_id_district_id_4339193e37", unique: true
  end

  create_table "order_orders", force: :cascade do |t|
    t.datetime "datetime", precision: nil
    t.datetime "last_change_date", precision: nil
    t.string "article"
    t.string "barcode"
    t.float "price"
    t.integer "discount"
    t.string "region"
    t.string "warehouse"
    t.integer "comission_id"
    t.integer "warehouse_id"
    t.integer "region_id"
    t.integer "income_id"
    t.integer "project_id"
    t.integer "nm_id"
    t.boolean "canceled"
    t.string "gnumber"
    t.string "order_type"
    t.string "odid"
    t.string "srid"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.string "country"
    t.string "district"
    t.integer "spp"
    t.float "price_with_disc"
    t.float "finished_price"
    t.datetime "cancel_date"
    t.string "tech_size"
    t.string "subject"
    t.integer "district_id"
    t.integer "location_id"
    t.index ["project_id", "srid"], name: "index_order_orders_on_project_id_and_srid", unique: true
  end

  create_table "order_stat_districts", force: :cascade do |t|
    t.integer "nm_id"
    t.integer "district_id"
    t.integer "count_total", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "period"
    t.integer "project_id"
    t.integer "count_local", default: 0
  end

  create_table "order_stats", force: :cascade do |t|
    t.integer "quantity", default: 0
    t.float "sum", default: 0.0
    t.float "advert", default: 0.0
    t.float "pocs"
    t.float "ros"
    t.float "price"
    t.float "price_spp"
    t.float "profit", default: 0.0
    t.integer "nm_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.date "period"
    t.integer "project_id"
    t.float "cost", default: 0.0
    t.float "storage", default: 0.0
    t.boolean "has_error", default: true
  end

  create_table "orders_stats_weeklies", force: :cascade do |t|
    t.integer "nm_id"
    t.integer "qty"
    t.float "sum"
    t.float "price"
    t.float "discount"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "period_start"
    t.datetime "period_end"
  end

  create_table "prices", force: :cascade do |t|
    t.integer "nm_id"
    t.float "discount"
    t.float "price"
    t.float "promo_code"
  end

  create_table "projects_projects", force: :cascade do |t|
    t.integer "user_id"
    t.string "token"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "projects_role_user_on_projects", force: :cascade do |t|
    t.integer "user_id"
    t.integer "project_id"
    t.integer "project_role_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "projects_roles", force: :cascade do |t|
    t.string "user_role"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "changable"
    t.boolean "access_profit"
  end

  create_table "projects_user_settings", force: :cascade do |t|
    t.boolean "notification_enabled", default: true
    t.integer "project_id"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "sale_report_lists", force: :cascade do |t|
    t.integer "realizationreport_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "project_id"
    t.date "date_from"
    t.date "date_to"
    t.date "create_dt"
  end

  create_table "sale_reports", force: :cascade do |t|
    t.integer "realizationreport_id"
    t.date "date_from"
    t.date "date_to"
    t.date "create_dt"
    t.string "currency_name"
    t.json "suppliercontract_code"
    t.string "rrd_id"
    t.integer "gi_id"
    t.string "subject_name"
    t.integer "nm_id"
    t.string "brand_name"
    t.string "sa_name"
    t.string "ts_name"
    t.string "barcode"
    t.string "doc_type_name"
    t.integer "quantity"
    t.decimal "retail_price", precision: 15, scale: 2
    t.decimal "retail_amount", precision: 15, scale: 2
    t.integer "sale_percent"
    t.float "commission_percent"
    t.string "office_name"
    t.string "supplier_oper_name"
    t.date "order_dt"
    t.date "sale_dt"
    t.date "rr_dt"
    t.string "shk_id"
    t.decimal "retail_price_withdisc_rub", precision: 15, scale: 2
    t.integer "delivery_amount"
    t.integer "return_amount"
    t.decimal "delivery_rub", precision: 15, scale: 2
    t.string "gi_box_type_name"
    t.float "product_discount_for_report"
    t.integer "supplier_promo"
    t.string "rid"
    t.float "ppvz_spp_prc"
    t.float "ppvz_kvw_prc_base"
    t.float "ppvz_kvw_prc"
    t.float "sup_rating_prc_up"
    t.float "is_kgvp_v2"
    t.float "ppvz_sales_commission"
    t.decimal "ppvz_for_pay", precision: 15, scale: 2
    t.float "ppvz_reward"
    t.float "acquiring_fee"
    t.string "acquiring_bank"
    t.float "ppvz_vw"
    t.float "ppvz_vw_nds"
    t.integer "ppvz_office_id"
    t.string "ppvz_office_name"
    t.integer "ppvz_supplier_id"
    t.string "ppvz_supplier_name"
    t.string "ppvz_inn"
    t.string "declaration_number"
    t.string "bonus_type_name"
    t.string "sticker_id"
    t.string "site_country"
    t.decimal "penalty", precision: 15, scale: 2
    t.decimal "additional_payment", precision: 15, scale: 2
    t.decimal "rebill_logistic_cost", precision: 15, scale: 2
    t.string "rebill_logistic_org"
    t.string "kiz"
    t.string "srid"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "project_id"
  end

  create_table "sales", force: :cascade do |t|
    t.integer "project_id"
    t.bigint "realizationreport_id"
    t.datetime "date_from"
    t.datetime "date_to"
    t.datetime "create_dt"
    t.string "currency_name"
    t.string "suppliercontract_code"
    t.bigint "rrd_id"
    t.bigint "gi_id"
    t.string "subject_name"
    t.bigint "nm_id"
    t.string "brand_name"
    t.string "sa_name"
    t.string "ts_name"
    t.string "barcode"
    t.string "doc_type_name"
    t.bigint "quantity"
    t.bigint "retail_price"
    t.bigint "retail_amount"
    t.bigint "sale_percent"
    t.float "commission_percent"
    t.string "office_name"
    t.string "supplier_oper_name"
    t.datetime "order_dt"
    t.datetime "rr_dt"
    t.datetime "sale_dt"
    t.bigint "shk_id"
    t.float "retail_price_withdisc_rub"
    t.bigint "delivery_amount"
    t.bigint "return_amount"
    t.float "delivery_rub"
    t.string "gi_box_type_name"
    t.float "product_discount_for_report"
    t.string "supplier_promo"
    t.bigint "rid"
    t.float "ppvz_spp_prc"
    t.float "ppvz_kvw_prc"
    t.float "ppvz_kvw_prc_base"
    t.float "sup_rating_prc_up"
    t.float "is_kgvp_v2"
    t.float "ppvz_sales_commission"
    t.float "ppvz_for_pay"
    t.float "ppvz_reward"
    t.float "acquiring_fee"
    t.string "acquiring_bank"
    t.float "ppvz_vw"
    t.float "ppvz_vw_nds"
    t.bigint "ppvz_office_id"
    t.string "ppvz_office_name"
    t.string "ppvz_supplier_name"
    t.string "ppvz_inn"
    t.string "declaration_number"
    t.string "bonus_type_name"
    t.string "sticker_id"
    t.string "site_country"
    t.bigint "ppvz_supplier_id"
    t.float "penalty"
    t.float "additional_payment"
    t.float "rebill_logistic_cost"
    t.string "rebill_logistic_org"
    t.string "kiz"
    t.string "srid"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "uid"
    t.index ["uid"], name: "index_sales_on_uid", unique: true
  end

  create_table "sales_operations", force: :cascade do |t|
    t.string "name"
    t.integer "factor"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "schedule_templates", force: :cascade do |t|
    t.string "name"
    t.integer "period_minute"
    t.string "job_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "only_one"
    t.boolean "only_admin"
    t.string "queue"
  end

  create_table "search_queries", force: :cascade do |t|
    t.string "project_id"
    t.string "query"
    t.boolean "active"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "stock_histories", force: :cascade do |t|
    t.date "date"
    t.float "log_warehouse_coef"
    t.integer "office_id"
    t.string "warehouse"
    t.float "warehouse_coef"
    t.integer "gi_id"
    t.integer "chrt_id"
    t.string "size"
    t.string "barcode"
    t.string "subject"
    t.string "brand"
    t.string "vendor_code"
    t.integer "nm_id"
    t.float "volume"
    t.string "calc_type"
    t.float "warehouse_price"
    t.integer "barcodes_count"
    t.integer "pallet_place_code"
    t.integer "pallet_count"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "project_id"
  end

  create_table "stock_stocks", force: :cascade do |t|
    t.string "supplier_article"
    t.integer "project_id"
    t.integer "nm_id"
    t.integer "price"
    t.integer "discount"
    t.integer "quantity"
    t.integer "quantity_full"
    t.integer "in_way_to_client"
    t.integer "in_way_from_client"
    t.string "tech_size"
    t.boolean "is_supply"
    t.boolean "is_realization"
    t.string "warehouse_name"
    t.integer "warehouse_id"
    t.time "period"
    t.time "last_change_date"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "supply_schemes", force: :cascade do |t|
    t.string "nm_id"
    t.integer "warehouse_id"
    t.float "percent"
  end

  create_table "tariff_localization_indices", force: :cascade do |t|
    t.float "index"
    t.float "left"
    t.float "right"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tariff_localizations", force: :cascade do |t|
    t.float "index"
    t.float "left"
    t.float "right"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tariff_periodics", force: :cascade do |t|
    t.string "warehouseName"
    t.decimal "boxDeliveryBase", precision: 15, scale: 7
    t.decimal "boxDeliveryLiter", precision: 15, scale: 7
    t.decimal "boxStorageBase", precision: 15, scale: 7
    t.decimal "boxStorageLiter", precision: 15, scale: 7
    t.decimal "palletDeliveryValueBase", precision: 15, scale: 7
    t.decimal "palletDeliveryValueLiter", precision: 15, scale: 7
    t.decimal "palletStorageValueExpr", precision: 15, scale: 7
    t.datetime "period", precision: nil
  end

  create_table "tariffs", force: :cascade do |t|
    t.integer "tariff_periodic_id"
    t.string "warehouseName"
  end

  create_table "tariffs_actives", force: :cascade do |t|
    t.integer "project_id"
    t.integer "tariff_id"
    t.boolean "active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tariffs_lists", force: :cascade do |t|
    t.integer "tariff_id"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tariffs_periodics", force: :cascade do |t|
    t.string "warehouseName"
    t.decimal "boxDeliveryBase", precision: 15, scale: 7
    t.decimal "boxDeliveryLiter", precision: 15, scale: 7
    t.decimal "boxStorageBase", precision: 15, scale: 7
    t.decimal "boxStorageLiter", precision: 15, scale: 7
    t.decimal "palletDeliveryValueBase", precision: 15, scale: 7
    t.decimal "palletDeliveryValueLiter", precision: 15, scale: 7
    t.decimal "palletStorageValueExpr", precision: 15, scale: 7
    t.datetime "period", precision: nil
  end

  create_table "tariffs_tariffs", force: :cascade do |t|
    t.integer "tariff_periodic_id"
    t.string "warehouseName"
  end

  create_table "task_manager_categories", force: :cascade do |t|
    t.string "title"
    t.integer "project_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "task_manager_statuses", force: :cascade do |t|
    t.string "title"
    t.text "color"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "task_manager_task_statuses", force: :cascade do |t|
    t.string "title"
    t.text "color"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "task_manager_tasks", force: :cascade do |t|
    t.text "text"
    t.string "title"
    t.integer "category_id"
    t.integer "project_id"
    t.integer "status_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_to_project_roles", force: :cascade do |t|
    t.integer "user_id"
    t.integer "project_id"
    t.integer "project_role_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_workers", force: :cascade do |t|
    t.string "klass"
    t.boolean "enabled"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "token_ads"
    t.string "token_std"
    t.string "token_stats"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.integer "chat_id"
    t.boolean "bidder_enabled"
    t.boolean "autoresponder_enabled"
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at", precision: nil
    t.datetime "remember_created_at", precision: nil
    t.string "token"
    t.float "logistics_coef_ktr"
    t.integer "tg_chat_id"
    t.integer "project_id"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "versions_databases", force: :cascade do |t|
    t.text "raw_data"
    t.string "author"
    t.integer "version_no"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "warehouse_actives", force: :cascade do |t|
    t.integer "project_id"
    t.integer "warehouse_id"
    t.boolean "active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "warehouse_from_webs", force: :cascade do |t|
    t.integer "warehouse_web_id"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "belongs_wb", default: false
    t.integer "warehouse_id"
  end

  create_table "warehouse_mappings", force: :cascade do |t|
    t.string "name_mapped"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "warehouse_priorities", force: :cascade do |t|
    t.integer "warehouse_web_id"
    t.integer "time1"
    t.integer "time2"
    t.integer "time"
    t.integer "point_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "priority_query_id"
    t.integer "district_id"
  end

  create_table "warehouse_priorities2", force: :cascade do |t|
    t.string "warehouse"
    t.string "region"
    t.integer "time"
    t.integer "dist"
    t.time "period"
  end

  create_table "warehouse_warehouses", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.float "period_prepare"
    t.float "period_delivery"
    t.float "period_acceptance"
    t.integer "period_planning"
    t.boolean "allow_kgt"
    t.integer "warehouse_web_id"
    t.boolean "used_in_orders", default: false
    t.integer "district_id"
  end

  create_table "warehouses", force: :cascade do |t|
    t.string "name"
    t.float "period_prepare"
    t.float "period_acceptance"
    t.float "period_delivery"
    t.float "period_planning"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "warehouses_by_district", force: :cascade do |t|
    t.string "location"
    t.string "warehouseName"
    t.boolean "active"
  end

end
