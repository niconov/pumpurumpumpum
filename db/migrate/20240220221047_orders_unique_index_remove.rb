class OrdersUniqueIndexRemove < ActiveRecord::Migration[7.1]
  def change
    remove_index :order_orders, :srid
  end
end
