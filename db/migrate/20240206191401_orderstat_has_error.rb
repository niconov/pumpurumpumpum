class OrderstatHasError < ActiveRecord::Migration[7.1]
  def change
    add_column :order_stats, :has_error, :boolean, default: true
  end
end
