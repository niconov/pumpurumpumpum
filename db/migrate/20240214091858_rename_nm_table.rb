class RenameNmTable < ActiveRecord::Migration[7.1]
  def change
    rename_table :nms, :nm_nms
  end
end
