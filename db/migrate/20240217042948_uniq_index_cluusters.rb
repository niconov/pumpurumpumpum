class UniqIndexCluusters < ActiveRecord::Migration[7.1]
  def change
    add_index :advert_clusters, [:project_id, :advert_id, :cluster_name], unique: true
  end
end
