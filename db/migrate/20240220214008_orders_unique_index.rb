class OrdersUniqueIndex < ActiveRecord::Migration[7.1]
  def change
    add_index :order_orders, [:project_id, :srid], unique: true
  end
end
