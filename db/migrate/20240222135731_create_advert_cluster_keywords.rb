class CreateAdvertClusterKeywords < ActiveRecord::Migration[7.1]
  def change
    drop_table :advert_cluster_keywords
    create_table :advert_cluster_keywords do |t|
      t.string :keyword
    end
  end
end
