class AddFieldsAdvertStatAuto3 < ActiveRecord::Migration[7.1]
  def change
    rename_column :advert_stats_autos, :viws_7d, :views_7d
    rename_column :advert_stats_autos, :viws_3d, :views_3d
  end
end
