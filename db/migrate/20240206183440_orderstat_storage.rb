class OrderstatStorage < ActiveRecord::Migration[7.1]
  def change
    add_column :order_stats, :storage, :float, default: 0.0
  end
end
