class AddIndexToStatAuto < ActiveRecord::Migration[7.1]
  def change
    add_column :advert_stats_autos, :keyword_id, :integer
    add_index :advert_stats_autos, [:keyword_id, :advert_id, :project_id], unique: true
    remove_column :advert_stats_autos, :created_at
    remove_column :advert_stats_autos, :period
  end
end
