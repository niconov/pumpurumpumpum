class CreateNmStats < ActiveRecord::Migration[7.1]
  def change
    create_table :nm_stats do |t|
      t.datetime :period
      t.integer :nm_id
      t.string :vendorCode
      t.string :brandName
      t.string :object_name
      t.integer :object__id
      t.integer :openCardCount
      t.integer :addToCartCount
      t.integer :ordersCount
      t.float :ordersSumRub
      t.integer :buyoutsCount
      t.float :buyoutsSumRub
      t.integer :cancelCount
      t.float :cancelSumRub
      t.float :avgPriceRub
      t.integer :avgOrdersCountPerDay
      t.float :addToCartPercent
      t.float :cartToOrderPercent
      t.float :buyoutsPercent
      t.integer :stocksMp
      t.integer :stocksWb

      t.timestamps
    end
  end
end
