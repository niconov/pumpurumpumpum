class AddForcedActiveAndDisabledClusterChangeType < ActiveRecord::Migration[7.1]
  def change
    remove_column :advert_clusters, :forced_state
    add_column :advert_clusters, :forced_state, :integer, default: 0
  end
end
