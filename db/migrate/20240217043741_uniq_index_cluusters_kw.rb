class UniqIndexCluustersKw < ActiveRecord::Migration[7.1]
  def change
    add_index :advert_cluster_keywords, [:project_id, :advert_id, :keyword], unique: true
  end
end


