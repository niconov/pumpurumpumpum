class AdvertstatsAddExcluded < ActiveRecord::Migration[7.1]
  def change
    add_column :advert_stats_autos, :excluded, :boolean, default: false
  end
end
