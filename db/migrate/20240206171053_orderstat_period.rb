class OrderstatPeriod < ActiveRecord::Migration[7.1]
  def change
    add_column :order_stats, :period, :date
  end
end
