class PffffffffffX6 < ActiveRecord::Migration[7.1]
  def change
    change_column :advert_stats_autos, :ctr, :integer, precision: 15, scale: 2, :default => 0.0
  end
end
