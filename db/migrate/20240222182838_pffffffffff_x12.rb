class PffffffffffX12 < ActiveRecord::Migration[7.1]
  def change
    remove_column :advert_stats_autos, :sum
    add_column :advert_stats_autos, :sum, :float
  end
end
