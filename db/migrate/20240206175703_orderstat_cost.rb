class OrderstatCost < ActiveRecord::Migration[7.1]
  def change
    add_column :order_stats, :cost, :float
  end
end
