class Pffffffffff < ActiveRecord::Migration[7.1]
  def change
    add_column :advert_clusters, :cluster_id, :integer
    remove_column :advert_stats_autos, :views_3d
    remove_column :advert_stats_autos, :views_7d
    remove_column :advert_stats_autos, :clicks_3d
    remove_column :advert_stats_autos, :clicks_7d
    remove_column :advert_stats_autos, :ctr_3d
    remove_column :advert_stats_autos, :ctr_7d
    remove_column :advert_stats_autos, :sum_3d
    remove_column :advert_stats_autos, :sum_7d
    remove_column :advert_stats_autos, :days
    remove_column :advert_clusters, :count
    add_column :advert_clusters, :sum, :integer, default: 0, precision: 15, scale: 2
    add_column :advert_clusters, :clicks, :integer, default: 0, precision: 15, scale: 0
    add_column :advert_clusters, :views, :integer, default: 0, precision: 15, scale: 0
    add_column :advert_clusters, :ctr, :integer, default: 0, precision: 15, scale: 0
  end
end
