class CreateOrderDailyStats < ActiveRecord::Migration[7.1]

  def change
    create_table :order_daily_stats do |t|
      t.date :period
      t.integer :nm_id
      t.integer :sum, precision: 15, scale: 2
      t.integer :quantity, precision: 17, scale: 0
      t.integer :price, precision: 15, scale: 2
      t.integer :project_id
    end
  end
end
