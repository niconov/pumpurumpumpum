class AddIndexRemoveSpp2 < ActiveRecord::Migration[7.1]
  def change
    add_column :order_district_stats, :district_id, :integer
    add_index :order_district_stats, [:project_id, :period, :nm_id, :district_id], unique: true
  end
end
