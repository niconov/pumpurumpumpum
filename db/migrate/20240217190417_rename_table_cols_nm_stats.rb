class RenameTableColsNmStats < ActiveRecord::Migration[7.1]
  def change
    rename_column :nm_stats, :vendorCode, :vendor_code
    rename_column :nm_stats, :brandName, :brand_name
    rename_column :nm_stats, :openCardCount, :open_card_count
    rename_column :nm_stats, :addToCartCount, :add_to_cart_count
    rename_column :nm_stats, :ordersCount, :orders_count
    rename_column :nm_stats, :ordersSumRub, :orders_sum_rub
    rename_column :nm_stats, :buyoutsCount, :buyouts_count
    rename_column :nm_stats, :buyoutsSumRub, :buyouts_sum_rub
    rename_column :nm_stats, :cancelCount, :cancel_count
    rename_column :nm_stats, :cancelSumRub, :cancel_sum_rub
    rename_column :nm_stats, :avgPriceRub, :avg_price_rub
    rename_column :nm_stats, :avgOrdersCountPerDay, :avg_orders_count_per_day
    rename_column :nm_stats, :addToCartPercent, :add_to_cart_percent
    rename_column :nm_stats, :cartToOrderPercent, :cart_to_order_percent
    rename_column :nm_stats, :buyoutsPercent, :buyouts_percent
    rename_column :nm_stats, :stocksMp, :stocks_mp
    rename_column :nm_stats, :stocksWb, :stocks_wb
  end
end
