class PffffffffffX4 < ActiveRecord::Migration[7.1]
  def change
    remove_index :advert_stats_autos, [:keyword_id, :advert_id, :project_id], unique: true
    add_column :advert_stats_autos, :period, :date
    add_index :advert_stats_autos, [:project_id, :advert_id, :period, :keyword_id], unique: true

  end
end
