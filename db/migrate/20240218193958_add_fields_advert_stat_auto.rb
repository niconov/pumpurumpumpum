class AddFieldsAdvertStatAuto < ActiveRecord::Migration[7.1]
  def change
    add_column :advert_stats_autos, :views_3d, :integer 
    add_column :advert_stats_autos, :clicks_3d, :integer 
    add_column :advert_stats_autos, :ctr_3d, :float 
    add_column :advert_stats_autos, :sum_3d, :float 
    add_column :advert_stats_autos, :views_7d, :integer
    add_column :advert_stats_autos, :clicks_7d, :integer 
    add_column :advert_stats_autos, :ctr_7d, :float
    add_column :advert_stats_autos, :sum_7d, :float 
  end
end
