class AddFieldsAdvertStatAuto2 < ActiveRecord::Migration[7.1]
  def change
    change_column :advert_stats_autos, :views_3d, :integer, default: 0.0 
    change_column :advert_stats_autos, :clicks_3d, :integer, default: 0.0 
    change_column :advert_stats_autos, :ctr_3d, :float, default: 0.0 
    change_column :advert_stats_autos, :sum_3d, :float, default: 0.0 
    change_column :advert_stats_autos, :views_7d, :integer, default: 0.0
    change_column :advert_stats_autos, :clicks_7d, :integer, default: 0.0 
    change_column :advert_stats_autos, :ctr_7d, :float, default: 0.0
    change_column :advert_stats_autos, :sum_7d, :float, default: 0.0 
  end
end
