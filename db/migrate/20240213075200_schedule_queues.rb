class ScheduleQueues < ActiveRecord::Migration[7.1]
  def change
    add_column :schedule_templates, :queue, :string
  end
end
