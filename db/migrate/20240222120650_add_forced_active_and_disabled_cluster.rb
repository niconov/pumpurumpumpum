class AddForcedActiveAndDisabledCluster < ActiveRecord::Migration[7.1]
  def change
    add_column :advert_clusters, :forced_state, :string, default: false
  end
end
