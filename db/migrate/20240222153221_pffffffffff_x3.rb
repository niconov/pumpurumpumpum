class PffffffffffX3 < ActiveRecord::Migration[7.1]
  def change
    change_column :advert_stats_autos, :views, :integer, :default => 0
    change_column :advert_stats_autos, :clicks, :integer, :default => 0
    change_column :advert_stats_autos, :ctr, :float, :default => 0
    change_column :advert_stats_autos, :sum, :decimal, :default => 0
  end
end
