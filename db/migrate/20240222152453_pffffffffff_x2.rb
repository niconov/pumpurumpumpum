class PffffffffffX2 < ActiveRecord::Migration[7.1]
  def change
    remove_column :advert_clusters, :cluster_id
    remove_column :advert_clusters, :created_at
    remove_column :advert_clusters, :updated_at
  end
end
