class OrderstatDefault < ActiveRecord::Migration[7.1]
  def change
    change_column_default :order_stats, :quantity, 0
    change_column_default :order_stats, :sum, 0.0
    change_column_default :order_stats, :advert, 0.0
    change_column_default :order_stats, :profit, 0.0
    change_column_default :order_stats, :cost, 0.0
  end
end
