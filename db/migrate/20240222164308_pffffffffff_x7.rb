class PffffffffffX7 < ActiveRecord::Migration[7.1]
  def change
    change_column :advert_clusters, :ctr, :integer, precision: 15, scale: 2, :default => 0.0
  end
end
