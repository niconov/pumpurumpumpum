class AddIndexOrderDailyStats < ActiveRecord::Migration[7.1]
  def change
    add_index :order_daily_stats, [:project_id, :period, :nm_id], unique: true
  end
end
