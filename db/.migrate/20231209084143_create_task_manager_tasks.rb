class CreateTaskManagerTasks < ActiveRecord::Migration[7.1]
  def change
    create_table :task_manager_tasks do |t|
      t.text :text
      t.string :title
      t.integer :category_id
      t.integer :project_id
      t.integer :status_id

      t.timestamps
    end
  end
end
