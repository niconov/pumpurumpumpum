class CreateOrderStatDistricts < ActiveRecord::Migration[7.1]
  def change
    create_table :order_stat_districts do |t|
      t.integer :nm_id
      t.integer :district_id
      t.integer :count

      t.timestamps
    end
  end
end
