class CreateAutoresponsers < ActiveRecord::Migration[7.1]
  def change
    create_table :autoresponsers do |t|
      t.text :text
      t.integer :rating
      t.integer :project_id

      t.timestamps
    end
  end
end
