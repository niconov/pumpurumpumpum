class RenameUserIdToProjectId < ActiveRecord::Migration[7.1]
  def change
    rename_column :adverts, :user_id, :project_id
    rename_column :events, :user_id, :project_id
    rename_column :nms, :user_id, :project_id
    rename_column :orders, :user_id, :project_id
    rename_column :sales_stats_by_districts, :user_id, :project_id
    rename_column :search_queries, :user_id, :project_id
    rename_column :stocks, :user_id, :project_id
    rename_column :tariff_actives, :user_id, :project_id
    rename_column :warehouse_actives, :user_id, :project_id

  end
end


# Advert::Advert.where(project_id: 2).all.map {|i| i.project_id = 7; i.save}
# Event.where(project_id: 2).all.map {|i| i.project_id = 7; i.save}
# Nm::Nm.where(project_id: 2).all.map {|i| i.project_id = 7; i.save}
# Stock.where(project_id: 2).all.map {|i| i.project_id = 7; i.save}
# TariffActive.where(project_id: 2).all.map {|i| i.project_id = 7; i.save}
# WarehouseActive.where(project_id: 2).all.map {|i| i.project_id = 7; i.save}
# Order::Order.where(project_id: 2).all.map {|i| i.project_id = 7; i.save}

# SalesStatsByDistricts.where(project_id: 2).all.map {|i| i.project_id = 7; i.save}

# SearchQu.where(project_id: 2).all.map {|i| i.project_id = 7; i.save}
