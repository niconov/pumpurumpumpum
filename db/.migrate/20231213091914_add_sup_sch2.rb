class AddSupSch2 < ActiveRecord::Migration[7.1]
  def change
    change_column_default :deliveries_supply_schemas, :count, 0
  end
end
