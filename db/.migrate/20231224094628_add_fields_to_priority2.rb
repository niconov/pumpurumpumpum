class AddFieldsToPriority2 < ActiveRecord::Migration[7.1]
  def change
    remove_column :order_stat_districts, :demands, :integer
  end
end
