class AddSupSch4 < ActiveRecord::Migration[7.1]
  def change
    change_column_default :deliveries_supply_schemas, :count_local, 0
  end
end
