class CreateSalesOperations < ActiveRecord::Migration[7.1]
  def change
    create_table :sales_operations do |t|
      t.string :name
      t.integer :factor

      t.timestamps
    end
  end
end
