class CreateNmGroupQueries < ActiveRecord::Migration[7.1]
  def change
    create_table :nm_group_queries do |t|
      t.integer :project_id
      t.string :name
      t.string :query

      t.timestamps
    end
  end
end
