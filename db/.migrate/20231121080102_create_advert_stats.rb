class CreateAdvertStats < ActiveRecord::Migration[7.1]
  def change
    create_table :advert_stats do |t|
      t.float :views
      t.float :clicks
      t.float :ctr
      t.float :cpc
      t.float :sum
      t.float :atbs
      t.float :orders
      t.float :cr
      t.float :shks
      t.float :sum_price
      t.integer :nm_id
      t.datetime :period

      t.timestamps
    end
  end
end
