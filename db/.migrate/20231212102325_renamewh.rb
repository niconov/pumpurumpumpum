class Renamewh < ActiveRecord::Migration[7.1]
  def change
    drop_table :warehouse_warehouses
    rename_table :warehouses, :warehouse_warehouses
  end
end
