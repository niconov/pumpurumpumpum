class CreateSaleReports < ActiveRecord::Migration[7.1]
  def change
    create_table :sale_reports do |t|
      t.integer :realization_report_id
      t.date :date_from
      t.date :date_to
      t.date :create_dt
      t.string :currency_name
      t.json :suppliercontract_code
      t.string :rrd_id
      t.integer :gi_id
      t.string :subject_name
      t.integer :nm_id
      t.string :brand_name
      t.string :sa_name
      t.string :ts_name
      t.string :barcode
      t.string :doc_type_name
      t.integer :quantity
      t.integer :retail_price
      t.integer :retail_amount
      t.integer :sale_percent
      t.float :commission_percent
      t.string :office_name
      t.string :supplier_oper_name
      t.date :order_dt
      t.date :sale_dt
      t.date :rr_dt
      t.string :shk_id
      t.float :retail_price_withdisc_rub
      t.integer :delivery_amount
      t.integer :return_amount
      t.float :delivery_rub
      t.string :gi_box_type_name
      t.float :product_discount_for_report
      t.integer :supplier_promo
      t.string :rid
      t.float :ppvz_spp_prc
      t.float :ppvz_kvw_prc_base
      t.float :ppvz_kvw_prc
      t.float :sup_rating_prc_up
      t.float :is_kgvp_v2
      t.float :ppvz_sales_commission
      t.float :ppvz_for_pay
      t.float :ppvz_reward
      t.float :acquiring_fee
      t.string :acquiring_bank
      t.float :ppvz_vw
      t.float :ppvz_vw_nds
      t.integer :ppvz_office_id
      t.string :ppvz_office_name
      t.integer :ppvz_supplier_id
      t.string :ppvz_supplier_name
      t.string :ppvz_inn
      t.string :declaration_number
      t.string :bonus_type_name
      t.string :sticker_id
      t.string :site_country
      t.float :penalty
      t.float :additional_payment
      t.float :rebill_logistic_cost
      t.string :rebill_logistic_org
      t.string :kiz
      t.string :srid

      t.timestamps
    end
  end
end
