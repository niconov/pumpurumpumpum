class CreateNmPositions < ActiveRecord::Migration[7.1]
  def change
    create_table :nm_positions do |t|
      t.integer :nm_id
      t.integer :dist
      t.integer :time1
      t.integer :time2
      t.datetime :period
      t.integer :cpm
      t.integer :position
      t.integer :position_promo

      t.timestamps
    end
  end
end
