class Sales3 < ActiveRecord::Migration[7.1]
  def change
    remove_column :sales, :uid
    add_column :sales, :uid, :string
    add_index :sales, :uid, unique: true
  end
end
