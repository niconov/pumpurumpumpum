class JpbTemplatedAddF < ActiveRecord::Migration[7.1]
  def change
    add_column :schedule_templates, :only_one, :boolean
    add_column :schedule_templates, :only_admin, :boolean
  end
end
