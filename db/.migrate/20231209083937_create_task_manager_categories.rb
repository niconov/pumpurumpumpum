class CreateTaskManagerCategories < ActiveRecord::Migration[7.1]
  def change
    create_table :task_manager_categories do |t|
      t.string :title
      t.integer :project_id

      t.timestamps
    end
  end
end
