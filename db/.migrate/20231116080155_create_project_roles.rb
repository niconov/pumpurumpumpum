class CreateProjectRoles < ActiveRecord::Migration[7.1]
  def change
    create_table :project_roles do |t|
      t.integer :user_id
      t.string :user_role

      t.timestamps
    end
  end
end
