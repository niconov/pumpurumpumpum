class CreateAdvertCosts < ActiveRecord::Migration[7.1]
  def change
    create_table :advert_costs do |t|
      t.integer :update_num
      t.date :update_time
      t.float :update_sum
      t.integer :advert_id
      t.string :camp_name
      t.integer :advert_type
      t.integer :payment_type
      t.integer :advert_status

      t.timestamps
    end
  end
end
