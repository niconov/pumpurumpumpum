class CreateDeliveriesPickupPoints < ActiveRecord::Migration[7.1]
  def change
    create_table :deliveries_pickup_points do |t|
      t.string :regions
      t.string :dest
      t.string :name

      t.timestamps
    end
  end
end
