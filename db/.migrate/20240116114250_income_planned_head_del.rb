class IncomePlannedHeadDel < ActiveRecord::Migration[7.1]
  def change
    add_column :income_planned_heads, :is_deleted, :boolean, default: false
    add_column :income_planned_heads, :deleted_at, :time, default: nil
  end
end
