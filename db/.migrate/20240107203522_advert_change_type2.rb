class AdvertChangeType2 < ActiveRecord::Migration[7.1]
  def change
    remove_column :advert_adverts, :status
    rename_column :advert_adverts, :status_new, :status
  end
end
