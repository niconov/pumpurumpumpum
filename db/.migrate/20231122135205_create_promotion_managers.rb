class CreatePromotionManagers < ActiveRecord::Migration[7.1]
  def change
    create_table :promotion_managers do |t|
      t.integer :nm_id
      t.integer :advert_id
      t.float :budget_daily
      t.boolean :enabled

      t.timestamps
    end
  end
end
