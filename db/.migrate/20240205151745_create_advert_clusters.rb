class CreateAdvertClusters < ActiveRecord::Migration[7.1]
  def change
    create_table :advert_clusters do |t|
      t.integer :advert_id
      t.string :cluster_name
      t.integer :count
      t.boolean :excluded

      t.timestamps
    end
  end
end
