class AddProfitRole < ActiveRecord::Migration[7.1]
  def change
    add_column :project_roles, :access_profit, :boolean
  end
end
