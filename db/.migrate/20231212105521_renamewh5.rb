class Renamewh5 < ActiveRecord::Migration[7.1]
  def change
    rename_column :warehouse_from_webs, :warehouse_id, :warehouse_web_id
    add_column :warehouse_from_webs, :warehouse_id, :integer
  end
end
