class AddAdsBud < ActiveRecord::Migration[7.1]
  def change
    add_column :adverts, :is_low_budget, :boolean, default: true
  end
end
