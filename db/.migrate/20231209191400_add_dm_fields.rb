class AddDmFields < ActiveRecord::Migration[7.1]
  def change
    add_column :delivery_methods, :time_delivery, :integer, default: 0
  end
end
