class CreateWarehouseWarehouses < ActiveRecord::Migration[7.1]
  def change
    create_table :warehouse_warehouses do |t|
    t.string "name"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.float "period_prepare"
    t.float "period_delivery"
    t.float "period_acceptance"
    t.integer "period_planning"
    end
  end
end
