class CreateStockHistories < ActiveRecord::Migration[7.1]
  def change
    rename_table :stocks, :stock_stocks
    create_table :stock_histories do |t|
      t.date :date
      t.float :log_warehouse_coef
      t.integer :office_id
      t.string :warehouse
      t.float :warehouse_coef
      t.integer :gi_id
      t.integer :chrt_id
      t.string :size
      t.string :barcode
      t.string :subject
      t.string :brand
      t.string :vendor_code
      t.integer :nm_id
      t.integer :volume
      t.string :calc_type
      t.float :warehouse_price
      t.integer :barcodes_count
      t.integer :pallet_place_code
      t.integer :pallet_count
        
      t.timestamps
    end
  end
end
