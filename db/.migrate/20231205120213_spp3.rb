class Spp3 < ActiveRecord::Migration[7.1]
  def change
    remove_column :nm_comperitors, :raw
    remove_column :nm_comperitors, :raw_card
    add_column :nm_comperitors, :raw, :json
    add_column :nm_comperitors, :raw_card, :json
  end
end
