class CreateUserWorkers < ActiveRecord::Migration[7.1]
  def change
    create_table :user_workers do |t|
      t.string :klass
      t.boolean :enabled

      t.timestamps
    end
  end
end
