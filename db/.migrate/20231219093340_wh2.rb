class Wh2 < ActiveRecord::Migration[7.1]
  def change
    rename_column :warehouse_warehouses, :district_id_bak, :district_id
  end
end
