class AddAdvPay < ActiveRecord::Migration[7.1]
  def change
    remove_column :advert_costs, :payment_type
    add_column :advert_costs, :payment_type, :integer
  end
end
