class Nm6 < ActiveRecord::Migration[7.1]
  def change
    remove_column :nms, :project_id
    rename_column :nms, :project_id_i, :project_id
  end
end
