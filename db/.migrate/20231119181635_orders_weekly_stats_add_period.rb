class OrdersWeeklyStatsAddPeriod < ActiveRecord::Migration[7.1]
  def change
    add_column :orders_stats_weeklies, :period, :datetime
  end
end
