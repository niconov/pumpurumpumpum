class CreateDeliveriesWarehousePriorities < ActiveRecord::Migration[7.1]
  def change
    create_table :deliveries_warehouse_priorities do |t|
      t.integer :warehouse_id
      t.integer :time1
      t.integer :time2
      t.integer :time
      t.integer :point_id
      t.integer :priotity_query_id

      t.timestamps
    end
  end
end
