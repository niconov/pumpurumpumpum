class Scales < ActiveRecord::Migration[7.1]
  def change
    # change_column :sale_reports, :weight, :decimal, :precision => 10, :scale => 2

    change_column :sale_reports, :retail_price, :decimal, :precision => 15, :scale => 2
    change_column :sale_reports, :retail_amount, :decimal, :precision => 15, :scale => 2
    change_column :sale_reports, :retail_price_withdisc_rub, :decimal, :precision => 15, :scale => 2
    change_column :sale_reports, :delivery_rub, :decimal, :precision => 15, :scale => 2
    change_column :sale_reports, :ppvz_for_pay, :decimal, :precision => 15, :scale => 2
    change_column :sale_reports, :penalty, :decimal, :precision => 15, :scale => 2
    change_column :sale_reports, :additional_payment, :decimal, :precision => 15, :scale => 2
    change_column :sale_reports, :rebill_logistic_cost, :decimal, :precision => 15, :scale => 2
  end
end
