class CreateIncomePlanneds < ActiveRecord::Migration[7.1]
  def change
    create_table :income_planneds do |t|
      t.integer :warehouse_id
      t.time :period

      t.timestamps
    end
  end

  rename_table :incomes, :income_incomes


end
