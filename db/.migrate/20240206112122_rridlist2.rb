class Rridlist2 < ActiveRecord::Migration[7.1]
  def change
    add_column :sale_report_lists, :date_from, :date
    add_column :sale_report_lists, :date_to, :date
    add_column :sale_report_lists, :create_dt, :date
  end
end
