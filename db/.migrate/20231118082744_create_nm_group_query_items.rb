class CreateNmGroupQueryItems < ActiveRecord::Migration[7.1]
  def change
    create_table :nm_group_query_items do |t|
      t.integer :nm_group_query_id
      t.integer :nm_id

      t.timestamps
    end
  end
end
