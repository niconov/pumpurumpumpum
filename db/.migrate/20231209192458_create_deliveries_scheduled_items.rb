class CreateDeliveriesScheduledItems < ActiveRecord::Migration[7.1]
  def change
    create_table :deliveries_scheduled_items do |t|
      t.integer :nm_id
      t.integer :qty
      t.integer :deliveries_sheduled_id

      t.timestamps
    end
  end
end
