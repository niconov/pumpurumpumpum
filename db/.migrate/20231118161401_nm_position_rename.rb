class NmPositionRename < ActiveRecord::Migration[7.1]
  def change
    rename_column :nm_positions, :position_promo, :position_no_advert
  end
end
