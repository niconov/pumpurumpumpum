class CreateIncomePlannedItems < ActiveRecord::Migration[7.1]
  def change
    create_table :income_planned_items do |t|
      t.integer :income_planned_id
      t.integer :nm_id
      t.integer :quantity

      t.timestamps
    end
  end
end
