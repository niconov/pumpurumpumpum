class Projectrename < ActiveRecord::Migration[7.1]
  def change
    rename_table :project_user_settings, :projects_user_settings
  end
end
