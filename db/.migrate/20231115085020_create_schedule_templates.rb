class CreateScheduleTemplates < ActiveRecord::Migration[7.1]
  def change
    create_table :schedule_templates do |t|
      t.string :name
      t.integer :period_minute
      t.string :job_name

      t.timestamps
    end
  end
end
