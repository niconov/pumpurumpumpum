class RenameTablesProjects < ActiveRecord::Migration[7.1]
  def change
    rename_table 'projects', 'projects_projects'
    rename_table 'role_user_on_projects', 'projects_role_user_on_projects'
    rename_table 'project_roles', 'projects_roles'
  end
end
