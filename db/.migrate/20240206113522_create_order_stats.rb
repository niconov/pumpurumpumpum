class CreateOrderStats < ActiveRecord::Migration[7.1]
  def change
    create_table :order_stats do |t|
      t.integer :quantity
      t.float :sum
      t.float :advert
      t.float :pocs
      t.float :ros
      t.float :price
      t.float :price_spp
      t.float :profit
      t.integer :nm_id

      t.timestamps
    end
  end
end
