class CreateAdvertParams < ActiveRecord::Migration[7.1]
  def change
    create_table :advert_params do |t|
      t.integer :advert_id
      t.integer :nm_id
      t.boolean :carousel
      t.boolean :recom
      t.boolean :booster

      t.timestamps
    end
  end
end
