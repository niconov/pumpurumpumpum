class CreateAdvertPayments < ActiveRecord::Migration[7.1]
  def change
    create_table :advert_payments do |t|
      t.integer :payment_id
      t.date :date
      t.float :sum
      t.integer :payment_type
      t.integer :status_id
      t.string :card_status

      t.timestamps
    end
  end
end
