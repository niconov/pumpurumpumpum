class AddSupSch < ActiveRecord::Migration[7.1]
  def change
    add_column :deliveries_supply_schemas, :is_local, :boolean
  end
end
