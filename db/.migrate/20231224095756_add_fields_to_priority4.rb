class AddFieldsToPriority4 < ActiveRecord::Migration[7.1]
  def change
    remove_column :deliveries_supply_schemas, :warehouse_id
    add_column :deliveries_supply_schemas, :district_id, :integer
  end
end
