class CreateOrdersStatsWeeklies < ActiveRecord::Migration[7.1]
  def change
    create_table :orders_stats_weeklies do |t|
      t.integer :nm_id
      t.integer :qty
      t.float :sum
      t.float :price
      t.float :discount

      t.timestamps
    end
  end
end
