class AddTelegramIdUser < ActiveRecord::Migration[7.1]
  def change
    add_column :users, :tg_chat_id, :integer
  end
end
