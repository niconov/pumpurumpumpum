class AddOrderFields3 < ActiveRecord::Migration[7.1]
  def change
    add_column :orders, :finished_price, :float
  end
end
