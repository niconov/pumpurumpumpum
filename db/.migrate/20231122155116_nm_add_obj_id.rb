class NmAddObjId < ActiveRecord::Migration[7.1]
  def change
    remove_column :nms, :object_id
    add_column :nms, :obj_id, :integer
  end
end
