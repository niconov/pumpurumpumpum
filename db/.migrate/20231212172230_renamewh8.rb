class Renamewh8 < ActiveRecord::Migration[7.1]
  def change
    add_column :warehouse_warehouses, :used_in_orders, :boolean, default: false
  end
end
