class OrderStatsWeeklyUpdatePeriods < ActiveRecord::Migration[7.1]
  def change
    add_column :orders_stats_weeklies, :period_start, :datetime
    add_column :orders_stats_weeklies, :period_end, :datetime
    remove_column :orders_stats_weeklies, :period
  end
end
