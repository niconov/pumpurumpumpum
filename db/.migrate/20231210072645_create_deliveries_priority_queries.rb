class CreateDeliveriesPriorityQueries < ActiveRecord::Migration[7.1]
  def change
    create_table :deliveries_priority_queries do |t|
      t.string :query
      t.integer :pages

      t.timestamps
    end
  end
end
