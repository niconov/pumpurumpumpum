class CreateEvents < ActiveRecord::Migration[7.1]
  def change
    create_table :events do |t|
      t.string :subject
      t.string :text
      t.integer :user_id
      t.datetime :date

      t.timestamps
    end
  end
end
