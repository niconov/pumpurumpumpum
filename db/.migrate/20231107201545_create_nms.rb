class CreateNms < ActiveRecord::Migration[7.1]
  def change
    create_table :nms do |t|
      t.integer :nm_id
      t.boolean :prohibited
      t.string :supplier_article
      t.string :barcode
      t.string :imt_id
      t.string :user_id

      t.timestamps
    end
  end
end
