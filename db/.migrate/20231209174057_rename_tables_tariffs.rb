class RenameTablesTariffs < ActiveRecord::Migration[7.1]
  def change
    rename_table 'tariff_actives', 'tariffs_tariff_actives'
    rename_table 'tariff_lists', 'tariffs_tariff_lists'
    rename_table 'tariff_periodics', 'tariffs_tariff_periodics'
    rename_table 'tariffs', 'tariffs_tariffs'
  end
end
