class AddFields2 < ActiveRecord::Migration[7.1]
  def change
    remove_column :deliveries_warehouse_priorities, :priotity_query_id
    rename_column :deliveries_warehouse_priorities, :warehouse_id, :warehouse_web_id
  end
end
