class Spp1 < ActiveRecord::Migration[7.1]
  def change
    add_column :nm_customer_discounts, :project_id, :integer
    remove_column :nm_customer_discounts, :category_id
  end
end
