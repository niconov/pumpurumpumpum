class CreateAdvertClusterKeywords < ActiveRecord::Migration[7.1]
  def change
    create_table :advert_cluster_keywords do |t|
      t.integer :cluster_id
      t.string :keyword

      t.timestamps
    end
  end
end
