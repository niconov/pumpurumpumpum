class Renamewh4 < ActiveRecord::Migration[7.1]
  def change
    rename_table :warehouse_priorities, :warehouse_priorities2
    rename_table :deliveries_warehouse_priorities, :warehouse_priorities
  end
end
