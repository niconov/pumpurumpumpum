class Sales6 < ActiveRecord::Migration[7.1]
  def change
    change_column :sales, :realizationreport_id, :int8, :limit => 8
    change_column :sales, :rrd_id, :int8, :limit => 8
    change_column :sales, :gi_id, :int8, :limit => 8
    change_column :sales, :shk_id, :int8, :limit => 8
    change_column :sales, :gi_id, :int8, :limit => 8
    change_column :sales, :realizationreport_id, :int8, :limit => 8
    change_column :sales, :rrd_id, :int8, :limit => 8
    change_column :sales, :gi_id, :int8, :limit => 8
    change_column :sales, :nm_id, :int8, :limit => 8
    change_column :sales, :quantity, :int8, :limit => 8
    change_column :sales, :retail_price, :int8, :limit => 8
    change_column :sales, :retail_amount, :int8, :limit => 8
    change_column :sales, :sale_percent, :int8, :limit => 8
    change_column :sales, :shk_id, :int8, :limit => 8
    change_column :sales, :delivery_amount, :int8, :limit => 8
    change_column :sales, :return_amount, :int8, :limit => 8
    change_column :sales, :rid, :int8, :limit => 8
    change_column :sales, :ppvz_office_id, :int8, :limit => 8
    change_column :sales, :ppvz_supplier_id, :int8, :limit => 8
  end
end
