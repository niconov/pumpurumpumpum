class AdvertsAddRawResponse2 < ActiveRecord::Migration[7.1]
  def change
    remove_column :adverts, :response_raw
    add_column :adverts, :response_raw, :json
  end
end
