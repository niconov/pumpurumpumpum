class AdvertAddBudgetToday < ActiveRecord::Migration[7.1]
  def change
    add_column :advert_adverts, :balance_payment_sum_on_period, :integer
    add_column :advert_adverts, :balance_payment_period, :datetime
  end
end
