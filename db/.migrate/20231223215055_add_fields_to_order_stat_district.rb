class AddFieldsToOrderStatDistrict < ActiveRecord::Migration[7.1]
  def change
    add_column :order_stat_districts, :demands, :integer
  end
end
