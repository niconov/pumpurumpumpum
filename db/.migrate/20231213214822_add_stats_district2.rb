class AddStatsDistrict2 < ActiveRecord::Migration[7.1]
  def change
    remove_column :sales_stats_by_districts, :is_local
    add_column :order_stat_districts, :is_local, :boolean
  end
end
