class DeliveryUserProject < ActiveRecord::Migration[7.1]
  def change
    add_column :delivery_methods, :project_id, :integer
  end
end
