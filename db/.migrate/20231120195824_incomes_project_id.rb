class IncomesProjectId < ActiveRecord::Migration[7.1]
  def change
    add_column :incomes, :project_id, :integer
  end
end
