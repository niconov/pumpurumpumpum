class Nm2 < ActiveRecord::Migration[7.1]
  def change
    remove_column :nms, :nm_id
    rename_column :nms, :nm_id_n, :nm_id
  end
end
