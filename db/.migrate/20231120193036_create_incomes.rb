class CreateIncomes < ActiveRecord::Migration[7.1]
  def change
    create_table :incomes do |t|
      t.integer :income_id
      t.string :number
      t.datetime :date
      t.datetime :last_change_date
      t.string :supplier_article
      t.string :tech_size
      t.string :barcode
      t.integer :quantity
      t.float :total_price
      t.datetime :date_close
      t.string :warehouse_name
      t.integer :nm_id
      t.string :status

      t.timestamps
    end
  end
end
