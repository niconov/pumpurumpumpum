class IncomePlannedHeadPeriod < ActiveRecord::Migration[7.1]
  def change
    remove_column :income_planned_heads, :period
    add_column :income_planned_heads, :period, :date, default: Time.now
  end
end
