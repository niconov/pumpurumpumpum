class CreateProjectUserSettings < ActiveRecord::Migration[7.1]
  def change
    create_table :project_user_settings do |t|
      t.boolean :notification_enabled
      t.integer :project_id
      t.integer :user_id

      t.timestamps
    end
  end
end
