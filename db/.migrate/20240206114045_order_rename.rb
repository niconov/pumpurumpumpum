class OrderRename < ActiveRecord::Migration[7.1]
  def change
    rename_table :orders, :order_orders
  end
end
