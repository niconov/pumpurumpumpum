class Sales2 < ActiveRecord::Migration[7.1]
  def change
    remove_index :sales, :uid
    add_index :sales, :uid, unique: true
  end
end
