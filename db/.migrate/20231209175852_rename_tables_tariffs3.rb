class RenameTablesTariffs3 < ActiveRecord::Migration[7.1]
  def change
    rename_table 'tariff_actives', 'tariffs_actives'
    rename_table 'tariff_lists', 'tariffs_lists'
    rename_table 'tariff_periodics', 'tariffs_periodics'
  end
end
