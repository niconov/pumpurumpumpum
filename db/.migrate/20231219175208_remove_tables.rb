class RemoveTables < ActiveRecord::Migration[7.1]
  def change
    drop_table :sales_stats_by_districts
  end
end
