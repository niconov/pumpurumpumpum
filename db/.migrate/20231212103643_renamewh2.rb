class Renamewh2 < ActiveRecord::Migration[7.1]
  def change
    rename_table :deliveries_warehouse_priorities, :warehouse_warehouse_priorities
  end
end
