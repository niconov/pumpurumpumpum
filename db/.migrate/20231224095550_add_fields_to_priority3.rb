class AddFieldsToPriority3 < ActiveRecord::Migration[7.1]
  def change
    remove_column :deliveries_supply_schemas, :count_local
    remove_column :deliveries_supply_schemas, :is_local
  end
end
