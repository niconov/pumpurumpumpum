class AdvertsAddRawResponse < ActiveRecord::Migration[7.1]
  def change
    add_column :adverts, :response_raw, :text
  end
end
