class Addsalests2 < ActiveRecord::Migration[7.1]
  def change
    rename_column :sale_reports, :realization_report_id, :realizationreport_id
  end
end

