class NmAdd < ActiveRecord::Migration[7.1]
  def change
    add_column :nms, :ros_min, :float
    add_column :nms, :roi_min, :float
  end
end
