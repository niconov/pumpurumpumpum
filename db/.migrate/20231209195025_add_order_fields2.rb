class AddOrderFields2 < ActiveRecord::Migration[7.1]
  def change
    add_column :orders, :price_with_disc, :float
  end
end
