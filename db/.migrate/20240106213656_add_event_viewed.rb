class AddEventViewed < ActiveRecord::Migration[7.1]
  def change
    add_column :events, :viewed, :boolean, default: false
  end
end
