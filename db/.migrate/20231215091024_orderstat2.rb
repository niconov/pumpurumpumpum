class Orderstat2 < ActiveRecord::Migration[7.1]
  def change
      change_column_default :order_stat_districts, :count_local, 0
	    change_column_default :order_stat_districts, :count, 0
  end
end
