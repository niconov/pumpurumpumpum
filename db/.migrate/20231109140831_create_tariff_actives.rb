class CreateTariffActives < ActiveRecord::Migration[7.1]
  def change
    # drop_table :tariff_actives
    create_table :tariff_actives do |t|
      t.integer :user_id
      t.integer :tariff_id
      t.boolean :active

      t.timestamps
    end
  end
end
