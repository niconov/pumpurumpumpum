class CreateAppGlobalConstants < ActiveRecord::Migration[7.1]
  def change
    create_table :app_global_constants do |t|
      t.string :name
      t.string :value
      t.string :value_type

      t.timestamps
    end
  end
end
