class CreateSaleReportLists < ActiveRecord::Migration[7.1]
  def change
    create_table :sale_report_lists do |t|
      t.integer :realizationreport_id
      t.timestamps
    end
  end
end
