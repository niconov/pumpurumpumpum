class RemoveDistrictsWarehosusers < ActiveRecord::Migration[7.1]
  def change
    drop_table :warehouses_by_district
  end
end
