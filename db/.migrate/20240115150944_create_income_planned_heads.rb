class CreateIncomePlannedHeads < ActiveRecord::Migration[7.1]
  def change
    create_table :income_planned_heads do |t|
      t.integer :warehouse_id
      t.time :period
      t.integer :project_id

      t.timestamps
    end
  end
end
