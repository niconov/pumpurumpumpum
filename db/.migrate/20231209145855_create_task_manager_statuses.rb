class CreateTaskManagerStatuses < ActiveRecord::Migration[7.1]
  def change
    create_table :task_manager_statuses do |t|
      t.string :title
      t.text :color

      t.timestamps
    end
  end
end
