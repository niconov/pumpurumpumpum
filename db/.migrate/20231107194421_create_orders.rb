class CreateOrders < ActiveRecord::Migration[7.1]
  def change
    if not ActiveRecord::Base.connection.table_exists?(:orders)

      create_table :orders do |t|
        t.datetime :datetime
        t.datetime :last_change_date
        t.string :article
        t.string :barcode
        t.float :price
        t.integer :discount
        t.string :region
        t.string :warehouse
        t.integer :comission_id
        t.integer :warehouse_id
        t.integer :region_id
        t.integer :income_id
        t.integer :user_id
        t.integer :nm_id
        t.boolean :canceled
        t.string :barcode
        t.string :gnumber
        t.string :orde_typer
        t.string :odid
        t.string :srid, unique: true


        t.timestamps
      end
      add_index :orders, :srid, unique: true
    end
  end
end
