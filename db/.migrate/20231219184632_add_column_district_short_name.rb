class AddColumnDistrictShortName < ActiveRecord::Migration[7.1]
  def change
    add_column :districts, :name_short, :string
  end
end
