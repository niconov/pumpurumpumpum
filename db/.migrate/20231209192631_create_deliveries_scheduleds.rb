class CreateDeliveriesScheduleds < ActiveRecord::Migration[7.1]
  def change
    create_table :deliveries_scheduleds do |t|
      t.datetime :period
      t.integer :warehouse_id

      t.timestamps
    end
  end
end
