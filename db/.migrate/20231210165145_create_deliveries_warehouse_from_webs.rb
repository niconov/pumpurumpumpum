class CreateDeliveriesWarehouseFromWebs < ActiveRecord::Migration[7.1]
  def change
    create_table :deliveries_warehouse_from_webs do |t|
      t.integer :warehouse_id
      t.string :name

      t.timestamps
    end
  end
end
