class AdvertAddBudgetToday2 < ActiveRecord::Migration[7.1]
  def change
    change_column_default :advert_adverts, :balance_payment_sum_on_period, 0
    change_column_default :advert_adverts, :balance_payment_period, Time.now
  end
end
