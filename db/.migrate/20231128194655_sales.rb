class Sales < ActiveRecord::Migration[7.1]
  def change
    add_column :sales, :uid, :string
    add_index :sales, :uid, unique: true
  end
end
