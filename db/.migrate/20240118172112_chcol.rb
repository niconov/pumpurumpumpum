class Chcol < ActiveRecord::Migration[7.1]
  def change
    change_column :stock_histories, :volume, :float
  end
end
