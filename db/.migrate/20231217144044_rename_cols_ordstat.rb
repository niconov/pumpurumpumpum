class RenameColsOrdstat < ActiveRecord::Migration[7.1]
  def change
    rename_column :order_stat_districts, :count, :count_total
  end
end
