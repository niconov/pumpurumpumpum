class CreateWarehouseMappings < ActiveRecord::Migration[7.1]
  def change
    create_table :warehouse_mappings do |t|
      t.string :name_mapped
      t.string :name

      t.timestamps
    end
  end
end
