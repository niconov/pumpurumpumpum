class CreateNmCustomerDiscounts < ActiveRecord::Migration[7.1]
  def change
    create_table :nm_customer_discounts do |t|
      t.integer :spp
      t.integer :category_id
      t.float :bound_left
      t.float :bound_right
      t.integer :nm_group_query_id

      t.timestamps
    end
  end
end
