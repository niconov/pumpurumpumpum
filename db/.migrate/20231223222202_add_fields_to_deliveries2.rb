class AddFieldsToDeliveries2 < ActiveRecord::Migration[7.1]
  def change
    add_column :deliveries_pickup_points, :district_id, :integer
  end
end
