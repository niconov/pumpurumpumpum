class AddFieldsToNmPosition < ActiveRecord::Migration[7.1]
  def change
    add_column :nm_positions, :price_fictive, :float
    add_column :nm_positions, :price, :float
    add_column :nm_positions, :sale, :float
  end
end
