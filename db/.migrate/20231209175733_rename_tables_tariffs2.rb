class RenameTablesTariffs2 < ActiveRecord::Migration[7.1]
  def change
    rename_table 'tariffs_tariff_actives', 'tariff_actives'
    rename_table 'tariffs_tariff_lists', 'tariff_lists'
    rename_table 'tariffs_tariff_periodics', 'tariff_periodics'
  end
end
