class StockHistoryAddFields < ActiveRecord::Migration[7.1]
  def change
    add_column :stock_histories, :project_id, :integer
  end
end
