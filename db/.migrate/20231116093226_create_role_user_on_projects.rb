class CreateRoleUserOnProjects < ActiveRecord::Migration[7.1]
  def change
    create_table :role_user_on_projects do |t|
      t.integer :user_id
      t.integer :project_id
      t.integer :project_role_id

      t.timestamps
    end
  end
end
