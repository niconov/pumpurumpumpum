class Renamewh9 < ActiveRecord::Migration[7.1]
  def change
    drop_table :tariff_localizations
    create_table :tariff_localizations do |t|
      t.float :index
      t.float :left
      t.float :right

      t.timestamps
    end
  end
end
