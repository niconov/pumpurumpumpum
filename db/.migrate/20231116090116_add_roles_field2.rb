class AddRolesField2 < ActiveRecord::Migration[7.1]
  def change
    add_column :project_roles, :changable, :boolean
  end
end
