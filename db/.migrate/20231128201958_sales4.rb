class Sales4 < ActiveRecord::Migration[7.1]
  def change
    change_column :sales, :realizationreport_id, :bigint, :limit => 8
    change_column :sales, :rrd_id, :bigint
    change_column :sales, :gi_id, :bigint
    change_column :sales, :shk_id, :bigint
    change_column :sales, :gi_id, :bigint

    change_column :sales, :realizationreport_id, :bigint
    change_column :sales, :rrd_id, :bigint
    change_column :sales, :gi_id, :bigint
    change_column :sales, :nm_id, :bigint
    change_column :sales, :quantity, :bigint
    change_column :sales, :retail_price, :bigint
    change_column :sales, :retail_amount, :bigint
    change_column :sales, :sale_percent, :bigint
    change_column :sales, :shk_id, :bigint
    change_column :sales, :delivery_amount, :bigint
    change_column :sales, :return_amount, :bigint
    change_column :sales, :rid, :bigint
    change_column :sales, :ppvz_office_id, :bigint
    change_column :sales, :ppvz_supplier_id, :bigint


  end
end
