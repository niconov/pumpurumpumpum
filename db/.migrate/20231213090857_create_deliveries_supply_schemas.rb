class CreateDeliveriesSupplySchemas < ActiveRecord::Migration[7.1]
  def change
    create_table :deliveries_supply_schemas do |t|
      t.integer :nm_id
      t.integer :project_id
      t.integer :count
      t.integer :warehouse_id

      t.timestamps
    end
  end
end
