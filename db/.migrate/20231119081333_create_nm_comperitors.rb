class CreateNmComperitors < ActiveRecord::Migration[7.1]
  def change
    create_table :nm_comperitors do |t|
      t.integer :nm_group_competitor_id

      t.timestamps
    end
  end
end
