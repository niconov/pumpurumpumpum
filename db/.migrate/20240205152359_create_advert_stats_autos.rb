class CreateAdvertStatsAutos < ActiveRecord::Migration[7.1]
  def change
    create_table :advert_stats_autos do |t|
      t.datetime :period
      t.string :keyword
      t.integer :advert_id
      t.integer :viws
      t.integer :clicks
      t.float :ctr
      t.decimal :sum

      t.timestamps
    end
  end
end
