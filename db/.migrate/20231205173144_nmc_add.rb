class NmcAdd < ActiveRecord::Migration[7.1]
  def change
    add_column :nm_competitors, :price_rrc, :float
    add_column :nm_competitors, :price, :float
    add_column :nm_competitors, :price_with_spp, :float
  end
end
