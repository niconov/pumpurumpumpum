class AddSortingLevelDistricts < ActiveRecord::Migration[7.1]
  def change
    add_column :districts, :order_level, :integer, default: 0
  end
end
