class AdvertAdd < ActiveRecord::Migration[7.1]
  def change
    add_column :advert_adverts, :manager_enabled, :boolean, default: false
  end
end
