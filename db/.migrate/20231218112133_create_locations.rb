class CreateLocations < ActiveRecord::Migration[7.1]
  def change
    create_table :locations do |t|
      t.string :country
      t.string :district_name
      t.string :district_fullname
      t.string :region

      t.timestamps
    end
  end
end
