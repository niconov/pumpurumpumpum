class CreateNmCompetitorConnectors < ActiveRecord::Migration[7.1]
  def change
    create_table :nm_competitor_connectors do |t|
      t.integer :nm_id
      t.integer :nm_competitor_id

      t.timestamps
    end
  end
end
