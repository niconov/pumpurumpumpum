class AddUsersData < ActiveRecord::Migration[7.1]
  def change
    add_column :users, :chat_id, :integer if not ActiveRecord::Base.connection.column_exists? :users, :chat_id
    add_column :users, :token, :string if not ActiveRecord::Base.connection.column_exists? :users, :token
    add_column :users, :bidder_enabled, :boolean if not ActiveRecord::Base.connection.column_exists? :users, :bidder_enabled
    add_column :users, :autoresponder_enabled, :boolean if not ActiveRecord::Base.connection.column_exists? :users, :autoresponder_enabled
    
  end
end
