class OrdersAddReg < ActiveRecord::Migration[7.1]
  def change
    add_column :orders, :country, :string
    add_column :orders, :district, :string
  end
end
