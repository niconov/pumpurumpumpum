class AddFields < ActiveRecord::Migration[7.1]
  def change
    add_column :deliveries_warehouse_from_webs, :belongs_wb, :boolean, default: false
  end
end
