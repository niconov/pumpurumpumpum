class Orders < ActiveRecord::Migration[7.1]
  def change
    add_column :orders, :cancel_date, :datetime
    add_column :orders, :tech_size, :string
    add_column :orders, :subject, :string
    add_column :orders, :district_id, :integer
    add_index  :orders, :srid, unique: true

  end
end
