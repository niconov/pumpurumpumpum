class CreateNmGroupCompetitors < ActiveRecord::Migration[7.1]
  def change
    create_table :nm_group_competitors do |t|
      t.integer :nm_id
      t.string :name

      t.timestamps
    end
  end
end
