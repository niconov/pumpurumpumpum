class Deffields < ActiveRecord::Migration[7.1]
  def change
    change_column :projects_user_settings, :notification_enabled, :boolean, :default => true
  end
end
