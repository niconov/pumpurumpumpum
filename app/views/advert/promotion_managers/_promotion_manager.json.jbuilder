json.extract! promotion_manager, :id, :nm_id, :advert_id, :budget_daily, :enabled, :created_at, :updated_at
json.url promotion_manager_url(promotion_manager, format: :json)
