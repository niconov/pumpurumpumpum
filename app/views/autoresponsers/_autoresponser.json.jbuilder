json.extract! autoresponser, :id, :text, :rating, :project_id, :created_at, :updated_at
json.url autoresponser_url(autoresponser, format: :json)
