json.extract! project_role, :id, :user_id, :user_role, :created_at, :updated_at
json.url project_role_url(project_role, format: :json)
