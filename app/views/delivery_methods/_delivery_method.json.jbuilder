json.extract! delivery_method, :id, :period, :name, :tariff_id, :price, :created_at, :updated_at
json.url delivery_method_url(delivery_method, format: :json)
