json.extract! deliveries_scheduled, :id, :period, :warehouse_id, :created_at, :updated_at
json.url deliveries_scheduled_url(deliveries_scheduled, format: :json)
