json.extract! deliveries_scheduled_item, :id, :nm_id, :qty, :deliveries_sheduled_id, :created_at, :updated_at
json.url deliveries_scheduled_item_url(deliveries_scheduled_item, format: :json)
