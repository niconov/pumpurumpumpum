json.extract! app_global_constant, :id, :name, :value, :value_type, :created_at, :updated_at
json.url app_global_constant_url(app_global_constant, format: :json)
