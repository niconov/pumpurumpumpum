json.extract! sales_operation, :id, :name, :factor, :created_at, :updated_at
json.url sales_operation_url(sales_operation, format: :json)
