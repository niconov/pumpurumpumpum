json.extract! event, :id, :subject, :text, :user_id, :date, :created_at, :updated_at
json.url event_url(event, format: :json)
