json.extract! sale, :id, :date_from, :created_at, :updated_at
json.url sale_url(sale, format: :json)
