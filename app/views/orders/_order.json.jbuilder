json.extract! order, :id, :datetime, :last_change_date, :article, :barcode, :price, :discount, :region, :warehouse, :comission_id, :warehouse_id, :region_id, :income_id, :user_id, :nm_id, :canceled, :barcode, :gnumber, :orde_typer, :odid, :srid, :srid, :created_at, :updated_at
json.url order_url(order, format: :json)
