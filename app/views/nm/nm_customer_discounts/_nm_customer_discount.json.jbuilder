json.extract! nm_customer_discount, :id, :spp, :category_id, :bound_left, :bound_right, :nm_group_query_id, :created_at, :updated_at
json.url nm_customer_discount_url(nm_customer_discount, format: :json)
