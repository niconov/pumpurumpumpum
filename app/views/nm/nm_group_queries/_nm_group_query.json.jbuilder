json.extract! nm_group_query, :id, :project_id, :name, :query, :created_at, :updated_at
json.url nm_group_query_url(nm_group_query, format: :json)
