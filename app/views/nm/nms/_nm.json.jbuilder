json.extract! nm, :id, :nm_id, :prohibited, :supplier_article, :barcode, :imt_id, :user_id, :created_at, :updated_at
json.url nm_url(nm, format: :json)
