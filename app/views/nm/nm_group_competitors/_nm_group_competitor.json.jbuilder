json.extract! nm_group_competitor, :id, :nm_id, :name, :created_at, :updated_at
json.url nm_group_competitor_url(nm_group_competitor, format: :json)
