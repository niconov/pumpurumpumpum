json.extract! task_manager_status, :id, :title, :color, :created_at, :updated_at
json.url task_manager_status_url(task_manager_status, format: :json)
