json.extract! task_manager_category, :id, :title, :project_id, :created_at, :updated_at
json.url task_manager_category_url(task_manager_category, format: :json)
