json.extract! task_manager_task, :id, :text, :title, :category_id, :project_id, :status_id, :created_at, :updated_at
json.url task_manager_task_url(task_manager_task, format: :json)
