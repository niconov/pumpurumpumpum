class WbUpdateNmStatJob
  include Sidekiq::Job
  sidekiq_options retry: false,
                  lock: :until_and_while_executing,
                  on_conflict: { client: :log, server: :reject }, 
                  timeout: 1800

  def perform(project_id, *args)
    # if PPJobController.can_be_started self.class.to_s, project_id
    #   sleep 10
    project = Project::Project.find(project_id)
    ts = Time.now.beginning_of_day
    7.times do  
      Nm::Stat.fetch project, ts
      ts = ts - 1.day
    end
  end
end
