require_relative "../lib/wb_api/wb_api.rb"

class WbUpdatePricesJob
  include Sidekiq::Job
  sidekiq_options retry: false,
                  lock: :until_and_while_executing,
                  on_conflict: { client: :log, server: :reject }, 
                  timeout: 1800

  def perform(user, *args)
    project = Project::Project.find(user)
    Price.update_from_api project
    # api = WBApi.new project
    # api.prices_update
    Nm::Nm.where(project_id: project.id).all.map {|u| u.spp_update; u.save}
  end
end
