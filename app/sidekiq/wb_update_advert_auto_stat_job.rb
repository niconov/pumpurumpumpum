class WbUpdateAdvertAutoStatJob
  include Sidekiq::Job
  sidekiq_options retry: false,
                  lock: :until_and_while_executing,
                  on_conflict: { client: :log, server: :reject }, 
                  timeout: 1800

  def perform(project, *args)
    # Do something
    project = Project::Project.find project
    Advert::StatsAuto.fetch project
  end
  
end
