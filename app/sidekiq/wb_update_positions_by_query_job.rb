require_relative "../lib/wb_api/wb_api.rb"

class WbUpdatePositionsByQueryJob
  include Sidekiq::Job
  sidekiq_options retry: false,
                  lock: :until_and_while_executing,
                  on_conflict: { client: :log, server: :reject }, 
                  timeout: 1800

  def perform(project_id, *args)
    project = Project::Project.find(project_id)
    api = WBApi.new project
    api.positions_by_queries_update
    Nm::Nm.where(project_id: project.id).all.map {|u| u.spp_update; u.save}
  end
end
