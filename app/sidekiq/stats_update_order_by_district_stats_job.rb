class StatsUpdateOrderByDistrictStatsJob
  include Sidekiq::Job
  sidekiq_options retry: false,
                  lock: :until_and_while_executing,
                  on_conflict: { client: :log, server: :reject }, 
                  timeout: 1800
                  
  def perform(project_id)
    project = Project::Project.find_by project_id
    Order::DistrictStats.build(project)
  end

end
