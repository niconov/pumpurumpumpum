require_relative "../lib/wb_api/wb_api.rb"
require_relative "../lib/job_controller/main.rb"

class WbUpdateOrdersJob
  include Sidekiq::Job
  sidekiq_options retry: false,
                  lock: :until_and_while_executing,
                  on_conflict: { client: :log, server: :reject }, 
                  timeout: 1800

  def perform(project_id, *args)
    # if PPJobController.can_be_started self.class.to_s, project_id
    #   sleep 10
      project = Project::Project.find(project_id)
      Order::Order.fetch project
      Order::Stat.update_list project
    # end
  end
end
