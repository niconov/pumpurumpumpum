require_relative "../lib/wb_api/wb_api.rb"

class WbAutoresponderJob
  include Sidekiq::Job
  sidekiq_options retry: false,
                  lock: :until_and_while_executing,
                  on_conflict: { client: :log, server: :reject }, 
                  timeout: 1800

  def perform(user, *args)
    api = WBApi.new Project::Project.find(user)
    api.autoresponder
  end

end

