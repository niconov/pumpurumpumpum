class   QueryCleanerJob
  include Sidekiq::Job
  sidekiq_options retry: false,
                  lock: :until_and_while_executing,
                  on_conflict: { client: :log, server: :reject }, 
                  timeout: 1800

  def perform(*args)
    # наброски на жесткую очистку. Как вариант, есть middleware sq uniq jobs
    # Sidekiq::Workers.new.each do |_process_id, _thread_id, work|
    #   # ap work
    #   payload = JSON.parse work['payload']
    #   # ap payload
    #   ts = Time.now.to_f - payload['enqueued_at']
    #   if ts > 900
    #     ap _thread_id
    #  kill?!
    #   end
    # end ;;;

    ap 'Очистка очереди'
    Sidekiq::RetrySet.new.clear
    template_names = ScheduleTemplate.where(only_one: false).select(:job_name).pluck(:job_name)
    items = []
    q = Sidekiq::Queue.new("default")
    q.map do |i| 
      i_h = {name: i.item['class'], args: i.item['args']}
      items.include?(i_h) ? i.delete : items.push(i_h)
    end 
    # items = []
    # items_all = []
    # Sidekiq::Workers.new.each do |_process_id, _thread_id, work|
    #   items_all.push work
    #   i = JSON.parse work['payload']
    #   i = {class: i['class'], args: i['args'] }
    #   if items.include?(i)  
    #     ap i
    #     Thread.list.each do |t|
    #       # ap "search tid #{_thread_id}"
    #       # ap "t.object_id #{t.object_id}"
    #       # ap "t.object_id.to_s(36) #{t.object_id.to_s(36)}"
    #       if t.object_id.to_s(36) == _thread_id
    #         t.exit
    #       end
    #     end
    #   else
    #     items.push(i)
    #   end
    # end
    ;;
      

    # Sidekiq::ScheduledSet.new.clear
  end
end

