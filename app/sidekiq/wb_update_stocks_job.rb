require_relative "../lib/wb_api/wb_api.rb"

class WbUpdateStocksJob
  include Sidekiq::Job
  sidekiq_options retry: false,
                  lock: :until_and_while_executing,
                  on_conflict: { client: :log, server: :reject }, 
                  timeout: 1800

  def perform(project, *args)
    api = WBApi.new Project::Project.find(project)
    api.stocks_update
  end
end

