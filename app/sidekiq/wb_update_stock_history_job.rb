class WbUpdateStockHistoryJob
  include Sidekiq::Job
  sidekiq_options retry: false,
                  lock: :until_and_while_executing,
                  on_conflict: { client: :log, server: :reject }, 
                  timeout: 1800

  def perform(project, *args)
    project = Project::Project.find(project) if project.class == Integer
    Stock::History.fetch project, Time.now.beginning_of_day - 1.day, 2
    Stock::History.set_types_incomings
  end
end