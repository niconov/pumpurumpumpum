class OrderStatsWeeklyUpdateJob
  include Sidekiq::Job
  sidekiq_options retry: false, timeout: 1800

  def perform(*args)
    ts = Time.now.beginning_of_day
    days = 7

    has_orders = true
    day = 0
    while has_orders 
      ts_s = ts - (day + days).days
      ts_e = ts - day.days
      where = "datetime between '#{ts_s}' and '#{ts_e}'"
      sel =  "count(distinct srid) as qty, nm_id, "
      sel += "sum( price * (100 - discount) / 100 ) as sum, "
      sel += "avg( price ) as price, "
      sel += "avg( discount ) as discount "
      grouping = [:nm_id]
      # orders = Order::Order.where(where).select(sel).group(grouping).all
      orders = Order::Order.where(where).select(sel).group(grouping).all
      # orders = orders.sort_by {|i| i.datetime}
      # ap "=" *100
      # ap orders.first
      # ap orders.last
      # ap "=" *100
      nmids = []
      orders.map do |i|
        osw = OrdersStatsWeekly.find_or_create_by(nm_id: i.nm_id, period_start: ts_s, period_end: ts_e)
        osw.sum = i.sum
        osw.qty = i.qty
        osw.price = i.price
        osw.discount = i.discount
        # osw.project_id = 
        osw.save
        nmids.push i.nm_id
      end
      nmids.uniq!
      has_orders = Order::Order.where("datetime < '#{ts_e}'").any?
      print "Update Orders Stats. Day #{day}. Between '#{ts_s}' and '#{ts_e}'. Has orders: #{has_orders} \n"
      ap nmids
      day += 1
    end

  end
end
