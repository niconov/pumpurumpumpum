require_relative "../lib/wb_api/wb_api.rb"

class WbUpdateWarehousePriorityJob
  include Sidekiq::Job
  sidekiq_options retry: false,
                  lock: :until_and_while_executing,
                  on_conflict: { client: :log, server: :reject }, 
                  timeout: 1800

  def perform(*args)
    api = WBApi.new nil
    api.update_delivery_priorities
  end

end
