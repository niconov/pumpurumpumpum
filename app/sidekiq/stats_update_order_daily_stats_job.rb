class StatsUpdateOrderDailyStatsJob
  include Sidekiq::Job
  sidekiq_options retry: false,
                  lock: :until_and_while_executing,
                  on_conflict: { client: :log, server: :reject }, 
                  timeout: 1800
                  
  def perform(project_id)
    project = Project::Project.find_by project_id
    Order::DailyStats.build(project)
  end
  
end
