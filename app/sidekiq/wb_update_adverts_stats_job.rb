require_relative "../lib/wb_api/wb_api.rb"

class WbUpdateAdvertsStatsJob
  include Sidekiq::Job
  sidekiq_options retry: false,
                  lock: :until_and_while_executing,
                  on_conflict: { client: :log, server: :reject }, 
                  timeout: 1800

  def perform(project, *args)
    project = Project::Project.find(project)
    Advert::Stat.update_from_api project
  end
end
