class WbTariffUpdateJob
  include Sidekiq::Job
  sidekiq_options retry: false,
                  lock: :until_and_while_executing,
                  on_conflict: { client: :log, server: :reject }, 
                  timeout: 1800

  def perform(*args)
    
    Tariff::Tariff.fetch
  end
end
