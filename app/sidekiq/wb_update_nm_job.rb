require_relative "../lib/wb_api/wb_api.rb"

class WbUpdateNmJob
  include Sidekiq::Job
  sidekiq_options retry: false,
                  lock: :until_and_while_executing,
                  on_conflict: { client: :log, server: :reject }, 
                  timeout: 1800

  def perform(project, *args)
    Nm::Nm.fetch Project::Project.find(project)
  end
end

