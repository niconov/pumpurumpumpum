class WbUpdateSaleReportJob
  include Sidekiq::Job
  sidekiq_options retry: false,
                  lock: :until_and_while_executing,
                  on_conflict: { client: :log, server: :reject }, 
                  timeout: 1800

  def perform(project_id, *args)
    project = Project::Project.find project_id
    Sale::Report.fetch project
  end
end
