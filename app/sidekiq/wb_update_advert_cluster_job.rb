class WbUpdateAdvertClusterJob
  include Sidekiq::Job
  sidekiq_options retry: false,
                  lock: :until_and_while_executing,
                  on_conflict: { client: :log, server: :reject }, 
                  timeout: 1800

  def perform(project, *args)
    project = Project::Project.find project
    Advert::Cluster.fetch project
  end
  
end
