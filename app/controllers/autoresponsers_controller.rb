class AutoresponsersController < ApplicationController
  before_action :set_autoresponser, only: %i[ show edit update destroy ]

  # GET /autoresponsers or /autoresponsers.json
  def index
    @autoresponsers = Autoresponser.where(project_id:current_user.project_id).order(:rating).all
  end

  # GET /autoresponsers/1 or /autoresponsers/1.json
  def show
  end

  # GET /autoresponsers/new
  def new
    set_availiable_ratings
    @autoresponser = Autoresponser.new
  end

  # GET /autoresponsers/1/edit
  def edit
    set_availiable_ratings
    # ratings.map {|r| if }
  end

  # POST /autoresponsers or /autoresponsers.json
  def create
    @autoresponser = Autoresponser.new(autoresponser_params)
    @autoresponser.project_id = current_user.project_id

    respond_to do |format|
      if @autoresponser.save
        format.html { redirect_to autoresponser_url(@autoresponser), notice: "Autoresponser was successfully created." }
        format.json { render :show, status: :created, location: @autoresponser }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @autoresponser.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /autoresponsers/1 or /autoresponsers/1.json
  def update
    respond_to do |format|
      if @autoresponser.update(autoresponser_params)
        format.html { redirect_to autoresponser_url(@autoresponser), notice: "Autoresponser was successfully updated." }
        format.json { render :show, status: :ok, location: @autoresponser }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @autoresponser.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /autoresponsers/1 or /autoresponsers/1.json
  def destroy
    @autoresponser.destroy!

    respond_to do |format|
      format.html { redirect_to autoresponsers_url, notice: "Autoresponser was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_autoresponser
      @autoresponser = Autoresponser.find(params[:id])
      redirect_to '/' if @autoresponser.project_id != current_user.project_id
    end

    # Only allow a list of trusted parameters through.
    def autoresponser_params
      params.require(:autoresponser).permit(:text, :rating, :project_id)
    end

    def set_availiable_ratings
      ratings_exist = Autoresponser.where(project_id: current_user.project_id).pluck(:rating)
      ap ratings_exist
      @ratings_available = []
      Autoresponser.ratings_list.map do |r|
        if @autoresponser and @autoresponser.rating 
          cur_rating = r
        else
          cur_rating = nil
        end
        if not ratings_exist.include? r or cur_rating == r
          @ratings_available.push r
        end
      end
    end

end
