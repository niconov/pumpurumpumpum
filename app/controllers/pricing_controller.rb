class PricingController < ApplicationController
	before_action :check_permissions


	def index

		@result = Nm::Nm.where(project_id: current_user.project_id).order(:object, :supplier_article)
		# pre = {}
		# Stock::Stock.where(project_id: current_user.project_id).map do |s|
		# 	if s.quantity > 0
		# 		nm = s.nm
		# 		t = s.get_tariff_current
		# 		pre[nm.nm_id] = {quantity: 0.0, profit: 0.0, profit_pre_pcs: 0.0, nm: nm, roi: 0.0, ros: 0.0} if not pre[nm.nm_id]
		# 		profit = nm.profit_without_logistics - t.calc_by_nm(nm)
		# 		pre[nm.nm_id][:profit] = pre[nm.nm_id][:profit] + profit * s.quantity
		# 		pre[nm.nm_id][:quantity] = pre[nm.nm_id][:quantity] + s.quantity
		# 		pre[nm.nm_id][:profit_per_pcs] = pre[nm.nm_id][:profit] / pre[nm.nm_id][:quantity]
		# 		pre[nm.nm_id][:price] = nm.price_total
		# 		pre[nm.nm_id][:cost] = nm.cost_total
		# 		pre[nm.nm_id][:roi] = 100 * pre[nm.nm_id][:profit] / (pre[nm.nm_id][:cost] * pre[nm.nm_id][:quantity])
		# 		pre[nm.nm_id][:ros] = 100 * pre[nm.nm_id][:profit] / (pre[nm.nm_id][:price] * pre[nm.nm_id][:quantity])
		# 		pre[nm.nm_id][:text_class] = pre[nm.nm_id][:profit_per_pcs] >= 0 ? "text-success" : "text-error"
		# 	end
		# end


		# @result = @result.sort_by {|i| [i['nm'].object, i['nm'].supplier_article]}
		# @result = @result.map do |r|
		# 	nm_id = r['nm'].nm_id
		# 	if pre[nm_id]
		# 		r['roi_by_stock'] = pre[nm_id][:roi].round(2)
		# 		r['ros_by_stock'] = pre[nm_id][:ros].round(2)
		# 		r['profit_by_stock'] = pre[nm_id][:profit_per_pcs].round(2)
		# 		r['cost_by_stock'] = pre[nm_id][:cost].round(2)
		# 	end
		# 	r
		# end
		
	end

	def warehouses
		@nms = Nm::Nm.where(project_id: current_user.project_id).order(:object, :supplier_article)
		@tariffs = Tariff::Active.where(project_id: current_user.project_id).all

	end

	def update_prices_all
		Nm::Nm.find_by( project_id: current_user.project_id, nm_id: params['nm_id']).map do |nm|
			nm.price_set_recommended current_user.project
		end
		Price.update_from_api current_user.project
	end

	def update_price_item
		nm = Nm::Nm.find_by project_id: current_user.project_id, nm_id: params['nm_id']
		if nm
			nm.price_set_recommended current_user.project
			Price.update_from_api current_user.project
			redirect_back fallback_location: pricings_path
		end	
	end

	def graph_prices_vs_sales
		@endpoint = graph_prices_vs_sales_data_path nm_id: params['nm_id']
		@nm = Nm::Nm.find_by nm_id: params['nm_id']
		# ts = Time.now
		# days = 5
		# nm_id = params['nm_id']
		# w = "nm_id = '#{nm_id}' and last_change_date between '#{ts - days.days}' and '#{ts}' "
		# s = "DATE_TRUNC('day', last_change_date) as date, count(distinct id) as sales, avg((price * (100 - discount)/100)) as price"
		# g = "DATE_TRUNC('hour', last_change_date)"
		# ap w 
		# ap s 
		# ap g
		# salesdata = Order::Order.where(w).select(s).group(g).all
		# salesdata.map do |i| ap i end
	end

	def graph_prices_vs_sales_data
		ts = Time.now
		days = 5
		nm_id = params['nm_id']
		period = 'hour'
		w = "nm_id = '#{nm_id}' and last_change_date between '#{ts - days.days}' and '#{ts}' "
		s = "DATE_TRUNC('#{period}', last_change_date) as date, count(distinct id) as sales, avg((price * (100 - discount)/100)) as price"
		g = "DATE_TRUNC('#{period}', last_change_date)"
		salesdata = Order::Order.where(w).select(s).group(g).all.to_json
  	render json: salesdata
	end

	private

	def check_permissions
		redirect_to '/' if current_user.id != 1 
	end
end