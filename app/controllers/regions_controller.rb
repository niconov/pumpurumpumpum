class RegionsController < ApplicationController

	def regions
		@regions = Region.order(country: :asc, district: :asc, region: :asc).all
	end

	def districts
		@districts = District.order(name: :asc).all
	end

end