class Nm::NmsController < ApplicationController
  before_action :authenticate_user!
  before_action :check_permissions_profit, only: [:show, :edit, :update]

  before_action :set_nm, only: %i[ show edit destroy ]



  # GET /nms or /nms.json
  def index
    @nms = Nm::Nm.where(project_id: current_user.project_id).order(object: :asc, supplier_article: :asc).all
  end

  # GET /nms/1 or /nms/1.json
  def show
    project = current_user.project
    @nm = Nm::Nm.find_by nm_id: params[:id]
    @adstats = Advert::Stat.where(project_id: current_user.project_id, nm_id: @nm.nm_id).where.not(sum: 0.0).order(period: :desc)
    @advert_total = (@adstats.inject(0.0) { |mem, var| mem += (var.sum || 0.0) })
    @nm_competitors = @nm.nm_competitor.order(price: :asc)
    wh = { project_id: current_user.project_id, nm_id: @nm.nm_id, canceled: false }
    sel = "nm_id, date_trunc('day', datetime) as period, count(id) as cnt, sum(price * (100 - discount)/100  ) as sum"
    gr = "period, nm_id"
    @orders = Order::Order.where(wh).select(sel).group(gr).order(:period).last(30)
    @orders = @orders.reverse

    days = 365 * 2
    @days = days
    wh = "nm_id = #{@nm.nm_id} and datetime > '#{Time.now.beginning_of_day - days*24*3600}'"
    sel = "count(id), date_trunc('day', datetime) as period"
    @chart_data = Order::Order.select(sel).where(wh).group(gr).group_by {|i| i.period}
    @chart_data.keys.map do |k|
      k_new = k.strftime "%Y-%m-%d" 
      @chart_data[k_new] = @chart_data[k][0].count
      @chart_data.delete k
    end 
    sel = "count(id) as count, date_trunc('day', datetime) as period"
    wh = "project_id = #{project.id} and datetime > '#{Time.now.beginning_of_day - days*24*3600}'"
    nm_ids = Nm::Nm.select(:nm_id).where(project_id: project.id, obj_id: @nm.obj_id).distinct.pluck(:nm_id)
    @chart_data_gl = Order::Order.select(sel).where(wh).where(nm_id: nm_ids).group(gr).group_by {|i| i.period}
    @chart_data_gl.keys.map do |k|
      k_new = k.strftime "%Y-%m-%d" 
      cnt = @chart_data_gl[k].count
      @chart_data_gl[k_new] = (@chart_data_gl[k].inject(0.0) {|s, i| s+= i.count}) 
      @chart_data_gl.delete k
    end 

    # sel = "avg(finished_price) as price, date_trunc('day', datetime) as period"
    # @chart_data_pr = Order::Order.select(sel).where(wh).where(nm_id: @nm.nm_id).group(gr).group_by {|i| i.period}
    # @chart_data_pr.keys.map do |k|
    #   k_new = k.strftime "%Y-%m-%d" 
    #   cnt = @chart_data_pr[k].count
    #   sum = ((@chart_data_pr[k].inject(0.0) {|s, i| s+= i.price}) )
    #   if sum
    #     # ap sum
    #     @chart_data_pr[k_new] = (sum / cnt).round(2)
    #   else
    #     @chart_data_pr[k_new] = 0
    #   end
    #   @chart_data_pr.delete k
    # end 

    @dataset_sum = [
      {
        dataset: {yAxisID: 'a'}, name: "#{@nm.supplier_article.strip}", data: @chart_data
      }, 
      {
        dataset: {yAxisID: 'b'}, name: 'По категории', data: @chart_data_gl
      }, 
      # {
      #   dataset: {yAxisID: 'c'}, name: 'Цена с СПП', data: @chart_data_pr
      # }
    ]
    @graph_settings = {
        elements: {
          point: {
            radius: 2,
            pointHoverRadius: 5
          }
        },
        interaction: {
          mode: 'nearest',
          axis: 'x',
          intersect: false
        },
        plugins: {
          tooltip: {
            displayColors: true,
            boxPadding: 10
          },
          legend: {
            position: 'bottom',
            align: 'start'
          }
        },
        scales: {
                y: {
                    grid: {display: false},
                    ticks: {
                      display: false,
                      padding: 0,
                      backdropColor: "rgba(0,0,0,0)"

                    },
                    position: 'right',
                },
                a: {
                    type: 'linear',
                    position: 'left',
                    grid: {display: false},


                }, 
                b: {
                    type: 'linear',
                    position: 'left',
                    grid: {display: false},

                }, 
                c: {
                    type: 'linear',
                    position: 'right',
                    grid: {display: false},

                }
        }
      }

    # @chart_data.map { |e| ap e  }
    # sel = "count(distinct srid), sum(price * (1-(discount/100)), TO_CHAR(last_change_date, 'YYYY-MM-DD')"
    # gr = ''
    # @orders = Order::Order.where(nm_id: @nm.nm_id, project_id: current_user.project_id).select(sel).group(:last_change_date)
    # ap @orders
    # @nmccs = @nm.nm_competitor.order(price: :desc).all
    # @nmccs = @nmccs.sort_by {|i| i.price; ap i.price}
    # @nm.nm_competitor.map { |e| ap e.as_json  }
    # map { |e| e.nm_group_query  }

  end

  def competitor_action
    data = params['nm_competitor_connector']
    # ap data
    # ap data['nm_id']
    # ap data['nm_competitor_id']
    # ap data['todo']
    # ap params
    # ap Nm::Nm.find_by nm_id: params['nm_competitor_connector']['nm_id'].to_i
    nm = Nm::Nm.find_by nm_id: data['nm_id']
    # ap nm
    # ap current_user.project_id == nm.project_id

    if nm and current_user and current_user.project_id == nm.project_id
      # redirect_to '/' if 
      case data['todo']
      when 'add'
        nmc = NmCompetitor.find_by nm_id: data[:nm_competitor_id]
        if nmc
          NmCompetitorConnector.find_or_create_by nm_id: nm.nm_id, nm_competitor_id: nmc.id
        else
          ap 'nm not found'
          ap data
          nm.nm_group_query_item.map do |ngq|
            ngq.nm_group_query 
            opts = {
              query_id: ngq.nm_group_query.id, 
              nm_id: data['nm_competitor_id']
            }
            nc = NmCompetitor.new opts 
            nc.save
            ncc = NmCompetitorConnector.new nm_id: nm.nm_id, nm_competitor_id: nc.id
            ncc.save
            # data['nm_competitor_id']
          end
        end

      when 'delete'
        ap data
        ap data['nmc_id']
        ap "--------------"
        nmc = NmCompetitorConnector.find_by nm_competitor_id: data['nmc_id']
        ap nmc
        nmc.destroy
      else
        ap 'undefined action'
        ap data
        ap data['todo']
        
      end
      # 1/0
      redirect_back fallback_location: nm_nm_path(id: nm.nm_id)

      # ap @nm
      # NmCompetitor.find_or_create_by nm_id: params['nm_competitor_id']
      # NmCompetitorConnector.find_or_create_by @nm
    else
      ap 'AAAAAAAAAAA'
      redirect_back fallback_location: nm_nm_path(id: nm.nm_id)
    end
      
  end

  # GET /nms/new
  def new
    @nm = Nm::Nm.new
  end

  # GET /nms/1/edit
  def edit
  end

  # POST /nms or /nms.json
  def create
    @nm = Nm::Nm.new(nm_params)

    respond_to do |format|
      if @nm.save
        @nm.update_log_out_price
        Turbo::StreamsChannel.broadcast_append_to "nm_nm_#{@nm.nm_id}", target: "nm_nm_#{@nm.nm_id}", partial: 'pricing/item', locals: { nm: @nm }
        format.html { redirect_to nm_url(@nm), notice: "Nm was successfully created." }
        # format.turbo_stream { render turbo_stream: turbo_stream.replace("pricing_nm#{@nm.nm_id}", partial: 'pricing/item', locals: {nm: @nm}) }

        format.json { render :show, status: :created, location: @nm }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @nm.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /nms/1 or /nms/1.json
  def update
    @nm = Nm::Nm.find_by(nm_id: nm_params[:nm_id], project_id: current_user.project_id)
    # ap nm_params
    respond_to do |format|
      params =  nm_params.to_hash
      params["unit_per_pallet"] = nm_params[:unit_per_box].to_i * nm_params[:box_per_pallet].to_i
      params["pallet_mass"] = (params["unit_per_pallet"].to_f * nm_params[:unit_mass].to_f) + 20
      if @nm.update(params)
        @nm.update_log_out_price
        format.turbo_stream do
          render turbo_stream: turbo_stream.replace(@nm, partial: 'pricing/item', locals: {nm: @nm}) 
        end
        format.html { redirect_back fallback_location: dashboard_logistics_planning_path}
        # format.html { redirect_to dashboard_logistics_planning_path, notice: "Nm was successfully updated." }
        # format.html { redirect_to nm_url(@nm.id), notice: "Nm was successfully updated." }
        # format.json { render :show, status: :ok, location: @nm }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @nm.errors, status: :unprocessable_entity }
      end
    end
  end

  def update_price
    @nm = Nm::Nm.find_by(nm_id: nm_params[:nm_id], project_id: current_user.project_id)
    respond_to do |format|
      params =  nm_params.to_hash
      if @nm.update(params)
        format.turbo_stream do
          render turbo_stream: turbo_stream.replace(@nm, partial: 'pricing/item', locals: {nm: @nm}) 
        end
      end
    end
    
  end

  # DELETE /nms/1 or /nms/1.json
  def destroy
    @nm.destroy!

    respond_to do |format|
      format.html { redirect_to nms_url, notice: "Nm was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
  
  # Use callbacks to share common setup or constraints between actions.
  def set_nm
    @nm = Nm::Nm.find_by nm_id: params[:id]
    if current_user.project_id.to_i != @nm.project_id.to_i
      redirect_back fallback_location: dashboard_logistics_planning_path
    end
  end

  # Only allow a list of trusted parameters through.
  def nm_params
    params.require(:nm_nm).permit(:roi_min, :ros_min, :cost_packaging, :cost_package, :cost_log_out, :cost_log_in, :cost, :unit_mass, :unit_per_box, :box_per_pallet, :nm_id, :prohibited, :supplier_article, :barcode, :imt_id, :user_id, :project_id)
  end

  def check_permissions_profit
    redirect_to '/' if not current_user.access_profit   
  end
end
