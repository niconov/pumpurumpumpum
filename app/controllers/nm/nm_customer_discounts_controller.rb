class Nm::NmCustomerDiscountsController < ApplicationController
  before_action :set_nm_customer_discount, only: %i[ show edit update destroy ]

  # GET /nm_customer_discounts or /nm_customer_discounts.json
  def index

    def search_request(query, page)
      search_params_defaults = {
        "resultset" => "catalog",
        "appType" => "1",
        "curr" => "rub",
        "spp" => 99,
        "regions" => '80,38,83,4,64,33,68,70,30,40,86,69,1,66,22,48,31,114',
        "dest" => "-1252424",
      }
    
      url = 'https://search.wb.ru/exactmatch/ru/common/v4/search'
      headers = { 'Content-Type' => 'application/json'}
      con = Faraday.new(headers: headers)
      resp = con.get(url) do |req|
        search_params_defaults.map do |k, v|
          req.params[k] = v
        end
        req.params['page'] = page + 1
        req.params['query'] = query
      end
      products = JSON.parse resp.body
      if resp.status == 200
        products = products['data']['products']
        product_ids = products.map do |pr|
          # print "#{pr['id']} #{pr['salePriceU']/100} \n"
          pr['id']
        end
        url_1 = "https://card.wb.ru/cards/detail"
        resull = con.get(url_1) do |req|
          req.params['nm'] = (product_ids.inject('') {|s, i| s += "#{i};"}).chop
          req.params['appType'] = 0
          req.params['curr'] = 'rub'
          req.params['spp'] = 99
        end
        res1 = JSON.parse resull.body
        req1 = res1['data']['products']
        temp = {price_min: 0.0, price_max: 99999999999.0}
        spps = {}
        req1.map do |i|
          if i['extended']
            i['extended']['basicPriceU'] = i['extended']['basicPriceU'] if i['extended'] and i['extended']['basicPriceU']
            spp = i['extended']['clientSale']
            spp = 0 if spp == nil
            price = i['extended']['basicPriceU'] 
            if price
              price = price.to_f / 100 / 100
              price = price.round
              price = price * 100
              spps[spp] = { 'price_min' => price, 'price_max' => price} if not spps[spp]
              spps[spp]['price_min'] = price if spps[spp]['price_min'] > price
              spps[spp]['price_max'] = price if spps[spp]['price_max'] < price
            end

          end
          # ap i['id']
          # ap i['extended'] if i['extended'] and i['extended']['clientSale'] and i['extended']['clientSale'] > 10
        end
        return spps
      end
    end




    NmGroupQuery.where(project_id: current_user.project_id).map do |ngq|
      ap ngq.as_json
      spps = search_request ngq.query, 2
      NmCustomerDiscount.where(nm_group_query: ngq.id).destroy_all
      keys = spps.keys.sort
      keys.map do |spp|
        print "#{spp}\t #{spps[spp]['price_min']}\t#{spps[spp]['price_max']}\n"
        opt = {
          spp: spp,
          nm_group_query_id: ngq.id,
        }
        if keys.first == spp
          opt[:bound_left]   = 0
          opt[:bound_right]  = spps[spp]['price_max']
        end
        if keys.last == spp
          opt[:bound_left]   = spps[spp]['price_min']
          opt[:bound_right]  = 999999999
        end 
        if keys.first != spp and keys.last != spp
          opt[:bound_left]   = spps[spp]['price_min'] 
          opt[:bound_right]  = spps[spp]['price_max']
        end

        opt = NmCustomerDiscount.new opt
        opt.save
      end

      ap "=" * 100
      # NmCustomerDiscount.find_or_create_by(nm_group_query_id: ngq.id)
    end
    @nm_customer_discounts = NmCustomerDiscount.order(nm_group_query_id: :asc, spp: :asc).all
  end

  # GET /nm_customer_discounts/1 or /nm_customer_discounts/1.json
  def show
  end

  # GET /nm_customer_discounts/new
  def new
    @nm_customer_discount = NmCustomerDiscount.new
  end

  # GET /nm_customer_discounts/1/edit
  def edit
  end

  # POST /nm_customer_discounts or /nm_customer_discounts.json
  def create
    @nm_customer_discount = NmCustomerDiscount.new(nm_customer_discount_params)

    respond_to do |format|
      if @nm_customer_discount.save
        format.html { redirect_to nm_customer_discount_url(@nm_customer_discount), notice: "Nm customer discount was successfully created." }
        format.json { render :show, status: :created, location: @nm_customer_discount }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @nm_customer_discount.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /nm_customer_discounts/1 or /nm_customer_discounts/1.json
  def update
    respond_to do |format|
      if @nm_customer_discount.update(nm_customer_discount_params)
        format.html { redirect_to nm_customer_discount_url(@nm_customer_discount), notice: "Nm customer discount was successfully updated." }
        format.json { render :show, status: :ok, location: @nm_customer_discount }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @nm_customer_discount.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /nm_customer_discounts/1 or /nm_customer_discounts/1.json
  def destroy
    @nm_customer_discount.destroy!

    respond_to do |format|
      format.html { redirect_to nm_customer_discounts_url, notice: "Nm customer discount was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_nm_customer_discount
      @nm_customer_discount = NmCustomerDiscount.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def nm_customer_discount_params
      params.require(:nm_customer_discount).permit(:spp, :category_id, :bound_left, :bound_right, :nm_group_query_id)
    end
end
