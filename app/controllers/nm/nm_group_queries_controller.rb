class Nm::NmGroupQueriesController < ApplicationController
  before_action :set_nm_group_query, only: %i[ show edit update destroy ]
  before_action :check_query_access, only: %i[ create edit update destroy ]

  # GET /nm_group_queries or /nm_group_queries.json
  def index
    @nm_group_queries = NmGroupQuery.all
  end

  # GET /nm_group_queries/1 or /nm_group_queries/1.json
  def show
    @items = Nm::Nm.where(project_id: current_user.project_id).order(object: :asc, supplier_article: :asc).map do |nm|
      {
        nm: nm,
        item: NmGroupQueryItem.find_by(nm_group_query_id: params[:id], nm_id: nm.nm_id)
      }
    end
  end

  def update_nm_positions
    nm_id = params[:nm_id]
    nm = Nm::Nm.where nm_id: nm_id, project_id: current_user.project_id
    if nm
      ngiqs = NmGroupQueryItem.where(nm_id: nm_id).pluck(:nm_group_query_id)
      # ap ngiqs
      NmGroupQuery.where(id: ngiqs).map do |i|
        # ap i
        WbUpdatePositionsOneJob.perform_async current_user.project_id, i.id
        event_ui "Обновление позиций запущено"
      end
    end
  end

  # GET /nm_group_queries/new
  def new
    @nm_group_query = NmGroupQuery.new
  end

  # GET /nm_group_queries/1/edit
  def edit
  end

  # POST /nm_group_queries or /nm_group_queries.json
  def create
    @nm_group_query = NmGroupQuery.new(nm_group_query_params)
    @nm_group_query.project_id = current_user.project_id

    respond_to do |format|
      if @nm_group_query.save
        format.html { redirect_to nm_nm_group_query_url(@nm_group_query), notice: "Nm group query was successfully created." }
        format.json { render :show, status: :created, location: @nm_group_query }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @nm_group_query.errors, status: :unprocessable_entity }
      end
    end
  end

  def query_item_action
    nm_id = params[:nm_id]
    nm = Nm::Nm.find_by(nm_id: nm_id, project_id: current_user.project_id)
    nm_group_query_id = params[:nm_group_query_id]
    nm_group_query = NmGroupQuery.find_by(id: nm_group_query_id)

    if nm and nm_group_query 
      case params[:user_action]
      when 'add'
        ap 'add'
        if NmGroupQueryItem.create(nm_group_query_id: nm_group_query_id, nm_id: nm.nm_id)
          ap 'created'
          redirect_to nm_nm_group_query_url(nm_group_query)
        else 
          ap 'not created'
          redirect_to nm_group_query_url(nm_group_query)
        end
      when 'delete'
        ap 'delete'
        NmGroupQueryItem.where(nm_group_query_id: nm_group_query_id, nm_id: nm.nm_id).destroy_all
        redirect_to nm_nm_group_query_url(nm_group_query)
      else
        ap 'no action valid'
        redirect_to nm_nm_group_query_url(nm_group_query)
      end
    end
    
  end

  # PATCH/PUT /nm_group_queries/1 or /nm_group_queries/1.json
  def update
    respond_to do |format|
      if @nm_group_query.project_id != current_user.project_id
        @nm_group_query.project_id = current_user.project_id
      end
      if @nm_group_query.update(nm_group_query_params)
        format.html { redirect_to nm_nm_group_query_url(@nm_group_query), notice: "Nm group query was successfully updated." }
        format.json { render :show, status: :ok, location: @nm_group_query }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @nm_group_query.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /nm_group_queries/1 or /nm_group_queries/1.json
  def destroy
    @nm_group_query.destroy!

    respond_to do |format|
      format.html { redirect_to nm_nm_group_queries_url, notice: "Nm group query was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_nm_group_query
      @nm_group_query = NmGroupQuery.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def nm_group_query_params
      params.require(:nm_group_query).permit(:project_id, :name, :query)
    end

    def check_query_access
      if current_user.id != 1
        redirect_to '/'
      end     
    end
end
