class Nm::NmGroupCompetitorsController < ApplicationController
  before_action :set_nm_group_competitor, only: %i[ show edit update destroy ]

  # GET /nm_group_competitors or /nm_group_competitors.json
  def index
    @nm_group_competitors = NmGroupCompetitor.where(project_id: current_user.project_id)
    # @nm_group_competitors = NmGroupCompetitor.all
    @nms = {}
    @nm_group_competitors.map
  end

  # GET /nm_group_competitors/1 or /nm_group_competitors/1.json
  def show
  end

  # GET /nm_group_competitors/new
  def new
    @nm_group_competitor = NmGroupCompetitor.new
    @nms = Nm::Nm.where(project_id: current_user.project_id).order(object: :asc, supplier_article: :asc).all
  end

  # GET /nm_group_competitors/1/edit
  def edit
    @nms = Nm::Nm.where(project_id: current_user.project_id).order(object: :asc, supplier_article: :asc).all
    @nms = @nms.select do |nm|
      not NmGroupCompetitor.find_by(nm_id: nm.nm_id)
    end
  end

  # POST /nm_group_competitors or /nm_group_competitors.json
  def create
    user_has_access = false
    @nm_group_competitor = NmGroupCompetitor.new(nm_group_competitor_params)
    @nm_group_competitor.project_id = current_user.project_id
    nm = Nm::Nm.find_by(nm_id: nm_group_competitor_params[:nm_id], project_id: current_user.project_id)
    user_has_access = true if nm

    respond_to do |format|
      if user_has_access and @nm_group_competitor.save
        format.html { redirect_to nm_group_competitor_url(@nm_group_competitor), notice: "Nm group competitor was successfully created." }
        format.json { render :show, status: :created, location: @nm_group_competitor }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @nm_group_competitor.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /nm_group_competitors/1 or /nm_group_competitors/1.json
  def update
    ap nm_group_competitor_params
    respond_to do |format|
      if @nm_group_competitor.update(nm_group_competitor_params)
        format.html { redirect_to nm_group_competitor_url(@nm_group_competitor), notice: "Nm group competitor was successfully updated." }
        format.json { render :show, status: :ok, location: @nm_group_competitor }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @nm_group_competitor.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /nm_group_competitors/1 or /nm_group_competitors/1.json
  def destroy
    @nm_group_competitor.destroy!

    respond_to do |format|
      format.html { redirect_to nm_group_competitors_url, notice: "Nm group competitor was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_nm_group_competitor
      @nm_group_competitor = NmGroupCompetitor.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def nm_group_competitor_params
      params.require(:nm_group_competitor).permit(:nm_id, :name)
    end
end
