class Warehouse::WarehouseController < ApplicationController

  def index
    @warehouses_web = Warehouse::FromWeb.where(belongs_wb: true).order(name: :asc).all
    @warehouses = Warehouse::Warehouse.order(name: :asc).all
  end
  
end
