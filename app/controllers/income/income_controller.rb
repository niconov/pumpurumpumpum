class Income::IncomeController < ApplicationController
  # GET /incomes or /incomes.json
  def index
    sel = 'income_id, warehouse_name, last_change_date'
    @incomes = Income::Income.select(sel).where(project_id: current_user.project_id).order(last_change_date: :desc).distinct.all
  end

  # GET /incomes/1 or /incomes/1.json
  def show
    @income_id = params[:id].to_i
    @incomes = Income::Income.where(project_id: current_user.project_id, income_id: @income_id).all
    @wh = ''
    if @incomes.count > 0
      inc_f = @incomes.first
      @wh = inc_f.warehouse_name
      @ts = inc_f.date
      @ts_close = inc_f.date
    end
  end

  def update_box_type
    ap params
    box_types = ["Короб", "Монопалета"]
    body = params.require(:income_income).permit(:income_id, :box_type)
    if box_types.include? body[:box_type] 
      Income::Income.where(project_id: current_user.project_id, income_id: body[:income_id]).update_all(box_type: body[:box_type])
      event_ui 'Тип поставки обновлен'
    else
      event_ui 'Неизвестный тип поставки'
    end
  end

  private


    # Only allow a list of trusted parameters through.
    def income_params
      params.require(:income).permit(:income_id, :number, :date, :last_change_date, :supplier_article, :tech_size, :barcode, :quantity, :total_price, :date_close, :warehouse_name, :nm_id, :status)
    end

end
