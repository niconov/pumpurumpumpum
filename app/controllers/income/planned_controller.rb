class Income::PlannedController < ApplicationController
  before_action :authenticate_user!

  def index
    @planned = Income::Planned::Head.where(is_deleted: false, project_id: current_user.project_id).order(:period, :updated_at)
  end

  def archived
    @planned = Income::Planned::Head.where(is_deleted: true, project_id: current_user.project_id).order(:period, :updated_at)
  end

  def update
    set_ip
    if @ip
      @ip.update ip_params
      @ip.save
    end
  end

  def show
    set_ip
    @warehouses = Warehouse::Warehouse.order(:name).all
  end

  def delete
    set_ip
    if @ip
      @ip.is_deleted = !@ip.is_deleted 
      if @ip.is_deleted 
        @ip.deleted_at = Time.now
      else
        @ip.deleted_at = nil
      end
      @ip.save
      redirect_to income_planned_path
    end
  end

  def create
    if current_user.project_id
      @ip = Income::Planned::Head.new
      @ip.project_id = current_user.project_id
      @ip.save
      redirect_to income_planned_show_path @ip
    else
      redirect_to '/'
    end
  end

  private

  def ip_params
    params.require(:income_planned_head).permit(:warehouse_id, :id, :period)
  end

  def set_ip
    ip_id = params[:id] || ip_params[:id]
    @ip = Income::Planned::Head.find_by project_id: current_user.project_id, id: ip_id
  end
end