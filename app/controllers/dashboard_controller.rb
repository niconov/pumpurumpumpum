require "descriptive-statistics"

class DashboardController < ActionController::Base
	before_action :authenticate_user!
	before_action :check_incomes_box_types, only: [:daily]
	before_action :check_permissions_profit, only: [:logistics_planning]
	layout 'application'

	def nm_stats
		if params[:date]
			@ts_nav = Time.parse params[:date]
		else
			@ts_nav = Time.now
		end
		ts_start = @ts_nav.beginning_of_day
		ts_end = @ts_nav.end_of_day
		@ts = ts_start
		@stats = Nm::Stat.where(project_id: current_user.project_id).where("period between '#{ts_start}' and '#{ts_end}' ").order(:object_name, :vendor_code)
		# .where("('nm_stats.stocksMp' != '0' or 'nm_stats.stocksWb' != '0' or  'nm_stats.ordersCount' != '0')")
	end

	def weekly_orders
		@period = 28
		ts1 = Time.now
		ts2 = Time.now.beginning_of_day - @period.days
		wh1 = "project_id = #{current_user.project.id} and canceled = false and datetime between '#{ts2}' and '#{ts1}'"
		wh2 = "project_id = #{current_user.project.id} and canceled = false and datetime between '#{ts2-@period.days}' and '#{ts1-@period.days}'"
		gr = 'article, subject, nm_id'
		sel = 'count(id) as cnt, sum( (price * ((100-discount))/100)) as sum, avg(discount) as discount, article, subject, nm_id'
		@orders = {}
		@nms = []

		@sum_total = 0.0
		@sum_total_prev = 0.0

		@cnt_total = 0.0
		@cnt_total_prev = 0.0

		@mass_total = 0.0
		@mass_total_prev = 0.0
		@volume_total = 0.0
		@volume_total_prev = 0.0

		orders1 = Order::Order.where(wh1).select(sel).group(gr).order(:subject, :article)
		orders2 = Order::Order.where(wh2).select(sel).group(gr).order(:subject, :article)
		orders1.map do |i| 
			@nms.push i.article if not @nms.include? i.article
			@orders[i.article] = {}
			@orders[i.article][:now] = i
			@orders[i.article][:nm] = Nm::Nm.find_by nm_id: i.nm_id
			@volume_total += @orders[i.article][:nm].volume * i.cnt
			@mass_total += @orders[i.article][:nm].unit_mass * i.cnt
			@sum_total += i.sum
			@cnt_total += i.cnt
		end
		orders2.map do |i| 
			@nms.push i.article if not @nms.include? i.article
			@orders[i.article][:prev] = i
			@orders[i.article][:nm] = Nm::Nm.find_by nm_id: i.nm_id if not @orders[i.article][:nm]
			@volume_total_prev += @orders[i.article][:nm].volume * i.cnt
			@mass_total_prev += @orders[i.article][:nm].unit_mass * i.cnt
			@sum_total_prev += i.sum
			@cnt_total_prev += i.cnt
		end


		wh1 = "project_id = #{current_user.project.id} and canceled = false and datetime between '#{ts2}' and '#{ts1}'"
		wh2 = "project_id = #{current_user.project.id} and canceled = false and datetime between '#{ts2-@period.days}' and '#{ts1-@period.days}'"
		gr = 'subject'
		sel = 'count(id) as cnt, sum( (price * ((100-discount))/100)) as sum, subject'


		@orders_by_cat = {}
		@cats = []
		orders1 = Order::Order.where(wh1).select(sel).group(gr).order(:subject)
		orders2 = Order::Order.where(wh2).select(sel).group(gr).order(:subject)

		orders1.map do |i| 
			@cats.push i.subject if not @cats.include? i.subject
			@orders_by_cat[i.subject] = {}
			@orders_by_cat[i.subject][:now] = i
		end
		orders2.map do |i| 
			@cats.push i.subject if not @cats.include? i.subject
			@orders_by_cat[i.subject][:prev] = i
		end
		@cats = @cats.sort
	end

	def index
		subjects = Event.where(project_id: current_user.project_id).where.not(subject: 'uionly').select(:subject).distinct.pluck(:subject)
		@events  = subjects.map do |subject|
			events = Event.where(project_id: current_user.project_id, subject: subject).where.not(subject: 'uionly').limit(30).order(date: :desc).all
			{
				name: subject,
				events: events
			}
		end
		nm_ids = Nm::Nm.where(project_id: current_user.project_id).select(:nm_ids).pluck(:nm_id)
		nm_ids_with_ngqi = NmGroupQueryItem.where(nm_id: nm_ids).select(:nm_id).pluck(:nm_id)
		@nm_ids_for_quering = nm_ids.select {|i| not nm_ids_with_ngqi.include?(i)}
		@nm_ids_for_quering = Nm::Nm.where(project_id: current_user.project_id, nm_id: @nm_ids_for_quering).all

		ts = Time.now.beginning_of_day - 7.days
		@income_ids = Order::Order.where(project_id: current_user.project_id).where("datetime > '#{ts}'").order(income_id: :desc).select(:income_id).distinct.pluck(:income_id)
		@income_ids = @income_ids.map {|id| Income::Income.where(box_type: nil).find_by(income_id: id) }
		@income_ids.compact!

		# u = User.find_by(id: self.user_id)
		# ap current_user.tg_chat_id
	end


	def nm_positions
		@result = Nm::Nm.where(project_id: current_user.project_id).order(object: :asc, supplier_article: :asc).map do |nm|
			np = NmPosition.where(nm_id: nm.nm_id).order(period: :asc).last
			if np
				if np.position == nil or np.position > 10
					c = 'text-error'
				end
			else
				c = ''
			end
			{
				nm: nm,
				np: np,
				c: c,
				stock: nm.stock_total
			}
		end
		@result = @result.select {|i| i[:stock] != 0}
		
	end


	def profit_by_current_stocks
		redirect_to '/' if not current_user.access_profit
		pre = {}
		Stock::Stock.where(project_id: current_user.project_id).where.not(warehouse_id: 31).map do |s|
			if s.quantity > 0
				nm = s.nm
				t = s.get_tariff_current
				pre[nm.nm_id] = {quantity: 0.0, profit: 0.0, profit_pre_pcs: 0.0, nm: nm, roi: 0.0, ros: 0.0} if not pre[nm.nm_id]
				profit = nm.profit_without_logistics - t.calc_by_nm(nm)
				pre[nm.nm_id][:profit] = pre[nm.nm_id][:profit] + profit * s.quantity
				pre[nm.nm_id][:quantity] = pre[nm.nm_id][:quantity] + s.quantity
				pre[nm.nm_id][:profit_per_pcs] = pre[nm.nm_id][:profit] / pre[nm.nm_id][:quantity]
				pre[nm.nm_id][:price] = nm.price_total
				pre[nm.nm_id][:cost] = nm.cost_total
				pre[nm.nm_id][:roi] = 100 * pre[nm.nm_id][:profit] / (pre[nm.nm_id][:cost] * pre[nm.nm_id][:quantity])
				pre[nm.nm_id][:ros] = 100 * pre[nm.nm_id][:profit] / (pre[nm.nm_id][:price] * pre[nm.nm_id][:quantity])
				pre[nm.nm_id][:text_class] = pre[nm.nm_id][:profit_per_pcs] >= 0 ? "text-success" : "text-error"
			end
		end
		# ap pre
		@whes_mass_total = 0.0
		@whes_volume_total = 0.0
		pre.map do |k, v|
			vol = (v[:nm].volume * v[:quantity] / 1000).round(2)
			mass = (v[:nm].unit_mass * v[:quantity]).round(2)
			pre[k][:volume_total] = vol
			pre[k][:mass_total] = mass
			@whes_mass_total = @whes_mass_total + mass
			@whes_volume_total = @whes_volume_total + vol
		end
		@result = pre.map do |k, v| v end

		@result = @result.sort_by {|i| [i[:nm].object, i[:nm].supplier_article] }
		
	end

	def stocks_planning
		@result = []
		@days = 8
		@days_name = [] 
		ts_n = Time.now
		ts_b = ts_n.beginning_of_day - @days * 24 * 3600

		Nm::Nm.where(project_id: current_user.project_id).map do |nm|
			res = { "nm" => nm, 'sum' => 0.0, 'qty_total' => 0.0 }
			# ap nm
			# ap Order::Order.first
			orders = Order::Order.where("nm_id = #{nm.nm_id} and datetime between '#{ts_b}' and '#{ts_n}' ").all
			orders = orders.group_by{|x| x.datetime.strftime("%m/%d")} 
			orders.map do |k, orders|
				@days_name.push k if not @days_name.include? k
				res["orders"] = {} if not res["orders"]
				res["orders"][k] = orders.count
				res["qty_total"] = res["qty_total"] + orders.count
				total = orders.inject(0.0) {|sum, i| sum += i.price * (100 - i.discount) / 100}
				res['sum'] = res['sum'] + total
			end
			res['quantity'] = Stock::Stock.where(nm_id: nm.nm_id).where.not(warehouse_id: 31).inject(0.0) {|sum, i| sum += i.quantity }
			@result.push res
		end
		@result = @result.sort_by {|i| -i['sum']}
		@result = @result.map do |stock|
			stock['qty_per_day'] = stock['qty_total'] / @days
			stock['qty_daily'] = stock['quantity'] / (stock['qty_total'] / @days)
			orders_list = []
			# ap stock
			if stock['orders']
				stock['orders'].map {|k, v| orders_list.push v}
				orders_stats = DescriptiveStatistics::Stats.new orders_list
				stats = orders_stats.relative_standard_deviation
				ap stock
				ap stats
				stock['standard_deviation'] = stats
				stock['huge_variance'] = stats > 40
			else
				stock['huge_variance'] = false
			end
			stock
		end
		@days_name.sort!
		@result = @result.sort_by {|i| [i['nm'].object, i['nm'].supplier_article] }
		# ap @days_name
	end

	def stocks_by_incomes
		if params[:date]
			@ts = Time.parse params[:date]
		else
			@ts = Time.now.beginning_of_day
		end

		stocks = Stock::History.where(date: @ts, project_id: current_user.project_id)
		stock_ids = stocks.select(:nm_id).distinct.pluck(:nm_id)
		@stocks = stocks.order(:warehouse, id: :desc).group_by {|i| i.nm_id}
		@nms = Nm::Nm.where(nm_id: stock_ids).order(:object, :supplier_article)
	end

	def warehouse_balance
		current_user.access_profit
		stocks = Stock::Stock.where("project_id = #{current_user.project_id} and quantity > 0").where.not(warehouse_id: 31).all
		# ap stocks
		@qty_total = 0
		@sum_total = 0.0
		@sum_total_cost = 0.0
		@whses = stocks.map { |e| e.warehouse_name }
		@whses.uniq!
		articles = stocks.map { |e| e.supplier_article }
		articles.uniq!
		@whses_with_stocks = []
		@articles = articles.map do |a|
			nm = Nm::Nm.find_by_supplier_article a
			r = {}
			r['object'] = nm.object
			r['supplierArticle'] = a
			r['sum'] = 0.0
			r['qty_total'] = 0
			@whses.map do |w|
				b = stocks.find {|l| l.warehouse_name == w and l.supplier_article == a }
				if b and b['quantity'] > 0
					r[w] = b
					r['sum'] = r['sum'] + b.quantity * (b.price * (100 - b.discount)/100)
					@sum_total += b.quantity * (b.price * (100 - b.discount)/100)
					@sum_total_cost += b.quantity * b.nm.cost_total
					@qty_total += b.quantity
					r['qty_total'] = r['qty_total'] + b.quantity
					@whses_with_stocks.push w
				else
					r[w] = nil
				end
			end
			r
		end
		@articles = @articles.sort_by {|i| [i['object'], i['supplierArticle']]}
		@whses_with_stocks.uniq!
	end

	def logistics_planning


		@result = []
		Nm::Nm.where(project_id: current_user.project_id).order(object: :asc, supplier_article: :asc).map do |nm| 
			cost_without_tariff = nm.comission_total + nm.cost + (nm.cost_log_in || 0.0) + (nm.cost_package || 0.0) + (nm.cost_packaging || 0.0)
			@result.push({ 
				"nm" => nm,
				"price" => nm.price,
				"cost_without_tariff" => cost_without_tariff,
				"tariffs" => {} 
			})
		end
		stop_words = ["КГТ", "СЦ", "СГТ", "КБТ", "Базовые"]
		@tariffs_active = current_user.project.tariffs.all

		@tariffs = @tariffs_active.map do |t| 
			# TariffPeriodic.find
			Tariff::Tariff.find t.tariff_id
		end

		@result.map! do |result|
			@tariffs.map do |t|
				dm = DeliveryMethod.find_by(tariff_id: t.id)
				nm = result['nm']
				tar = t.calc_by_nm(result['nm'])
				log_price_item = (dm.price / result['nm'].unit_per_pallet).round(2)
				cost_total = log_price_item + tar + result['cost_without_tariff']
				profit = (result['price'].total - cost_total).round(2)
				# (result['price'].total - result['nm'].cost_total - result['nm'].comission_total - (dm.price / result['nm'].unit_per_pallet) - t.calc_by_nm(result['nm'])).round(2)
				roi = (100*profit/cost_total).round(2)
				ros = (100*profit/result['price'].total).round(2)
				result["tariffs"][t.warehouseName] = {
					"t" => t,
					"tar" => tar,
					"log_pal" => dm.price,
					"log" => (dm.price / result['nm'].unit_per_pallet).round(2),
					"profit" => profit,
					"roi" => roi,
					"ros" => ros,
					"class" => profit < 0 ? "text-error" : ""
				
				}
			end
			result
		end
		# ap @result

		@tariff_names = @tariffs.map {|i| i.warehouseName}
		# @result.map! do |r|
		# 	r['tariffs'] = {}
		# 	@tariffs.map do |t|
		# 		r['tariffs'][t.warehouseName]['tar'] = t.calc_by_nm r['nm']
		# 	end
		# 	r
		# end
		
	end
		
	def sales_stats_by_district
		ssbds = SalesStatsByDistrict.where(project_id: current_user.project_id).all.to_a
		@ssbds = ssbds.map do |ssbd|
			ssbd = ssbd.as_json
			ssbd['nm'] = Nm::Nm.find_by_id(ssbd['nm_id'])
			ssbd['district'] = District.find_by_id(ssbd['district_id'])
			ssbd
		end

	end

	def trading
		ts = Time.now
		@nm_ids = current_user.project.orders.where("order_type = 'Клиентский' and datetime between '#{ts - 7.days}' and '#{ts}'").select(:nm_id).distinct.pluck(:nm_id)
		@nms = @nm_ids.map do |nm_id| 
			nm = Nm::Nm.find_by nm_id: nm_id 
			if nm == nil
				Event.event_ui current_user.project, "В отчет попала неизвестная номенклатура #{nm_id}, требуется обновить список НМ или присуствует ошибка ВБ", 'error'
			end
			nm
		end
		@nms.compact!
		
		# @nms = @nm_ids.map {|nm_id| Nm::Nm.find_by nm_id: nm_id }
		# ap @nms
		@nms = @nms.sort_by {|i| [i.object, i.supplier_article]}
	end

	def check_incomes_box_types
		# Order::Order.where("project_id = '#{current_user.project_id}' and ")
		# redirect_to income_income_index_path
		# Event.event_ui current_user.project, "Есть поставки без типов", 'error'

	end

	def stocks_history
		@period = 24
		@ts = Time.now.beginning_of_day - @period.days
		wh = "date > '#{@ts}'"

		@first = Stock::History.where(wh).where(project_id: current_user.project_id).order(date: :asc).first
		if @first
			@ts = @first.date
		end
		nm_ids = Stock::History.select(:nm_id).where(project_id: current_user.project_id).distinct.where("date > '#{@ts}'").pluck(:nm_id)
		@nms = Nm::Nm.where(nm_id: nm_ids, project_id: current_user.project_id).order(:object, :supplier_article)

	end


	def daily
		@mass_total = 0.0
		@volume_total = 0.0
		last_order = Order::Order.where("project_id = '#{current_user.project_id}' and datetime is not Null and order_type = 'Клиентский'").order(datetime: :desc).first
		ts = last_order.datetime

		if params[:date]
			begin
				ts_b = Time.parse params[:date]
			rescue 
			end
		end
		if not ts_b or ts_b.beginning_of_day  > Time.now.beginning_of_day
			ts_now = Time.now
			if (ts_now - ts_now.beginning_of_day) < 2 * 3600
				ts = (ts_now.beginning_of_day - 1)
			end
    	ts_b = ts.beginning_of_day	
    end
    @ts_nav = ts_b
    adstats = Advert::Stat.where(period: ts_b, project_id: current_user.project_id).all
    @advert_total = adstats.inject(0.0) { |mem, var| mem += (var.sum || 0.0 ) }
		# ts_b = (Time.now - 24*3600).beginning_of_day	
    orders_stats = {}

		@nm_stats = Nm::Stat.where(project_id: current_user.project_id).where("period between '#{ts_b}' and '#{ts_b.end_of_day}'")
		@nm_stats = @nm_stats.group_by {|i| i.nm_id}
		@nm_stats.keys.map do |k|
			@nm_stats[k] = @nm_stats[k][0] ? @nm_stats[k][0] : nil
		end

		days = 7
		@last_datetime = ts_b
		@sum_total = 0.0
		@qty_total = 0
		@profit_total = 0.0
		@subjects = []
		@subjects_total = {}
		names = ["profit_total", "ads", "sum", "qty", 'qty_avg', "profit" , 'sum_avg', 'qty_diff', 'sum_diff', 'cost'].uniq


		nm_hash = {}
		where_now = "project_id = '#{current_user.project_id}' and datetime between '#{ts_b}' and '#{ts_b + 1.day - 1}' and order_type = 'Клиентский'"
		select_now =  "avg(price) as price, avg(discount) as discount"
		select_now += ", sum((price*(100-discount)/100)) as sum"
		select_now += ", count(distinct srid) as qty"
		select_now += ", max(datetime) as datetime"
		select_now += ", nm_id, comission_id, warehouse, income_id"
		group_now = [:nm_id, :comission_id, :warehouse, :income_id]

		orders_now = Order::Order.where(where_now).select(select_now).group(group_now).map do |order|
			if nm_hash[order.nm_id]
				nm = nm_hash[order.nm_id]
			else
				nm = Nm::Nm.find_by nm_id: order.nm_id
				nm_hash[order.nm_id] = nm
			end
      if nm == nil
        ap order
      end
			cost = nm.total_cost * order.qty
			begin 
				profit_per_pcs_now = order.calc_profit
			rescue => e
				ap "Error while calc_profit"
				ap order.as_json
				ap e				
				raise
			end
			profit = profit_per_pcs_now * order.qty
			sum = order.sum

			if not orders_stats[order.nm_id]
				if not @subjects_total[nm.object]
					@subjects_total[nm.object] = {}
					names.map {|i| @subjects_total[nm.object][i] = 0.0 }
				end
				@subjects.push nm.object if not @subjects.include? nm.object
				orders_stats[order.nm_id] = {'nm' =>nm, 'qty' => 0, 'qty_avg' => 0.0, 'sum' => 0.0, 'sum_avg' => 0.0, 'profit' => 0.0, "profit_total" => 0.0, "cost" => 0.0, 'ads' => 0.0} 
			end

			orders_stats[order.nm_id]['cost'] = orders_stats[order.nm_id]['cost'] + cost
			orders_stats[order.nm_id]['sum'] = orders_stats[order.nm_id]['sum'] + sum
			orders_stats[order.nm_id]['qty'] = orders_stats[order.nm_id]['qty'] + order.qty
			orders_stats[order.nm_id]['profit'] = orders_stats[order.nm_id]['profit'] + profit
			@subjects_total[nm.object]['cost'] = @subjects_total[nm.object]['cost'] + cost
			@subjects_total[nm.object]['sum'] = @subjects_total[nm.object]['sum'] + sum
			@subjects_total[nm.object]['qty'] = @subjects_total[nm.object]['qty'] + order.qty
			@subjects_total[nm.object]['profit'] = @subjects_total[nm.object]['profit'] + profit
			@mass_total += order.qty * nm.unit_mass if nm.unit_mass 
			@volume_total += order.qty * nm.volume/1000 if nm.volume
			@profit_total += profit
			@sum_total += sum
			@qty_total += order.qty
			order

		end

		orders_stats.keys.map do |nm_id|
			nm = orders_stats[nm_id]['nm']
			ads = nm.advert_total_on_date ts_b
			orders_stats[nm_id]['ads'] = ads
			orders_stats[nm_id]['profit'] = orders_stats[nm_id]['profit'] - ads
			orders_stats[nm_id]['cost'] = orders_stats[nm_id]['cost'] +ads
			@subjects_total[nm.object]['ads'] = @subjects_total[nm.object]['ads'] + ads
			@subjects_total[nm.object]['profit'] = @subjects_total[nm.object]['profit'] - ads
			@subjects_total[nm.object]['cost'] = @subjects_total[nm.object]['cost'] + ads
		end


		orders_now = orders_now.sort_by {|i| i.datetime}
		order_last = orders_now.last
		@last_datetime = order_last.datetime if order_last
		@sum_total = @sum_total.round(2)


		ts_e = @last_datetime - 1.day
		ts_s = @last_datetime.beginning_of_day - 1.day

		where_prev = "project_id = '#{current_user.project_id}' and \n    "
		days.times do |i|
			where_prev += " or " if i != 0
			where_prev += "datetime between '#{ts_s - i.days}' and '#{ts_e - i.days}' \n"
		end
		select_prev =  "nm_id, "
		select_prev += "sum((price*(100-discount)/100)) as sum, "
		select_prev += "count(distinct srid) as qty"
		group_prev = [:nm_id]

		orders_now = Order::Order.where(where_prev).select(select_prev).group(group_prev).map do |order|
			nm_id = order.nm_id
			if nm_hash[order.nm_id]
				nm = nm_hash[order.nm_id]
			else
				nm = Nm::Nm.find_by nm_id: order.nm_id
				nm_hash[order.nm_id] = nm
			end

			if nm
				if nm.project_id.to_i == current_user.project_id.to_i
					if not orders_stats[order.nm_id]
						if not @subjects_total[nm.object]
							@subjects_total[nm.object] = {}
							names.map {|i| @subjects_total[nm.object][i] = 0.0 }
						end
						@subjects.push nm.object if not @subjects.include? nm.object
						orders_stats[order.nm_id] = {'nm' =>nm, 'qty' => 0, 'qty_avg' => 0.0, 'sum' => 0.0, 'sum_avg' => 0.0, 'profit' => 0.0, "cost" => 0.0} 
					end
					orders_stats[nm_id]['qty_avg'] = orders_stats[nm_id]['qty_avg'] + order.qty
					orders_stats[nm_id]['sum_avg'] = orders_stats[nm_id]['sum_avg'] + order.sum 
					@subjects_total[nm.object]['sum_avg'] = @subjects_total[nm.object]['sum_avg'] + (order.sum / days)
					@subjects_total[nm.object]['qty_avg'] = @subjects_total[nm.object]['qty_avg'] + (order.qty.to_f / days)
				end
			end
		end

		orders_stats.keys.map do |nm_id|
			orders_stats[nm_id]['qty_avg']  = (orders_stats[nm_id]['qty_avg'] / days).round(2)
			orders_stats[nm_id]['qty_diff'] = (100 * ((orders_stats[nm_id]['qty'] / orders_stats[nm_id]['qty_avg'])-1 )).round(2)
			orders_stats[nm_id]['sum_avg']  = (orders_stats[nm_id]['sum_avg'] / days).round(2)
			orders_stats[nm_id]['sum_diff'] = (100 * ((orders_stats[nm_id]['sum'] / orders_stats[nm_id]['sum_avg'])-1 )).round(2)
			orders_stats[nm_id]['profit_per_pcs'] = ((orders_stats[nm_id]['profit'] / orders_stats[nm_id]['qty'])).round(2)
			orders_stats[nm_id]['roi'] = (((orders_stats[nm_id]['profit'] / (orders_stats[nm_id]['cost']))) * 100).round(2)
			# orders_stats[nm_id]['roi'] = (((orders_stats[nm_id]['profit'] / (orders_stats[nm_id]['sum'] - orders_stats[nm_id]['profit']))) * 100).round(2)
			orders_stats[nm_id]['ros'] = ((orders_stats[nm_id]['profit'] / orders_stats[nm_id]['sum'])*100).round(2)
			orders_stats[nm_id]['stock'] = Stock::Stock.calc_for_nm orders_stats[nm_id]['nm']
			orders_stats[nm_id]['price'] = orders_stats[nm_id]['sum'] / orders_stats[nm_id]['qty']
		end

    @days = days
    @orders_stats = []
    orders_stats.map {|k, v| @orders_stats.push v }
    @orders_stats = @orders_stats.sort_by {|o| o['nm'].supplier_article }
    @subjects.sort!
	
		@total = {}
		names.map {|i| @total[i] = 0.0 }

		@subjects_total.keys.map do |s|
			@subjects_total[s]['sum_diff'] = ((@subjects_total[s]['sum'] / @subjects_total[s]['sum_avg']) - 1 ) * 100
			@subjects_total[s]['qty_diff'] = ((@subjects_total[s]['qty'] / @subjects_total[s]['qty_avg']) - 1 ) * 100
			@subjects_total[s]['roi'] = ((@subjects_total[s]['profit'] / (@subjects_total[s]['cost'])) ) * 100
			@subjects_total[s]['ros'] = ((@subjects_total[s]['profit'] / @subjects_total[s]['sum']) ) * 100
		end

		@subjects_total.keys.map do |s|
			names.map do |key|
				@total[key] = @total[key] + @subjects_total[s][key]
			end
			@total['roi'] = ((@total['profit'] / (@total['cost'])))  * 100
			@total['ros'] = ((@total['profit'] / @total['sum']))  * 100
			@total['sum_diff'] = ((@total['sum'] / @total['sum_avg']) - 1 ) * 100
			@total['qty_diff'] = ((@total['qty'] / @total['qty_avg']) - 1 ) * 100
		end
		wh = "project_id = #{current_user.project_id} and last_change_date > '#{Time.now.beginning_of_day}'"
		sel = "count(distinct srid) as count, sum((price*(100-discount)/100)) as sum, region, district, country, article"
		gr = "region, district, country, article"
		ord = {article: :asc, sum: :desc}
		@orders_by_regions = Order::Order.where(wh).select(sel).group(gr).order(ord).all
	end
	
	def con_anal
		require "./app/lib/wb_api/wb_api.rb"
		
		project = Project.find current_user.project_id
		api = WBApi.new project
		# api.incomings_update
		# ap 'upd'
		Advert::Advert.where(project_id: current_user.project_id).all.map do |advert|
			# ap advert.as_json
			# api.full_stats advert.advert_id if advert.active?
			# sleep 6
		end
		# ap 'upd'
		api.full_stats 11777168


		@adstats = Advert::Stat.all
		# ap api

		# res = api.search_request 'пакеты фасовочные', 1
		# body = JSON.parse res.body
		# products = body['data']['products']
		# products_ids = products.map {|i| i['id']}
		# ap products_ids
		# products_ids = products_ids[0..29]
		# # products_ids = []
		# # products_ids.push 44475717

		# products_ids = [
		#   12082,
		#   86131,
		#   28719801
		#   # 37891,
		#   # 214403,
		#   # 228823,
		#   # 182755,
		#   # 188221,
		#   # 56339,
		#   # 315237,
		#   # 231792,
		#   # 231503,
		#   # 464875,
		#   # 470022,
		#   # 546275,
		#   # 73636,
		#   # 224597,
		#   # 162801,
		#   # 93820,
		#   # 661820,
		#   # 29631,
		#   # 415897,
		#   # 29267,
		#   # 1073857,
		#   # 768162,
		#   # 509773,
		#   # 1169402,
		#   # 775429,
		#   # 299593,
		#   # 394085,
		#   # 28719801
		# ]
		# url = "https://suppliers-shipment.wildberries.ru/api/v1/suppliers/find_by_ids"
		# conn = Faraday.new url, proxy: 'https://192.168.88.1:8080'
		# body_now = products_ids
		# headers = {
		# 	'Accept-Encoding': 'gzip, deflate, br',
		# 	'Accept-Language': 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7,zh;q=0.6,zh-HK;q=0.5,zh-TW;q=0.4,zh-CN;q=0.3',
		# 	'Cache-Control': 'no-cache',
		# 	'Content-Length': '205',
		# 	'Dnt': '1',
		# 	'Origin': 'https://www.wildberries.ru',
		# 	'Pragma': 'no-cache',
		# 	'Referer': 'https://www.wildberries.ru/catalog/44475717/detail.aspx',
		#   'content-type': 'application/json', 
		#   'X-Client-Name': 'site', 
		#   'Sec-Ch-Ua': '"Google Chrome";v="119", "Chromium";v="119", "Not?A_Brand";v="24"',
		# 	'Sec-Ch-Ua-Mobile': '?0',
		# 	'Sec-Ch-Ua-Platform': "macOS",
		# 	'Sec-Fetch-Dest': 'empty',
		# 	'Sec-Fetch-Mode': 'cors',
		# 	'Sec-Fetch-Site': 'same-site',
		# 	'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/119.0.0.0 Safari/537.36'
		# }


		# res = conn.post do |req|
		# 	headers.map do |k, v|
		# 		req.headers[k] = v
		# 		ap req.headers
		# 	end
		# 	req.body = body_now.to_json
		# end
		# ap JSON.parse res.body

	end

	private

	def check_permissions_profit
		redirect_to '/' if not current_user.access_profit		
	end

end
