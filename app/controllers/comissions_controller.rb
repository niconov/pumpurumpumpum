class ComissionsController < ApplicationController
	before_action :authenticate_user!

	def index
		@comissions = Comission.paginate(page: params[:page] || 1, per_page: 50)
	end
end
