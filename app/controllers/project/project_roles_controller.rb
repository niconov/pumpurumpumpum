class Project::ProjectRolesController < ApplicationController
  before_action :set_project_role, only: %i[ show edit update destroy ]
  before_action :check_role_admin

  # GET /project_roles or /project_roles.json
  def index
    @project_roles = Project::Role.order(id: :asc).all
  end

  # GET /project_roles/1 or /project_roles/1.json
  def show
  end

  # GET /project_roles/new
  def new
    @project_role = Project::Role.new
  end

  # GET /project_roles/1/edit
  def edit
  end

  # POST /project_roles or /project_roles.json
  def create
    @project_role = Project::Role.new(project_role_params)

    respond_to do |format|
      if @project_role.save
        format.html { redirect_to project_roles_url, notice: "Project role was successfully created." }
        format.json { render :show, status: :created, location: @project_role }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @project_role.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /project_roles/1 or /project_roles/1.json
  def update
    respond_to do |format|
      if @project_role.update(project_role_params)
        format.html { redirect_to project_roles_url, notice: "Project role was successfully updated." }
        format.json { render :show, status: :ok, location: @project_role }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @project_role.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /project_roles/1 or /project_roles/1.json
  def destroy
    @project_role.destroy!

    respond_to do |format|
      format.html { redirect_to project_roles_url, notice: "Project role was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_project_role
      @project_role = Project::Role.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def project_role_params
      params.require(:project_role).permit(:user_id, :user_role, :changable, :access_profit)
    end
    
    def check_role_admin
      if current_user.id != 1
        redirect_to '/', notice: "Доступа нет!" 
      else
        role = Project::Role.find_by(user_role: "Администратор")
        if not role
          role = Project::Role.new
          role.user_role = "Администратор"
          role.changable = false
          role.save
        end
      end
    end
end
