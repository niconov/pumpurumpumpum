class Project::ProjectsController < ApplicationController
  before_action :set_project, only: %i[ show edit update destroy ]
  before_action :check_permissions, only: %i[ show edit update destroy ]

  # GET /projects or /projects.json
  def index
    roles = Project::RoleUserOnProject.where(user_id: current_user.id)
    @projects = roles.map {|i| i.project }
  end

  # GET /projects/1 or /projects/1.json
  def show
    @roles = Project::RoleUserOnProject.where(project_id: @project.id)
  end

  # GET /projects/new
  def new
    @project = Project::Project.new
  end

  # GET /projects/1/edit
  def edit
  end

  def set_as_current_project
    ap params
  end

  # POST /projects or /projects.json
  def create
    service = ::Projects::Creator.new(name: project_params[:name],
                                      token: project_params[:token],
                                      user: current_user).call

    respond_to do |format|
      if service.success?
        format.html {
          redirect_to project_projects_path
          event_ui service.project, 'Собираем информацию...'
        }
        format.json { render :show, status: :created, location: service.project }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: service.public_error, status: :unprocessable_entity }
      end
    end
  end

  def user_settings_update
    ap "params"
    ap params
    ap "params.require(:user_settings)"
    ap params.require(:project_user_setting)
    ap user_setting_param[:setting_id]
    s = Project::UserSetting.find_by user_id: current_user.id, id: user_setting_param[:settings_id]
    if s
      s.update notification_enabled: user_setting_param[:notification_enabled]
    end
  end

  def role_user_on_projects_action
    project = Project::Project.find_by_id params[:project_id]
    # ap project
    # ap params
    case params[:user_action]
    when 'set_as_current'
      rup = Project::RoleUserOnProject.find_by user_id: current_user.id, project_id: params[:project_id]
      if rup 
        current_user.project_id = rup.project_id
        current_user.save
        redirect_to project_projects_url, notice: 'Проект выбран'
      else
        redirect_to project_projects_url, notice: 'Пользователь не имеет доступа к проекту'
      end
    when 'delete'
      role = Project::Role.find_by_id params[:role_id]
      rup = Project::RoleUserOnProject.find_by user_id: params[:user_data], project_role_id: role.id, project_id: project.id
      if project.user_id != rup.user_id
        if project.is_user_can_invite current_user
          rup.destroy
          redirect_to project_projects_url(project), notice: 'Пользователь разжалован'
        else
          redirect_to project_projects_url(project), notice: 'Вы не можете снимать с должности'
        end
      else
        redirect_to project_projects_url(project), notice: 'Невозможно удалить автора проекта'
      end
    when 'invite'
      if project.is_user_can_invite current_user
        user = User.find_by_email params[:user_data]
        role = Project::Role.find_by_id params[:role_id]
        if role.user_role != "Администратор" and role.id != 3
          if user and project and role
            rup = Project::RoleUserOnProject.new user_id: user.id, project_role_id: role.id, project_id: project.id
            if rup.save
              redirect_to project_projects_url(project), notice: 'Пользователь приглашен'
            end
          end
        else
          redirect_to project_projects_url(project), notice: 'Вы не можете назначать администратора'
        end
        redirect_to project_projects_url(project), notice: 'Вы не можете приглашать пользователей'
      end
    end
    # redirect_to project_url(project)
  end


  # PATCH/PUT /projects/1 or /projects/1.json
  def update
    respond_to do |format|
      if @project.update(project_params)
        format.html { redirect_to project_url(@project), notice: "Project was successfully updated." }
        format.json { render :show, status: :ok, location: @project }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /projects/1 or /projects/1.json
  def destroy
    # @project.destroy!

    # respond_to do |format|
    #   format.html { redirect_to projects_url, notice: "Project was successfully destroyed." }
    #   format.json { head :no_content }
    # end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_project
      @project = Project::Project.find(params[:id])
    end

    def user_setting_param
      params.require(:project_user_setting).permit(:settings_id, :notification_enabled)
    end

  # Only allow a list of trusted parameters through.
  def project_params
    params.require(:project_project).permit(:token, :name)
  end

    def check_permissions
      role = Project::RoleUserOnProject.find_by(project_id: params[:id], user_id: current_user.id)
      if not role 
        redirect_to '/'
      end
    end

end
