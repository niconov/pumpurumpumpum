class SalesController < ApplicationController
  before_action :set_sale, only: %i[ show edit update destroy ]

  # GET /sales or /sales.json
  def index
    @sales = Sale.where(project_id: current_user.project_id).select("distinct realizationreport_id").distinct.pluck('realizationreport_id')
  end 

  # GET /sales/1 or /sales/1.json
  def show
    @rid = params['id']
    sales = Sale.where(project_id: current_user.project_id, realizationreport_id: @rid)
    @operations = sales.select('distinct supplier_oper_name').distinct.pluck('supplier_oper_name')
    update_operations @operations
    @sas = sales.select('distinct sa_name').distinct.pluck('sa_name')
    @by_ops = {}
    sel_text = 'sa_name, count(id), sum(ppvz_for_pay) as ppvz_for_pay, SUM(delivery_amount) as delivery_amount, SUM(delivery_rub) as delivery_rub, sum(penalty) as penalty'
    @operations.map do |op|
      @by_ops[op] = sales.where(supplier_oper_name: op).select(sel_text).group('sa_name').all
    end
    @result = {}
    sel_text = 'supplier_oper_name, count(id), sum(ppvz_for_pay) as ppvz_for_pay, SUM(delivery_amount) as delivery_amount, SUM(delivery_rub) as delivery_rub, sum(penalty) as penalty'
    # sales = Sale.where(project_id: current_user.project_id, realizationreport_id: @rid)
    @profit_global = 0.0
    @sas.map do |sa|
      nm = Nm::Nm.find_by supplier_article: sa
      @result[sa] = {'total' => 0.0, "count" => 0.0,'ops' => []}
      @result[sa]['ops'] = sales.where(sa_name: sa).select(sel_text).group('supplier_oper_name').all
      @result[sa]['ops'] = @result[sa]['ops'].map do |op|
        sop = SalesOperation.find_by name: op['supplier_oper_name']
        ['ppvz_for_pay', 'delivery_rub', 'penalty'].map do |op_name|
          if op[op_name]
            op[op_name] = op[op_name] * sop.factor 
            @result[sa]['total'] = @result[sa]['total'] + op[op_name]
          end
        end
        if op.supplier_oper_name == 'Продажа'
          @result[sa]['count'] = @result[sa]['count'] + op.count
          @result[sa]['price'] = (@result[sa]['total'] / @result[sa]['count']).round(2) 
          @result[sa]['profit'] = (@result[sa]['count'] * (@result[sa]['price'] - nm.total_cost)).round(2) 
          @profit_global += @result[sa]['profit']
        end
      op
      end
    end
    # ap @result

  end

  # GET /sales/new
  def new
    @sale = Sale.new
  end

  # GET /sales/1/edit
  def edit
  end

  # POST /sales or /sales.json
  def create
    @sale = Sale.new(sale_params)

    respond_to do |format|
      if @sale.save
        format.html { redirect_to sale_url(@sale), notice: "Sale was successfully created." }
        format.json { render :show, status: :created, location: @sale }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @sale.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sales/1 or /sales/1.json
  def update
    respond_to do |format|
      if @sale.update(sale_params)
        format.html { redirect_to sale_url(@sale), notice: "Sale was successfully updated." }
        format.json { render :show, status: :ok, location: @sale }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @sale.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sales/1 or /sales/1.json
  def destroy
    @sale.destroy!

    respond_to do |format|
      format.html { redirect_to sales_url, notice: "Sale was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sale
      @sale = Sale.find_by(realizationreport_id: params[:id])
      redirect_to '/' if @sale.project_id != current_user.project_id
    end

    # Only allow a list of trusted parameters through.
    def sale_params
      params.require(:sale).permit(:date_from)
    end

    def update_operations(ops)
      ops.map do |op|
        sop = SalesOperation.find_by name: op
        if not sop
          sop = SalesOperation.new name: op
          sop.factor = -1
          sop.save
        end
      end
    end
end
