class SchedulerController < ApplicationController

	def main
		if current_user.id == 1
			@tasks = ScheduleTemplate.order(:name).all
		else
			@tasks = ScheduleTemplate.order(:name).where(only_admin: false).all
		end
		@tasks = @tasks.map do |template|
			# task = task_find_or_create_by template
			# task = task_find_or_create_by template
			# ap task
			{
				"template" => template,
				"task" => get_task_by_template(template)
			}
		end

    if current_user.id == 1
  		@jobs = Sidekiq::Cron::Job.all 
  		@jobs.map do |j|
  			if j.args[0] == 1
  				# ap j.args
  				j.destroy
  			end
  		end
			@jobs = @jobs.sort_by {|i| i.name}
    end

	end
 
	def task_action
		# WbUpdateAdvertsStatsJob.perform_inline(current_user.project_id)
		template = ScheduleTemplate.find_by(id: params[:id])
		has_access = true

		if template.only_admin == true
			has_access = current_user.id == 1
		else 
			has_access = true
		end
		# ap has_access
		# ap template
		if template and has_access
			task = task_find_or_create_by template
			# ap task
			case params[:task_action]
			when 'enable'
				task.enable!
				event_ui "#{template.name} Задача поставлена в план"
			when 'disable'
				task.destroy
				event_ui "#{template.name} Задача удалена из очереди"
			when 'enque'
				task.enque!
				event_ui "#{template.name} Задача запущена"
			end
		end
		redirect_to scheduler_main_path
	end

	private

	def task_find_or_create_by(template)
		task = get_task_by_template template
		if task == nil
			name = template.only_one ? template.name : "#{template.name} #{current_user.project_id}"
			if template.period_minute >= 24*60
				period = "every #{template.period_minute/60}d"
			elsif template.period_minute >=60 and template.period_minute < 24*60 
				period = "every #{template.period_minute/60}h"
			else
				period = "every #{template.period_minute}m"
			end
			# if template.only_one then source = 'schedule' else source = 'dynamic' end
			source = 'schedule'
			job = Sidekiq::Cron::Job.create(source: source, name: name, cron: period, class: template.job_name, args: current_user.project_id, status: 'disabled', queue: template.queue)

			task = get_task_by_template template
			ap task
		end
		return task
	end

	def get_task_by_template(template) 
		Sidekiq::Cron::Job.all.find {|i| i.klass == template.job_name and i.args[0] == current_user.project_id }
		
	end

		
end
