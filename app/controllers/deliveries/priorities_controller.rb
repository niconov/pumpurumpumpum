class Deliveries::PrioritiesController < ApplicationController
  def index
    sel = "
      warehouse_priorities.warehouse_web_id,
      time ,
      point_id ,
      name ,
      belongs_wb 
    "
    where = 'belongs_wb = true'
    order = {time: :asc}
    @priorities = Warehouse::Priority.left_joins(:warehouse_wb).select(sel).where(where).order(order).all
    @priorities = @priorities.uniq {|i| [i.point_id, i.warehouse_web_id] }
    @priorities = @priorities.group_by {|i| i.point_id}
    # 1/0
  end
end

