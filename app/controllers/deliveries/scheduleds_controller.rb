class Deliveries::ScheduledsController < ApplicationController
  before_action :set_deliveries_scheduled, only: %i[ show edit update destroy ]

  # GET /deliveries/scheduleds or /deliveries/scheduleds.json
  def index
    @deliveries_scheduleds = Deliveries::Scheduled.all
  end

  # GET /deliveries/scheduleds/1 or /deliveries/scheduleds/1.json
  def show
  end

  # GET /deliveries/scheduleds/new
  def new
    @deliveries_scheduled = Deliveries::Scheduled.new
  end

  # GET /deliveries/scheduleds/1/edit
  def edit
  end

  # POST /deliveries/scheduleds or /deliveries/scheduleds.json
  def create
    @deliveries_scheduled = Deliveries::Scheduled.new(deliveries_scheduled_params)

    respond_to do |format|
      if @deliveries_scheduled.save
        format.html { redirect_to deliveries_scheduled_url(@deliveries_scheduled), notice: "Scheduled was successfully created." }
        format.json { render :show, status: :created, location: @deliveries_scheduled }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @deliveries_scheduled.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /deliveries/scheduleds/1 or /deliveries/scheduleds/1.json
  def update
    respond_to do |format|
      if @deliveries_scheduled.update(deliveries_scheduled_params)
        format.html { redirect_to deliveries_scheduled_url(@deliveries_scheduled), notice: "Scheduled was successfully updated." }
        format.json { render :show, status: :ok, location: @deliveries_scheduled }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @deliveries_scheduled.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /deliveries/scheduleds/1 or /deliveries/scheduleds/1.json
  def destroy
    @deliveries_scheduled.destroy!

    respond_to do |format|
      format.html { redirect_to deliveries_scheduleds_url, notice: "Scheduled was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_deliveries_scheduled
      @deliveries_scheduled = Deliveries::Scheduled.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def deliveries_scheduled_params
      params.require(:deliveries_scheduled).permit(:period, :warehouse_id)
    end
end
