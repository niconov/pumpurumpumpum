class Deliveries::ScheduledItemsController < ApplicationController
  before_action :set_deliveries_scheduled_item, only: %i[ show edit update destroy ]

  # GET /deliveries/scheduled_items or /deliveries/scheduled_items.json
  def index
    @deliveries_scheduled_items = Deliveries::ScheduledItem.all
  end

  # GET /deliveries/scheduled_items/1 or /deliveries/scheduled_items/1.json
  def show
  end

  # GET /deliveries/scheduled_items/new
  def new
    @deliveries_scheduled_item = Deliveries::ScheduledItem.new
  end

  # GET /deliveries/scheduled_items/1/edit
  def edit
  end

  # POST /deliveries/scheduled_items or /deliveries/scheduled_items.json
  def create
    @deliveries_scheduled_item = Deliveries::ScheduledItem.new(deliveries_scheduled_item_params)

    respond_to do |format|
      if @deliveries_scheduled_item.save
        format.html { redirect_to deliveries_scheduled_item_url(@deliveries_scheduled_item), notice: "Scheduled item was successfully created." }
        format.json { render :show, status: :created, location: @deliveries_scheduled_item }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @deliveries_scheduled_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /deliveries/scheduled_items/1 or /deliveries/scheduled_items/1.json
  def update
    respond_to do |format|
      if @deliveries_scheduled_item.update(deliveries_scheduled_item_params)
        format.html { redirect_to deliveries_scheduled_item_url(@deliveries_scheduled_item), notice: "Scheduled item was successfully updated." }
        format.json { render :show, status: :ok, location: @deliveries_scheduled_item }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @deliveries_scheduled_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /deliveries/scheduled_items/1 or /deliveries/scheduled_items/1.json
  def destroy
    @deliveries_scheduled_item.destroy!

    respond_to do |format|
      format.html { redirect_to deliveries_scheduled_items_url, notice: "Scheduled item was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_deliveries_scheduled_item
      @deliveries_scheduled_item = Deliveries::ScheduledItem.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def deliveries_scheduled_item_params
      params.require(:deliveries_scheduled_item).permit(:nm_id, :qty, :deliveries_sheduled_id)
    end
end
