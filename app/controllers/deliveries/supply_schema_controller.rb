class Deliveries::SupplySchemaController < ApplicationController

  def schema
    
  end

  def stats

    # where = "project_id = '#{current_user.project_id}' and period < '#{Time.now.beginning_of_week}'"
    # orders = OrderStatDistrict.where(where).order(period: :desc).all
    # Deliveries::SupplySchema.where(project_id: current_user.project_id).all.destroy_all
    # wh_priorities = {}
    # @orders = orders.map do |o|
    #   ap o
    #   district = o.district
    #   ap district
    #   if not wh_priorities[district.id]
    #     wh_priorities[district.id] = o.nearest_active_warehouse 
    #   end
    #   wh = wh_priorities[district.id].first
    #   opts = {
    #     project_id: current_user.project_id, 
    #     nm_id: o.nm_id, 
    #     warehouse_id: wh.id
    #   }
    #   susch = Deliveries::SupplySchema.find_or_create_by opts
    #   susch.count = susch.count + o.count
    #   susch.count_local = susch.count_local + o.count if o.is_local
    #   # if district.id == wh.district.id
    #   #   ap "=" * 100
    #   #   ap district
    #   #   ap wh.district
    #   #   ap "=" * 100
    #   # end
    #   susch.save
    #   o
    # end

    # @schema = Deliveries::SupplySchema.where(project_id: current_user.project_id).all
    # @nms = Deliveries::SupplySchema.where(project_id: current_user.project_id).select('distinct nm_id').all.map { |e| e.nm }
    # @nms = @nms.sort_by {|i| [i.object, i.supplier_article] }

    # select('distinct district_id').where(project_id: current_user.project_id).all
    # @nms = OrderStatDistrict.select('distinct nm_id').where(project_id: current_user.project_id).all
    @nms = Nm::Nm.where(project_id: current_user.project_id).order(object: :asc, supplier_article: :asc).all
    @result = {}
    @orders_local_count = 0
    @orders_count = 0
    @weeks = 4
    ts = Time.now.beginning_of_week

    @districts = District.order(order_level: :desc).all
    @districts = @districts.sort_by do |district|
        sel = "sum(count_total) as count_total, sum(count_local) as count_local, 1 as iii"
        gr = "iii"
        wh = "district_id = #{district.id} and period between '#{ts - @weeks.weeks}' and '#{ts}'"
        osd = OrderStatDistrict.where(wh).select(sel).order(iii: :asc).group(gr)
        if osd != []
          -osd.first.count_total
        else
          0
        end
    end

    @totals = {}

    @district_stats = {}
    @districts.map do |district|
      @result[district.id] = {}
      @nms.map do |nm|
        # wh = {nm_id: nm.nm_id, district_id: district.id}
        sel = "sum(count_total) as count_total, sum(count_local) as count_local, 1 as iii"
        gr = "iii"
        wh = "nm_id = #{nm.nm_id} and district_id = #{district.id} and period between '#{ts - @weeks.weeks}' and '#{ts}'"
        osd = OrderStatDistrict.where(wh).select(sel).order(iii: :asc).group(gr)
        # ap osd
        @orders_local_count += osd.first.count_local if osd != []
        @orders_count += osd.first.count_total if osd != []
        @result[district.id][nm.nm_id] = osd.first
  
        @totals[nm.nm_id] = 0 if @totals[nm.nm_id] == nil
        @totals[nm.nm_id] = @totals[nm.nm_id] + osd.first.count_total if osd != []

        # @district_stats[district.id] = {total: 0, local: 0} if not @district_stats[district.id]
        # @district_stats[district.id][:local] = @district_stats[district.id][:local] + osd.first.count_local if osd != []
        # @district_stats[district.id][:total] = @district_stats[district.id][:total] + osd.first.count_total if osd != []
      end
    end

    ap @totals


    # @districts = @districts.map { |e| e.warehouse }
    # @districts = @districts.sort_by {|i| i.id }
    # # ap @schema.map { |e| e }
    # ap @warehouses
    # # 1/0
    # ap wh_priorities



  end
end

