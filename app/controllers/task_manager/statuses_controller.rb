class TaskManager::StatusesController < ApplicationController
  before_action :set_task_manager_status, only: %i[ show edit update destroy ]

  # GET /task_manager/statuses or /task_manager/statuses.json
  def index
    @task_manager_statuses = TaskManager::Status.all
  end

  # GET /task_manager/statuses/1 or /task_manager/statuses/1.json
  def show
  end

  # GET /task_manager/statuses/new
  def new
    @task_manager_status = TaskManager::Status.new
  end

  # GET /task_manager/statuses/1/edit
  def edit
  end

  # POST /task_manager/statuses or /task_manager/statuses.json
  def create
    @task_manager_status = TaskManager::Status.new(task_manager_status_params)

    respond_to do |format|
      if @task_manager_status.save
        format.html { redirect_to task_manager_status_url(@task_manager_status), notice: "Status was successfully created." }
        format.json { render :show, status: :created, location: @task_manager_status }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @task_manager_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /task_manager/statuses/1 or /task_manager/statuses/1.json
  def update
    respond_to do |format|
      if @task_manager_status.update(task_manager_status_params)
        format.html { redirect_to task_manager_status_url(@task_manager_status), notice: "Status was successfully updated." }
        format.json { render :show, status: :ok, location: @task_manager_status }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @task_manager_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /task_manager/statuses/1 or /task_manager/statuses/1.json
  def destroy
    @task_manager_status.destroy!

    respond_to do |format|
      format.html { redirect_to task_manager_statuses_url, notice: "Status was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_task_manager_status
      @task_manager_status = TaskManager::Status.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def task_manager_status_params
      params.require(:task_manager_status).permit(:title, :color)
    end
end
