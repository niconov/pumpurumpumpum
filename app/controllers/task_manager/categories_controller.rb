class TaskManager::CategoriesController < ApplicationController
  before_action :set_task_manager_category, only: %i[ show edit update destroy ]

  # GET /task_manager/categories or /task_manager/categories.json
  def index
    @task_manager_categories = TaskManager::Category.all
  end

  # GET /task_manager/categories/1 or /task_manager/categories/1.json
  def show
  end

  # GET /task_manager/categories/new
  def new
    @task_manager_category = TaskManager::Category.new
    @task_manager_category.project_id = current_user.project_id
  end

  # GET /task_manager/categories/1/edit
  def edit
  end

  # POST /task_manager/categories or /task_manager/categories.json
  def create
    @task_manager_category = TaskManager::Category.new(task_manager_category_params)
    @task_manager_category.project_id = current_user.project_id

    respond_to do |format|
      if @task_manager_category.save
        format.html { redirect_to task_manager_category_url(@task_manager_category), notice: "Category was successfully created." }
        format.json { render :show, status: :created, location: @task_manager_category }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @task_manager_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /task_manager/categories/1 or /task_manager/categories/1.json
  def update
    respond_to do |format|
      if @task_manager_category.update(task_manager_category_params)
        format.html { redirect_to task_manager_category_url(@task_manager_category), notice: "Category was successfully updated." }
        format.json { render :show, status: :ok, location: @task_manager_category }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @task_manager_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /task_manager/categories/1 or /task_manager/categories/1.json
  def destroy
    @task_manager_category.destroy!

    respond_to do |format|
      format.html { redirect_to task_manager_categories_url, notice: "Category was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_task_manager_category
      @task_manager_category = TaskManager::Category.where(project_id: current_user.project_id).find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def task_manager_category_params
      params.require(:task_manager_category).permit(:title, :project_id)
    end
end
