class TaskManager::TasksController < ApplicationController
  before_action :set_task_manager_task, only: %i[ show edit update destroy ]

  # GET /task_manager/tasks or /task_manager/tasks.json
  def index
    @task_manager_tasks = TaskManager::Task.all
  end

  # GET /task_manager/tasks/1 or /task_manager/tasks/1.json
  def show
  end

  # GET /task_manager/tasks/new
  def new
    @task_manager_task = TaskManager::Task.new
    @task_manager_task.project_id = current_user.project_id
  end

  # GET /task_manager/tasks/1/edit
  def edit
  end

  # POST /task_manager/tasks or /task_manager/tasks.json
  def create
    @task_manager_task = TaskManager::Task.new(task_manager_task_params)
    @task_manager_task.project_id = current_user.project_id
    status = TaskManager::Status.find_by_title 'Новая'
    @task_manager_task.status_id = status.id
    respond_to do |format|
      if @task_manager_task.save
        format.html { redirect_to task_manager_task_url(@task_manager_task), notice: "Task was successfully created." }
        format.json { render :show, status: :created, location: @task_manager_task }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @task_manager_task.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /task_manager/tasks/1 or /task_manager/tasks/1.json
  def update
    respond_to do |format|
      if @task_manager_task.update(task_manager_task_params)
        format.html { redirect_to task_manager_task_url(@task_manager_task), notice: "Task was successfully updated." }
        format.json { render :show, status: :ok, location: @task_manager_task }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @task_manager_task.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /task_manager/tasks/1 or /task_manager/tasks/1.json
  def destroy
    @task_manager_task.destroy!

    respond_to do |format|
      format.html { redirect_to task_manager_tasks_url, notice: "Task was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_task_manager_task
      @task_manager_task = TaskManager::Task.where(project_id: current_user.project_id).find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def task_manager_task_params
      params.require(:task_manager_task).permit(:text, :title, :category_id, :project_id, :status_id)
    end
end
