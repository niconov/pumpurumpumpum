class AppGlobalConstantsController < ApplicationController
  before_action :set_app_global_constant, only: %i[ show edit update destroy ]
  before_action :check_permissions

  # GET /app_global_constants or /app_global_constants.json
  def index
    @app_global_constants = AppGlobalConstant.all
  end

  # GET /app_global_constants/1 or /app_global_constants/1.json
  def show
  end

  # GET /app_global_constants/new
  def new
    @app_global_constant = AppGlobalConstant.new
  end

  # GET /app_global_constants/1/edit
  def edit
  end

  # POST /app_global_constants or /app_global_constants.json
  def create
    @app_global_constant = AppGlobalConstant.new(app_global_constant_params)

    respond_to do |format|
      if @app_global_constant.save
        format.html { redirect_to app_global_constant_url(@app_global_constant), notice: "App global constant was successfully created." }
        format.json { render :show, status: :created, location: @app_global_constant }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @app_global_constant.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /app_global_constants/1 or /app_global_constants/1.json
  def update
    respond_to do |format|
      if @app_global_constant.update(app_global_constant_params)
        format.html { redirect_to app_global_constant_url(@app_global_constant), notice: "App global constant was successfully updated." }
        format.json { render :show, status: :ok, location: @app_global_constant }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @app_global_constant.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /app_global_constants/1 or /app_global_constants/1.json
  def destroy
    @app_global_constant.destroy!

    respond_to do |format|
      format.html { redirect_to app_global_constants_url, notice: "App global constant was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_app_global_constant
      @app_global_constant = AppGlobalConstant.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def app_global_constant_params
      params.require(:app_global_constant).permit(:name, :value, :value_type)
    end

    def check_permissions
      redirect_to '/' if current_user.id != 1
    end
end
