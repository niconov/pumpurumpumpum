class Advert::AdvertsController < ApplicationController
  before_action :authenticate_user!

  before_action :set_advert, only: %i[render_async_cluster_stats cluster_set_forced_state clear_bad_keywords flush_excluded_keyword start stop pause show edit update destroy update_from_api]
  before_action :check_permissions, only: %i[render_async_cluster_stats cluster_set_forced_state clear_bad_keywords flush_excluded_keyword start stop pause show edit update destroy update_from_api ]

  # GET /adverts or /adverts.json
  def index
    statuses = [-1, 7, 8]
    @result = {
      enabled: Advert::Advert.where(project_id: current_user.project_id).where.not(status: statuses).order(name: :asc).all,
      disabled: Advert::Advert.where(project_id: current_user.project_id).where(status: statuses).order(name: :asc).all,
    }
  end

  def render_async_cluster_stats
    cluster = Advert::Cluster.find(params[:cluster_id])
    render partial: "advert/adverts/show_table_advert_clusters", locals:{ cluster: cluster }
  end

  def advert_action
    ad = Advert::Advert.find_by project_id: current_user.project_id, advert_id: params[:id]
    if ad
      case params[:advert_action]
      when 'manager_start'
        ad.manager_enabled = true
        if ad.save
          event_ui "Менеджер РК #{ad.name} включен"
        end
      when 'manager_stop'
        ad.manager_enabled = false
        if ad.save
          event_ui "Менеджер РК #{ad.name} отключен"
        end
      when 'run'
        ad.start
        event_ui "Запрос для РК #{ad.name} отправлен: запуск"
      when 'stop'
        ad.pause
        event_ui "Запрос для РК #{ad.name} отправлен: пауза"
      when 'close'
        ad.stop
        event_ui "Запрос для РК #{ad.name} отправлен: завершение"
      when 'budget_replenish'
        ad.budget_replenish
        event_ui "Запрос для РК #{ad.name} отправлен: обновление бюджета"
      end
      else

    end

  end

  def cluster_set_forced_state
    ap params
    ap cluster_params
    cluster = Advert::Cluster.find(params[:cluster_id])
    ap cluster
    if cluster.update cluster_params
      event_ui "Установлен принудительный статус по фразе \"#{cluster.cluster_name}\" АРК #{@advert.name}: #{cluster.forced_state_humanize}"
    end
  end

  def set_cpm
    ad = Advert::Advert.find_by project_id: current_user.project_id, id: params[:id]
    if ad
      ad.cpm = params['advert_advert']['cpm'].to_i
      res = ad.set_cpm
      if res.status == 200 
          event_ui "Ставка установлена в ВБ"
        unless ad.save
          event_ui "Произошла ошибка"
        end
      else
        event_ui "Произошла установки статуса #{res.status} #{JSON.parse res.body}"
      end
    else
      event_ui "РК не найдена"
    end
  end

  def flush_excluded_keyword
    if @advert.flush_excluded! 
      event_ui "РК #{@advert.advert_id} #{@advert.name}: Ограничения ключевых фраз удалены"
    end
  end

  def clear_bad_keywords
    if @advert.exclude!
      event_ui "РК #{@advert.advert_id} #{@advert.name}: плохие ключевые фразы удалены"
    end
  end

  def set_daily_budget_max
    ad = Advert::Advert.find_by project_id: current_user.project_id, id: params[:id]
    if ad
      ad.budget_max = params['advert_advert']['budget_max'].to_i
      if ad.save
        event_ui "Бюджет установлен"
      else
        event_ui "Произошла ошибка"
      end
    else
      event_ui "РК не найдена"
    end
  end

  def update_from_api
    @advert.update_from_api
    redirect_to @advert
  end

  def start
    @advert.start
    redirect_back(fallback_location: root_path)
  end

  def pause
    @advert.pause
    redirect_back(fallback_location: root_path)
  end

  def stop
    @advert.stop
    redirect_back(fallback_location: root_path)
  end


  # GET /adverts/1 or /adverts/1.json
  def show
    @advert_stats = Advert::Stat.where(advert_id: @advert.advert_id, project_id: current_user.project_id).order(period: :desc).where.not(sum: 0.0)
    @advert_total_sum = @advert_stats.inject(0.0) {|sum, i| sum += i.sum}
    @advert_total_views = @advert_stats.inject(0.0) {|sum, i| sum += i.views}
    @days = @advert_stats.count
    if @days == 0
      @advert_avg_views = ''
    else
      @advert_avg_views = (@advert_total_views / @days).round
    end
    @ks_derv = Advert::StatsAuto.where(advert_id: @advert.advert_id).select('avg(views) as avg, sum(views) as sum, count(*) as count')[0]
    @keyword_stat = Advert::StatsAuto.where(project_id: current_user.project_id, advert_id: @advert.advert_id).order(views: :desc)
    if @ks_derv and @ks_derv.avg != nil
      @min_views = @ks_derv.avg/4
      @keyword_stat = @keyword_stat.where("views > #{@min_views}")
    else 
      @min_views = 0
    end
    profit = Advert::Param.where(advert_id: @advert.advert_id).map do |i| 
      nm = Nm::Nm.find_by nm_id: i.nm_id
      nm.profit_worst
    end
    @profit_worst = profit.min
    # nm.profit_worst

  end

  # GET /adverts/new
  def new
    @advert = Advert::Advert.new
  end

  # GET /adverts/1/edit
  def edit
  end

  # POST /adverts or /adverts.json
  def create
    @advert = Advert.new(advert_params)

    respond_to do |format|
      if @advert.save
        format.html { redirect_to advert_url(@advert), notice: "Advert was successfully created." }
        format.json { render :show, status: :created, location: @advert }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @advert.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /adverts/1 or /adverts/1.json
  def update
    respond_to do |format|
      if @advert.update(advert_params)
        format.html { redirect_to advert_url(@advert), notice: "Advert was successfully updated." }
        format.json { render :show, status: :ok, location: @advert }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @advert.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /adverts/1 or /adverts/1.json
  def destroy
    @advert.destroy!

    respond_to do |format|
      format.html { redirect_to adverts_url, notice: "Advert was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_advert
      @advert = Advert::Advert.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def advert_params
      params.require(:advert).permit(:user_id, :project_id, :advert_id, :name, :type_id, :max_cpm, :min_place, :bid_current, :search_query, :status, :param_value, :param_name, :timestamps)
    end

    def cluster_params
      params.require(:advert_cluster).permit(:forced_state)
    end

    def check_permissions
      if @advert.project_id != current_user.project_id
        redirect_to '/'         
      end
    end
end
