class Advert::PromotionManagersController < ApplicationController
  before_action :set_promotion_manager, only: %i[ set_status setup show edit update destroy ]
  before_action :check_permissions, only: %i[ set_status setup show edit update destroy ]

  # GET /promotion_managers or /promotion_managers.json
  def index
    Nm::Nm.where(project_id: current_user.project_id).map do |nm|
      ap nm
      pm = Advert::PromotionManager.find_or_create_by project_id: current_user.project_id, nm_id: nm.nm_id
      pm.enabled = false if pm.enabled == nil
      pm.budget_daily = 0.0 if pm.budget_daily == nil
      pm.save
    end
    @promotion_managers = Advert::PromotionManager.where(project_id: current_user.project_id).order(enabled: :desc).all
    @promotion_managers.map do |i|
      nm = Nm::Nm.find_by nm_id: order.nm_id
      ap nm
      if nm
        ads = Advert::Advert.where.not(status: [-1, 7]).where(name: "#{nm.supplier_article}::PumPurum")
        if ads.count == 1
          ad = ads.first        
          i.advert_id = ad.advert_id
          i.save
        end
        if ads.count > 1
          ap "Many adverts with name #{nm.supplier_article}::PumPurum"
          ads.map {|d| ap d.as_json}
        end
      end
    end
    @promotion_manager = []
    @promotion_managers = Advert::PromotionManager.where(project_id: current_user.project_id).order(enabled: :desc).map do |i|
      {
        pm: i,
        nm: Nm::Nm.find_by(nm_id: order.nm_id),
        ad: Advert::Advert.find_by(advert_id: i.advert_id)
      }
    end
    @promotion_managers = @promotion_managers.select {|i| i[:nm] != nil }
    @promotion_managers = @promotion_managers.sort_by {|i| [i[:nm].object, i[:nm].supplier_article]}
  end

  # GET /promotion_managers/1 or /promotion_managers/1.json
  def show
  end

  def set_status
    advert = Advert::Advert.find_by advert_id: @promotion_manager.advert_id
    if advert.configured_for_promotion_manager? @promotion_manager
      @promotion_manager.enabled = params[:status] == 'true'
      @promotion_manager.save
    end
    redirect_back(fallback_location: root_path)

  end

  def setup_all
    Advert::PromotionManager.where(project_id: current_user.project_id).map do |pm|
      pm.setup
    end    
    redirect_to promotion_managers_url
  end

  # GET /promotion_managers/new
  def new
    @promotion_manager = Advert::PromotionManager.new
  end
  
  def setup
    @promotion_manager.setup
    redirect_to edit_advert_promotion_manager_path @promotion_manager
  end

  # GET /promotion_managers/1/edit
  def edit
    @advert = Advert::Advert.find_by advert_id: @promotion_manager.advert_id

    # ap @promotion_manager
    # api.advert_create_with_manager advert.advert_id, @promotion_manager
    # ap api.advert_auto_get_nms advert.advert_id
    # advert_id = api.advert_create_with_manager @promotion_manager
  end

  # POST /promotion_managers or /promotion_managers.json
  def create
    @promotion_manager = Advert::PromotionManager.new(promotion_manager_params)

    respond_to do |format|
      if @promotion_manager.save
        format.html { redirect_to promotion_manager_url(@promotion_manager), notice: "Promotion manager was successfully created." }
        format.json { render :show, status: :created, location: @promotion_manager }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @promotion_manager.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /promotion_managers/1 or /promotion_managers/1.json
  def update
    respond_to do |format|
      if @promotion_manager.update(promotion_manager_params)
        format.html { redirect_to promotion_manager_url(@promotion_manager), notice: "Promotion manager was successfully updated." }
        format.json { render :show, status: :ok, location: @promotion_manager }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @promotion_manager.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /promotion_managers/1 or /promotion_managers/1.json
  def destroy
    @promotion_manager.destroy!

    respond_to do |format|
      format.html { redirect_to promotion_managers_url, notice: "Promotion manager was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private

  def setup_item(promotion_manager)
    
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_promotion_manager
    @promotion_manager = Advert::PromotionManager.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def promotion_manager_params
    params.require(:promotion_manager).permit(:nm_id, :advert_id, :budget_daily, :enabled)
  end

  def check_permissions
    if current_user.project_id != @promotion_manager.project_id
      redirect_to '/'
    end
  end
end
