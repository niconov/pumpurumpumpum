class Tariff::TariffsActiveController < ApplicationController
  before_action :authenticate_user!

  def index
    tariffs = Tariff::Tariff.order(warehouseName: :asc)
    @tariffs = tariffs.map do |t|
      {
        t: t,
        tp: t.tariff_periodic,
        ta: Tariff::Active.find_by(project_id: current_user.project_id, tariff_id: t.id),
      }
    end
    @tariffs = @tariffs.sort_by do |i| 
      if i[:ta]
        is_active = i[:ta].active ? 0 : 1
      else
        is_active = 1
      end
      # is_active = i[:ta] and i[:ta].active ? 1 : 0
      [is_active, i[:tp].warehouseName] 
    end
  end

  def set
    wh = Tariff::Tariff.find(params[:id])
    wha = Tariff::Active.find_or_create_by(project_id: current_user.project_id, tariff_id: wh.id)
    wha.active = true
    wha.save
    redirect_to tariff_tariffs_active_index_path 
 
  end

  def unset
    wha = Tariff::Active.find_by(project_id: current_user.project_id, tariff_id: params[:id].to_i)
    if wha
      wha.delete
    end
    redirect_to tariff_tariffs_active_index_path 
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    # def set_warehouse_active
    #   @warehouse_active = TariffActive.find(params[:id])
    # end

    # Only allow a list of trusted parameters through.
    # def warehouse_active_params
    #   params.require(:warehouse_active).permit(:id)
    # end
end
