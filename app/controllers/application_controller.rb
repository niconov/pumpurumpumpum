class ApplicationController < ActionController::Base

    before_action :authenticate_user!
	before_action :configure_permitted_parameters, if: :devise_controller?

    def event_ui(text, status = 'success')
        Event.create project_id: current_user.project_id, text: text, subject: 'uionly', date: Time.now, status: status
    end

protected

    def configure_permitted_parameters
        # devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:name, :email, :password) }
        devise_parameter_sanitizer.permit(:account_update, keys: [:bidder_enabled, :autoresponder_enabled, :token, :email, :password, :current_password])
    end

end
