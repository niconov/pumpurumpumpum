class SalesOperationsController < ApplicationController
  before_action :only_admin, only: %i[ show edit update destroy ]
  before_action :set_sales_operation, only: %i[ show edit update destroy ]

  # GET /sales_operations or /sales_operations.json
  def index
    @sales_operations = SalesOperation.all
  end

  # GET /sales_operations/1 or /sales_operations/1.json
  def show
  end

  # GET /sales_operations/new
  def new
    @sales_operation = SalesOperation.new
  end

  # GET /sales_operations/1/edit
  def edit
  end

  # POST /sales_operations or /sales_operations.json
  def create
    @sales_operation = SalesOperation.new(sales_operation_params)

    respond_to do |format|
      if @sales_operation.save
        format.html { redirect_to sales_operation_url(@sales_operation), notice: "Sales operation was successfully created." }
        format.json { render :show, status: :created, location: @sales_operation }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @sales_operation.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sales_operations/1 or /sales_operations/1.json
  def update
    respond_to do |format|
      if @sales_operation.update(sales_operation_params)
        format.html { redirect_to sales_operation_url(@sales_operation), notice: "Sales operation was successfully updated." }
        format.json { render :show, status: :ok, location: @sales_operation }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @sales_operation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sales_operations/1 or /sales_operations/1.json
  def destroy
    @sales_operation.destroy!

    respond_to do |format|
      format.html { redirect_to sales_operations_url, notice: "Sales operation was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sales_operation
      @sales_operation = SalesOperation.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def sales_operation_params
      params.require(:sales_operation).permit(:name, :factor)
    end

    def only_admin
      redirect_to '/' if current_user.id != 1
    end
end
