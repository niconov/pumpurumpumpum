class Sale::ReportsController < ApplicationController
  before_action :check_permissions, only: %i[ show report_csv ]

  # GET /sale_reports or /sale_reports.json
  def index
    @sale_reports = Sale::ReportList.where(project_id: current_user.project.id).order(date_from: :desc, realizationreport_id: :desc)
  end

  def report_csv
    file = Sale::Report.report_csv @rr_id
    sr = Sale::Report.find_by realizationreport_id: @rr_id
    filename = "Финотчет ВБ #{@rr_id} #{sr.date_from.strftime('%Y-%m-%d')} #{sr.date_to.strftime('%Y-%m-%d')}.csv"
    send_data file, :type => "text/plain", :filename=>filename, :disposition => 'attachment'
    # redirect_to sale_report_path @rr_id
  end

  # GET /sale_reports/1 or /sale_reports/1.json
  def show
    srs = Sale::Report.where(realizationreport_id: @rr_id )
    @operations = srs.select(:supplier_oper_name).distinct.pluck(:supplier_oper_name).sort
    @doc_type_names = srs.select(:doc_type_name).distinct.pluck(:doc_type_name).sort
    @period_start = srs.first.date_from
    @period_end = srs.first.date_to
    @item = srs.last
    @count = srs.count
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def check_permissions
      @rr_id = params[:id]
      @sale_report = Sale::Report.find_by(realizationreport_id: params[:id])
    end
end
