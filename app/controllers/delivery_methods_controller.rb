class DeliveryMethodsController < ApplicationController
  before_action :authenticate_user!

  before_action :set_delivery_method, only: %i[ show edit update destroy ]

  # GET /delivery_methods or /delivery_methods.json
  def index
    @delivery_methods = DeliveryMethod.where(project_id: current_user.project_id).order(tariff_id: :asc).all
  end

  # GET /delivery_methods/1 or /delivery_methods/1.json
  def show
  end

  # GET /delivery_methods/new
  def new
    @delivery_method = DeliveryMethod.new
    @tariffs = get_tariffs
  end

  # GET /delivery_methods/1/edit
  def edit
    @tariffs = get_tariffs 
  end

  # POST /delivery_methods or /delivery_methods.json
  def create
    @delivery_method = DeliveryMethod.new(delivery_method_params)
    @delivery_method.project_id = current_user.project_id

    respond_to do |format|
      if @delivery_method.save
        format.html { redirect_to delivery_methods_url, notice: "Delivery method was successfully created." }
        format.json { render :show, status: :created, location: @delivery_method }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @delivery_method.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /delivery_methods/1 or /delivery_methods/1.json
  def update
    respond_to do |format|
      if @delivery_method.update(delivery_method_params)
        format.html { redirect_to delivery_methods_url, notice: "Delivery method was successfully updated." }
        format.json { render :show, status: :ok, location: @delivery_method }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @delivery_method.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /delivery_methods/1 or /delivery_methods/1.json
  def destroy
    @delivery_method.destroy!

    respond_to do |format|
      format.html { redirect_to delivery_methods_url, notice: "Delivery method was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private

    def get_tariffs
      t = Tariff::Tariff.order(id: :asc).all.uniq {|i| i.warehouseName }
      t.sort_by {|i| i.warehouseName}
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_delivery_method
      @delivery_method = DeliveryMethod.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def delivery_method_params
      params.require(:delivery_method).permit(:period, :name, :tariff_id, :price, :time_delivery)
    end
end
