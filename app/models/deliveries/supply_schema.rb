class Deliveries::SupplySchema < ApplicationRecord
	# belongs_to :warehouse, class_name: "Warehouse::Warehouse"
	belongs_to :nm, foreign_key: :nm_id, primary_key: :nm_id


	def self.district_stock(project, district, nm)
		district = District.find_by(name: "южный") if district.name == "северо-кавказский"
		warehouses = district.warehouses.map {|i| i.id }
		warehouses.uniq!
		stocks = Stock::Stock.where(nm_id: nm.nm_id, warehouse_id: warehouses)
		stocks = stocks.inject(0) {|s,i| s += i.quantity}
		stocks
	end

	def self.count_by(project, nm)
		sel = "sum(count) as count"
		gr = "count"
		wh = { nm_id: nm.nm_id, project_id: project.id }
		dss = Deliveries::SupplySchema.select(sel).where(wh).group(gr)
		dss.inject(0) {|s, i| s+= i.count}
	end

	def self.calc_logistic_params(district)

		{mass: 0.0, volume: 0.0}
	end

	# def self.district_demands(project, district, nm)
	# 	districts_grouped = ["южный", "северо-кавказский"]
	# 	if districts_grouped.include? district.name
	# 		districts = districts_grouped.map {|i| District.find_by name: i }
	# 	else
	# 		districts = [district]
	# 	end
	# 	districts_ids = districts.map &:id
	# 	sel = "sum(demands) as sum"
	# 	gr = "demands"
	# 	wh = {district_id: districts_ids, nm_id: nm.nm_id}
	# 	res = OrderStatDistrict.where(wh).select(sel).group(gr)
	# 	res.inject(0) {|s, i| s += i.sum}
	# end

end









# 