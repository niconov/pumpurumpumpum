class Deliveries::PickupPoint < ApplicationRecord

	def wh_list_by_priority
		Warehouse::Priority.where(point_id: id).where.not(time: nil).order(time: :asc)
	end

end
