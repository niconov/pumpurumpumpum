class Sale::Report < ApplicationRecord


  def self.operations_add
    ['Продажа'].inject('') {|s, i| s += " '#{i}'"}
    ['Продажа']
    # .inject('') {|s, i| s += " '#{i}'"}
  end

  def self.operations_sub
    []
  end

  def self.clean
    Sale::Report.where(date_from: nil, date_to: nil, create_dt: nil).destroy_all
  end

  def self.report(realizationreport_id, doc_type_name = nil, supplier_oper_name = nil)
    Sale::Report.clean
    if doc_type_name == nil or supplier_oper_name == nil
      full_stats = true
      wh = {realizationreport_id: realizationreport_id}
    else
      full_stats = false
      wh = {realizationreport_id: realizationreport_id, doc_type_name: doc_type_name, supplier_oper_name: supplier_oper_name}
    end

    sel = "
      nm_id, 
      sum(penalty) as penalty, 
      sum(additional_payment) as additional_payment, 
      sum(rebill_logistic_cost) as rebill_logistic_cost, 
      sum(quantity) as quantity, 
      sum(retail_amount) as retail_amount, 
      sum(delivery_amount) as delivery_amount, 
      sum(return_amount) as return_amount, 
      sum(delivery_rub) as delivery_rub, 
      sum(ppvz_for_pay) as ppvz_for_pay,
      doc_type_name,
      supplier_oper_name
    "
    # puts sel
    
    gr = [:nm_id, :doc_type_name, :supplier_oper_name]
    
    srs = Sale::Report.select(sel).where(wh).group(gr) 

    srs1= srs[0]
    srs_g_nm = srs.group_by {|i| i.nm_id }
    result = []
    # ap srs_g_nm
    srs_g_nm.map do |nm_id, items|
      res = {       
        "nm_id": nm_id,                         
        "penalty": 0.0,
        "additional_payment": 0.0,
        "rebill_logistic_cost": 0.0,
        "quantity": 0.0,
        "retail_amount": 0.0,
        "delivery_amount": 0.0,
        "return_amount": 0.0,
        "delivery_rub": 0.0,
        "ppvz_for_pay": 0.0,
      }
      items.map! do |i|
        factor = -1
        factor =  1 if i.doc_type_name == "Продажа"
        # factor =  0 if i.supplier_oper_name == "Возмещение издержек по перевозке"

        fields_all = ["quantity", "penalty", "retail_amount",  "additional_payment", "rebill_logistic_cost", "delivery_rub", "ppvz_for_pay"]
        fields_sup = ["quantity", "delivery_amount", "return_amount"]
        (fields_all - fields_sup).map do |f|
          # ap f
          # ap res
          res[f.to_sym] += factor * i.send(f) if i.send(f) != nil
        end

        fields_operation_factor_sup = {
          "Возмещение издержек по перевозке" => {
            "quantity" => 0
          },
          "Логистика" => {
            "delivery_amount" => 1,
            "return_amount" => 1
          },
          "Возврат" => {
            "quantity" => 1,
          },
        }

        fields_sup.map do |f|
        sup_factor = -1
        sup_factor = 1 if i.doc_type_name == "Продажа"
          
          if fields_operation_factor_sup[i.supplier_oper_name] and full_stats
            if fields_operation_factor_sup[i.supplier_oper_name][f]
              sup_factor = fields_operation_factor_sup[i.supplier_oper_name][f]
            end
          end
          # ap f
          # ap sup_factor
          res[f.to_sym] += sup_factor * i.send(f) if i.send(f) != nil
        end

      end
      # ap res
      result.push res
      # srs_g_nm[nm_id] = i_by_nm_id.group_by {|i| }
      # items[0]
    end

    # ap result
    izd = result.inject(0.0) {|s, i| s += i[:rebill_logistic_cost]}
    sales = result.inject(0.0) {|s, i| s += i[:retail_amount]}
    ppvz = result.inject(0.0) {|s, i| s += i[:ppvz_for_pay]}
    delivery_rub = result.inject(0.0) {|s, i| s += i[:delivery_rub]}
    # ap 'Издержки'
    # ap izd
    # ap 'Продажи сумма'
    # ap sales
    # ap 'К перечислению за товар'
    # ap ppvz
    # ap "Логистика"
    # ap delivery_rub
    # ap "итого"
    # ap izd + ppvz + delivery_rub

    # result_first = result.pop
    # ap result_first
    # ap result.inject(result_first) {|s, i| ap s; s.keys.map {|k| s[k] = s[k] + i[k] } }

    # srs = srs_g_nm.sort_by do |sr|
    #   nm = Nm::Nm.find_by nm_id: sr.nm_id 
    #   if nm
    #     [nm.object, nm.supplier_article]
    #   else
    #     ['', '']
    #   end
    # end
    # keys = []
    # srs.map do |sr|
    #   sr.attributes.keys.map do |k|
    #     keys.push if not keys.include?
    #   end
    # end
    # keys.map do |k|
    #   srs.all? {|sr| not [0.0, nil].include? sr.send(k) }
    # end
    # srs
    ;;
    # ap srs1
    result.map do |r|
      i = Sale::Report.new
      r.keys.map do |k|
        i.send "#{k}=", r[k]
      end
      i
    end
    
    
  end

  def self.report_csv(realizationreport_id)
    data = Sale::Report.report realizationreport_id
    if data.count > 0
      attribute_names = data.first.attribute_names
      attribute_names.map! do |attribute|
        if data.first.send attribute
          attribute
        else
          nil
        end
      end  
      attribute_names.compact!
      st = ";"
      text = "object#{st}supplier_article#{st}"
      text += attribute_names.join(st) + "\n"
      data = data.sort_by do |i|
        nm = Nm::Nm.find_by nm_id: i.nm_id
        if nm
          [nm.object, nm.supplier_article]
        else
          ["0","0"]
        end
      end
      data.each do |item|
        arr = attribute_names.map {|attribute| item.send attribute }
        nm = Nm::Nm.find_by nm_id: item.nm_id
        if nm
          obj = nm.object.strip
          sa = nm.supplier_article.strip
        else 
          obj = 'Неопознанный'
          sa = 'Неопознанный'
        end
        arr.unshift sa 
        arr.unshift obj
        text += arr.join(st) + "\n"
      end
      text.gsub! ".", ","
      text.encode 'cp1251'
    end
  end

  def self.fetch(project)
    url = 'https://statistics-api.wildberries.ru/api/v1/supplier/reportDetailByPeriod'
    stop = true
    while stop
      sr_last = Sale::Report.where(project_id: project.id).order(date_from: :asc, rrd_id: :asc).last
      if sr_last
        ts = sr_last.date_from
        rrd_id = sr_last.rrd_id
      else
        ts = Time.now - (3 * 365).days 
        rrd_id = 0
      end
      puts "Sale::Report.fetch Project ##{project.id}: fetching with dateFrom #{ts.strftime('%d.%m.%Y %H:%M:%S')} and rrd_id #{rrd_id}"
      res = project.api.get(url) do |req|
        req.params['dateFrom'] = ts.beginning_of_day.strftime "%Y-%m-%d"
        req.params['dateTo'] = ts + (10 * 365).days
        # req.params['limit'] = 20000  
        req.params['rrdid'] = rrd_id
      end
      res = JSON.parse res.body
      keys = Set[]
      # ap res
      if res == nil
        # ap "stop"
        stop = false
        break 
      end
      if res != nil 
        res.map {|r| keys.merge r.keys}
      
        res.map! do |i|
          if i.keys != keys
            (keys - i.keys).map do |k|
              i[k] = nil
            end
          end
          i
        end
        res_count = res.count
        ts_counter = Time.now
        counter_global = 0
        puts "Sale::Report.fetch Project ##{project.id}: fetched #{res_count}"
        current_report_id = 0
        current_rrd_id = 0
        # ap res[1..10]
        batch_n = 0
        batch_size = 500
        res.each_slice(batch_size) do |res_batch|
          batch_n += 1
          puts "Sale::Report.fetch Project ##{project.id}: insert #{batch_n} batch. #{batch_size* batch_n} records "
          res_batch.map! do |r|
            r.keys.map do |k|
              r[k.underscore] = r.delete k
            end
            r['project_id'] = project.id
            r
          end
          # ap res[1..10]
          Sale::Report.insert_all res_batch
        end

      end
    end
    Sale::ReportList.update_list project
    true
  end
  

end
