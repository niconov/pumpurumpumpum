class Sale::ReportList < ApplicationRecord

  def self.update_list(project)
    rr_ids = Sale::Report.where(project_id: project.id).select(:realizationreport_id).distinct.pluck(:realizationreport_id)
    rr_ids.map do |rr_id|
      srl = Sale::ReportList.find_or_create_by project_id: project.id, realizationreport_id: rr_id
      sr = Sale::Report.find_by realizationreport_id: rr_id
      srl.date_from = sr.date_from
      srl.date_to = sr.date_to
      srl.create_dt = sr.create_dt
      srl.save
    end
  end
end
