module Api
	extend ActiveSupport::Concern

  # def self.included(base)
  #   base.extend(ClassMethods)
  # end

  # module ClassMethods
	# 	def api(project)
	# 		headers = { 'Content-Type' => 'application/json', 'Authorization' => project.token }
  # 		Faraday.new(headers: headers)
	# 	end
  # end

	def api
		headers = { 'Content-Type' => 'application/json', 'Authorization' => project.token }
    Faraday.new(headers: headers)
	end

	def project
		if self.class == Project::Project
			self
		else
			Project::Project.find self.project_id
		end
	end

end