class Tariff::Active < ApplicationRecord

	belongs_to :tariff, class_name: "Tariff::Tariff"

	after_commit :update_nm


	def warehouse
		tariff.warehouse
	end

	private

	def update_nm
		Nm::Nm.where(project_id: project_id).all.map {|i| i.update_log_out_price }
	end

end
