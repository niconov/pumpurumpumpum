class Tariff::Tariff < ApplicationRecord
	belongs_to :tariff_periodic, class_name: "Tariff::Periodic"
	# , foreign_key: :tariff_id, primary_key: :id

	def self.calc_by_order(order)
		# ap order
		nm = Nm::Nm.find_by nm_id: order.nm_id
		wh_name = Warehouse::Warehouse.name_mapping order.warehouse
		tar = Tariff::Tariff.find_by(warehouseName: wh_name)
		# ap tar
		if order.income_id == 0
			return tar.calc_by_volume nm.volume
		else
			income = Income::Income.find_by income_id: order.income_id
      if not income
        return tar.calc_by_volume nm.volume, "Короб"
      end
			if income.box_type == "Короб"
				return tar.calc_by_volume nm.volume, "Короб"
			elsif income.box_type == "Монопалета"
				return tar.calc_by_volume nm.volume, "Монопалета"
			else
				ap order.income_id
				return nil
				raise "Не удалось определить тип поставки №#{order.income_id}"
			end
		end
		return tar.calc_by_volume nm.volume
	end

	def calc_by_nm(nm, box = "Короб")
		# ap nm.as_json
		calc_by_volume(nm.volume, box)
	end

	def calc_by_volume(volume, box = "Короб")
		volume_base = AppGlobalConstant.find_by name: 'volume_base'
		if volume
			vol = volume.ceil
		else
			vol = 0.0
		end
		if box == "Короб"
			t_base = tariff_periodic.boxDeliveryBase
			t_add  = tariff_periodic.boxDeliveryLiter
		else
			t_base = tariff_periodic.palletDeliveryValueBase
			t_add  = tariff_periodic.palletDeliveryValueLiter
		end			
		# BUG! Ждем ответа вб, как определять в таком случае стоимость логистики
		if t_base == nil or t_add == nil
			if box == "Короб"
				t_base = tariff_periodic.palletDeliveryValueBase
				t_add  = tariff_periodic.palletDeliveryValueLiter
			else
				t_base = tariff_periodic.boxDeliveryBase
				t_add  = tariff_periodic.boxDeliveryLiter
			end
		end
		total = t_base 
		# ap t_base
		# ap vol
		# ap volume_base.get_value
		# ap t_add

		if not vol or not volume_base.get_value or not t_add
			ap "=" * 100
			ap "Error while calc_profit"
			ap 'vol'
			ap vol
			ap 'volume_base.get_value'
			ap volume_base.get_value
			ap 't_base'
			ap t_base
			ap 't_add'
			ap t_add
			ap 'tariff::tariff'
			ap self.as_json
			ap "Tariff::Periodic"
			ap tariff_periodic.as_json
		end
		total += (vol - volume_base.get_value) * t_add if vol > volume_base.get_value
		total = total
		total
	end

	def warehouse
		Warehouse::Warehouse.find_by name: warehouseName
	end

	def district
		warehouse.district
		
	end

	def self.fetch(periods_count = 3 )
		project = Project::Project.find(6)
		url_box = 'https://common-api.wildberries.ru/api/v1/tariffs/box'
		url_pal = 'https://common-api.wildberries.ru/api/v1/tariffs/pallet'
		period_start = Time.now.beginning_of_day - 1.day
		periods_count.times do
			puts "Tariff::Tariff fetch on date: #{period_start}"

			st_box = 0
			while st_box != 200
				res_box = project.api.get(url_box) do |req|
					req.params['date'] = period_start.strftime "%Y-%m-%d"
				end
				st_box = res_box.status
				sleep 25 if st_box != 200
			end
			# ap res_box.status
			res_box = JSON.parse res_box.body
			tariffs_box = res_box['response']['data']['warehouseList']
			tariffs_box.map do |i|
				wh = i['warehouseName']
				t =  Tariff::Periodic.find_or_create_by warehouseName: wh, period: period_start
				t.boxDeliveryBase = i['boxDeliveryBase'].gsub(',', '.').to_f
				t.boxDeliveryLiter = i['boxDeliveryLiter'].gsub(',', '.').to_f
				t.boxStorageBase = i['boxStorageBase'].gsub(',', '.').to_f
				t.boxStorageLiter = i['boxStorageLiter'].gsub(',', '.').to_f
				if not t.save
					ap t
					ap t.errors
				end
			end

			st_pal = 0
			while st_pal != 200
				res_pal = project.api.get(url_pal) do |req|
					req.params['date'] = period_start.strftime "%Y-%m-%d"
				end
				st_pal = res_pal.status
				sleep 25 if st_pal != 200
			end

			# ap res_pal.status
			res_pal = JSON.parse res_pal.body
			tariffs_pal = res_pal['response']['data']['warehouseList']
			tariffs_pal.map do |i|
				wh = i['warehouseName']
				t =  Tariff::Periodic.find_or_create_by warehouseName: wh, period: period_start
				t.palletDeliveryValueBase = i['palletDeliveryValueBase'].gsub(',', '.').to_f
				t.palletDeliveryValueLiter = i['palletDeliveryValueLiter'].gsub(',', '.').to_f
				t.palletStorageValueExpr = i['palletStorageValueExpr'].gsub(',', '.').to_f
				ap t if t.palletDeliveryValueBase == nil
				ap i if t.palletDeliveryValueBase == nil
				if not t.save
					ap t
					ap t.errors.full_messages
				end			
				# ap t
			end
			# ap tariffs_pal
			period_start += 1.day
			sleep 1
		end

		Tariff::Periodic.where(period: Time.now.beginning_of_day).map do |tp|
			tt = Tariff::Tariff.find_or_create_by warehouseName: tp.warehouseName
			if tp.palletDeliveryValueBase  == nil 
				ap tp 
				ap tt
				ap tp.warehouseName
			end
			if not tt
				ap 'cant find Tariff::Tariff'
				ap tt 
				ap tp
			end
			tt.tariff_periodic_id = tp.id
			ap tp
			if not tt.save
				ap tt.errors.full_messages
			end		
		end
	end



end
