class Warehouse::FromWeb < ApplicationRecord

	def update_warehouse_connection
		# ap self
		name_mapped = Warehouse::Warehouse.name_mapping name
		wh = Warehouse::Warehouse.find_by name: name_mapped
		# ap wh
		# ap self
		if wh
			self.warehouse_id = wh.id
			wh.warehouse_web_id = self.warehouse_web_id
			wh.save
			self.save
		else
			# ap self
		end
		# 1/0
	end

	def warehouse
		Warehouse::Warehouse.find_by warehouse_web_id: warehouse_web_id
	end

end
