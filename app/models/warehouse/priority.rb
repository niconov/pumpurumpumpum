class Warehouse::Priority < ApplicationRecord
	has_one :point, class_name: 'Deliveries::PickupPoint', foreign_key: :id, primary_key: :point_id
	has_one :warehouse_wb, class_name: 'Warehouse::FromWeb', foreign_key: :warehouse_web_id, primary_key: :warehouse_web_id
	has_one :warehouse, class_name: 'Warehouse::Warehouse', foreign_key: :warehouse_web_id, primary_key: :warehouse_web_id
	
	def district
		warehouse.district
	end
end
