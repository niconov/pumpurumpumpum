class Warehouse::Warehouse < ApplicationRecord
	# has_one :district, class_name: "District", foreign_key: :id, primary_key: :district_id

	# before_update :backtrace

	# def self.update_mapping
	# 	name_mapping = "Белая Дача"
	# 	name =	"Белая дача"
	# 	Warehouse::Mapping.create name_mapped: name_mapping, name: name
	# end

	def self.name_mapping(name)
		wm = Warehouse::Mapping.find_by name_mapped: name
		if wm then wm.name else name end
	end

	def district
		District.where(id: self.district_id).first
	end

	# def backtrace
	# 	if self.changes[:district_id]
	# 		ap caller_locations
	# 	end

		
	# end
		
end
