class OrderStatDistrict < ApplicationRecord

    def district
        District.where(district_id: self.district_id).first
    end

    def district_pickup_point
        # ap district.name
        district_name = district.location.gsub ' федеральный округ', ''
        ap district_name.downcase.strip
        Deliveries::PickupPoint.where(name: district_name.downcase.strip).first
    end

    def wh_list_priorities
        whs = district_pickup_point.wh_list_by_priority.map {|i| {prority: i, warehouse_wb: i.warehouse_wb}}
        whs = whs.select {|i| i[:warehouse_wb].belongs_wb == true}
        whs = whs.uniq {|i| i[:warehouse_wb].name}
        whs
    end

  def self.sum_by_nm_4w(project, nm)
    ts = Time.now.beginning_of_week
    wh = "project_id = '#{project.id}' and nm_id = '#{nm.nm_id}' and period between '#{ts - 4.weeks}' and '#{ts}'"
    OrderStatDistrict.where(wh).inject(0) {|s, i| s += i.count_total}
  end

  def nearest_active_warehouse
    district_name = district.location.gsub ' федеральный округ', ''
    dws = District.where(name: district_name).all
    warehouses_local = dws.map do |dw|
        Warehouse::Warehouse.find_by district_id: dw.id
    end

    wh_active_whs = Tariff::Active.where(project_id: project_id).map do |ta|
        ta.warehouse
    end
    wh_active_whs = wh_active_whs.sort {|i| i.id}

    wh_active = wh_list_priorities.select do |i|
        wh = i[:warehouse_wb].warehouse
        if wh
            res = wh_active_whs.any? do |z| 
                # print "- #{z.name}\n"
                wh.warehouse_web_id == z.warehouse_web_id 
            end
            print "#{wh.name} #{res}\n"
            ap wh.as_json
            res = wh_active_whs.any? do |z| 
                print "- #{z.name}\n"
                ap z.as_json
                wh.warehouse_web_id == z.warehouse_web_id 
            end
            res
        else
            false
        end
    end

    wh_active.map { |e| e[:warehouse_wb].warehouse }
  end

end
