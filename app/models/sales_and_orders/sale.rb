class Sale < ApplicationRecord

    def self.update_all_api(project)
        url = 'https://statistics-api.wildberries.ru/api/v1/supplier/reportDetailByPeriod'
        has_new_rows = true
        rrdid = 0
        ts_start = Time.now.beginning_of_week - 7.days
        sales_count = 0
        while has_new_rows
            response = project.api.get(url) do |req|
                req.params['rrdid'] = rrdid
                req.params['limit'] = 10000
                req.params['dateFrom'] = ts_start
                req.params['dateTo'] = ts_start + 7.days
                # ap req.params
            end
            

            if response.status == 200 
                if response.body != nil
                    sales = JSON.parse response.body
                    sales.map! do |s| 
                        s['project_id'] = project.id
                        s['uid'] = "#{s['realizationreport_id']}_#{s['rrd_id']}"
                        # ['date_from', 'date_to', 'create_dt', 'order_dt', 'sale_dt'].map do |k|
                        #   s[k] = Time.parse s[k]
                        # end
                        i = Sale.find_or_create_by uid: s['uid']
                        i.update s
                        i.save
                        s
                    end
                    ap sales.last
                    has_new_rows = false if sales == []
                    sales_count += sales.count

                    ap "Fetched #{sales.count}/#{sales_count}"
                    rrdid = sales.last['rrd_id'] if sales != []
                # Sale.insert_all sales, unique_by: ['uid']
                # , unique_by: ["uid", "id"]
            end
            else
                sleep 60
            end
        end

    end


end
