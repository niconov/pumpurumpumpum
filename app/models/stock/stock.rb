class Stock::Stock < ApplicationRecord
	def self.calc_for_nm(nm)
		Stock::Stock.where(nm_id: nm.nm_id.to_s).where.not(warehouse_id: 31).all.inject(0) { |sum, e| sum += e.quantity }
	end

	def get_tariff_current
		wh = Warehouse::Warehouse.name_mapping warehouse_name
		t = Tariff::Tariff.find_by(warehouseName: wh)
		t
	end

	def nm
		Nm::Nm.find_by nm_id: nm_id
	end

	def self.quantity_by_nm(project, nm)
		ts = Time.now.beginning_of_week
		wh = "project_id = '#{project.id}' and nm_id = '#{nm.nm_id}'"
    Stock::Stock.where(wh).where.not(warehouse_id: 31).inject(0) {|s, i| s += i.quantity}
	end





end
