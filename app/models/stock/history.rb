class Stock::History < ApplicationRecord

  def self.paid_storage_on_date(nm, date)
    sh = Stock::History.where(nm_id: nm.nm_id, date: date)
    sh.inject(0.0) {|s,i| s += i.warehouse_price }
  end

  def self.set_types_incomings
    incomings = Stock::History.select(:calc_type).pluck(:calc_type)
    incomings = Stock::History.select(:gi_id).pluck(:gi_id)
    income_types = []
    incomings.map do |income_id|
      sh = Stock::History.find_by gi_id: income_id
      unless income_types.include? sh.calc_type
        income_types.push sh.calc_type
        if sh.calc_type.include? 'паллет'
          Income::Income.where(income_id: income_id).update_all(box_type: "Монопалета")
        else
          Income::Income.where(income_id: income_id).update_all(box_type: "Короб")
        end
      end
    end
  end

  def self.stock_on_date(project, nm, date)
		stocks = Stock::History.where(project_id: project.id, nm_id: nm.nm_id, date: date)
		stocks.inject(0) {|m, i| m += i.barcodes_count}
	end

  def self.fetch(project, ts = Time.now.beginning_of_day, period = 1)
    body = {
      type: 'paid_storage',
      params: {
        dateFrom: ts.strftime("%Y-%m-%d"),
        dateTo: (ts + period.days).strftime("%Y-%m-%d"),
        diviser: 1
      }
    }
    # ap body
    url = 'https://statistics-api.wildberries.ru/api/v1/delayed-gen/tasks/create'
    st0 = 0
    max_attempts = 10
    attempt = 0
    while (st0/100).ceil != 2 and attempt < max_attempts 
      attempt += 1
      res = project.api.post(url, body.to_json)
      st0 = res.status
      if (st0/100).ceil != 2
        sleep_period = 60
        print "Stock::History: sleep #{sleep_period} (report query). Status [#{st0}] #{(st0/100)}, attempt [#{attempt}]\n" 
        ap JSON.parse res.body if st0 != 429
        sleep sleep_period 
      end
    end
    if res.status == 201
      body = JSON.parse res.body
      id = body['data']['taskId']
      sleep 1
      url2 = 'https://statistics-api.wildberries.ru/api/v1/delayed-gen/tasks/download'
      body = {id: id}
      st = 0
      attempt = 0
      while (st/100).ceil != 2 or attempt > max_attempts
        attempt += 1
        res = project.api.get(url2) do |req| 
          req.body = body.to_json
        end
        st = res.status
        if (st/100).ceil != 2
        sleep_period = 20
        print "Stock::History: sleep #{sleep_period} (report fetch). Status [#{st}] #{(st/100)}, attempt [#{attempt}]\n" 
          sleep sleep_period 
        end
      end
      result = JSON.parse res.body
      result.map do |r|
        item = Stock::History.find_or_create_by date: r['date'], barcode: r['barcode'], gi_id: r['giId'], project_id: project.id
        r.keys.map do |key|
          r[key.underscore] = r.delete key
        end
        item.update r
      end

    end

  end
end
