class Order::Order < ApplicationRecord

  def self.quantity_by_day(project, nm, ts1, ts2)
		wh = "project_id = '#{project.id}' and nm_id = '#{nm.nm_id}' and datetime between '#{ts1}' and '#{ts2}'"
		sel = "count(id) as quantity, date_trunc('day', datetime) as period"
    items = Order::Order.where(wh).select(sel).group(:period)
		items = items.group_by {|i| i.period}
		items.keys.map {|k| items[k] = items[k].inject(0) {|s,i| s += i.quantity} }
		items
	end

  def calc_profit
    com = Comission.calc_by_order self
    tar = Tariff::Tariff.calc_by_order self
    nm = Nm::Nm.find_by nm_id: self.nm_id
    nm.cost = 0 if not nm.cost
    nm.cost_log_in = 0 if not nm.cost_log_in 
    nm.cost_log_out = 0 if not nm.cost_log_out 
    nm.cost_package = 0 if not nm.cost_package 
    nm.cost_packaging = 0 if not nm.cost_packaging  

    profit = total - nm.cost - nm.cost_log_in - nm.cost_log_out - nm.cost_package - nm.cost_packaging - tar - com
    return profit
  end

  def self.count_on_date(project, nm, ts)
		wh = "project_id = '#{project.id}' and nm_id = '#{nm.nm_id}' and datetime between '#{ts}' and '#{ts + 1.day}'"
    Order::Order.select('(id)').where(wh).count
  end

  def total
    self.price * (100 - self.discount) / 100
  end

  def update_warehouses_id
    warehouse_name = Warehouse::Warehouse.name_mapping self.warehouse
    warehouse = Warehouse::Warehouse.find_by name: warehouse_name
    self.warehouse_id = warehouse.id
    # ap self.warehouse_id
    ap self.save
  end

  def self.district_stats_update
    # Order::Order.update_local_data_all
    total_orders = 0
    Project::Project.order(id: :asc).all.map do |project|
      ap "Обноалвяем статистику для #{project.id}"
      project_id = project.id

      sel   = "warehouse_id, warehouse, date_trunc('week', datetime) as period, count(id) as cnt, nm_id, district_id"
      grp   = "period, district_id, nm_id, warehouse_id, warehouse"
      ord   = "nm_id desc, period asc"
      where = "project_id = #{project_id} and district_id is not Null and order_type = 'Клиентский' and canceled = False"

      OrderStatDistrict.where(project_id: project.id).all.destroy_all
      orders_by_month = Order::Order.select(sel).group(grp).order(ord).where(where)
      orders_by_month.map do |order|

        wh = Warehouse::Warehouse.find_by id: order.warehouse_id
        if not wh 
          ap "Order::Order.district_stats_update Не найден склад для заказа:"
          ap order
        end

        opts = {}
        opts[:nm_id] = order.nm_id
        opts[:district_id] = order.district_id
        opts[:period] = order.period
        opts[:project_id] = project.id

        if ['', nil].include? order.district_id
          ap "Ошибка, не найден district_id" 
          ap order
        end

        if order.district_id == nil or order.district_id == 1
          ap order
        end

        order_stat = OrderStatDistrict.find_or_create_by opts
        order_stat.nm_id = order.nm_id
        order_stat.district_id = order.district_id
        order_stat.period = order.period
        order_stat.project_id = project.id

        is_local = wh.district_id == order.district_id ? true : false

        order_stat.count_total  = order_stat.count_total + order.cnt
        order_stat.count_local = order_stat.count_local + order.cnt if is_local
        unless order_stat.save
          ap order_stat.errors.messages
        end
        total_orders += order.cnt

      end
    end
  end


  def self.update_demands(project)


    ts = Time.now.beginning_of_week
    district_supply_mapping = {}
    project.supplied_districts.map { |i| district_supply_mapping[i.id] = i.district_alias } 

    project.order_district_stats.select(:district_id).distinct.pluck(:district_id).map do |district_id|
      if district_supply_mapping[district_id] == nil
        district = District.find(district_id)
        district_to_supply = District.find_nearest_for_district project, district.district_alias
        district_supply_mapping[district_id] = district_to_supply.district_alias
      end
    end

    # district_supply_mapping.map do |k,v|
    #   ap "#{District.find(k).name} -> #{v.name}"
    # end


    nm_ids = project.order_district_stats.select(:nm_id).distinct.pluck(:nm_id)
    wh = "period between '#{ts - 4.weeks}' and '#{ts}'"
    Deliveries::SupplySchema.where("project_id = #{project.id}").destroy_all
    nm_stats = Hash.new 0
    project.order_district_stats.where(wh).map do |osd|
      district = district_supply_mapping[osd.district_id]
      opts = {
        district_id: district.id,
        nm_id: osd.nm_id,
        project_id: project.id
      }
      # ap district.name
      schema = Deliveries::SupplySchema.find_or_create_by opts
      schema.count = schema.count + osd.count_total
      nm = Nm::Nm.find_by nm_id: osd.nm_id
      nm_stats[nm.supplier_article] = nm_stats[nm.supplier_article] + osd.count_total 
      unless schema.save
        ap schema.errors.messages
      end
    end
    # ap district_supply_mapping
    ap nm_stats



    # <Deliveries::SupplySchema:0x00007fa87d8f86a8 id: nil, nm_id: nil, project_id: nil, count: 0, created_at: nil, updated_at: nil, district_id: nil> 

    # district_ids.map do |district_id|

    #     nm_ids.map do |nm_id|
    #         osd_to_redistribution = OrderStatDistrict.find_by nm_id: nm_id, project_id: project.id, district_id: district_id
    #         # ap osd_to_redistribution
    #         ts = Time.now.beginning_of_week
    #         wh = "period between '#{ts - 4.weeks}' and '#{ts}' and nm_id = #{nm_id} and project_id = '#{project.id}' and district_id = '#{district_to_supply.id}'"
    #         sel = "sum(count_total) as sum, 1 as a"
    #         gr = "a"
    #         osd_to_supply = OrderStatDistrict.where(wh).select(sel).group(gr).inject(0) {|s,i| s += i.sum }
    #         ap osd_to_supply

    #         if osd_to_redistribution
    #             osd_to_supply.demands = osd_to_supply.demands + osd_to_redistribution.count_total 
    #             unless osd_to_supply.save
    #                 ap "Не смогли сохранить osd: update_demands"
    #             end
    #         end
    #     end
    # end
    ;;; '' ;;;
  end


  def self.fetch(project)
    url = 'https://statistics-api.wildberries.ru/api/v1/supplier/orders'
    last_order = Order::Order.last_order project
    
    if last_order
      ts = last_order.last_change_date.beginning_of_day - 1.days
    else
      ts = Time.now.beginning_of_day - 365.days
    end

    print "Orders: Start fetching from #{ts}\n"
    status = 0
    ts_bench = Time.now
    while status != 200
      result = project.api.get(url) do |req|
        req.params[:dateFrom] = ts
        req.params[:flag] = 0
      end
      status = result.status
      if status != 200
        ap result
        ap JSON.parse result.body
        sleep 15 if status == 429
      end
    end
    orders = JSON.parse result.body
    print "Orders: fetched #{orders.count} (#{(Time.now - ts_bench).round(3)} seconds)\n"
    # ap orders
    cnt_total = orders.count
    # last_order = nil
    # last_order_time = nil
    # ap orders[0]
    ts_bench = Time.now
    comission_cache = {}
    warehouse_cache = {}
    district_cache = {}
    location_cache = {}

    orders.map! do |order_api|
      # order = Order::Order.find_or_create_by srid: order_api['srid'], project_id: project.id
      # ap 'prepare'
      order = Order::Order.prepare_api order_api
      # ap 'comission'
      
      cnk = "#{order_api['category']} order_api['subject']"
      if comission_cache[cnk]
        comission = comission_cache[cnk]
      else
        comission_cache[cnk] = Comission.find_by category: order_api['category'], subject: order_api['subject']
        comission = comission_cache[cnk]
      end
      order[:comission_id] = comission.id

      if warehouse_cache[order_api['warehouseName']]
        warehouse = warehouse_cache[order_api['warehouseName']]
      else
        warehouse_name = Warehouse::Warehouse.name_mapping order_api['warehouseName']
        warehouse_cache[order_api['warehouseName']] = Warehouse::Warehouse.find_by name: warehouse_name
        warehouse = warehouse_cache[order_api['warehouseName']]
      end

      order[:warehouse_id] = warehouse.id
      # ap 'district'

      if district_cache[order_api['warehouseName']]
        district = district_cache[order_api['warehouseName']]
      else
        district_cache[order_api['warehouseName']] = District.get_by_order_api order_api
        district = district_cache[order_api['warehouseName']]
      end
      order[:district_id] = district.id
      # ap 'location'

      loc_k = "#{order_api['countryName']}+#{order_api['oblastOkrugName']} #{order_api['regionName']}"
      if location_cache[loc_k]
        loc = location_cache[loc_k]
      else
        location_cache[loc_k] = Location.get_by_order_api order
        loc = location_cache[loc_k]
      end

      order[:location_id] = loc.id
      order[:country] = loc.country 
      order[:district] = loc.district_fullname 
      order[:project_id] = project.id
      # ap order.as_json
      # last_order = order 
      # if order.last_change_date.beginning_of_day != last_order_time
      #   last_order_time = order.last_change_date.beginning_of_day
      #   ap "#{last_order_time} (#{(Time.now.beginning_of_day - last_order_time.beginning_of_day)/(24*3600)})"
      # end
      # unless order.save
      #   ap "Не сохраили заказик, блин..."
      #   ap order.errors
      # end
      order
    end
    # orders_gr = orders.group_by {|i| i[:srid]}
    # orders_gr.map do |k, v|
    #   if v.count > 1
    #     ap orders_gr[k]
    #   end
    # end
    lastest_order = orders.max_by {|i| i[:last_change_date]}
    print "Orders: prepared #{orders.count} (#{(Time.now - ts_bench).round(3)} seconds). Fetched orders from #{ts} to #{lastest_order ? lastest_order[:last_change_date] : ''} \n"
    Order::Order.upsert_all orders, returning: :srid, unique_by: :index_order_orders_on_project_id_and_srid
    # ap last_order.last_change_date if last_order
    return true
  end

  def self.prepare_api(order_api)
    {
      "nm_id": order_api['nmId'],
      "datetime": order_api['date'],
      "last_change_date": order_api['lastChangeDate'],
      "article": order_api['supplierArticle'],
      "barcode": order_api['barcode'],
      "subject": order_api['subject'],
      "price": order_api['totalPrice'],
      "discount": order_api['discountPercent'],
      "warehouse": order_api['warehouseName'],
      "income_id": order_api['incomeID'],
      "canceled": order_api['isCancel'],
      "cancel_date": order_api['cancelDate'],
      "gnumber": order_api['gNumber'],
      "order_type": order_api['orderType'],
      "odid": order_api['odid'],
      "srid": order_api['srid'],
      "country": order_api['countryName'],
      "district": order_api['oblastOkrugName'],
      "region": order_api['regionName'],
      "spp": order_api['spp'],
      "price_with_disc": order_api['priceWithDisc'],
      "finished_price": order_api['finishedPrice'],
      "tech_size": order_api['techSize']

    }
  end

  private 

  def self.last_order(project)
    Order::Order.where(project_id: project.id).order(last_change_date: :asc).last
  end

end
