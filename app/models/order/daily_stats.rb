class Order::DailyStats < ApplicationRecord

  def self.build(project)
    nm_ids = project.orders.select(:nm_id).distinct.pluck(:nm_id)
    if Order::DailyStats.where(project_id: project.id).count == 0
      first_order = project.orders.order(:datetime).first
      ts = first_order.datetime.beginning_of_day
    else
      last_order_s_d = Order::DailyStats.where(project_id: project.id).order(:period).last
      ts = last_order_s_d.period.beginning_of_day
    end
    puts "Order::DailyStats.build for Project ID #{project.id} and Nms count #{nm_ids.count} beginning from date #{ts}"
    nm_ids.map do |nm_id|
      sel = "
        nm_id,
        count(id) as quantity,
        sum(price_with_disc) as sum,
        date_trunc('day', datetime) as period
      "
      wh = "nm_id = #{nm_id} and datetime > '#{ts - 1.day}'"
      gr = "nm_id, DATE_TRUNC('day', datetime)"
      orders_by_districts = project.orders.where(wh).select(sel).group(gr).map do |order|
        {
        "nm_id" => order.nm_id,
        "period" => order.period,
        "sum" => order.sum,
        "quantity" => order.quantity,
        "price" => order.sum / order.quantity,
        "project_id" => project.id
        }
      end
      u_b = [:project_id, :period, :nm_id]
      Order::DailyStats.upsert_all orders_by_districts, unique_by: u_b
    end

  end

end
