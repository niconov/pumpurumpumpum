class Order::Stat < ApplicationRecord

  def self.update_list(project)
    os_last = Order::Stat.where(project_id: project.id).order(period: :asc).last
    if not os_last
      Order::Stat.initialize_sequence(project) 
    end
    os_last = Order::Stat.where(project_id: project.id).order(period: :asc).last
    ts = os_last.period.to_time 
    ts_now = Time.now.beginning_of_day + 1.day
    days = (((ts_now-ts)/(3600*24)))
    # ap os_last
    # ap ts
    # ap ts_now
    # ap days
    # ap '-'
    days.to_i.times do |i| 
      # ap ts
      Order::Stat.fill_on_period project, ts
      ts = ts + 1.day
    end

  end

  private

  def self.fill_on_period(project, ts)
    # ts = Time.now.beginning_of_day - 2.days
    wh = "project_id = #{project.id} and datetime between '#{ts}' and '#{ts + 1.day - 1}' and order_type = 'Клиентский' "
    orders = Order::Order.where(wh)
    # ap orders
    nm_ids = orders.select(:nm_id).distinct.pluck(:nm_id)
    nm_ids.map do |nm_id|
      nm = Nm::Nm.find_by project_id: project.id, nm_id: nm_id
      if nm and nm_id
        select_now =  "avg(price) as price, avg(discount) as discount"
        select_now += ", sum((price*(100-discount)/100)) as sum"
        select_now += ", sum(finished_price) as sum_spp"
        select_now += ", count(distinct srid) as qty"
        select_now += ", max(datetime) as datetime"
        select_now += ", nm_id, comission_id, warehouse, income_id"
        gr  = [:nm_id, :comission_id, :warehouse, :income_id]

        orders_by_whs = orders.where(nm_id: nm_id).select(select_now).group(gr)
        params = {project_id: project.id, period: ts, nm_id: nm.nm_id}
        sum_spp = 0.0
        nm_cost = nm.total_cost
        order_stat = orders_by_whs.inject(Order::Stat.find_or_create_by(params)) do |os, order|
          sum_spp += order.sum_spp
          os.quantity = os.quantity + order.qty
          os.cost = os.cost + (nm_cost * order.qty)
          os.sum = os.sum + order.sum
          if not os.has_error
            begin 
              profit_per_pcs_now = order.calc_profit
              os.profit = os.profit + (profit_per_pcs_now * order.qty)
            rescue
              ap 'has_errors'
              os.has_error = true
            end
          end
          os
        end
        ads = nm.advert_total_on_date ts
        order_stat.price = order_stat.sum / order_stat.quantity
        order_stat.price_spp = sum_spp / order_stat.quantity
        order_stat.advert = ads
        order_stat.pocs = 100 * (((order_stat.profit - order_stat.advert - order_stat.storage) / (order_stat.cost + ads))  )
        order_stat.storage += Stock::History.paid_storage_on_date nm, ts
        order_stat.ros = 100 * ((order_stat.profit - order_stat.advert - order_stat.storage)/ (order_stat.sum))  
        order_stat.save
      end
    end


  end

  def self.initialize_sequence(project)
    order = Order::Order.where(project_id: project.id).order(datetime: :asc).first
    if order
      ts = order.datetime.beginning_of_day
      Order::Stat.fill_on_period project, ts
    end
  end



  # def fill_force
  # end

end
