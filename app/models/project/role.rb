class Project::Role < ApplicationRecord
	def self.admin
		Project::Role.find_by(user_role: "Администратор")
	end

	def self.list_for_inviting
		Project::Role.where.not(user_role: 'Администратор')
	end
end
