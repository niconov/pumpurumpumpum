class Project::Project < ApplicationRecord
	include Api
	has_many :nms
	has_many :tariffs, class_name: "Tariff::Active"
	has_many :orders, class_name: "Order::Order"
	has_many :events

	def author
		User.find_by(id: user_id)
	end

	def adverts
		Advert::Advert.where(project_id: id)
	end
	
	def adverts_active
		adverts.where.not(status: [-1, 4, 7])
	end

	def adverts_auto_active
		adverts_active.where(type_id: 8)
	end


	# def api
	# 	headers = { 'Content-Type' => 'application/json', 'Authorization' => self.token }
  #   Faraday.new(headers: headers)
	# end

	def self.admin
		Project::Project.find AppGlobalConstant.get "AdminProject"
	end

	def user
		author
	end

	def nms
		Nm::Nm.where(project_id: id).order(:object, :supplier_article)
	end

	def warehouses_active
		Tariff::Active.where(project_id: id).map { |i| i.warehouse }
	end

	def order_district_stats
		OrderStatDistrict.where(project_id: id)
	end

	def supplied_districts
		districts = Tariff::Active.where(project_id: id).map { |i| i.warehouse.district }
		districts = districts.map {|i| i.district_alias}
		districts = districts.sort_by {|i| i.order_level }
		districts.uniq.reverse
	end

	def is_user_can_invite(user)
		self.user_id == user.id
	end

end
