class Project::RoleUserOnProject < ApplicationRecord

	def settings
		Project::UserSetting.find_or_create_by project_id: project_id, user_id: user_id
	end

	def notifications_enabled?
		settings.notification_enabled
	end

	def user
		User.find_by_id user_id
	end

	def project
		Project::Project.find_by_id project_id
	end

	def role
		Project::Role.find_by_id project_role_id
	end

	
end
