class Location < ApplicationRecord
	def self.find_district_by_fullname(fullname)
		name = fullname.to_s.gsub(' федеральный округ', '').downcase
		District.find_by name: name
	end

	def self.get_by_order_api(order)
		stop_locs = ['', nil]
		if not [order[:country], order[:district]].any? { |i| stop_locs.include? i }
			#<Location:0x00007fd08b5f1b90 id: nil, 
			# ap '1'
			opts = {
				country: order[:country], 
				district_name: order[:district].gsub(' федеральный округ', '').downcase,
				district_fullname: order[:district], 
				region: order[:region]
			}
			loc = Location.find_or_create_by opts
			# ap loc
			# order.location_id = loc.id
			return loc
		else
			if order[:district] == '' and not stop_locs.include? order[:country]
				# ap '2'
				opts = {
					country: order[:country],
					district_name: order[:country],
					district_fullname: order[:country], 
					region: order[:region]
				}
				loc = Location.find_or_create_by opts
				# ap "loc"
				# ap loc
				# order.location_id = loc.id
				return loc
			elsif order[:country] == '' and order[:district] == '' and not stop_locs.include? order[:region]

				# ap '3'
				loc = Location.find_or_create_by region: order[:region]
				# order.location_id = loc.id
				# order.country = loc.country 
				# order.district = loc.district_fullname 
				# ap "loc order "
				# ap loc
				# ap order
				return loc
			elsif [order[:region], order[:country], order[:district]].all? { |i| stop_locs.include? i }
				# ap '4'
				opts = {
					country: 'undefined', 
					district_name: 'undefined',
					district_fullname: 'undefined',
					region: 'undefined'
				}
				loc = Location.find_or_create_by opts
				# order.location_id = loc.id
				return loc

			else				
				# ap '5'
				opts = {
					country: order[:country],
					district_fullname: order[:district], 
					region: order[:region]
				}
				ap order

				ap opts
				1/0
			end
			# order.save
		end
	end
end
