class District < ApplicationRecord
  # self.table_name = "districts"

  def warehouses
    Warehouse::Warehouse.where(district_id: id)    
  end

  def self.get_by_order_api(order_api)
    country = order_api['countryName'].downcase
    district = order_api['oblastOkrugName'].downcase
    if country == 'россия'
      district = district.gsub " федеральный округ", ""
      district = District.find_by name: district
    else
      district = District.find_by name: country
    end
    district
  end

  def self.find_nearest_for_district(project, district_id)
    warehouses_active = project.warehouses_active
    warehouses_active_web_ids = warehouses_active.map &:warehouse_web_id
    wh = {
      warehouse_web_id: warehouses_active_web_ids, 
      district_id: district_id
    }
    wps = Warehouse::Priority.order(:time).where(wh).all
    wps = wps.uniq {|wp| wp.warehouse_web_id}
    whs = wps.map {|wp| wp.warehouse}
    wh = whs[0]
    wh.district
  end

  def district_alias
    district = self
    if self.name == ''
      district = District.find_by name: 'центральный'
    end
    if self.name == 'северо-кавказский'
      district = District.find_by name: 'южный'
    end
    district
  end
  
end