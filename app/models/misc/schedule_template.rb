class ScheduleTemplate < ApplicationRecord

  scope :for_user, -> { where(only_admin: false, only_one: false) }

  def self.destroy_scheduled_all
    jobs = Sidekiq::Cron::Job.all
    jobs.map do |j|
      j.destroy
    end
  end

  def self.setup
    puts "ScheduleTemplate.setup: Setup System Jobs"
    ScheduleTemplate.where(only_one: true).map do |st|
      project_const = AppGlobalConstant.find_by name: 'AdminProject'
      project = Project::Project.find_by id: project_const.value
      st.setup project
    end

    puts "ScheduleTemplate.setup: Setup Customer Jobs"
    ScheduleTemplate.where(only_one: false).map do |st|
      Project::Project.where.not(token: nil).map do |project|
        ap project.id
        st.setup project
      end
    end
  end

  def setup(project)
      # ap self
    task = get_task project
		if task != nil
      task.destroy
    end
    job = Sidekiq::Cron::Job.create(source: 'schedule', name: gen_name(project), cron: period_text, class: self.job_name, args: project.id, status: 'enabled', queue: queue)
  end

  def get_task(project)
		Sidekiq::Cron::Job.all.find {|i| i.name == gen_name(project) and i.klass == self.job_name and i.args[0] == project.id }
  end

  def gen_name(project)
    name = only_one ? self.name : "#{self.name} #{project.id}"
  end

  def period_text
    if period_minute >= 24*60
      period = "every #{period_minute/60}d"
    elsif period_minute >=60 and period_minute < 24*60 
      period = "every #{period_minute/60}h"
    else
      period = "every #{period_minute}m"
    end
    period
  end

end
