class Event < ApplicationRecord
  after_create_commit do 
  	unless viewed
  		Turbo::StreamsChannel.broadcast_append_to "notifications.#{project_id}", target: "notifications.#{project_id}", partial: 'layouts/notifications/item', locals: { event: self }
  		# self.viewed = false
  		# self.save
  	end
  	send_tg_message
  end

  def self.event_ui(project, text, status = 'success')
    Event.create project_id: project.id, text: text, subject: 'uionly', date: Time.now, status: status
  end


	def get_object
		case subject
		when 'adverts'
			adv = Advert.find_by_id self.subject_id
		when 'nm'
			nm = Nm::Nm.find_by nm_id: self.subject_id
		else
			''
		end
	end

	def self.create_with_api_reponse(response, project, subject = nil, id = nil)
		status = (response.status / 100).round == 2 ? "success" : "error"
		opts = {project_id: project.id, subject_id: id, subject: subject, date: Time.now, status: status}
		if status == 'success'
			text = response.body == "" ? "Запрос выполнен успешно" : JSON.parse(response.body )
		else
			text = response.body == "" ? "Произошла ошибка" : JSON.parse(response.body)
		end
		opts[:text] = text
		Event.create opts
	end


	def send_tg_message
		if subject != 'uionly'
			Project::RoleUserOnProject.where(project_id: self.project_id).map do |rup|
				if rup and rup.user and rup.notifications_enabled?
					if rup.user.tg_chat_id
						Telegram.bot.send_message(chat_id: rup.user.tg_chat_id, text: text)
					end
				end
			end
		end
	end



end
