class AppGlobalConstant < ApplicationRecord
	def get_value
		case value_type
		when 'string'
			return value
		when 'float'
			return value.to_f
		else
			return value
		end
	end

	def self.get(name)
		c = AppGlobalConstant.find_by name: name
		c.get_value
	end

end
