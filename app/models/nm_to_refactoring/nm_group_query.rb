class NmGroupQuery < ApplicationRecord
	has_many :nm_group_query_item
	has_many :nm_competitor, primary_key: :id, foreign_key: :query_id
end
