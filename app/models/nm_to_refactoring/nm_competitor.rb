class NmCompetitor < ApplicationRecord
	belongs_to :nm_group_query, foreign_key: :query_id


	def prepare_from_raw
		if self.raw_card
			if self.raw_card['extended']
				if raw_card['extended']['priceU']
					self.price_rrc = raw_card['extended']['priceU'] / 100
				else
					self.price_rrc = raw_card['priceU'] / 100
				end
				if raw_card['extended']['basicPriceU']
					self.price = raw_card['extended']['basicPriceU'] / 100
				else
					self.price = raw_card['priceU'] / 100
				end
				if raw_card['extended']['salePriceU']
					self.price_with_spp = raw_card['extended']['salePriceU'] / 100 
				else
					self.price_with_spp = raw_card['salePriceU'] / 100 
				end
			else
				if raw_card['priceU']
					self.price_rrc = raw_card['priceU'] / 100
				end
				if raw_card['salePriceU']
					self.price = raw_card['salePriceU'] / 100
				end
				if raw_card['salePriceU']
					self.price_with_spp = raw_card['salePriceU'] / 100 
				end
			end
		end
		# if not self.raw_card
		# 	ap self

		# 	1/0 if not self.price_rrc
		# 	1/0 if not self.price
		# 	1/0 if not self.price_with_spp
		# end
		self.save
	end

end
