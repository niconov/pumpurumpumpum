class Price < ApplicationRecord

	def total
		i = nil
		if discount != 0
			i = price * (100 - discount) / 100
		else
			i = price
		end
		i
	end

	def self.update_from_api(project)
    response = project.api.get('https://suppliers-api.wildberries.ru/public/api/v1/info?quantity=0')
    list = JSON.parse response.body
    list.map! do |i|
      {
        'nm_id' => i['nmId'],
        'price' => i['price'],
        'discount' => i['discount'],
        'promo_code' => i['promoCode'],
      }
    end
    list.map do |i|
      db_i = Price.find_or_create_by nm_id: i['nm_id']
      db_i.price = i['price']
      db_i.discount = i['discount']
      db_i.promo_code = i['promo_code']
      db_i.save
    end
		
	end

end
