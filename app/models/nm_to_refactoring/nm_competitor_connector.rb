class NmCompetitorConnector < ApplicationRecord
	belongs_to :nm_competitor
	belongs_to :nm, primary_key: :nm_id, foreign_key: :nm_id, class_name: "Nm::Nm"
end
