class NmPosition < ApplicationRecord
	before_create :event_create
	before_update :event_update
	after_update_commit do 
		Turbo::StreamsChannel.broadcast_update_to "nm_positions", target: "nm_position.#{nm_id}", partial: 'nm/position', locals: { pos: self }
	end

	def importance_change(position = self.position, x_prev)
		importance_alpha = 2.0
		importance_beta  = 1.0

		# ap "Current pos #{position}, prev #{x_prev}"
	  delta_x = (position - x_prev).abs

	  f_x = importance_alpha / (position ** importance_beta)
	  f_y = importance_alpha / (x_prev ** importance_beta)

	  importance_change = delta_x * (f_x - f_y).abs

	  # ap "#{position} -> #{x_prev}: #{importance_change} "
	  # ap "delta #{delta_x}, fx #{f_x}, fy #{f_y}"

	  return importance_change
	end

	private


	def event_create
		if position == nil
			# ap previous
			prev = previous
			if previous
				self.position = previous.position
			end
		end
		check_changes
	end


	def event_update
		check_changes
	end

	private 

	def previous
		NmPosition.where("period < '#{period}' and nm_id = #{nm_id}").order(period: :desc).first
	end

	def check_changes
		only_for_fields = ['position']
		can_be_created = false
		text = ''
		if self.changes != {} and self.changes.keys
			nm = Nm::Nm.find_by nm_id: self.nm_id
			self.changes.map do |field, changes|
				if only_for_fields.include? field 
					if field == 'position'
            if changes[1] and changes[0]
  						field_after   = changes[1] ? changes[1] + 1 : 9999999
  						field_before  = changes[0] ? changes[0] + 1 : 9999999

  						importance = importance_change field_before, field_after
  						field_after   = changes[1] ? changes[1] + 1 : '-'
  						if id == nil
  							npp = previous
  							# ap npp
  							if npp and npp.position
  								field_before = npp.position + 1
  							else
  								field_before = '-'
  							end
  						else
  							field_before  = changes[0] ? changes[0] + 1 : '-'
  						end
  						if importance > 3 and changes[0] != '-' and field_after != field_before
  							can_be_created = true
  							text = "🔎 Позиция в поиске [#{nm.supplier_article}] #{field_after} <- #{field_before} [#{importance.round(4)}]"
  						end
            end
					end
				end
			end
			h = {
				project_id: nm.project_id,
				subject: 'nms',
				text: text,
				date: Time.now,
				priority: 0, 
				subject_id: nm.nm_id
			}
			Event.create h if can_be_created
		end
	end

end
