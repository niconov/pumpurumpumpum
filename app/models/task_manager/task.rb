class TaskManager::Task < ApplicationRecord
  belongs_to :category, class_name: "TaskManager::Category"
  belongs_to :status, class_name: "TaskManager::Status"


end
