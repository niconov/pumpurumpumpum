class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  def project
    Project::Project.find_by id: self.project_id
  end

  def roles_all
    Project::RoleUserOnProject.where(user_id: id)
  end

  def roles
    Project::RoleUserOnProject.where(user_id: id, project_id: project_id)
  end

  def access_profit
    roles.any? { |e| e.role.access_profit  }
  end



end
