class Income::Planned::Head < ApplicationRecord
  has_many :items, class_name: "Income::Planned::Item", primary_key: :id, foreign_key: :income_planned_id
	has_one :warehouse, class_name: 'Warehouse::Warehouse', foreign_key: :id, primary_key: :warehouse_id
end
