class Income::Planned::Item < ApplicationRecord
  belongs_to :income_planned_head, class_name: "Income::Planned::Head", primary_key: :id, foreign_key: :income_planned_id
  belongs_to :nm, class_name: "Nm", primary_key: :nm_id, foreign_key: :nm_id
end
