class Income::Income < ApplicationRecord

	def self.qty_on_date(project, nm, ts)
		wh = "project_id = '#{project.id}' and nm_id = '#{nm.nm_id}' and date = '#{ts}'"
    Income::Income.where(wh).inject(0) {|s, i| s += i.quantity}
	end

	def self.quantity_by_day(project, nm, ts1, ts2)
		wh = "project_id = '#{project.id}' and nm_id = '#{nm.nm_id}' and date between '#{ts1}' and '#{ts2}'"
		sel = "sum(quantity) as quantity, date_trunc('day', date) as period"
    items = Income::Income.where(wh).select(sel).order(:date).group(:date, :quantity)
		items = items.group_by {|i| i.period}
		items.keys.map {|k| items[k] = items[k].inject(0) {|s,i| s += i.quantity} }
		items
	end

	def total_cost
		nm = Nm::Nm.find_by nm_id: self.nm_id
		wh = Warehouse::Warehouse.name_mapping self.warehouse_name
		tariff = Tariff::Tariff.find_by warehouseName: wh
		if tariff
			dm = DeliveryMethod.find_by tariff_id: tariff.id
		else
			dm = nil
		end
		if nm
			if nm.unit_per_pallet != nm.unit_per_box * nm.box_per_pallet
				nm.unit_per_pallet = nm.unit_per_box * nm.box_per_pallet
				nm.save
			end
		end
		if dm and self.supplier_article != 'Неопознанный товар' and nm.unit_per_pallet
			return (dm.price / nm.unit_per_pallet).round(2)
		else
			if nm and self.supplier_article != 'Неопознанный товар' and nm.unit_per_pallet
				nm.cost_log_out
			else
				return 0.0
			end
		end
	end

end
