class Advert::PromotionManager < ApplicationRecord

	def setup
    project = Project::Project.find self.project_id
    advert = Advert::Advert.find_by(project_id: self.project_id, advert_id: self.advert_id)
    if not self.advert_id
      advert = Advert::Advert.create_with_promotion_manager self
      if advert
	      self.advert_id = advert.advert_id
	      self.save
	    end
    end
    if self.advert_id
      advert = Advert::Advert.find_by(project_id: self.project_id, advert_id: self.advert_id)
      if advert
	      advert.update_from_api 
	      ap advert.set_zones_by_promotion_manager self
	      ap advert.set_nms_by_promotion_manager self
	    end
    end
	end

end
