module Advert::Api::Budget

  module ClassMethods
		def budget_replenish_all(project)
			Advert::Advert.where(project_id: project.id, manager_enabled: true).map do |ad|
				if ad.budget_replenish
					sleep 1
				end
			end
		end
	end

	def self.included(base)
    base.extend(ClassMethods)
  end

	def budget_avg_daily
		days = 10
		ts = Time.now.beginning_of_day - days.days
		sel = 'sum(sum), count(id) as count'
		res = Advert::Stat.select(sel).where("period > '#{ts}' and advert_id = #{advert_id}").all
		sum = res.inject(0.0) {|s,i| s+= i.sum || 0} 
		cnt = res.inject(0.0) {|s,i| s+= i.count  || 0} 
		cnt > 0 ? sum / cnt : 0 
	end

	def set_cpm
		url = 'https://advert-api.wb.ru/adv/v0/cpm'
		opts = {
			"advertId" => advert_id,
			"type" => type_id,
			"cpm" => cpm.round
		}
		if not is_auto
			opts["param"] = 1/0
		end
		res = api.post url, opts.to_json
		if res.status != 200
			Event.create_with_api_reponse res, project
		end
		res
	end

	def check_balance
		self.budget = 0.0 if self.budget == nil
		opts = {project_id: project_id, subject_id: id, subject: "adverts", date: Time.now}
		b_avg = budget_avg_daily
		b_avg = b_avg.round(2)
		if b_avg > 0 and not is_low_budget
			if budget < b_avg
				opts[:text] = "РК #{advert_id} #{self.name}: Бюджет кончается. Средний расход #{b_avg}руб, текущий бюджет #{budget}"
				Event.create opts
			end
		end
		self.is_low_budget = budget < b_avg
		# WARN: не сохранять, действие в before_action! self.save
	end

	def update_budget
		url = 'https://advert-api.wb.ru/adv/v1/budget'
		res = api.get(url) do |req|
			req.params["id"] = self.advert_id
		end
		if res.status == 200
			result = JSON.parse res.body
			self.budget = result['total']
			self.save
		else
			Event.create_with_api_reponse res, project
		end
		res
	end

	def check_balance_payment
		ts_now = Time.now.beginning_of_day
		if balance_payment_period < ts_now
			self.balance_payment_period = Time.now 
			self.balance_payment_sum_on_period = 0
			self.save
		end
	end

	def budget_replenish
		if manager_enabled and budget_max and budget_max > 0
			# ap "Replenishment started"
			check_balance_payment
			sum_replenishment = budget_max - budget 
			# ap "Advert #{name} #{advert_id} Replenishment sum is #{sum_replenishment}"
			if sum_replenishment > 0 and sum_replenishment < budget_max + 500
				sum_replenishment = ((sum_replenishment / 50.0).round)*50
				if sum_replenishment < 500
					sum_replenishment = 500
				end

				budget_replenish_api sum_replenishment
				return true
			else
				return false
			end
		else
			# ap "Replenishment failed: manager #{manager_enabled}, #{budget_max} #{balance_payment_sum_on_period}"
			return false
		end
	end

	private

	def budget_replenish_api(sum_to_replenish)
		ap "budget_replenish_api #{sum_to_replenish}"
		check_balance_payment
		raise "Budget too large!" if sum_to_replenish > 500 + budget_max - balance_payment_sum_on_period
		url = 'https://advert-api.wb.ru/adv/v1/budget/deposit'
		body = {
			"sum" => sum_to_replenish,
			"type" => 1,
			"return" => true
		}

		res = api.post(url, body.to_json) do |req|
			req.params['id'] = advert_id
		end
		body = JSON.parse res.body
		if res.status == 200
			ap body
			self.budget = body['total'] 
			self.balance_payment_sum_on_period += sum_to_replenish
			self.save
			# opts = {project_id: project_id, subject_id: id, subject: "adverts", date: Time.now}
			# opts[:text] = "РК #{name} #{advert_id}: Бюджет пополнен на #{sum_to_replenish}. Текущий бюджет: #{budget}"
			# Event.create opts
			return true
		else
			ap body
			return false
		end
	end

end