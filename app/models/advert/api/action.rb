module Advert::Api::Action
	
	def stop
		url = 'https://advert-api.wb.ru/adv/v0/stop'
		res = api.get(url) { |req| req.params['id'] = advert_id }
		self.update_from_api
		if res.status == 200
			self.status = 7
			self.save
		else
			Event.create_with_api_reponse res, project
		end	
		res
	end

	def pause
		url = 'https://advert-api.wb.ru/adv/v0/pause'
		res = api.get(url) { |req| req.params['id'] = advert_id }
		self.update_from_api
		if res.status == 200
			self.status = 11
			self.save
		else
			Event.create_with_api_reponse res, project
		end	
		res
	end

	def start
		ap 'starting'
		url = 'https://advert-api.wb.ru/adv/v0/start'
		res = api.get(url) { |req| req.params['id'] = advert_id }
		ap res
		self.update_from_api
		if res.status == 200
			self.status = 9
			self.save
		else
			Event.create_with_api_reponse res, project
		end	
		res
	end


	# def set_cpm
	# 	url = 'https://advert-api.wb.ru/adv/v0/cpm'
	# 	opts = {
	# 		"advertId" => advert_id,
	# 		"type" => type_id,
	# 		"cpm" => cpm.round
	# 	}
	# 	if not is_auto
	# 		opts["param"] = 1/0
	# 	end
	# 	res = project.api.post url, opts.to_json
	# 	if res.status != 200
	# 		Event.create_with_api_reponse res, project
	# 	end
	# 	res
	# end

	# def set_zones_by_promotion_manager(promotion_manager)
	# 	url = 'https://advert-api.wb.ru/adv/v1/auto/active'
	# 	project.api.post(url) do |req|
	# 		req.params['id'] = advert_id
	# 		req.body = {
	# 			recom: false,
	# 			booster: true,
	# 			carousel: false
	# 		}.to_json
	# 	end
	# end

end