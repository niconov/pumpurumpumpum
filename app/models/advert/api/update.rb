module Advert::Api::Update
	# include Api

  def self.included(base)
    base.extend(ClassMethods)
  end

  module ClassMethods

		def stats_fetch(project)
			url = "https://advert-api.wb.ru/adv/v2/fullstats"
			advert_ids = Advert::Advert.where(project_id: project.id).where.not(status: [-1, 7, 8]).select(:advert_id).distinct.pluck(:advert_id)
			ts = Time.now.beginning_of_day
			dates = (0..4).map {|i| (ts-i.day).strftime("%Y-%m-%d")} 
			body = (advert_ids.map {|i| {"id" => i, "dates" => dates} })

			result = project.api.post url, body.to_json
			JSON.parse result.body
			
		end

		def list(project)
			url = 'https://advert-api.wb.ru/adv/v1/promotion/count'
			response = project.api.get(url)
			JSON.parse response.body
		end

		def fetch(project)
			puts "Advert::Advert.fetch for project #{project.id}"
			adverts_result = []
			adverts_api = Advert::Advert.list project
			# ap "adverts_api['adverts'].count"
			# ap adverts_api['adverts'].count
			adverts_api['adverts'].map do |adverts|
				adverts_list = adverts['advert_list']
				adverts_list.map! do |ad|
					ad['type'] = adverts['type']
					ad['status'] = adverts['status']
					ad
				end
				adverts_result += adverts_list
			end
			# ap "creating by result "
			puts "Advert::Advert.fetch fetched #{adverts_result.count} adverts"
			adverts_result.map do |advert_api|
				# ap advert_api
				params = {advert_id: advert_api['advertId'], project_id: project.id}
				ad = Advert::Advert.find_by params
				if not ad
					puts "Advert::Advert.fetch create new advert id #{advert_api['advertId']}"
					ad = Advert::Advert.find_or_create_by params
				end
				ad.type_id = advert_api['type']
				ad.status = advert_api['status']
				ad.save
			end
			# ap "update verbose"

			Advert::Advert.update_verbose_from_api project
		end

		def update_verbose_from_api(project)
			Advert::Advert.where(project_id: project.id).where.not(status: [7, -1]).each_slice(25) do |adverts|
				Advert::Advert.create_or_update_by_advert_id project, adverts
			end
		end

	end


	def update_from_api
		# ap 'update_from_api 1'
		Advert::Advert.create_or_update_by_advert_id project, advert_id
		# ap 'update_from_api 2'
		self.update_budget
		# ap 'update_from_api 3'
		# self.reload
	end

	# def update_budget
	# 	# ap 'update_budget'
  #   project = self.project
	# 	url = 'https://advert-api.wb.ru/adv/v1/budget'
	# 	res = project.api.get(url) do |req|
	# 		req.params["id"] = self.advert_id
	# 	end
	# 	if res.status == 200
	# 		result = JSON.parse res.body
	# 		budget = result['total']
	# 		save
	# 	else
	# 		Event.create_with_api_reponse res, project
	# 	end
	# 	res
	# end


	def fill_with_api_response(advert_api)

    advert_params = advert_api['params']
    self.response_raw = advert_api
		self.name = advert_api['name']
		self.advert_id = advert_api['advertId']
		self.type_id = advert_api['type']
		self.status = advert_api['status']

    if advert_params and self.status != 9
      if advert_params[0]["subjectId"]
        self.param_value = advert_params[0]["subjectId"]
      else
        self.param_value = advert_params[0][self.param_name]
      end
    end
    if advert_api['autoParams'] and advert_api['type'] == 8
    	self.cpm = advert_api['autoParams']['cpm']

    	nms = advert_api['autoParams']['nms']
    	Advert::Param.where(advert_id: self.advert_id).where.not(nm_id: nms).map { |e| e.destroy! }
	    if nms
	    	nms.map do |nm|
		    	adparam = Advert::Param.find_or_create_by(advert_id: self.advert_id, nm_id: nm)
		    	adparam.carousel = advert_api['autoParams']['active']['carousel']
		    	adparam.recom = advert_api['autoParams']['active']['recom']
		    	adparam.booster = advert_api['autoParams']['active']['booster']
		    	self.cpm = advert_api['autoParams']['cpm']
		    	adparam.save!
		    end
		  end
    end
    self.save!
	end


end