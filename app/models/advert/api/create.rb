module Advert::Api::Create

  def self.included(base)
    base.extend(ClassMethods)
  end


  module ClassMethods

		def create_with_promotion_manager(promotion_manager)
	    # require './app/lib/wb_api/wb_api.rb'
	    project = Project::Project.find promotion_manager.project_id
	    # api = WBApi.new project
			url = 'https://advert-api.wb.ru/adv/v1/save-ad'
			nm = Nm::Nm.find_by nm_id: promotion_manager.nm_id
			body = {
				name: "#{nm.supplier_article}::PumPurum",
				btype: 1,
				sum: 500,
				type: 8,
				on_pause: true,
				subjectId: nm.obj_id,
				nms: [promotion_manager.nm_id],
				cpm: 1
			}
			response = api.post(url) do |req|
				req.body = body.to_json
			end
			if response.status == 200
				adv = Advert::Advert.new
				adv.project_id = promotion_manager.project_id
				adv.advert_id = response.body
				adv.save
				return adv
			else
				nil
			end
		end


		def create_or_update_by_advert_id(project, advert_id)
	    # require './app/lib/wb_api/wb_api.rb'
	    # ap advert_id
	    # api = WBApi.new project
			url = 'https://advert-api.wb.ru/adv/v1/promotion/adverts'
			body = advert_id.class == Array ? advert_id.map {|i| i.advert_id} : [advert_id]
	  	resp = project.api.post(url, body.to_json) 
	  	adverts_api = JSON.parse resp.body
	  	adverts_api.map do |advert_api|
	  		advert = Advert::Advert.find_or_create_by advert_id: advert_api['advertId'], project_id: project.id
				# ap 'advert_api'
				# ap advert_api
				# ap 'advert_ fill'
				advert.fill_with_api_response advert_api
				# ap 'advert_ fill ok'
			end
      adverts_api.map do |advert|
        advert = Advert::Advert.find_by advert_id: advert['advertId'], project_id: project.id
        advert.update_budget
        sleep 0.3
      end
		end
	end

end
