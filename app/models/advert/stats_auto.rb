class Advert::StatsAuto < ApplicationRecord

  def cluster
    Advert::Cluster.find_by(advert_id: advert_id, cluster_name: keyword, project_id: project_id)
  end

  def self.fetch(project, advert = nil)
    url = 'https://advert-api.wb.ru/adv/v2/auto/daily-words'
    if advert
      advert_list = [advert]
    else
      advert_list = project.adverts_auto_active
    end

    keyword_cache = {}

    advert_list.map do |advert|
      res = project.api.get(url) {|req| req.params['id'] =  advert.advert_id}
      if res.status == 200
        result = JSON.parse res.body
        # ap result
        puts "Advert::StatsAuto fetched data for #{advert.advert_id}"
        list = []
        fields = ['views', 'clicks', 'sum']
        if result
          result.map do |adstat|
            # ap adstat
            date = Time.parse adstat["date"]
            stats = adstat["stat"].map do |ads|
              ads['period'] = date
              ads['advert_id'] = advert.advert_id
              ads['project_id'] = project.id
              kw = ads['keyword']
              if keyword_cache[kw]
                keyword_id = keyword_cache[kw].id
              else
                keyword_cache[kw] = Advert::ClusterKeyword.find_or_create_by(keyword: kw)
                keyword_id = keyword_cache[kw].id
              end
              ads['keyword_id'] = keyword_id
              ads['views'] = ads['views']
              ads['clicks'] = ads['clicks']
              ads['ctr'] = ads['clicks'].to_f / ads['views'] if ads['views'] != 0
              ads
            end
            # ap stats
            u_f = [:project_id, :advert_id, :period, :keyword_id]
            Advert::StatsAuto.upsert_all stats, unique_by: u_f
          end
          # sel = "
          #   sum(clicks) as clicks,
          #   sum(views) as views,
          #   sum(sum) as sum,
          #   project_id,
          #   advert_id,
          #   keyword
          # "
          # ts_3 = Time.now.beginning_of_day - 3.days
          # ts_7 = Time.now.beginning_of_day - 7.days
          # wh = {project_id: project.id, advert_id: advert.advert_id}
          # wh3 = "period > '#{ts_3}'"
          # wh7 = "period > '#{ts_7}'"
          # gr = :keyword, :advert_id, :project_id

          # asais = Advert::StatsAutoItem.where(wh).select(sel).group(gr).map do |asai|
          #   keyword = asai.keyword
          #   asai = asai.as_json
          #   if keyword_cache[keyword]
          #     asai['keyword_id'] = keyword_cache[keyword].id
          #   else
          #     keyword_cache[keyword] = Advert::ClusterKeyword.find_or_create_by keyword: keyword
          #     asai['keyword_id'] = keyword_cache[keyword].id
          #   end
          #   if asai['views'] != 0
          #     asai['ctr'] = asai['clicks'] / asai['views']
          #   else
          #     asai['ctr'] = 0.00000001
          #   end
          #   asai
          # end
          # Advert::StatsAuto.upsert_all asais, unique_by: [:project_id, :advert_id, :keyword_id]
          
        end
      else
        puts "Advert::StatsAuto no data for #{advert.advert_id} #{advert.name} with status #{advert.status}"
      end
      sleep 1

    end
  end

end
