class Advert::Cost < ApplicationRecord

	def self.update_from_api(project, ts_from = nil, ts_to = nil)
		days = 30
		ts = Time.now.beginning_of_day - 320*24*3600
		ts_from = ts - days.days if ts_from == nil
		ts_to = ts + 1.day if ts_to == nil

		url = 'https://advert-api.wb.ru/adv/v1/upd'
		res = project.api.get(url) do |req|
			req.params['from'] = ts_from.strftime "%Y-%m-%d"
			req.params['to'] = ts_to.strftime "%Y-%m-%d"
		end
		body = JSON.parse res.body 
		if res.status == 200
			body.map do |item|
				if item['updTime']
					item['updTime'] = Time.parse item['updTime']
				end
				adc = Advert::Cost.find_or_create_by advert_id: item['advertId'], update_time: item['updTime']
				adc.update_num = item['updNum']
				adc.update_time = item['updTime']
				adc.update_sum = item['updSum']
				adc.advert_id = item['advertId']
				adc.camp_name = item['campName']
				adc.advert_type = item['paymentType']
				adc.payment_type = item['advertType']
				adc.advert_status = item['advertStatus']
				adc.save
			end
		end
	end

	def self.update_from_api_onbodarding(project)
		period_cnt = 3 * 12
		days = 30
		ts = Time.now.beginning_of_day + 2.days
		period_cnt.times do |period_n|
			ts_from = ts - (period_n + 1) * 24 * 3600 * days
			ts_to   = ts - (period_n ) * 24 * 3600 * days
			Advert::Cost.update_from_api project, ts_from, ts_to
		end
	end

end
