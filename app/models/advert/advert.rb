class Advert::Advert < ApplicationRecord
	include Api
	include Advert::Api::Action
	include Advert::Api::Budget
	include Advert::Api::Create
	include Advert::Api::Update
	include Advert::Api::Nms
	include Advert::Manager::Manager
	
	after_initialize :manager_setup
	
	def manager_setup
		if has_attribute? 'type_id'
			extend Advert::Manager::Auto if is_auto
		end
	end

	after_create :event_create
	before_update do
		event_update
		check_balance
	end
	after_update_commit do 
		Turbo::StreamsChannel.broadcast_replace_to "adverts.#{project_id}", target: "advert.#{id}", partial: 'advert/adverts/index_row', locals: { advert: self }
	end

	def is_closed?
		[-1, 7, 8].include? status
	end

	def is_auto
		type_id == 8
	end

	def budget_replenish
		if manager_enabled and budget_max and budget_max > 0
			# ap "Replenishment started"
			check_balance_payment
			sum_replenishment = budget_max - budget 
			# ap "Advert #{name} #{advert_id} Replenishment sum is #{sum_replenishment}"
			if sum_replenishment > 0 and sum_replenishment < budget_max + 500
				sum_replenishment = ((sum_replenishment / 50.0).round)*50
				if sum_replenishment < 500
					sum_replenishment = 500
				end

				budget_replenish_api sum_replenishment
			else
				return false
			end
		else
			# ap "Replenishment failed: manager #{manager_enabled}, #{budget_max} #{balance_payment_sum_on_period}"
		end
	end

	# def check_balance_payment
	# 	ts_now = Time.now.beginning_of_day
	# 	if balance_payment_period < ts_now
	# 		self.balance_payment_period = Time.now 
	# 		self.balance_payment_sum_on_period = 0
	# 		self.save
	# 	end
	# end

	def project
		Project::Project.find_by project_id: project_id
	end

	def manager
		Advert::PromotionManager.find_by(advert_id: advert_id)
	end

	def params
		Advert::Param.where(advert_id: advert_id)
	end

	def nms
		params.map {|i| Nm::Nm.find_by project_id: project_id, nm_id: i.nm_id }
	end

	def active?
		[11, 9].include? self.status.to_i
	end

	def total_sum
		Advert::Stat.where(advert_id: self.advert_id).select('sum(sum) as sum')[0].sum
	end

	def daily_sum(days = 7)
		stats = Advert::Stat.where(advert_id: self.advert_id).order(period: :desc).first(days)
		days = stats.count
		sum = stats.inject(0.0) {|sum, i| sum += i.sum }
		if days > 0
			return (sum / days).round(2)
		else
			return 0.0
		end
	end




	# def self.update_all_stats_for_project(project)
	# 	url = "https://advert-api.wb.ru/adv/v2/fullstats"
	# 	advert_ids = Advert::Advert.where(project_id: project.id).where.not(status: [-1, 7, 8]).select(:advert_id).distinct.pluck(:advert_id)
	# 	ts = Time.now.beginning_of_day
	# 	dates = (0..4).map {|i| (ts-i.day).strftime("%Y-%m-%d")} 
	# 	body = (advert_ids.map {|i| {"id" => i, "dates" => dates} })
	# 	# body = (advert_ids.map {|i| {"id" => i} })

	# 	# body = [
	# 	#   {
	# 	#     "id" => 12286972
	# 	#   }
	# 	# ]
	# 	# ap body
	# 	print body.to_json
	# 	print "\n"
	# 	result = project.api.post url, body.to_json
	# 	# ap 
	# 	result
	# 	# ap 
	# 	JSON.parse result.body
		
	# end

	# def self.test
	# 	names_has_copies = []
	# 	names = Advert.where(project_id: 6).where.not(status: 7).pluck(:name)
	# 	names_uniq = Advert.where(project_id: 6).where.not(status: 7).distinct.pluck(:name)
	# 	names_uniq.map do |name|
	# 		cnt = names.count name
	# 		if cnt > 1
	# 			ap name
	# 		end
	# 	end
	# end

	# def self.list_all_from_api(project)
	# 	url = 'https://advert-api.wb.ru/adv/v1/promotion/count'
	# 	response = project.api.get(url)
	# 	JSON.parse response.body
	# end

	# def self.update_all_api(project)
	# 	adverts_result = []
	# 	adverts_api = Advert::Advert.list_all_from_api project
	# 	adverts_api['adverts'].map do |adverts|
	# 		adverts_list = adverts['advert_list']
	# 		adverts_list.map! do |ad|
	# 			ad['type'] = adverts['type']
	# 			ad['status'] = adverts['status']
	# 			ad
	# 		end
	# 		adverts_result += adverts_list
	# 	end
	# 	adverts_result.map do |advert_api|
	# 		ad = Advert::Advert.find_or_create_by advert_id: advert_api['advertId'], project_id: project.id
	# 		ad.type_id = advert_api['type']
	# 		ad.status = advert_api['status']
	# 		ad.save
	# 	end
	# 	Advert::Advert.update_verbose_from_api project
	# end

	# def self.update_verbose_from_api(project)
	# 	Advert::Advert.where(project_id: project.id).where.not(status: [7, -1]).each_slice(25) do |adverts|
	# 		Advert::Advert.create_or_update_by_advert_id project, adverts
	# 	end
	# end

	# def update_from_api
	# 	Advert::Advert.create_or_update_by_advert_id project, advert_id
	# 	self.update_budget
	# 	self.reload
	# end

	# def update_budget
	# 	url = 'https://advert-api.wb.ru/adv/v1/budget'
	# 	res = project.api.get(url) do |req|
	# 		req.params["id"] = self.advert_id
	# 	end
	# 	if res.status == 200
	# 		result = JSON.parse res.body
	# 		self.budget = result['total']
	# 		self.save
	# 	else
	# 		Event.create_with_api_reponse res, project
	# 	end
	# 	res
	# end

	# def stop
	# 	url = 'https://advert-api.wb.ru/adv/v0/stop'
	# 	res = project.api.get(url) { |req| req.params['id'] = advert_id }
	# 	self.update_from_api
	# 	if res.status == 200
	# 		self.status = 7
	# 		self.save
	# 	else
	# 		Event.create_with_api_reponse res, project
	# 	end	
	# 	res
	# end

	# def pause
	# 	url = 'https://advert-api.wb.ru/adv/v0/pause'
	# 	res = project.api.get(url) { |req| req.params['id'] = advert_id }
	# 	self.update_from_api
	# 	if res.status == 200
	# 		self.status = 11
	# 		self.save
	# 	else
	# 		Event.create_with_api_reponse res, project
	# 	end	
	# 	res
	# end

	# def start
	# 	url = 'https://advert-api.wb.ru/adv/v0/start'
	# 	res = project.api.get(url) { |req| req.params['id'] = advert_id }
	# 	self.update_from_api
	# 	if res.status == 200
	# 		self.status = 9
	# 		self.save
	# 	else
	# 		Event.create_with_api_reponse res, project
	# 	end	
	# 	res
	# end

	# def set_cpm
	# 	url = 'https://advert-api.wb.ru/adv/v0/cpm'
	# 	opts = {
	# 		"advertId" => advert_id,
	# 		"type" => type_id,
	# 		"cpm" => cpm.round
	# 	}
	# 	if not is_auto
	# 		opts["param"] = 1/0
	# 	end
	# 	res = project.api.post url, opts.to_json
	# 	if res.status != 200
	# 		Event.create_with_api_reponse res, project
	# 	end
	# 	res
	# end

	# def set_zones_by_promotion_manager(promotion_manager)
	# 	url = 'https://advert-api.wb.ru/adv/v1/auto/active'
	# 	project.api.post(url) do |req|
	# 		req.params['id'] = advert_id
	# 		req.body = {
	# 			recom: false,
	# 			booster: true,
	# 			carousel: false
	# 		}.to_json
	# 	end
	# end

	def configured_for_promotion_manager?(promotion_manager)
		# ap promotion_manager.as_json
		configured = true
		nms = Advert::Param.where(advert_id: promotion_manager.advert_id).pluck(:nm_id)
		ppp = Advert::Param.where(advert_id: promotion_manager.advert_id).all

		configured = false if nms != [promotion_manager.nm_id]
		configured = false if Advert::Param.where(advert_id: promotion_manager.advert_id).any? do |e|
			not (e.booster == true and e.recom == false and e.carousel == false)
		end
		# ap 'configured'
		# ap configured
		configured
	end

	# def set_nms_by_promotion_manager(promotion_manager)
	# 	nms = Advert::Param.where(advert_id: promotion_manager.advert_id).pluck(:nm_id)
	# 	body = {
	# 		delete: nms.select {|i| i != promotion_manager.nm_id },
	# 		add: nms.include?(promotion_manager.nm_id) ? [] : [promotion_manager.nm_id] 
	# 	}


	# 	if body[:delete] != [] or body[:add] != []
	# 		url = 'https://advert-api.wb.ru/adv/v1/auto/updatenm'
	# 		response = project.api.post(url) do |req|
	# 			req.params['id'] = promotion_manager.advert_id
	# 			req.body = body.to_json
	# 		end
	# 		advert = Advert::Advert.find_by advert_id: promotion_manager.advert_id
	# 		advert.update_from_api
	# 	end
	# end

	# def self.create_with_promotion_manager(promotion_manager)
  #   # require './app/lib/wb_api/wb_api.rb'
  #   project = Project::Project.find promotion_manager.project_id
  #   # api = WBApi.new project
	# 	url = 'https://advert-api.wb.ru/adv/v1/save-ad'
	# 	nm = Nm::Nm.find_by nm_id: promotion_manager.nm_id
	# 	body = {
	# 		name: "#{nm.supplier_article}::PumPurum",
	# 		btype: 1,
	# 		sum: 500,
	# 		type: 8,
	# 		on_pause: true,
	# 		subjectId: nm.obj_id,
	# 		nms: [promotion_manager.nm_id],
	# 		cpm: 1
	# 	}
	# 	response = project.api.post(url) do |req|
	# 		req.body = body.to_json
	# 	end
	# 	if response.status == 200
	# 		adv = Advert::Advert.new
	# 		adv.project_id = promotion_manager.project_id
	# 		adv.advert_id = response.body
	# 		adv.save
	# 		return adv
	# 	else
	# 		nil
	# 	end
	# end

	def sum_today
		ts = Time.now.beginning_of_day
		as = Advert::Stat.find_by(period: ts, advert_id: self.advert_id)
		as ? as.sum : 0.0
	end

	# def self.create_or_update_by_advert_id(project, advert_id)
  #   require './app/lib/wb_api/wb_api.rb'
  #   # ap advert_id
  #   api = WBApi.new project
	# 	url = 'https://advert-api.wb.ru/adv/v1/promotion/adverts'
	# 	body = advert_id.class == Array ? advert_id.map {|i| i.advert_id} : [advert_id]
  # 	resp = project.api.post(url, body.to_json) 
  # 	adverts_api = JSON.parse resp.body
  # 	adverts_api.map do |advert_api|
  # 		advert = Advert::Advert.find_or_create_by advert_id: advert_api['advertId'], project_id: project.id
	# 		advert.fill_with_api_response advert_api
	# 	end
	# end

	def type_humanize
		case type_id
		when 4
			"Каталог"
		when 5
			"Карточка"
		when 6
			"Поиск"
		when 7
			"Рекомендации"
		when 8
			"АРК"
		when 9
			"Поиск+Каталог"
		else
			"Неизвестный статус"
		end
		
	end

	def status_humanize(status_pre = nil)
		st = status_pre != nil ? status_pre.to_i : status.to_i 
		case st
		when -1
			"Удаление"
		when 4
			"Готова к запуску"
		when 7
			"Завершена"
		when 8
			"Отказ"
		when 9
			"Запущена"
		when 11
			"Пауза"
		else
			"Неизвестный статус"
		end
	end

	def project
		Project::Project.find self.project_id
	end


	# def check_balance
	# 	self.budget = 0.0 if self.budget == nil
	# 	opts = {project_id: project_id, subject_id: id, subject: "adverts", date: Time.now}
	# 	b_avg = budget_avg_daily
	# 	b_avg = b_avg.round(2)
	# 	if b_avg > 0 and not is_low_budget
	# 		if budget < b_avg
	# 			opts[:text] = "РК #{advert_id} #{self.name}: Бюджет кончается. Средний расход #{b_avg}руб, текущий бюджет #{budget}"
	# 			Event.create opts
	# 		end
	# 	# else
	# 	# 	opts[:text] = "РК #{advert_id}: Бюджет не установлен. Текущий бюджет #{budget}"
	# 	# 	Event.create opts
	# 	end
	# 	self.is_low_budget = budget < b_avg
	# end

	# def self.budget_replenish_all(project)
	# 	Advert::Advert.where(project_id: project.id, manager_enabled: true).map do |ad|
	# 		ad.budget_replenish
	# 	end
	# end

	# def fill_with_api_response(advert_api)

  #   ap advert_api
  #   advert_params = advert_api['params']
  #   self.response_raw = advert_api
	# 	self.name = advert_api['name']
	# 	self.advert_id = advert_api['advertId']
	# 	self.type_id = advert_api['type']
	# 	self.status = advert_api['status']

  #   if advert_params and self.status != 9
  #     if advert_params[0]["subjectId"]
  #       self.param_value = advert_params[0]["subjectId"]
  #     else
  #       self.param_value = advert_params[0][self.param_name]
  #     end
  #   end
  #   if advert_api['autoParams'] and advert_api['type'] == 8
  #   	self.cpm = advert_api['autoParams']['cpm']

  #   	nms = advert_api['autoParams']['nms']
  #   	Advert::Param.where(advert_id: self.advert_id).where.not(nm_id: nms).map { |e| e.destroy! }
	#     if nms
	#     	nms.map do |nm|
	# 	    	adparam = Advert::Param.find_or_create_by(advert_id: self.advert_id, nm_id: nm)
	# 	    	adparam.carousel = advert_api['autoParams']['active']['carousel']
	# 	    	adparam.recom = advert_api['autoParams']['active']['recom']
	# 	    	adparam.booster = advert_api['autoParams']['active']['booster']
	# 	    	self.cpm = advert_api['autoParams']['cpm']
	# 	    	adparam.save!
	# 	    end
	# 	  end
  #   end
  #   self.save!
	# end


	private

	# def budget_replenish_api(sum_to_replenish)
	# 	ap "budget_replenish_api #{sum_to_replenish}"
	# 	check_balance_payment
	# 	raise "Budget too large!" if sum_to_replenish > 500 + budget_max - balance_payment_sum_on_period
	# 	url = 'https://advert-api.wb.ru/adv/v1/budget/deposit'
	# 	body = {
	# 		"sum" => sum_to_replenish,
	# 		"type" => 1,
	# 		"return" => true
	# 	}
	# 	res = project.api.post(url, body.to_json) do |req|
	# 		req.params['id'] = advert_id
	# 	end
	# 	body = JSON.parse res.body
	# 	if res.status == 200
	# 		ap body
	# 		self.budget = body['total'] 
	# 		self.balance_payment_sum_on_period += sum_to_replenish
	# 		self.save
	# 		opts = {project_id: project_id, subject_id: id, subject: "adverts", date: Time.now}
	# 		opts[:text] = "РК #{name} #{advert_id}: Бюджет пополнен на #{sum_to_replenish}. Текущий бюджет: #{budget}"
	# 		Event.create opts
	# 		return true
	# 	else
	# 		ap body
	# 		return false
	# 	end
	# end

	def event_create
		text = "РК создана: [#{self.advert_id}] #{self.name}. Тип #{self.type_humanize}. Статус #{self.status_humanize}"
		h = {
			project_id: self.project_id,
			subject: 'adverts',
			text: text,
			date: Time.now,
			priority: 0, 
			subject_id: self.advert_id
		}
		Event.create h
	end

	def event_update
		only_for_fields = ['name', 'status', 'cpm' ]
		can_be_created = false
		if self.changes != {} and self.changes.keys
			text = "📢 РК обновлена [#{self.advert_id}] #{self.name}.\n"
			self.changes.map do |field, changes|
				if only_for_fields.include? field 
					can_be_created = true
					field_before = changes[0] ? changes[0] : '-'
					field_after  = changes[1] ? changes[1] : '-'
					if field == 'status'
					text += "\nТекущий статус: #{self.status_humanize} (было #{self.status_humanize field_before}) \n"
					else
						text += "[#{field}]: #{field_before} > #{field_after} \n"
					end
				end
			end
			h = {
				project_id: self.project_id,
				subject: 'adverts',
				text: text,
				date: Time.now,
				priority: 0, 
				subject_id: self.advert_id
			}
			Event.create h if can_be_created
		end
		self
	end


end
