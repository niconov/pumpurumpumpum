module AdvertBudget
	extend ActiveSupport::Concern

	def check_balance
		self.budget = 0.0 if self.budget == nil
		opts = {project_id: project_id, subject_id: id, subject: "adverts", date: Time.now}
		b_avg = budget_avg_daily
		b_avg = b_avg.round(2)
		if b_avg > 0 and not is_low_budget
			if budget < b_avg
				opts[:text] = "РК #{advert_id} #{self.name}: Бюджет кончается. Средний расход #{b_avg}руб, текущий бюджет #{budget}"
				Event.create opts
			end
		# else
		# 	opts[:text] = "РК #{advert_id}: Бюджет не установлен. Текущий бюджет #{budget}"
		# 	Event.create opts
		end
		self.is_low_budget = budget < b_avg
		self.save
	end

	def self.budget_replenish_all(project)
		Advert::Advert.where(project_id: project.id, manager_enabled: true).map do |ad|
			ad.budget_replenish
		end
	end

	def check_balance_payment
		ts_now = Time.now.beginning_of_day
		if balance_payment_period < ts_now
			self.balance_payment_period = Time.now 
			self.balance_payment_sum_on_period = 0
			self.save
		end
	end

	def budget_replenish
		if manager_enabled and budget_max and budget_max > 0
			# ap "Replenishment started"
			check_balance_payment
			sum_replenishment = budget_max - budget 
			# ap "Advert #{name} #{advert_id} Replenishment sum is #{sum_replenishment}"
			if sum_replenishment > 0 and sum_replenishment < budget_max + 500
				sum_replenishment = ((sum_replenishment / 50.0).round)*50
				if sum_replenishment < 500
					sum_replenishment = 500
				end

				budget_replenish_api sum_replenishment
			else
				return false
			end
		else
			# ap "Replenishment failed: manager #{manager_enabled}, #{budget_max} #{balance_payment_sum_on_period}"
		end
	end

	private

	def budget_replenish_api(sum_to_replenish)
		ap "budget_replenish_api #{sum_to_replenish}"
		check_balance_payment
		raise "Budget too large!" if sum_to_replenish > 500 + budget_max - balance_payment_sum_on_period
		url = 'https://advert-api.wb.ru/adv/v1/budget/deposit'
		body = {
			"sum" => sum_to_replenish,
			"type" => 1,
			"return" => true
		}
		res = project.api.post(url, body.to_json) do |req|
			req.params['id'] = advert_id
		end
		body = JSON.parse res.body
		if res.status == 200
			ap body
			self.budget = body['total'] 
			self.balance_payment_sum_on_period += sum_to_replenish
			self.save
			opts = {project_id: project_id, subject_id: id, subject: "adverts", date: Time.now}
			opts[:text] = "РК #{name} #{advert_id}: Бюджет пополнен на #{sum_to_replenish}. Текущий бюджет: #{budget}"
			Event.create opts
			return true
		else
			ap body
			return false
		end
	end

end