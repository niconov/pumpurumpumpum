class Advert::Stat < ApplicationRecord

  def self.update_from_api(project, ts_from = nil, ts_to = nil)
    days = 5
    ts = Time.now.beginning_of_day 
    ts_from = ts - days.days if ts_from == nil
    ts_to = ts + 1.day if ts_to == nil
    dates = (0..days).map {|i| (ts-i.day).strftime("%Y-%m-%d")} 

    # ap 'aaaa'
    url = "https://advert-api.wb.ru/adv/v2/fullstats"

    advert_ids_db = Advert::Advert.where(project_id: project.id).where.not(status: [-1, 8]).select(:advert_id).distinct.pluck(:advert_id)
    # ap advert_ids
    batch_size = 50
    batch_count = 1 + (advert_ids_db.count / batch_size).ceil
    batch_i = 0
    advert_ids_db.each_slice(batch_size) do |advert_ids|
      batch_i += 1
      ap "Advert::Stat Project #{project.id}. Batch: #{batch_i}/#{batch_count}. "
      body = (advert_ids.map {|i| {"id" => i, "dates" => dates} })

      # ap "Обновление статистики РК #{advert_id}"

      st = 0 
      advert_stats_list = []
      while st != 200
        response = project.api.post url, body.to_json
        st = response.status
        # ap st
        if st == 200
          advert_stats_list = JSON.parse response.body
        end
        sleep 10      
      end
      # ap advert_stats_list
      # print "Fetched #{advert_stats_list.count} adverts.\n"

      result = {}


      fields = ["views", "clicks", "sum", "atbs", "orders", "shks", "sum_price"]
      # ap advert_stats_list
      if advert_stats_list != nil
        advert_stats_list.map do |advert_stats|
          # ap advert_stats
          # 1/0
          advert_id = advert_stats['advertId']
          stats_by_days = advert_stats['days']
          # if advert_id == 12873051
          #   ap advert_stats
          # end
          if stats_by_days
            stats_by_days.map! do |daily_stats|
              # ap daily_stats

              day = Time.parse(daily_stats['date']).beginning_of_day
              # adstat = nil
              is_new = true
              daily_stats['apps'].map do |app_stats|
                app_stats['nm'].map do |nm_stats|
                  # ap advert_id
                  # ap day
                  # ap nm_stats
                  nm_id = nm_stats['nmId']
                  adstat = Advert::Stat.find_or_create_by(advert_id: advert_id, nm_id: nm_id, period: day) if adstat == nil
                  fields.map do |f| 
                    if is_new
                      adstat.send("#{f}=", nm_stats[f] || 0.0) 
                    else
                      adstat.send("#{f}=", adstat.send(f) + nm_stats[f]) if nm_stats[f]
                    end
                  end
                  # if adstat.nm_id == 193643010
                  #   ap adstat
                  #   ap nm_stats
                  # end
                  is_new = false
                  adstat.advert_id = advert_id
                  adstat.project_id = project.id
                  adstat.ctr = adstat.views  ? (100 * adstat.clicks / adstat.views).round(2) : 0.0
                  adstat.cpc = adstat.clicks ? (adstat.sum / adstat.clicks).round(2) : 0.0
                  adstat.cr = adstat.clicks  ? (100 * adstat.orders / adstat.clicks).round(2) : 0.0


                  adstat.save
                  # ap "=" * 100
                end
              end
            end
          end
          # 1/0
        end
      end
      sleep_time = 15
      ap "Advert::Stat Project #{project.id}. Batch #{batch_i} updated. Sleeping #{sleep_time}sec"
      sleep sleep_time
    end

  end

end
