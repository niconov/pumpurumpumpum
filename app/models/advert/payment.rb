class Advert::Payment < ApplicationRecord

	def self.update_from_api(project, ts_from = nil, ts_to = nil)
		days = 31
		ts = Time.now.beginning_of_day
		ts_from = ts - days.days if ts_from == nil
		ts_to = ts if ts_to == nil

		url = 'https://advert-api.wb.ru/adv/v1/payments'
		res = project.api.get(url) do |req|
			req.params['from'] = ts_from.strftime "%Y-%m-%d"
			req.params['to'] = ts_to.strftime "%Y-%m-%d"
		end
		body = JSON.parse res.body 
		if res.status == 200
			body.map do |item|
				adp = Advert::Payment.find_or_create_by id: res['id']
				# ap item
				# ap item['sum']
				adp.sum = item['sum']
				adp.payment_id = item['id']
				adp.date = item['date']
				adp.payment_type = item['type']
				adp.status_id = item['statusId']
				adp.card_status = item['cardStatus']
				adp.save
			end
		end
	end
	
end
