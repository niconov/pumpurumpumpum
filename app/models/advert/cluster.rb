class Advert::Cluster < ApplicationRecord
  has_many :keywords, class_name: "Advert::ClusterKeyword"

  def stats_last_days(days)
    sel = "
      sum(clicks) as clicks,
      sum(views) as views,
      0 as ctr,
      sum(sum) as sum
    "
    wh = "period > '#{Time.now.beginning_of_day - days.days}'"
    sa = Advert::StatsAuto.where(project_id: project_id, advert_id: advert_id, keyword: cluster_name).select(sel).where(wh)

    sa = sa[0] if sa != []

    if sa.views == 0 or sa.views == nil or sa.clicks == nil
      sa.views = 0.0 if sa.views == nil
      sa.clicks = 0.0 if sa.clicks == nil
      sa.ctr = 0.0
      sa.sum = 0.0
    else
      sa.ctr = sa.clicks.to_f / sa.views
    end
    sa
  end

  def forced_state_humanize
    case forced_state
    when -1
      "Исключен"
    when 0
      "По правилам стратегии"
    when 1
      "Включен"
    end
  end

  def self.fetch(project, advert = nil)
    if advert == nil
      adverts = project.adverts_auto_active
    else
      adverts = project.adverts_auto_active.where(advert_id: advert.advert_id)
    end
    url = 'https://advert-api.wb.ru/adv/v2/auto/stat-words'
    cnt = 0
    cnt_total = adverts.count
    adverts.map do |advert|
      cnt += 1
      status = 0
      cnt_error_times = 0
      while status != 200
        puts "Advert::Cluster.fetch Project ID #{project.id} and Advert #{advert.advert_id} #{advert.name} Fetch #{cnt}/#{cnt_total}"
        res = project.api.get(url) {|req| req.params['id'] = advert.advert_id}
        status = res.status
        if status != 200
          sleep 1
          cnt_error_times += 1
        end
      end
      ap res if res == nil
      if res != nil
        body = JSON.parse res.body
        puts "Advert::Cluster.fetch Project ID #{project.id} and Advert #{advert.advert_id} update Advert::Cluster (Call API attempts before success: #{cnt_error_times})"

        active_clusters = []
        clusters_upsert = body['clusters'].map do |cluster|
          active_clusters.push cluster['cluster']
          {
            advert_id: advert.advert_id,
            project_id: project.id,
            cluster_name: cluster['cluster'],
            excluded: false,
          }
        end
        # ap clusters_upsert
        # ts1 = Time.now
        puts "Advert::Cluster.fetch Project ID #{project.id} and Advert #{advert.advert_id}: upsert Advert::Cluster"
        Advert::Cluster.upsert_all clusters_upsert, unique_by: [:project_id, :advert_id, :cluster_name]
        # Advert::Cluster.where(project_id: project.id, advert_id: advert.advert_id).where.not(cluster_name: active_clusters).update_all(excluded: true)

        excluded_clusters_upsert = body['excluded'].map do |cluster_ex|
          {
            advert_id: advert.advert_id, 
            project_id: project.id, 
            cluster_name: cluster_ex,
            excluded: true
          }
        end
        Advert::Cluster.upsert_all excluded_clusters_upsert, unique_by: [:project_id, :advert_id, :cluster_name]
        puts "Advert::Cluster.fetch Project ID #{project.id} and Advert #{advert.advert_id}: Start upd cluster stats"
        sel = "
          sum(sum) as sum,
          sum(views) as views,
          sum(sum) as sum,
          sum(clicks) as clicks
        "
        Advert::Cluster.where(advert_id: advert.advert_id, project_id: project.id).map do |cluster|
          sa =  Advert::StatsAuto.where(keyword: cluster.cluster_name, advert_id: advert.advert_id, project_id: project.id).select(sel).group(:keyword)
          if sa == []
            cluster.views = 0.0 
            cluster.clicks = 0.0 
            cluster.sum = 0.0 
            cluster.ctr = 0.0
          else
            sa = sa[0] 
            if cluster.views == 0 or cluster.views == nil or cluster.clicks == nil
              cluster.views = 0.0 if sa.views == nil
              cluster.clicks = 0.0 if sa.clicks == nil
              cluster.ctr = 0.0
            else
              cluster.sum = sa.sum
              cluster.views = sa.views
              cluster.clicks = sa.clicks
              cluster.ctr = sa.clicks.to_f / sa.views.to_f
            end
          end
          cluster.save
        end

      else
        puts "Advert::Cluster.fetch for project id #{project.id} and advert #{advert.advert_id} Response NULL!"
      end


      sleep 0.3
    end

  end

end
