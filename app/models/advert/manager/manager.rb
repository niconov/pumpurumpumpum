module Advert::Manager::Manager

  module ClassMethods

    def run_manager!(project)
      Advert::Advert.exclude! project
    end
  
    def exclude!(project)
      project.adverts_auto_active.map do |advert| 
        advert.exclude! if advert.manager_enabled 
      end
    end

  end

	def self.included(base) base.extend(ClassMethods) end

end