module Advert::Manager::Auto

  def stats
    Advert::StatsAuto.where(advert_id: advert_id, project_id: project_id)
  end

  def stats_with_views
    stats.where('views > 100')
  end

  def clusters
    Advert::Cluster.where(advert_id: advert_id, project_id: project_id)
  end

  def flush_excluded!
    set_exclude_api_call []
  end

  def set_exclude_api_call(excluding)
    url = "https://advert-api.wb.ru/adv/v1/auto/set-excluded"
    status = 0
    to_exclude = excluding
    body = {excluded: excluding}
    while status != 200
      res = project.api.post(url, body.to_json) {|req| req.params['id'] = advert_id}
      status = res.status
      if status != 200 
        if status != 429
          ap res
          ap JSON.parse res.body
        end
        sleep 10 
      else
        clusters.where(cluster_name: excluding).update_all(excluded: true)
        clusters.where.not(cluster_name: excluding).update_all(excluded: false)
        stats.where(keyword: excluding).update_all(excluded: true)
        stats.where.not(keyword: excluding).update_all(excluded: false)
      end
    end
    return true
  end

  def exclude!
    if manager_enabled
      set_exclude_api_call clusters_to_exclude
    else
      puts "Advert::Manager::Auto: #{advert_id} #{name} manager disabled"
    end
  end

  def clusters_to_exclude
    profit_worst = 1/0.0
    orders_div_opens_worst = 1/0.0
    view_cost = cpm / 1000

    nms.map do |nm|
      stat = Nm::Stat.where(nm_id: nm.nm_id).select("1.0 * sum(orders_count)/sum(open_card_count) as orders_div_opens")
      profit_by_stock = nm.profit_by_stock
      profit_by_stock = nm.profit_worst if not profit_by_stock or profit_by_stock == 0
      if not stat.none?
        orders_div_opens = stat[0].orders_div_opens
        puts "Advert::Manager::Auto: #{advert_id} #{name} #{nm.nm_id} #{nm.supplier_article} stat orders div opens:  #{(orders_div_opens*100).round(1)} profit: #{profit_by_stock}"
        if profit_worst > profit_by_stock
          profit_worst = profit_by_stock 
        end
        if orders_div_opens_worst > orders_div_opens
          orders_div_opens_worst = orders_div_opens 
        end
      else
        nil
      end
    end
    # ap stats_by_nms

    # s = stats_with_views.select('100*avg(ctr) as ctr').group(:advert_id).all
    # s = s[0] if not s.none?
    # ap s.ctr

    sel_text = "
      keyword, 
      ctr_7d as ctr, 
      views_7d as views, 
      clicks_7d as clicks, 
      sum_7d as sum, 
      7 as days,
      0.0::float as profit,
      0.0::float as cpo,
      0.0::float as advert_total_cost,
      0.0::float as profit_normalized 
    "
    wh = { excluded: false }
    wh_text = "views_3d > 100"

    result = stats.where(wh_text).where(wh).select(sel_text).map do |item|
      item.advert_total_cost = item.views * view_cost
      if item.ctr.nan?
        item.cpo = 999999999
      else
        item.cpo = view_cost / (item.ctr + (0.0001))
      end
      item.profit = (profit_worst - item.cpo)
      # ap "=" * 100
      # ap view_cost.to_f 
      # ap item.views   
      # ap item.clicks   
      # ap item.ctr   
      # ap (item.ctr + (0.0001))
      # ap item.profit
      # ap item.cpo
      # ap item.profit.class
      item.profit_normalized = (profit_worst.to_f) - (item.cpo / orders_div_opens_worst)
      item.ctr = item.ctr * 100
      item
    end
    result = result.sort_by {|i| i.profit }

    tp_fields = [:keyword, :advert_total_cost, :profit, :ctr, :cpo, :views, :clicks, :sum]
    # tp result, tp_fields
    to_exclude = result.select {|i| i.profit < 0}
    not_excluded = result.select {|i| i.profit > 0}
    planned_clicks = not_excluded.inject(0.0) {|s, i| s += i.clicks  / i.days} 
    planned_views = not_excluded.inject(0.0) {|s, i| s += i.views  / i.days}
    planned_budget = planned_views * cpm / 1000 / 7
    puts "Planned Views: #{planned_views}, Clicks: #{planned_clicks} Budget: #{planned_budget}"
    # tp to_exclude , tp_fields
    keyword_to_exclude = to_exclude.map {|i| i[:keyword]}
    # ap keyword_to_exclude
    keyword_to_exclude
  end

end
