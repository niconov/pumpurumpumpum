class Nm::Nm < ApplicationRecord
	has_many :nm_group_query_item, primary_key: :nm_id
	has_many :nm_competitor_connector, primary_key: :nm_id, foreign_key: :nm_id
	has_many :nm_competitor, through: :nm_competitor_connector, primary_key: :nm_id, foreign_key: :nm_id
 
  after_create_commit { broadcast_prepend_to "pricing_item" }
	before_update :event_update
  after_update_commit { broadcast_replace_to "pricing_item.#{nm_id}", partial: "pricing/item" }

  def is_pricing_ready?
  	self.cost != nil  and self.ros_min != nil
  end

  def chart_data_orders(hours = 24)
    sel = "count(id) as price, date_trunc('hour', datetime) as period"
    wh = "nm_id = #{nm_id} and project_id = #{project_id} and datetime > '#{Time.now.beginning_of_hour - hours*3600}'"
    gr = "period, nm_id"
    chart_data_pr = Order::Order.select(sel).where(wh).where(nm_id: nm_id).group(gr).group_by {|i| i.period}
    periods = chart_data_pr.keys.sort
    # ap chart_data_pr

    # period_last = periods.last
    periods.map do |k|
    	# if period_last == k
    	# 	nmp = NmCompetitor.find_by(nm_id: nm_id)
    	# 	last_price = nmp.price_with_spp || nmp.price if nmp
    	# end
      # k_new = k.strftime "%Y-%m-%d" 
      cnt = chart_data_pr[k].count
      sum = ((chart_data_pr[k].inject(0.0) {|s, i| s+= i.price}) )
      if sum
        # ap sum
        chart_data_pr[k] = sum
      else
        chart_data_pr[k] = 0
      end
      # chart_data_pr.delete k
    end 

    # ap chart_data_pr

    raw_labels = chart_data_pr.keys.sort 
    raw_data = raw_labels.map {|i| chart_data_pr[i] } 
    # ap raw_labels
    # ap raw_data


		[raw_labels, raw_data]  	
  end

  def chart_data_real_price(days = 10)
    sel = "avg(finished_price) as price, date_trunc('day', datetime) as period"
    wh = "nm_id = #{nm_id} and project_id = #{project_id} and datetime > '#{Time.now.beginning_of_day - days*24*3600}'"
    gr = "period, nm_id"
    chart_data_pr = Order::Order.select(sel).where(wh).where(nm_id: nm_id).group(gr).group_by {|i| i.period}
    
    periods = chart_data_pr.keys.sort

    period_last = periods.last
    periods.map do |k|
    	if period_last == k
    		nmp = NmCompetitor.find_by(nm_id: nm_id)
    		last_price = nmp.price_with_spp || nmp.price if nmp
    	end
      k_new = k.strftime "%Y-%m-%d" 
      cnt = chart_data_pr[k].count
      sum = ((chart_data_pr[k].inject(0.0) {|s, i| s+= i.price}) )
      if sum
        # ap sum
        chart_data_pr[k_new] = last_price || (sum / cnt).round
      else
        chart_data_pr[k_new] = 0
      end
      chart_data_pr.delete k
    end 

    raw_labels = chart_data_pr.keys.sort 
    raw_data = raw_labels.map {|i| chart_data_pr[i] } 


		[raw_labels, raw_data]

  end

	def total_cost
		r = 0.0
		r += cost if cost
		r += cost_log_in if cost_log_in
		r += cost_log_out if cost_log_out
		r += cost_packaging if cost_packaging
		r += cost_package if cost_package
		# ap "cost #{cost}" if cost
		# ap "cost_log_in #{cost_log_in}" if cost_log_in
		# ap "cost_log_out #{cost_log_out}" if cost_log_out
		# ap "cost_packaging #{cost_packaging}" if cost_packaging
		# ap "cost_package #{cost_package}" if cost_package
		r
	end

	def cost_total
		total_cost
	end

	def has_competitor_error
		price_total = price.total
		# self.nm_competitor.where
	end

	def profit_recomended
		# item['profit_rec'] = (item['nm'].ros_min * item['price'].total / 100 ).round(2) 
		ros_min * price.total / 100
	end

	def price_customer
		nc = NmCompetitor.find_by nm_id: nm_id
		nc ? nc.price_with_spp : false
	end

	def spp_update
		pc = price_customer
		if pc
			# ap "   ----------    "
			# print "#{price_customer} #{price.total.round}\n"
			self.spp = (((pc / price.total ) - 1) * 100).round.abs
		else
			self.spp = 0
		end
		self.spp
	end



	def profit_by_stock_verbose
		res = { quantity: 0.0, profit: 0.0, profit_per_pcs: 0.0, roi: 0.0, ros: 0.0 }

		stocks = Stock::Stock.where(project_id: project_id, nm_id: nm_id).map do |s|
			if s.quantity > 0
				t = s.get_tariff_current
				profit = profit_without_logistics - t.calc_by_nm(self)
				res[:profit] = res[:profit] + profit * s.quantity
				res[:quantity] = res[:quantity] + s.quantity
				res[:profit_per_pcs] = res[:profit] / res[:quantity]
				res[:roi] = 100 * res[:profit] / (total_cost  * res[:quantity])
				res[:ros] = 100 * res[:profit] / (price.total * res[:quantity])
			end
		end
		# ap res
		res[:roi_by_stock]    = res[:roi].round(2)
		res[:ros_by_stock]    = res[:ros].round(2)
		res[:profit_by_stock] = res[:profit_per_pcs].round(2)
		res
	end

	def profit_by_stock
		profit_by_stock_verbose[:profit_by_stock]
	end

	def roi_by_stock
		profit_by_stock_verbose[:roi_by_stock]
	end

	def ros_by_stock
		profit_by_stock_verbose[:ros_by_stock]
	end

	def update_log_out_price
		check_unit_per_pallet
		prices = Tariff::Active.where(project_id: self.project_id).map do |ta|
			# ap ta
			# ap ta.tarif
			dm = DeliveryMethod.find_by tariff_id: ta.tariff_id
			# ap dm
			if dm 
				dm.price
			else
				0.0
			end
		end
		# ap prices
		
		dm_price = prices.max
		# ap dm_price
		if dm_price != nil
			c = (dm_price / unit_per_pallet).round(2)
			# ap c
			if c != Float::INFINITY
				self.cost_log_out = c
			else
				self.cost_log_out = 0.0
			end
			save
		end

	end

	def check_unit_per_pallet
		if unit_per_box != nil and box_per_pallet != nil and unit_per_pallet != unit_per_box * box_per_pallet
			self.unit_per_pallet = unit_per_box * box_per_pallet
			self.save
		end
	end

	def price
		Price.where("nm_id = #{nm_id.to_i}").first
	end

	def price_total
		nm_price = price
		return (nm_price.price * (100 - nm_price.discount) / 100)
	end

	def comission
		Comission.find_by_subject(object)
	end

	def comission_total
		# ap self
		price_total * comission.fbo / 100
	end

	def profit_with_logistics(tariff, box = "Короб")
		profit_without_logistics - tariff.calc_by_nm(self, box)
	end

	def profit_without_logistics
		price_total - total_cost - comission_total
	end

	def profit_worst
		# item['profit'] = (item['price'].total - item['nm'].total_cost - item['com'] - item['tariff']).round(2)
		t = price.total 
		t = t - total_cost if total_cost
		t = t - comission_total if comission_total
		t = t - tariff_worst if tariff_worst
		t
	end

	def roi_worst
		# item['roi'] = (100 * item['profit'] / nm.total_cost).round(2)
		100 * profit_worst / total_cost
	end

	def pocs_worst
		# item['ros'] = (100 * item['profit'] / item['price'].total).round(2)
		100 * profit_worst / price.total
	end


	def stock_total
		Stock::Stock.where(nm_id: nm_id).all.inject(0) {|sum, i| sum += i.quantity}
	end

	def position
		NmPosition.order(updated_at: :asc).where(nm_id: nm_id).last
	end

	def tariff_worst
		tas = Tariff::Active.where(active: true, project_id: self.project_id).pluck(:tariff_id)
		tar_prices = Tariff::Tariff.where(id: tas).map do |t|
			t.calc_by_nm self
		end
		tar_prices.max
	end

	def adverts
		aps = Advert::Param.select(:advert_id).where(nm_id: nm_id).distinct.pluck(:advert_id)
		aps.map {|i| Advert::Advert.find_by advert_id: i}
	end


	def advert_total_on_date(date)
		adcosts =  Advert::Stat.where(nm_id: self.nm_id, period: date).select('sum(sum) as sum')[0].sum
		adcosts || 0.0
	end

	def recommended_price
		((total_cost + tariff_worst) / (1 - (ros_min/100)  - (comission.fbo/100))).round(2)
	end

	def recommended_price_wb
		(100 * recommended_price / (100 - price.discount)).round
	end

	def price_set_recommended(project)
		url_price    = 'https://suppliers-api.wildberries.ru/public/api/v1/prices'
		url_discount = 'https://suppliers-api.wildberries.ru/public/api/v1/updateDiscounts'

		body_price    = [ { "nmId" => nm_id.to_i, "price" => recommended_price_wb.to_i } ].to_json
		body_discount = [ { "nm" => nm_id.to_i,   "discount" => price.discount.round   } ].to_json

		project.api.post url_price,    body_price
		project.api.post url_discount, body_discount
	end



	def event_update
		only_for_fields = ['spp']
		can_be_created = false
		text = ''
		if self.changes != {} and self.changes.keys
			nm = Nm::Nm.find_by nm_id: self.nm_id
			self.changes.map do |field, changes|
				if only_for_fields.include? field 
					if field == 'spp'
						field_after   = changes[1] ? changes[1] : 0
						field_before  = changes[0] ? changes[0] : 0
						can_be_created = field_before != field_after
						text_temp = field_after > field_before ? "увеличилось" : "уменьшилось"
						text += "#{self.supplier_article.strip}: СПП #{text_temp} до #{field_after}%. Было #{field_before}%"
					end
				end
			end
			h = {
				project_id: nm.project_id,
				subject: 'nm',
				text: text,
				date: Time.now,
				priority: 5, 
				subject_id: self.nm_id
			}
			Event.create h if can_be_created
		end
		
	end

	def self.fetch(project)
		puts "Nm::Nm.fetch for Project #{project.id}"
		url = 'https://suppliers-api.wildberries.ru/content/v2/get/cards/list'
    body = {
      "settings": {
        "cursor": {
          "limit": 1000
        },
        "filter": {
          "withPhoto": -1
        }
      }
    }
		# total = 0
		# limit = 1000
		# Добавить пагинацию!
		status = 0
		while status != 200 and status == 0
			response = project.api.post(url, body.to_json)
			status = response.status 
			sleep 10 if status != 200
		end
		body = JSON.parse response.body
		body['cards'].map do |card|
			nm = Nm::Nm.find_or_create_by nm_id: card['nmID']
			nm.nm_id = card['nmID']
			nm.prohibited = card['isProhibited']
			nm.supplier_article = card['vendorCode']
			nm.barcode = card['sizes'][0]['skus'][0]
			nm.imt_id = card['nmID']
			nm.project_id = project.id
			nm.object = card["subjectName"]
			nm.obj_id = card["subjectID"]
			skus = ''
			card['sizes'].map do |s|
				s['skus'].map do |i|
					skus += i + " "
				end
			end
			skus.strip!
			nm.skus = skus
			nm.dim_wb_a = card['dimensions']['width']
			nm.dim_wb_b = card['dimensions']['length']
			nm.dim_wb_c = card['dimensions']['height'] 
			if nm.dim_wb_a and nm.dim_wb_b and nm.dim_wb_c
				nm.volume = (nm.dim_wb_a * nm.dim_wb_b * nm.dim_wb_c / 1000).ceil
			else
				nm.volume = 0
			end
			# ap nm.as_json
			# ap nm
			nm.save
		end

	end

end
