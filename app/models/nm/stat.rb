class Nm::Stat < ApplicationRecord

  def self.fetch(project, date = nil)
    url = 'https://suppliers-api.wb.ru/content/v1/analytics/nm-report/detail'
    
    date = Time.now - 1.day if date == nil
    puts "Nm::Stat.fetch for project #{project.id} on date #{date}"
    body = {
      'page' => 0,
      'period' => {
        'begin' => date.beginning_of_day.strftime('%Y-%m-%d %H:%M:%S'),
        'end' => (date + 1.day).beginning_of_day.strftime('%Y-%m-%d %H:%M:%S')
      } 
    }
    if Time.now.beginning_of_day == date.beginning_of_day
      body['period']['end'] = Time.now.strftime('%Y-%m-%d %H:%M:%S')
    end
    next_page = true
    sleep_period = 20
    while next_page
      status = 0
      body['page'] = body['page'] + 1
      while status != 200
        res = project.api.post(url, body.to_json)
        status = res.status
        if status != 200
          puts "Nm::Stat.fetch [#{project.id}] API response #{status}. Sleep #{sleep_period}"
          ap res if status != 429
          sleep sleep_period
        end
      end
      body = JSON.parse res.body 
      Nm::Stat.upsert_api project, body
      next_page = body['data']['isNextPage']
      sleep 20 if next_page
    end
  end

  private 

  def self.upsert_api(project, body)
    puts "Nm::Stat.upsert_api: Upsert #{body['data']['cards'].count} cards"
    body['data']['cards'].map do |card|
      period = card['statistics']['selectedPeriod']['begin']
      ns = Nm::Stat.find_or_create_by nm_id: card['nmID'], period: period  

      ['begin', 'end'].map {|i| card['statistics']['selectedPeriod'].delete i}
      conversions = card['statistics']['selectedPeriod'].delete 'conversions'
      conversions.map do |k, v|
        ns.send "#{k.underscore}=", v
      end  

      ['vendorCode', 'brandName'].map do |k|
        ns.send "#{k.underscore}=", card[k]
      end
      ns.object__id = card['object']['id']
      ns.object_name = card['object']['name']
      card['statistics']['selectedPeriod'].map do |k, v|
        ns.send "#{k.underscore}=", v
      end  
      ns.stocks_wb = card['stocks']['stocksWb']
      ns.stocks_mp = card['stocks']['stocksMp']
      ns.project_id = project.id
      ns.save
    end

  end

end
