// Configure your import map in config/importmap.rb. Read more: https://github.com/rails/importmap-rails
//= require jquery
//= require jquery_ujs
//= require foundation


$(document).on('turbo:load', function() {
    $(function(){ $(document).foundation(); });
});
// Turbo.session.drive = false
// import "@hotwired/turbo-rails"
import { Turbo } from "@hotwired/turbo-rails"
import "controllers"
import "chartkick"
import "Chart.bundle"


