module ApplicationHelper

  def admin_project_id
    AppGlobalConstant.get "AdminProject"
  end

	def datetime_pretty_two(ts)
		"#{ts.strftime('%d.%m.%Y')}\n#{ts.strftime('%H:%M:%S')}"
	end

	def datetime_pretty_one(ts)
		"#{ts.strftime('%H:%M')} #{ts.strftime('%d.%m.%Y')}"
	end

  def tailwind_classes_for(flash_type)
    {
      notice: "bg-green-400 border-l-4 border-green-700 text-white",
      error:   "bg-red-400 border-l-4 border-red-700 text-black",
    }.stringify_keys[flash_type.to_s] || flash_type.to_s
  end

  def current_project
  	current_user.project
  end

  def current_project_id
  	current_user.project_id
  end


	def header_menu_item_list
		{
			dashboard: {
        items: [
          { name: "Торговый отчет", path: dashboard_trading_path },
          { name: "Оперативный отчет", path: dashboard_daily_path },
          { name: "Заказы: м/м",  path: dashboard_weekly_orders_path },
          { name: "Отчет по складам",  path: dashboard_warehouse_balance_path },
          { name: "Планирование остатков",  path: dashboard_stocks_planning_path },
          { name: "Продажи по регионам",  path: dashboard_sales_stats_by_district_path },
          { name: "Позиции номенклатуры",  path: dashboard_nm_positions_path },
          { name: "Анализ финансов по остаткам",  path: dashboard_profit_by_current_stocks_path },
          { name: "Финансовые отчты",  path: sale_reports_path },
          { name: "Воронка продаж",  path: dashboard_nm_stats_path },
        ],
        name: "Отчеты"
      },
      trading: {
        items: [
          { name: "Ценообразование", path: pricings_path },
          { name: "Ценообразование по складам", path: pricing_wh_path },
          # { name: "Менеджер рекламы", path: advert_promotion_managers_path },
          # { name: "Условия СПП", path: nm_nm_customer_discounts_path },
          { name: "Реклама", path: advert_adverts_path },
          { name: "Поисковые запросы", path: nm_nm_group_queries_path },
          # { name: "Товары конкурентов", path: nm_nm_group_competitors_path },
          { name: "Номенклатура", path: nm_nms_path },
          { name: "Отчеты о продажах", path: sale_reports_path },
        ],
        name: "Торговля"
      },
      nsi: {
        items: [
          { name: "Заказы", path: orders_path },
          { name: "Тарифы", path: tariff_tariffs_path },
          { name: "Комиссии", path: comissions_path },
          { name: "Регионы", path: regions_regions_path },
          { name: "Федеральные округа", path: regions_districts_path },
          { name: "Склады по регионам", path: regions_district_warehouses_path },
          { name: "Склады", path: warehouse_warehouse_index_path },
        ],
        name: "НСИ"
      },
      project: {
        items: [
          { name: "Автоответчик", path: autoresponsers_path},
          { name: "Планировщик заданий", path: scheduler_main_path},
          { name: "Проекты", path: project_projects_path},
          { name: "Профиль", path: edit_user_registration_path},
          { name: "Константы", path: app_global_constants_path, only_admin: true},
          
        ],
        name: "Проект"
      },
      incomes: {
        items: [
          { name: "Запланированные поставки", path: income_planned_path},
          { name: "Поступления", path: income_income_index_path },
          { name: "История остатков", path: dashboard_stocks_history_path },
          { name: "Партии", path: dashboard_stocks_by_incomes_path },
        ],
        name: "Поставки"
      },


      log: {
        items: [
          { name: "Приоритеты складов", path: deliveries_priorities_index_path},
          { name: "Активные склады", path: tariff_tariffs_active_index_path},
          { name: "Способы доставки", path: delivery_methods_path},
          { name: "Потребности по складам", path: deliveries_supply_schema_schema_path},
          { name: "Статистика по регионам", path: deliveries_supply_schema_stats_path},
        ],
        name: "Логистика"
      }
		}
  end

	def event_name_ru(name)
		case name
    when 'nms'
      'Товары'
		when 'nm'
			'Товары'
		when 'adverts'
			'Реклама'
		else
			name
		end
	end

	def advert_status_icon(advert)
		case advert.status.to_i
		when -1
			'<i class="fa fa-delete-left"></i>'
		when 4
			"Готова к запуску"
			'<i class="fa fa-circle"></i>'
		when 7
			'<i class="fa fa-stop text-error"></i>'
		when 8
			"Отказ"
		when 9
			'<i class="fa fa-play text-success"></i>'
		when 11
			'<i class="fa fa-pause text-warning"></i>'
		else
			'<i class="fa fa-question"></i>'
		end
		
	end
end
