module AdvertsHelper
	def adv_type_humanize(advert)
		advert.type_humanize
	end

	def adv_status_humanize(advert)
		advert.status_humanize
	end

end
