module Projects
  class Creator < BaseService
    attr_accessor :name, :token, :user
    validates :token, :user, :name, presence: true

    attr_reader :project

    def call
      # @todo сделать валидацию на токен
      Project::Project.transaction do
        @project = create_project
        create_role_for_project
      end

      ScheduleTemplate.for_user.each do |schedule|
        # запускаем здачи
        klass = schedule.job_name.constantize
        klass.perform_async

        # включаем задачи
        enable_task(schedule)
      end
    end

    private

    def create_project
      Project::Project.create!(name:, token:, user_id: user.id)
    end

    def create_role_for_project
      admin_role = ::Project::Role.admin
      ::Project::RoleUserOnProject.create!(user_id: user.id, project_role_id: admin_role.id, project_id: @project.id)
    end

    def enable_task(schedule_tpl)
      task_find_or_create_by schedule_tpl
    end

    def task_find_or_create_by(template)
      task = get_task_by_template template
      if task == nil
        name = template.only_one ? template.name : "#{template.name} #{project.id}"
        if template.period_minute >= 24 * 60
          period = "every #{template.period_minute / 60}d"
        elsif template.period_minute >= 60 and template.period_minute < 24 * 60
          period = "every #{template.period_minute / 60}h"
        else
          period = "every #{template.period_minute}m"
        end
        # if template.only_one then source = 'schedule' else source = 'dynamic' end

        Sidekiq::Cron::Job.create(source: 'schedule', name: name, cron: period, class: template.job_name, args: project.id, status: 'disabled', queue: template.queue)

        task = get_task_by_template template
      end

      task.enable!
    end

    def get_task_by_template(template)
      Sidekiq::Cron::Job.all.find { |i| i.klass == template.job_name and i.args[0] == project.id }
    end

  end
end