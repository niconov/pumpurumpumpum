# frozen_string_literal: true

class BaseService
  include ActiveModel::Model

  class Failure < RuntimeError; end

  @transactional = false

  attr_accessor :response_status, :result, :exception

  class << self
    attr_reader :transactional, :debug

    def with_transaction
      @transactional = true
    end
  end

  def initialize(params = {})
    super params
    @response_status = default_failure_status
    replace_method_call
  end

  def failure?
    !success?
  end

  def success?
    errors.empty?
  end

  def public_error
    errors.full_messages.first
  end

  def private_error
    errors
      .messages
      .map { |key, messages| [key == :base ? nil : key, messages.join(', ')].compact }
      .map { |error| error.join(' ') }.join('; ')
  end

  private

  def replace_method_call
    class_eval do
      alias_method :handle_call, :call if method_defined?(:call)
      alias_method :handle_call, :call! if method_defined?(:call!)

      %i[call call!].each do |method|
        define_method(method) do
          return self if invalid?

          @result = self.class.transactional ? ActiveRecord::Base.transaction { handle_call } : handle_call
          self
        rescue Failure
          handle_error
          self
        rescue StandardError => e
          Rails.logger.error(:base, "service raised exception: #{e}")
          # Sentry.capture_exception(e)
          raise e if method == :call!

          @exception = e
          handle_error
          self
        end
      end
    end
  end

  def safe(message = default_error_message, field: default_error_field, status: default_failure_status)
    yield
  rescue StandardError => e
    add_error(message, field: field)
    @response_status = status
    raise e
  end

  def handle_error
    add_error_if_empty
    return_on_error
  end

  def failure!(message = nil, field: default_error_field, status: default_failure_status)
    add_error(message, field: field) if message
    @response_status = status
    raise Failure, message
  end

  def add_errors_from(object)
    object.errors.each do |field, msg|
      add_error(msg, field: field)
    end
  end

  def add_errors_to_object(object)
    errors.each do |field, msg|
      object.errors.add(msg, field)
    end
  end

  def add_error_if_empty
    add_error(default_error_message) if errors.empty?
  end

  def add_error(message, field: default_error_field)
    errors.add(field, message)
  end

  def return_on_error
    false
  end

  def default_error_field
    :base
  end

  def default_error_message
    'Something went wrong'
  end

  def default_failure_status
    400
  end

end