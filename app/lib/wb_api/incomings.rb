class WBApi

  def incomings_update
    # income = Income::Income.where(project_id: project.id).order(date: :asc).last
    url = "https://statistics-api.wildberries.ru/api/v1/supplier/incomes"
    ts = Time.now - 14.days
    response = get(url) do |req|
      req.params['dateFrom'] = ts
    end
    incomings = JSON.parse response.body
    incomings.map {|income| incoming_update_item_db income}
  end

  def incoming_update_item_db(income_api)
    # ap income_api
    keys = [
      "incomeId", 
      "number", 
      "date", 
      "lastChangeDate", 
      "supplierArticle", 
      "techSize", 
      "barcode", 
      "quantity", 
      "totalPrice", 
      "dateClose", 
      "warehouseName", 
      "nmId", 
      "status"
    ]
    
    income = Income::Income.find_or_create_by(income_id: income_api['incomeId'], nm_id: income_api['nmId'])

    keys.map do |key|
      # ap "=" * 100
      value = income_api[key]
      # ap value
      # ap income_api[key]
      # ap key
      # ap key.underscore
      income.send "#{key.underscore}=", value
    end
    income.project_id = project.id
    ap income.as_json
    income.save
    ap 'ok'

    
  end

end