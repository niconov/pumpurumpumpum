class WBApi

	def stocks_update
    url = 'https://statistics-api.wildberries.ru/api/v1/supplier/stocks'
    response = get(url) do |req|
      req.params["dateFrom"] = Time.now.beginning_of_day.to_datetime.rfc3339
    end

    stocks_api = JSON.parse response.body

    ap "Project ID #{project.id}. Fetched #{stocks_api.count} rows"

    if stocks_api.count > 0
      Stock::Stock.where(project_id: project.id).delete_all
      stocks_api.map do |stock|
        name_mapped = Warehouse::Warehouse.name_mapping stock['warehouseName']
        wh = Warehouse::Warehouse.find_or_create_by(name: name_mapped)
        s = {
          "project_id" => project.id,
          "is_supply" => stock['isSupply'],
          "price" => stock['Price'],
          "discount" => stock['Discount'],
          "is_realization" => stock['isRealization'],
          "tech_size" => stock['techSize'],
          "quantity_full" => stock['quantityFull'],
          "in_way_to_client" => stock['inWayToClient'],
          "in_way_from_client" => stock['inWayFromClient'],
          "quantity" => stock['quantity'],
          "nm_id" => stock['nmId'],
          "warehouse_name" => stock['warehouseName'],
          "warehouse_id" => wh.id,
          "supplier_article" => stock['supplierArticle'],
          "last_change_date" => stock['lastChangeDate'],
        }
        Stock::Stock.new(s).save
      end
    end


	end
	
end