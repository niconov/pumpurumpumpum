class WBApi
	private
	def search_priority_query(point, query, page)
		status = 0
    while status != 200
	    resp = get_public("https://search.wb.ru/exactmatch/ru/common/v4/search") do |req|
	      req.params = {
	        'query': query.query,
	        'resultset': 'catalog',
	        'appType': 1,
	        'curr': 'rub',
	        'dest': point.dest,
	        'regions': point.regions,
	        'page': page + 1,
	        'sort': 'popular',
	        'spp': 23
	      }
	    end
	    status = resp.status
	    # ap status
	    if status != 200
	      App.logger.debug "[#{status}] Response status error. Sleeping"
	      sleep 15
	    else
	      body = JSON.parse resp.body
	      return body['data']['products']
	    end
    end
	end

	public

	def update_delivery_priorities
		pq_count = Deliveries::PriorityQuery.count
		pp_count = Deliveries::PickupPoint.count
		cnt = 0
		Deliveries::PriorityQuery.all.map do |q|
			Deliveries::PickupPoint.all.map do |point|
				cnt += 1
				result = []
        q.pages.times do |page|
					result += search_priority_query point, q, page 
        end
        if result != []
        	# ap Deliveries::WarehousePriority.where(priority_query_id: q.id, point_id: point.id).all.destroy_all
        end
        ap "Update delivery priorities #{cnt}/#{pp_count*pq_count} queries: #{pq_count}, points: #{pp_count}"
        result.map do |r|
        	if r['sizes'].count == 1
        		# ap r['sizes']
	        	warehouse = r['sizes'][0]['wh']
	        	# ap warehouse
			      opts = {
			      	priority_query_id: q.id, 
			      	point_id: point.id, 
			      	district_id: point.district_id,
			      	warehouse_web_id: warehouse
			      } 
			      # ap opts

			      wp = Warehouse::Priority.find_or_create_by opts
			   		# ap wp
			   		wp.time1 = r['time1']
			   		wp.time2 = r['time2']
			   		wp.time  = r['time1'] + r['time2']
			   		wp.priority_query_id  = q.id
			   		wp.warehouse_web_id  = warehouse
			   		unless wp.save
		   				ap wp.errors.full_messages
		   			end
			   	end
		    end
				# sleep 5
			end
		end
	end
    # [94] {e

end