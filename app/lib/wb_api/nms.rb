class WBApi

	def nms_update
    # url = 'https://suppliers-api.wildberries.ru/content/v1/cards/cursor/list'
    url = 'https://suppliers-api.wildberries.ru/content/v2/get/cards/list'
    body = {
      "settings": {
        "cursor": {
          "limit": 1000
        },
        "filter": {
          "withPhoto": -1
        }
      }
    }
    # ap body
    response = post(url) { |req| req.body = body.to_json }
    # ap JSON.parse response.body
    if response.status == 200
      data = JSON.parse response.body
      cards = data['cards']
      # ap cards
      cnt = cards.count
      i = 0
      cards.map do |card|
        i += 1
        # puts "Номенклатура. Обновление: #{i}/#{cnt}"
        nm = Nm::Nm.find_or_create_by nm_id: card['nmID']
        nm.nm_id = card['nmID']
        nm.prohibited = card['isProhibited']
        nm.supplier_article = card['vendorCode']
        nm.barcode = card['sizes'][0]['skus'][0]
        nm.imt_id = card['nmID']
        nm.project_id = project.id
        nm.object = card["subjectName"]
        nm.obj_id = card["subjectID"]
        skus = ''
        # ap card 
        card['sizes'].map do |s|
          s['skus'].map do |i|
            skus += i + " "
          end
        end
        skus.strip!
        nm.skus = skus
        # ap nm.as_json
        nm.save
      end
    end
		
	end

  def nms_verbose_update
    url = 'https://suppliers-api.wildberries.ru/content/v1/cards/filter'
    project.nms.each_slice(100) do |nms_batch|
      response = post(url) do |req|
        body = {
          "vendorCodes" => nms_batch.map { |i| i.supplier_article },
          "allowedCategoriesOnly" => false
        }
        req.body = body.to_json
      end

      if response.status == 200
        nms_list_api = JSON.parse response.body
        nms_list_api['data'].map do |nm_api|
          nm_api['dimensions']
          nm = Nm::Nm.find_by project_id: project.id, supplier_article: nm_api['vendorCode']
          nm.dim_wb_a = nm_api['dimensions']['width'] if nm_api['dimensions']
          nm.dim_wb_b = nm_api['dimensions']['height'] if nm_api['dimensions']
          nm.dim_wb_c = nm_api['dimensions']['length'] if nm_api['dimensions']
          if nm.dim_wb_a and nm.dim_wb_c and nm.dim_wb_b
            nm.volume = (nm.dim_wb_a.to_f * nm.dim_wb_b.to_f * nm.dim_wb_c.to_f / 1000).ceil
          end
          nm_api['characteristics'].map do |c|
            k = 'Вес товара с упаковкой (г)'
            if c[k]
              nm.dim_wb_m = c[k] / 1000
            end
          end
          # nm.dim_a_wb = nm_api['dimensions']['width'] if nm_api['dimensions']
        end
      end



    end
    
  end

end
