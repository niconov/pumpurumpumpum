require "awesome_print"
class WBApi

	def adverts_update
		list = adverts_list.sort_by {|i| i['createTime'] }
		# ap list

		count_list = list.count
		counter = 0
		skipped = 0
		completed = 0

		list.map do |advert_api|
			counter +=1 

			advert = nil
			advert_id = advert_api['advertId']
			Advert::Advert.uncached do
				advert = Advert::Advert.find_by(project_id: project.id, advert_id: advert_id)
			end
			new_advert = advert == nil
			if not new_advert and [-1, 7].include? advert.status.to_i and advert_api['status'] == advert.status.to_i
				skipped += 1
			else
				if advert
					check_changing advert, advert_api
				else
					advert = Advert::Advert.create(advert_id: advert_api['advertId'], project_id: project.id)
					event_advert_created advert, advert_api
				end
				completed += 1
			end
			advert.update_from_api 
		end
		ap "Project #{project.id} #{project.name}: Updating adverts OK/SKIP/TOTAL #{completed}/#{skipped}/#{count_list}"

	end

	def adverts_list
		url = "https://advert-api.wb.ru/adv/v0/adverts"
  	resp = get url
		return JSON.parse resp.body
	end

	def advert_update_verbose(advert)
		advert_id = advert.advert_id.to_i
  	advert_api = advert_get_verbose advert_id

    advert_params = advert_api['params']
    advert.response_raw = advert_api
		advert.name = advert_api['name']
		advert.advert_id = advert_api['advertId']
		advert.type_id = advert_api['type']
		advert.status = advert_api['status']

    if advert_params and advert.status != 9
      if advert_params[0]["subjectId"]
        advert.param_value = advert_params[0]["subjectId"]
      else
        advert.param_value = advert_params[0][advert.param_name]
      end
    end
    if advert_api['autoParams'] and advert_api['type'] == 8
    	nms = advert_api['autoParams']['nms']
    	Advert::Param.where(advert_id: advert_id).where.not(nm_id: nms).map { |e| e.destroy! }
    	nms.map do |nm|
	    	adparam = Advert::Param.find_or_create_by(advert_id: advert_id, nm_id: nm)
	    	adparam.carousel = advert_api['autoParams']['active']['carousel']
	    	adparam.recom = advert_api['autoParams']['active']['recom']
	    	adparam.booster = advert_api['autoParams']['active']['booster']
	    	advert.cpm = advert_api['autoParams']['cpm']
	    	# ap adparam.as_json
	    	adparam.save!
	    end

    end
    advert.save!
    advert
	end

	def advert_get_verbose(advert_id)
		url = 'https://advert-api.wb.ru/adv/v0/advert'
  	resp = get(url) do |req|
      req.params = {'id' => advert_id.to_i }
  	end
  	return JSON.parse resp.body		
	end

	private



	def event_advert_created(advert, advert_api)
		text = "Создана новая РК #{advert_api['advertId']} #{advert_api['name']}"
		create_event advert, text
	end

	def check_changing(advert, advert_api)
		if advert.status.to_s != advert_api['status'].to_s
			ad_new = Advert.new status: advert_api['status']
			text = "Изменился статус РК #{advert_api['advertId']} '#{ad_new.status_humanize}' (было #{advert.status_humanize}) #{advert_api['name']} "
			create_event advert, text
		end
	end

	def create_event(advert, t)
		Event.create({
			subject: "adverts",
			text: t,
			subject_id: advert.id,
			date: Time.now,
			project_id: project.id,
			priority: 0
		})
	end
end		