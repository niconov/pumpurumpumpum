require "awesome_print"

require_relative "feedbacks.rb"
require_relative "adverts.rb"
require_relative "adverts_auto.rb"
# require_relative "adverts_stats.rb"
require_relative "orders.rb"
require_relative "nms.rb"
# require_relative "prices.rb"
require_relative "positions.rb"
require_relative "stocks.rb"
require_relative "incomings.rb"
require_relative "delivery_prioriries.rb"
require_relative "warehouse_from_web.rb"
require_relative "user_agents.rb"

class WBApi 

	attr_accessor :project, :proxy, :attempts, :timeout
	
	def initialize(project, attempts = 5, timeout = 15 )
		self.project = project
		self.proxy = 'https://109.195.187.178:9150'
		self.attempts = attempts
		self.timeout = timeout
	end

	def conn
		headers = { 'Content-Type' => 'application/json', 'Authorization' => project.token }
		return Faraday.new(headers: headers)
	end

	def conn_public
		headers = { 'Content-Type' => 'application/json'}
		# Faraday.new(headers: headers, proxy: proxy)
		return Faraday.new(headers: headers)
	end

	def get(url, &block)
		ts = Time.now
		attempts_count = 0
		status = 0
		while status != 200
			if block_given?
   				response = conn.get(url) {|i| yield i } 
   			else
   				response = conn.get(url)
   			end
	   		status = response.status
	   		if not [200, 0].include? status 
					attempts_count += 1
	   			raise if attempts_count == self.attempts
	   			raise if Time.now - ts > self.timeout
	   			ap response
	   			ap response.body
	   			sleep 5
	   		end 
		end
		return response
	end

	def patch(url, &block)
		ts = Time.now
		attempts_count = 0
		status = 0
		while status != 200
			if block_given?
   				response = conn.patch(url) {|i| yield i } 
   			else
   				response = conn.patch(url)
   			end	   		
	   		# print response.body
   			status = response.status
	   		if not [200, 0].include? status 
					attempts_count += 1
	   			raise if attempts_count == self.attempts
	   			raise if Time.now - ts > self.timeout
 	   			ap response
	   			ap response.body
	   			sleep 5
	   		end 
		end
		return response
	end

	def post(url, &block)
		ts = Time.now
		attempts_count = 0
		status = 0
		while status != 200
			if block_given?
 				response = conn.post(url) {|i| yield i } 
 			else
 				response = conn.post(url)
 			end	   		
   		# print response.body
 			status = response.status
   		if not [200, 0].include? status 
				attempts_count += 1
   			raise if attempts_count == self.attempts
   			raise if Time.now - ts > self.timeout
   			ap response
   			ap response.body
   			sleep 5
   		end 
		end
		return response
	end

		def post_with_body(url, body, &block)
		ts = Time.now
		attempts_count = 0
		status = 0
		while status != 200
			if block_given?
 				response = conn.post(url, body) {|i| yield i } 
 			else
 				response = conn.post(url, body)
 			end	   		
   		# print response.body
 			status = response.status
   		if not [200, 0].include? status 
				attempts_count += 1
   			raise if attempts_count == self.attempts
   			raise if Time.now - ts > self.timeout
   			ap response
   			ap response.body
   			sleep 5
   		end 
		end
		return response
	end

	def get_public(url, &block)
		ts = Time.now
		attempts_count = 0
		status = 0
		while status != 200
			if block_given?
 				response = conn_public.get(url) do |i| 
					i.headers['User-Agent'] = user_agents.sample 
					yield i 
				end
 			else
 				response = conn_public.get(url) do |req|
					req.headers['User-Agent'] = user_agents.sample
				end
 			end	   		

 			status = response.status
 			# ap JSON.parse response.body
   		if not [200, 0].include? status 
				attempts_count += 1
   			raise if attempts_count == self.attempts
   			raise if Time.now - ts > self.timeout
   			# ap response
   			# ap response.body
   			sleep 5
   		end 
		end
		return response
	end

	def post_public(url, &block)
		ts = Time.now
		attempts_count = 0
		status = 0
		headers = { 'Content-Type' => 'application/json', 'x-client-name': 'site' }
		while status != 200
			if block_given?
 				response = conn_public.post(url) {|i| yield i } 
 			else
 				response = conn_public.post(url)
 			end	   		
 			status = response.status
   		if not [200, 0].include? status 
				attempts_count += 1
   			raise if attempts_count == self.attempts
   			raise if Time.now - ts > self.timeout
   			ap response
 				ap JSON.parse response.body
   			sleep 5
   		end 
		end
		return response
	end




end