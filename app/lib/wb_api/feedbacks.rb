class WBApi

	def autoresponder
		url_response = 'https://feedbacks-api.wb.ru/api/v1/feedbacks'
		responses = {}
		Autoresponser.where(project_id: project.id).map do |ar|
			responses[ar.rating] = ar.text
		end
		unfb = self.feedbacks_archived
		unfb += self.feedbacks_unanswered
		unfb = unfb.select {|i| i['answer'] == nil }
		# unfb = unfb.select {|i| i['text'] == '' and i['answer'] == nil }
    unfb_count = 0
    aswd  = 0
    unfb.map do |fb|
    	rating = fb['productValuation']
      unfb_count += 1 
      if responses[rating]
    		patch(url_response) do |req|
          req.body = {'id' => fb['id'], 'text' => responses[rating] }.to_json
	        aswd += 1
    		end
      else
	      if fb['answer'] == nil and fb['text'] == ''
	        fb_resp = patch(url_response) do |req|
	          req.body = {'id' => fb['id'], 'text' => "Большое спасибо за обратную связь!" }.to_json
	        end
	        aswd += 1
	      end
      end
    end
    print "Client ID #{project.id}. Новых отзывов: #{unfb_count}, ответили на #{aswd} отзывов\n"
	end

	def feedbacks_archived
		url = 'https://feedbacks-api.wb.ru/api/v1/feedbacks/archive'

	    fb_resp = get(url) do |req|
	      req.params = {'take' => 1000, 'skip' => 0, 'order' => 'dateDesc' }
	    end
	    fb_resp_body = JSON.parse fb_resp.body
	    if fb_resp.status != 200
	      ap fb_resp_body
	    end
	    fbs = fb_resp_body['data']['feedbacks']
	    return fbs
	end

	def feedbacks_unanswered
		url = 'https://feedbacks-api.wb.ru/api/v1/feedbacks'
	    fb_resp = get(url) do |req|
	    	req.params = {take: 4000, skip: 0, isAnswered: false}
		end
		response = JSON.parse fb_resp.body
		return response['data']['feedbacks']
		# response['data']['feedbacks'].select {|i| i['text'] == nil }
	end

end