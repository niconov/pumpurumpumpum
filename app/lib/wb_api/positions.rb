class WBApi

	def search_params_defaults
		{
			"resultset" => "catalog",
			"appType" => 1,
			"curr" => "rub",
			"spp" => 99,
			"regions" => '80,38,83,4,64,33,68,70,30,40,86,69,1,66,22,48,31,114',
			"dest" => "-1252424",
		}
	end

	def search_request(query, page)
		url = 'https://search.wb.ru/exactmatch/ru/common/v4/search'
		get_public(url) do |req|
			search_params_defaults.map do |k, v|
				req.params[k] = v
			end
			req.params['page'] = page + 1
	    req.params['query'] = query
		end
	end

	def positions_by_queries_update(query_id = nil)
		pages = 5

		opts = {project_id: project.id}
		opts[:id] = query_id if query_id != nil

		queries = NmGroupQuery.where(opts).order("RANDOM()").map do |q|
			period = Time.now.beginning_of_day
			products = []

			ap "Запрос позиций #{q.query} #{q.name}"
			pages.times do |page|
				response = search_request q.query, page
				body = JSON.parse response.body
				products += body['data']['products']
			end
			ap "Получено #{products.count} по запросу #{q.query} #{q.name}"
			# ap products



			with_promo_position = products.select { |hash| hash['log'] && hash['log']['promoPosition'] }
			without_promo_position = products.reject { |hash| hash['log'] && hash['log']['promoPosition'] }

			sorted_array = with_promo_position.sort_by { |hash| hash['log']['promoPosition'] }

			sorted_array.each do |hash|
			  position = hash['log']['promoPosition'].to_i 
			  without_promo_position.insert(position, hash)
			end
			products = without_promo_position
			products.compact!
			products.each_with_index do |h, i|
			  products[i]['position'] = i
			end
			# ap products

			products.map do |pr|
				nmc = NmCompetitor.find_or_create_by query_id: q.id, nm_id: pr['id']
				nmc.price_rrc = pr['priceU'] / 100 if pr['priceU']
				nmc.price_with_spp = pr['salePriceU'] / 100 if pr['salePriceU']
				nmc.position = pr['position']
				nmc.raw = pr
				nmc.save
			end

			nmcs = NmCompetitor.where(query_id: q.id).all
			nmcs.each_slice(150) do |items|
				# ap items
        product_ids = items.map { |pr| pr.nm_id }
        url_1 = "https://card.wb.ru/cards/detail"
        
        resull = get_public(url_1) do |req|
          req.params['nm'] = (product_ids.inject('') {|s, i| s += "#{i};"}).chop
          req.params['appType'] = 0
          req.params['curr'] = 'rub'
          req.params['spp'] = 99
          # ap req.params
        end
        res1 = JSON.parse resull.body
        req1 = res1['data']['products']
        req1.map do |nm_a|
        	# ap nm_a
        	nmc = NmCompetitor.find_or_create_by(query_id: q.id, nm_id: nm_a['id'])
        	nmc.raw_card = nm_a
        	nmc.prepare_from_raw
        	nmc.save
        end
			end
			# NmCompetitor.where('price is NULL or price_rrc is NULL or price_with_spp is NULL').map do |nc|
			# 	nc.prepare_from_raw
			# end
			NmCompetitor.where(raw_card: nil, query_id: q.id).destroy_all

			products_with_promo = products.select {|i| i['log'] != {} }
			products = products.select {|i| i['log'] == {} }
			products_with_promo.map do |product|
				products.insert product['log']['promoPosition'], product
			end

			nms = NmGroupQueryItem.select(:nm_id).where(nm_group_query_id: q.id).pluck(:nm_id) 
			nms_found = []
			products.compact!
			products.each_with_index do |i, index|
				if nms.include? i['id']
					nms_found.push i['id']
					np = NmPosition.find_or_create_by nm_id: i['id'], period: period
					np_prev = NmPosition.where(nm_id: i['id']).order(updated_at: :desc).first
					np.promo = i['promoTextCat'] ? i['promoTextCat'] : '' 
					np.dist = i['dist']
					np.time1 = i['time1']
					np.time2 = i['time2']
					np.position = index
					if i['log'] != {}
						has_changes = true if np.position_no_advert != i['log']['position']
						np.cpm = i['log']['cpm']
						np.position_no_advert = i['log']['position']
					else
						has_changes = true if np.position_no_advert != index
						np.cpm = 0
						np.position_no_advert = index
					end

					has_changes = false
					has_changes = true if np.position != np_prev.position 
					has_changes = true if np.position_no_advert != np_prev.position_no_advert

					# if has_changes 
					# 	nml = Nm::Nm.find_by(nm_id: i['id'])
					# 	e = Event.new project_id: project.id, subject: 'nm', date: Time.now, priority: 0, subject_id: nml.nm_id
					# 	text =  "Позиция номенклатуры в поиске изменена. #{nml.nm_id} #{nml.supplier_article}. "
					# 	text += "Текущая позиция: #{np.position}/#{np.position_no_advert}. Было #{np_prev.position}/#{np_prev.position_no_advert}"
					# 	e.text = text
					# 	ap "Позиция номенклатуры #{nml.nm_id} #{nml.supplier_article} изменилась. #{np.position}/#{np.position_no_advert}. Было #{np_prev.position}/#{np_prev.position_no_advert}."
					# 	e.save!
					# end
					np.save!
					np.touch
				end
			end
			nms.map do |nm|
				np = NmPosition.find_by(nm_id: nm, period: period)
				if not np
					np = NmPosition.find_or_create_by nm_id: nm, period: period
					np.touch
					np.save
				end
			end

			nms_not_found = []
			nms.map do |nm|
				if not nms_found.include? nm
					nms_not_found.push nm
				end
			end

			nms_not_found.map do |nm|
				np = NmPosition.find_by(nm_id: nm, period: period)
				np.position = nil
				np.position_no_advert = nil
				np.save
				np.touch
			end

			# ap 'nms not found'
			# ap nms_not_found
			# ap 'nms_found'
			# ap nms_found
			# ap 'nms'
			# ap nms
			# break
			sleep 5
		end


	end

end