class WBApi

	def advert_create_with_manager(promotion_manager)
		# url = 'https://advert-api.wb.ru/adv/v1/save-ad'
		# # Advert::PromotionManager.find_by id: promotion_manager
		# nm = Nm::Nm.find_by nm_id: promotion_manager.nm_id
		# body = {
		# 	name: "#{nm.supplier_article}::PumPurum",
		# 	btype: 1,
		# 	sum: 500,
		# 	type: 8,
		# 	on_pause: true,
		# 	subjectId: nm.obj_id
		# }
		# response = post(url) do |req|
		# 	req.body = body.to_json
		# end
		# ap response
		# ap JSON.parse response.body
		# advert_id = JSON.parse response.body
		# promotion_manager.advert_id = advert_id
		# promotion_manager.save
		# JSON.parse response.body
	end


	def advert_auto_get_nms(advert_id)
		url = 'https://advert-api.wb.ru/adv/v1/auto/getnmtoadd'
		res = get url do |req|
			req.params['id'] = advert_id
		end
		ap 'advert_auto_get_nms'
		ap res
		return JSON.parse res.body
	end

	def advert_auto_set_zones(advert_id)
		# url_set_zone = 'https://advert-api.wb.ru/adv/v1/auto/active'
		# post(url_set_zone) do |req|
		# 	req.params['id'] = advert_id
		# 	req.body = {
		# 		recom: false,
		# 		booster: true,
		# 		carousel: false
		# 	}.to_json
		# end
	end

	def advert_auto_set_nms_by_promotion_manager(promotion_manager)
		verbose = advert_get_verbose promotion_manager.advert_id
		already_in_advert = verbose['autoParams']['nms']
		advert_auto_set_zones promotion_manager.advert_id

		nm = Nm::Nm.find_by nm_id: promotion_manager.nm_id
		nm_to_delete = Nm::Nm.where(obj_id: nm.obj_id).where.not(nm_id: nm.nm_id).pluck(:nm_id).map { |e| e.to_i  }

		body = {} 
		body[:add]    = [ promotion_manager.nm_id ]
		body[:delete] = nm_to_delete if nm_to_delete != []

		if body != {} 
			url = 'https://advert-api.wb.ru/adv/v1/auto/updatenm'
			response = post(url) do |req|
				req.params['id'] = promotion_manager.advert_id
				req.body = body.to_json
			end
			ap body
			ap response
		end

	end

end












# 