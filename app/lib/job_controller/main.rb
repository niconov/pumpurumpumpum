module PPJobController

  def self.can_be_started(cl, arg)
    ap "can_be_started"
    result = true
    Sidekiq::Workers.new.each do |_process_id, _thread_id, work|
      pl = JSON.parse work['payload']
      if pl['class'] == cl and pl['args'][0] == arg
        can_be_started = false
      end
    end
    ap result
    result
  end

end