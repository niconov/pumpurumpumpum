namespace :data do
  desc "Run all tasks"
  task import_all: [:comissions, :pickup_points, :warehouse_from_web, :warehouse_to_districts] do
    # If you add more tasks, append them here
    puts "Все задачи выполнены"
  end
end