require "csv"

namespace :data do
  task comissions: :environment do
    filepath = Dir.getwd + '/storage/comission.csv'
    puts "Попытка найти обновления комиссий (#{filepath})"
    if File.file? filepath
      puts "Найден новый файл с комиссиями"

      arr_of_rows = CSV.read filepath
      # ap arr_of_rows[0]
      arr_of_rows = arr_of_rows.drop(1)
      # ap arr_of_rows[0]
      arr_of_rows.map do |i|
        c = Comission.find_or_create_by category: i[0], subject: i[1]
        c.fbo = i[2]
        c.fbs = i[3]
        c.dbs = i[4]
        if c.changed
          changes = c.changes
          if changes != {}
            puts "Изменилась комиссия\nКатегория: #{c.category}\nПредмет: #{c.subject}"
            changes.keys.map do |key|
              puts "#{key}: #{changes[key][0]} -> #{changes[key][1]}"
            end
            puts "\n"
          end
        end
        c.save
      end
      File.rename filepath, "#{Dir.getwd}/storage/comission_#{Time.now.to_i}_processed.csv"
    else
      puts "Файл с комиссиями не найден"
    end

    # id: 7641,
    # category: "Ювелирные украшения",
    # subject: "Ювелирные часы-кулоны",
    # fbo: 15.0,
    # fbs: 15.0,
    # dbs: 15.0>
    puts 'Задача: comissions завершена'

  end
end