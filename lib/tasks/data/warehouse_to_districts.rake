namespace :data do
  task warehouse_to_districts: :environment do
    whs_to_districts = {
      "data": [
        {
          "location": "Центральный",
          "warehouses": [
            {
              "warehouseName": "Коледино",
              "boxDeliveryAndStorage": 145,
              "boxDeliveryAndStorageColor": "red",
              "boxDeliveryAndStorageVisible": true,
              "palletDelivery": 180,
              "palletDeliveryColor": "red",
              "palletStorage": 125,
              "palletStorageColor": "orange",
              "palletVisible": true
            },
            {
              "warehouseName": "Подольск",
              "boxDeliveryAndStorage": 180,
              "boxDeliveryAndStorageColor": "red",
              "boxDeliveryAndStorageVisible": true,
              "palletDelivery": 200,
              "palletDeliveryColor": "red",
              "palletStorage": 100,
              "palletStorageColor": "orange",
              "palletVisible": false
            },
            {
              "warehouseName": "Электросталь",
              "boxDeliveryAndStorage": 105,
              "boxDeliveryAndStorageColor": "orange",
              "boxDeliveryAndStorageVisible": true,
              "palletDelivery": 95,
              "palletDeliveryColor": "orange",
              "palletStorage": 95,
              "palletStorageColor": "orange",
              "palletVisible": true
            },
            {
              "warehouseName": "Белая дача",
              "boxDeliveryAndStorage": 130,
              "boxDeliveryAndStorageColor": "orange",
              "boxDeliveryAndStorageVisible": true,
              "palletDelivery": 125,
              "palletDeliveryColor": "orange",
              "palletStorage": 0,
              "palletStorageColor": "green",
              "palletVisible": true
            },
            {
              "warehouseName": "Белые Столбы",
              "boxDeliveryAndStorage": 175,
              "boxDeliveryAndStorageColor": "red",
              "boxDeliveryAndStorageVisible": true,
              "palletDelivery": 120,
              "palletDeliveryColor": "orange",
              "palletStorage": 100,
              "palletStorageColor": "orange",
              "palletVisible": false
            },
            {
              "warehouseName": "Тула",
              "boxDeliveryAndStorage": 125,
              "boxDeliveryAndStorageColor": "orange",
              "boxDeliveryAndStorageVisible": true,
              "palletDelivery": 280,
              "palletDeliveryColor": "red",
              "palletStorage": 280,
              "palletStorageColor": "red",
              "palletVisible": true
            },
            {
              "warehouseName": "Чехов 1",
              "boxDeliveryAndStorage": 135,
              "boxDeliveryAndStorageColor": "orange",
              "boxDeliveryAndStorageVisible": false,
              "palletDelivery": 190,
              "palletDeliveryColor": "red",
              "palletStorage": 190,
              "palletStorageColor": "red",
              "palletVisible": true
            },
            {
              "warehouseName": "Пушкино",
              "boxDeliveryAndStorage": 105,
              "boxDeliveryAndStorageColor": "orange",
              "boxDeliveryAndStorageVisible": true,
              "palletDelivery": 170,
              "palletDeliveryColor": "red",
              "palletStorage": 170,
              "palletStorageColor": "red",
              "palletVisible": true
            },
            {
              "warehouseName": "Домодедово",
              "boxDeliveryAndStorage": 145,
              "boxDeliveryAndStorageColor": "red",
              "boxDeliveryAndStorageVisible": false,
              "palletDelivery": 140,
              "palletDeliveryColor": "red",
              "palletStorage": 100,
              "palletStorageColor": "orange",
              "palletVisible": false
            },
            {
              "warehouseName": "Домодедово СГТ",
              "boxDeliveryAndStorage": 105,
              "boxDeliveryAndStorageColor": "orange",
              "boxDeliveryAndStorageVisible": false,
              "palletDelivery": 100,
              "palletDeliveryColor": "orange",
              "palletStorage": 100,
              "palletStorageColor": "orange",
              "palletVisible": false
            },
            {
              "warehouseName": "Чехов 2",
              "boxDeliveryAndStorage": 150,
              "boxDeliveryAndStorageColor": "red",
              "boxDeliveryAndStorageVisible": true,
              "palletDelivery": 210,
              "palletDeliveryColor": "red",
              "palletStorage": 210,
              "palletStorageColor": "red",
              "palletVisible": true
            },
            {
              "warehouseName": "Вёшки",
              "boxDeliveryAndStorage": 120,
              "boxDeliveryAndStorageColor": "orange",
              "boxDeliveryAndStorageVisible": true,
              "palletDelivery": 290,
              "palletDeliveryColor": "red",
              "palletStorage": 290,
              "palletStorageColor": "red",
              "palletVisible": true
            },
            {
              "warehouseName": "СГТ Внуково",
              "boxDeliveryAndStorage": 105,
              "boxDeliveryAndStorageColor": "orange",
              "boxDeliveryAndStorageVisible": false,
              "palletDelivery": 100,
              "palletDeliveryColor": "orange",
              "palletStorage": 30,
              "palletStorageColor": "green",
              "palletVisible": true
            },
            {
              "warehouseName": "Обухово",
              "boxDeliveryAndStorage": 105,
              "boxDeliveryAndStorageColor": "orange",
              "boxDeliveryAndStorageVisible": false,
              "palletDelivery": 170,
              "palletDeliveryColor": "red",
              "palletStorage": 170,
              "palletStorageColor": "red",
              "palletVisible": true
            },
            {
              "warehouseName": "Иваново",
              "boxDeliveryAndStorage": 105,
              "boxDeliveryAndStorageColor": "orange",
              "boxDeliveryAndStorageVisible": false,
              "palletDelivery": 135,
              "palletDeliveryColor": "orange",
              "palletStorage": 200,
              "palletStorageColor": "red",
              "palletVisible": true
            },
            {
              "warehouseName": "Подольск 3",
              "boxDeliveryAndStorage": 115,
              "boxDeliveryAndStorageColor": "orange",
              "boxDeliveryAndStorageVisible": false,
              "palletDelivery": 245,
              "palletDeliveryColor": "red",
              "palletStorage": 245,
              "palletStorageColor": "red",
              "palletVisible": true
            },
            {
              "warehouseName": "Радумля 1",
              "boxDeliveryAndStorage": 105,
              "boxDeliveryAndStorageColor": "orange",
              "boxDeliveryAndStorageVisible": true,
              "palletDelivery": 265,
              "palletDeliveryColor": "red",
              "palletStorage": 265,
              "palletStorageColor": "red",
              "palletVisible": true
            },
            {
              "warehouseName": "Радумля СГТ",
              "boxDeliveryAndStorage": 105,
              "boxDeliveryAndStorageColor": "orange",
              "boxDeliveryAndStorageVisible": true,
              "palletDelivery": 100,
              "palletDeliveryColor": "orange",
              "palletStorage": 100,
              "palletStorageColor": "orange",
              "palletVisible": true
            },
            {
              "warehouseName": "Подольск 4",
              "boxDeliveryAndStorage": 125,
              "boxDeliveryAndStorageColor": "orange",
              "boxDeliveryAndStorageVisible": true,
              "palletDelivery": 230,
              "palletDeliveryColor": "red",
              "palletStorage": 230,
              "palletStorageColor": "red",
              "palletVisible": true
            },
            {
              "warehouseName": "Рязань (Тюшевское)",
              "boxDeliveryAndStorage": 100,
              "boxDeliveryAndStorageColor": "orange",
              "boxDeliveryAndStorageVisible": false,
              "palletDelivery": 70,
              "palletDeliveryColor": "green",
              "palletStorage": 70,
              "palletStorageColor": "green",
              "palletVisible": true
            },
            {
              "warehouseName": "Клин",
              "boxDeliveryAndStorage": 100,
              "boxDeliveryAndStorageColor": "orange",
              "boxDeliveryAndStorageVisible": false,
              "palletDelivery": 90,
              "palletDeliveryColor": "green",
              "palletStorage": 90,
              "palletStorageColor": "green",
              "palletVisible": true
            }
          ]
        },
        {
          "location": "Северо-Западный",
          "warehouses": [
            {
              "warehouseName": "Санкт-Петербург Уткина Заводь",
              "boxDeliveryAndStorage": 120,
              "boxDeliveryAndStorageColor": "orange",
              "boxDeliveryAndStorageVisible": true,
              "palletDelivery": 95,
              "palletDeliveryColor": "orange",
              "palletStorage": 100,
              "palletStorageColor": "orange",
              "palletVisible": false
            },
            {
              "warehouseName": "Санкт-Петербург Шушары",
              "boxDeliveryAndStorage": 65,
              "boxDeliveryAndStorageColor": "green",
              "boxDeliveryAndStorageVisible": true,
              "palletDelivery": 215,
              "palletDeliveryColor": "red",
              "palletStorage": 215,
              "palletStorageColor": "red",
              "palletVisible": true
            },
            {
              "warehouseName": "СГТ Шушары",
              "boxDeliveryAndStorage": 105,
              "boxDeliveryAndStorageColor": "orange",
              "boxDeliveryAndStorageVisible": false,
              "palletDelivery": 100,
              "palletDeliveryColor": "orange",
              "palletStorage": 100,
              "palletStorageColor": "orange",
              "palletVisible": true
            }
          ]
        },
        {
          "location": "Южный + Северо-Кавказский",
          "warehouses": [
            {
              "warehouseName": "Краснодар",
              "boxDeliveryAndStorage": 85,
              "boxDeliveryAndStorageColor": "green",
              "boxDeliveryAndStorageVisible": true,
              "palletDelivery": 125,
              "palletDeliveryColor": "orange",
              "palletStorage": 125,
              "palletStorageColor": "orange",
              "palletVisible": true
            },
            {
              "warehouseName": "Крыловское",
              "boxDeliveryAndStorage": 105,
              "boxDeliveryAndStorageColor": "orange",
              "boxDeliveryAndStorageVisible": false,
              "palletDelivery": 150,
              "palletDeliveryColor": "red",
              "palletStorage": 150,
              "palletStorageColor": "red",
              "palletVisible": true
            },
            {
              "warehouseName": "Невинномысск",
              "boxDeliveryAndStorage": 85,
              "boxDeliveryAndStorageColor": "green",
              "boxDeliveryAndStorageVisible": true,
              "palletDelivery": 270,
              "palletDeliveryColor": "red",
              "palletStorage": 270,
              "palletStorageColor": "red",
              "palletVisible": true
            }
          ]
        },
        {
          "location": "Дальневосточный",
          "warehouses": [
            {
              "warehouseName": "Хабаровск",
              "boxDeliveryAndStorage": 160,
              "boxDeliveryAndStorageColor": "red",
              "boxDeliveryAndStorageVisible": true,
              "palletDelivery": 220,
              "palletDeliveryColor": "red",
              "palletStorage": 100,
              "palletStorageColor": "orange",
              "palletVisible": false
            },
            {
              "warehouseName": "СЦ Хабаровск",
              "boxDeliveryAndStorage": 100,
              "boxDeliveryAndStorageColor": "orange",
              "boxDeliveryAndStorageVisible": true,
              "palletDelivery": 200,
              "palletDeliveryColor": "red",
              "palletStorage": 200,
              "palletStorageColor": "red",
              "palletVisible": true
            }
          ]
        },
        {
          "location": "Казахстан",
          "warehouses": [
            {
              "warehouseName": "Астана",
              "boxDeliveryAndStorage": 20,
              "boxDeliveryAndStorageColor": "green",
              "boxDeliveryAndStorageVisible": true,
              "palletDelivery": 65,
              "palletDeliveryColor": "green",
              "palletStorage": 100,
              "palletStorageColor": "orange",
              "palletVisible": false
            },
            {
              "warehouseName": "Атакент",
              "boxDeliveryAndStorage": 20,
              "boxDeliveryAndStorageColor": "green",
              "boxDeliveryAndStorageVisible": true,
              "palletDelivery": 10,
              "palletDeliveryColor": "green",
              "palletStorage": 0,
              "palletStorageColor": "green",
              "palletVisible": true
            }
          ]
        },
        {
          "location": "Приволжский",
          "warehouses": [
            {
              "warehouseName": "Казань",
              "boxDeliveryAndStorage": 145,
              "boxDeliveryAndStorageColor": "red",
              "boxDeliveryAndStorageVisible": true,
              "palletDelivery": 260,
              "palletDeliveryColor": "red",
              "palletStorage": 260,
              "palletStorageColor": "red",
              "palletVisible": true
            },
            {
              "warehouseName": "СЦ Кузнецк",
              "boxDeliveryAndStorage": 100,
              "boxDeliveryAndStorageColor": "orange",
              "boxDeliveryAndStorageVisible": true,
              "palletDelivery": 100,
              "palletDeliveryColor": "orange",
              "palletStorage": 100,
              "palletStorageColor": "orange",
              "palletVisible": true
            }
          ]
        },
        {
          "location": "Уральский",
          "warehouses": [
            {
              "warehouseName": "Екатеринбург - Испытателей 14г",
              "boxDeliveryAndStorage": 200,
              "boxDeliveryAndStorageColor": "red",
              "boxDeliveryAndStorageVisible": true,
              "palletDelivery": 290,
              "palletDeliveryColor": "red",
              "palletStorage": 100,
              "palletStorageColor": "orange",
              "palletVisible": false
            },
            {
              "warehouseName": "Екатеринбург - Перспективный 12",
              "boxDeliveryAndStorage": 150,
              "boxDeliveryAndStorageColor": "red",
              "boxDeliveryAndStorageVisible": true,
              "palletDelivery": 60,
              "palletDeliveryColor": "green",
              "palletStorage": 60,
              "palletStorageColor": "green",
              "palletVisible": true
            }
          ]
        },
        {
          "location": "Беларусь",
          "warehouses": [
            {
              "warehouseName": "Минск",
              "boxDeliveryAndStorage": 105,
              "boxDeliveryAndStorageColor": "orange",
              "boxDeliveryAndStorageVisible": false,
              "palletDelivery": 125,
              "palletDeliveryColor": "orange",
              "palletStorage": 125,
              "palletStorageColor": "orange",
              "palletVisible": true
            }
          ]
        },
        {
          "location": "Сибирский",
          "warehouses": [
            {
              "warehouseName": "Новосибирск",
              "boxDeliveryAndStorage": 340,
              "boxDeliveryAndStorageColor": "red",
              "boxDeliveryAndStorageVisible": true,
              "palletDelivery": 295,
              "palletDeliveryColor": "red",
              "palletStorage": 100,
              "palletStorageColor": "orange",
              "palletVisible": false
            }
          ]
        }
      ],
      "error": false,
      "errorText": "",
      "additionalErrors": {
        "errors": nil
      }
    }

    whs_to_districts = whs_to_districts[:data]
    # ap whs_to_districts

    whs_to_districts.map do |whd|
      district_names = []
      if whd[:location] == 'Южный + Северо-Кавказский'
        district_names = ['южный', 'северо-кавказский']
      else
        district_names = [whd[:location]]
      end
      district_names.map do |district_name|
        district = District.find_by name: district_name.downcase
        if not district
          ap "=" * 100
          ap "Округ не найден (data_warehouse_to_districts)"
          ap district_name
          ap "=" * 100
        end
        warehouses_local = whd[:warehouses]
        warehouses_local.map do |wh_local|
          name_mapped = Warehouse::Warehouse.name_mapping wh_local[:warehouseName]
          wh = Warehouse::Warehouse.find_by name: name_mapped

          if not wh
            # ap "=" * 100
            # ap "Склад не найден (data_warehouse_to_districts)"
            # ap district
            # ap district_name
            # ap wh_local
          else
            wh.district_id = district.id
            wh.save
          end
        end
      end

    end
    # Warehouse::FromWeb.all.destroy_all
    # res.map do |r|
    #     wfw = Warehouse::FromWeb.find_or_create_by warehouse_web_id: r[:id]
    #     wfw.name = Warehouse::Warehouse.name_mapping r[:name]
    #     wfw.belongs_wb = r[:belongs_wb]
    #     wfw.save
    #     wfw.update_warehouse_connection if wfw.belongs_wb
    # end
    Warehouse::FromWeb.where(belongs_wb: true).all.map { |i| i.update_warehouse_connection }

    puts 'Задача: warehouse_to_districts завершена'
  end
end