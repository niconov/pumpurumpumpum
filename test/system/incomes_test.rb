require "application_system_test_case"

class IncomesTest < ApplicationSystemTestCase
  setup do
    @income = incomes(:one)
  end

  test "visiting the index" do
    visit incomes_url
    assert_selector "h1", text: "Incomes"
  end

  test "should create income" do
    visit incomes_url
    click_on "New income"

    fill_in "Barcode", with: @income.barcode
    fill_in "Date", with: @income.date
    fill_in "Date close", with: @income.date_close
    fill_in "Income", with: @income.income_id
    fill_in "Last change date", with: @income.last_change_date
    fill_in "Nm", with: @income.nm_id
    fill_in "Number", with: @income.number
    fill_in "Quantity", with: @income.quantity
    fill_in "Status", with: @income.status
    fill_in "Supplier article", with: @income.supplier_article
    fill_in "Tech size", with: @income.tech_size
    fill_in "Total price", with: @income.total_price
    fill_in "Warehouse name", with: @income.warehouse_name
    click_on "Create Income"

    assert_text "Income was successfully created"
    click_on "Back"
  end

  test "should update Income" do
    visit income_url(@income)
    click_on "Edit this income", match: :first

    fill_in "Barcode", with: @income.barcode
    fill_in "Date", with: @income.date
    fill_in "Date close", with: @income.date_close
    fill_in "Income", with: @income.income_id
    fill_in "Last change date", with: @income.last_change_date
    fill_in "Nm", with: @income.nm_id
    fill_in "Number", with: @income.number
    fill_in "Quantity", with: @income.quantity
    fill_in "Status", with: @income.status
    fill_in "Supplier article", with: @income.supplier_article
    fill_in "Tech size", with: @income.tech_size
    fill_in "Total price", with: @income.total_price
    fill_in "Warehouse name", with: @income.warehouse_name
    click_on "Update Income"

    assert_text "Income was successfully updated"
    click_on "Back"
  end

  test "should destroy Income" do
    visit income_url(@income)
    click_on "Destroy this income", match: :first

    assert_text "Income was successfully destroyed"
  end
end
