require "application_system_test_case"

class AdvertsTest < ApplicationSystemTestCase
  setup do
    @advert = adverts(:one)
  end

  test "visiting the index" do
    visit adverts_url
    assert_selector "h1", text: "Adverts"
  end

  test "should create advert" do
    visit adverts_url
    click_on "New advert"

    fill_in "Advert", with: @advert.advert_id
    fill_in "Bid current", with: @advert.bid_current
    fill_in "Max cpm", with: @advert.max_cpm
    fill_in "Min place", with: @advert.min_place
    fill_in "Name", with: @advert.name
    fill_in "Param name", with: @advert.param_name
    fill_in "Param value", with: @advert.param_value
    fill_in "Search query", with: @advert.search_query
    fill_in "Status", with: @advert.status
    fill_in "Timestamps", with: @advert.timestamps
    fill_in "Type", with: @advert.type_id
    fill_in "User", with: @advert.user_id
    click_on "Create Advert"

    assert_text "Advert was successfully created"
    click_on "Back"
  end

  test "should update Advert" do
    visit advert_url(@advert)
    click_on "Edit this advert", match: :first

    fill_in "Advert", with: @advert.advert_id
    fill_in "Bid current", with: @advert.bid_current
    fill_in "Max cpm", with: @advert.max_cpm
    fill_in "Min place", with: @advert.min_place
    fill_in "Name", with: @advert.name
    fill_in "Param name", with: @advert.param_name
    fill_in "Param value", with: @advert.param_value
    fill_in "Search query", with: @advert.search_query
    fill_in "Status", with: @advert.status
    fill_in "Timestamps", with: @advert.timestamps
    fill_in "Type", with: @advert.type_id
    fill_in "User", with: @advert.user_id
    click_on "Update Advert"

    assert_text "Advert was successfully updated"
    click_on "Back"
  end

  test "should destroy Advert" do
    visit advert_url(@advert)
    click_on "Destroy this advert", match: :first

    assert_text "Advert was successfully destroyed"
  end
end
