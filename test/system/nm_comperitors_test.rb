require "application_system_test_case"

class NmComperitorsTest < ApplicationSystemTestCase
  setup do
    @nm_comperitor = nm_comperitors(:one)
  end

  test "visiting the index" do
    visit nm_comperitors_url
    assert_selector "h1", text: "Nm comperitors"
  end

  test "should create nm comperitor" do
    visit nm_comperitors_url
    click_on "New nm comperitor"

    fill_in "Nm group competitor", with: @nm_comperitor.nm_group_competitor_id
    click_on "Create Nm comperitor"

    assert_text "Nm comperitor was successfully created"
    click_on "Back"
  end

  test "should update Nm comperitor" do
    visit nm_comperitor_url(@nm_comperitor)
    click_on "Edit this nm comperitor", match: :first

    fill_in "Nm group competitor", with: @nm_comperitor.nm_group_competitor_id
    click_on "Update Nm comperitor"

    assert_text "Nm comperitor was successfully updated"
    click_on "Back"
  end

  test "should destroy Nm comperitor" do
    visit nm_comperitor_url(@nm_comperitor)
    click_on "Destroy this nm comperitor", match: :first

    assert_text "Nm comperitor was successfully destroyed"
  end
end
