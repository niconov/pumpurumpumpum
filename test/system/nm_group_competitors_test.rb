require "application_system_test_case"

class NmGroupCompetitorsTest < ApplicationSystemTestCase
  setup do
    @nm_group_competitor = nm_group_competitors(:one)
  end

  test "visiting the index" do
    visit nm_group_competitors_url
    assert_selector "h1", text: "Nm group competitors"
  end

  test "should create nm group competitor" do
    visit nm_group_competitors_url
    click_on "New nm group competitor"

    fill_in "Name", with: @nm_group_competitor.name
    fill_in "Nm", with: @nm_group_competitor.nm_id
    click_on "Create Nm group competitor"

    assert_text "Nm group competitor was successfully created"
    click_on "Back"
  end

  test "should update Nm group competitor" do
    visit nm_group_competitor_url(@nm_group_competitor)
    click_on "Edit this nm group competitor", match: :first

    fill_in "Name", with: @nm_group_competitor.name
    fill_in "Nm", with: @nm_group_competitor.nm_id
    click_on "Update Nm group competitor"

    assert_text "Nm group competitor was successfully updated"
    click_on "Back"
  end

  test "should destroy Nm group competitor" do
    visit nm_group_competitor_url(@nm_group_competitor)
    click_on "Destroy this nm group competitor", match: :first

    assert_text "Nm group competitor was successfully destroyed"
  end
end
