require "application_system_test_case"

class AutoresponsersTest < ApplicationSystemTestCase
  setup do
    @autoresponser = autoresponsers(:one)
  end

  test "visiting the index" do
    visit autoresponsers_url
    assert_selector "h1", text: "Autoresponsers"
  end

  test "should create autoresponser" do
    visit autoresponsers_url
    click_on "New autoresponser"

    fill_in "Project", with: @autoresponser.project_id
    fill_in "Rating", with: @autoresponser.rating
    fill_in "Text", with: @autoresponser.text
    click_on "Create Autoresponser"

    assert_text "Autoresponser was successfully created"
    click_on "Back"
  end

  test "should update Autoresponser" do
    visit autoresponser_url(@autoresponser)
    click_on "Edit this autoresponser", match: :first

    fill_in "Project", with: @autoresponser.project_id
    fill_in "Rating", with: @autoresponser.rating
    fill_in "Text", with: @autoresponser.text
    click_on "Update Autoresponser"

    assert_text "Autoresponser was successfully updated"
    click_on "Back"
  end

  test "should destroy Autoresponser" do
    visit autoresponser_url(@autoresponser)
    click_on "Destroy this autoresponser", match: :first

    assert_text "Autoresponser was successfully destroyed"
  end
end
