require "application_system_test_case"

class AppGlobalConstantsTest < ApplicationSystemTestCase
  setup do
    @app_global_constant = app_global_constants(:one)
  end

  test "visiting the index" do
    visit app_global_constants_url
    assert_selector "h1", text: "App global constants"
  end

  test "should create app global constant" do
    visit app_global_constants_url
    click_on "New app global constant"

    fill_in "Name", with: @app_global_constant.name
    fill_in "Value", with: @app_global_constant.value
    fill_in "Value type", with: @app_global_constant.value_type
    click_on "Create App global constant"

    assert_text "App global constant was successfully created"
    click_on "Back"
  end

  test "should update App global constant" do
    visit app_global_constant_url(@app_global_constant)
    click_on "Edit this app global constant", match: :first

    fill_in "Name", with: @app_global_constant.name
    fill_in "Value", with: @app_global_constant.value
    fill_in "Value type", with: @app_global_constant.value_type
    click_on "Update App global constant"

    assert_text "App global constant was successfully updated"
    click_on "Back"
  end

  test "should destroy App global constant" do
    visit app_global_constant_url(@app_global_constant)
    click_on "Destroy this app global constant", match: :first

    assert_text "App global constant was successfully destroyed"
  end
end
