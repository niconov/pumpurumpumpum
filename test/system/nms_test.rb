require "application_system_test_case"

class NmsTest < ApplicationSystemTestCase
  setup do
    @nm = nms(:one)
  end

  test "visiting the index" do
    visit nms_url
    assert_selector "h1", text: "Nms"
  end

  test "should create nm" do
    visit nms_url
    click_on "New nm"

    fill_in "Barcode", with: @nm.barcode
    fill_in "Imt", with: @nm.imt_id
    fill_in "Nm", with: @nm.nm_id
    check "Prohibited" if @nm.prohibited
    fill_in "Supplier article", with: @nm.supplier_article
    fill_in "User", with: @nm.user_id
    click_on "Create Nm"

    assert_text "Nm was successfully created"
    click_on "Back"
  end

  test "should update Nm" do
    visit nm_url(@nm)
    click_on "Edit this nm", match: :first

    fill_in "Barcode", with: @nm.barcode
    fill_in "Imt", with: @nm.imt_id
    fill_in "Nm", with: @nm.nm_id
    check "Prohibited" if @nm.prohibited
    fill_in "Supplier article", with: @nm.supplier_article
    fill_in "User", with: @nm.user_id
    click_on "Update Nm"

    assert_text "Nm was successfully updated"
    click_on "Back"
  end

  test "should destroy Nm" do
    visit nm_url(@nm)
    click_on "Destroy this nm", match: :first

    assert_text "Nm was successfully destroyed"
  end
end
