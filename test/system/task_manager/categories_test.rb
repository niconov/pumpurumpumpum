require "application_system_test_case"

class TaskManager::CategoriesTest < ApplicationSystemTestCase
  setup do
    @task_manager_category = task_manager_categories(:one)
  end

  test "visiting the index" do
    visit task_manager_categories_url
    assert_selector "h1", text: "Categories"
  end

  test "should create category" do
    visit task_manager_categories_url
    click_on "New category"

    fill_in "Project", with: @task_manager_category.project_id
    fill_in "Title", with: @task_manager_category.title
    click_on "Create Category"

    assert_text "Category was successfully created"
    click_on "Back"
  end

  test "should update Category" do
    visit task_manager_category_url(@task_manager_category)
    click_on "Edit this category", match: :first

    fill_in "Project", with: @task_manager_category.project_id
    fill_in "Title", with: @task_manager_category.title
    click_on "Update Category"

    assert_text "Category was successfully updated"
    click_on "Back"
  end

  test "should destroy Category" do
    visit task_manager_category_url(@task_manager_category)
    click_on "Destroy this category", match: :first

    assert_text "Category was successfully destroyed"
  end
end
