require "application_system_test_case"

class TaskManager::TasksTest < ApplicationSystemTestCase
  setup do
    @task_manager_task = task_manager_tasks(:one)
  end

  test "visiting the index" do
    visit task_manager_tasks_url
    assert_selector "h1", text: "Tasks"
  end

  test "should create task" do
    visit task_manager_tasks_url
    click_on "New task"

    fill_in "Category", with: @task_manager_task.category_id
    fill_in "Project", with: @task_manager_task.project_id
    fill_in "Status", with: @task_manager_task.status_id
    fill_in "Text", with: @task_manager_task.text
    fill_in "Title", with: @task_manager_task.title
    click_on "Create Task"

    assert_text "Task was successfully created"
    click_on "Back"
  end

  test "should update Task" do
    visit task_manager_task_url(@task_manager_task)
    click_on "Edit this task", match: :first

    fill_in "Category", with: @task_manager_task.category_id
    fill_in "Project", with: @task_manager_task.project_id
    fill_in "Status", with: @task_manager_task.status_id
    fill_in "Text", with: @task_manager_task.text
    fill_in "Title", with: @task_manager_task.title
    click_on "Update Task"

    assert_text "Task was successfully updated"
    click_on "Back"
  end

  test "should destroy Task" do
    visit task_manager_task_url(@task_manager_task)
    click_on "Destroy this task", match: :first

    assert_text "Task was successfully destroyed"
  end
end
