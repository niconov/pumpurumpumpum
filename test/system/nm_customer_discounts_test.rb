require "application_system_test_case"

class NmCustomerDiscountsTest < ApplicationSystemTestCase
  setup do
    @nm_customer_discount = nm_customer_discounts(:one)
  end

  test "visiting the index" do
    visit nm_customer_discounts_url
    assert_selector "h1", text: "Nm customer discounts"
  end

  test "should create nm customer discount" do
    visit nm_customer_discounts_url
    click_on "New nm customer discount"

    fill_in "Bound left", with: @nm_customer_discount.bound_left
    fill_in "Bound right", with: @nm_customer_discount.bound_right
    fill_in "Category", with: @nm_customer_discount.category_id
    fill_in "Nm group query", with: @nm_customer_discount.nm_group_query_id
    fill_in "Spp", with: @nm_customer_discount.spp
    click_on "Create Nm customer discount"

    assert_text "Nm customer discount was successfully created"
    click_on "Back"
  end

  test "should update Nm customer discount" do
    visit nm_customer_discount_url(@nm_customer_discount)
    click_on "Edit this nm customer discount", match: :first

    fill_in "Bound left", with: @nm_customer_discount.bound_left
    fill_in "Bound right", with: @nm_customer_discount.bound_right
    fill_in "Category", with: @nm_customer_discount.category_id
    fill_in "Nm group query", with: @nm_customer_discount.nm_group_query_id
    fill_in "Spp", with: @nm_customer_discount.spp
    click_on "Update Nm customer discount"

    assert_text "Nm customer discount was successfully updated"
    click_on "Back"
  end

  test "should destroy Nm customer discount" do
    visit nm_customer_discount_url(@nm_customer_discount)
    click_on "Destroy this nm customer discount", match: :first

    assert_text "Nm customer discount was successfully destroyed"
  end
end
