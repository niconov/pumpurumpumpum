require "application_system_test_case"

class SaleReportsTest < ApplicationSystemTestCase
  setup do
    @sale_report = sale_reports(:one)
  end

  test "visiting the index" do
    visit sale_reports_url
    assert_selector "h1", text: "Sale reports"
  end

  test "should create sale report" do
    visit sale_reports_url
    click_on "New sale report"

    fill_in "Acquiring bank", with: @sale_report.acquiring_bank
    fill_in "Acquiring fee", with: @sale_report.acquiring_fee
    fill_in "Additional payment", with: @sale_report.additional_payment
    fill_in "Barcode", with: @sale_report.barcode
    fill_in "Bonus type name", with: @sale_report.bonus_type_name
    fill_in "Brand name", with: @sale_report.brand_name
    fill_in "Commission percent", with: @sale_report.commission_percent
    fill_in "Create dt", with: @sale_report.create_dt
    fill_in "Currency name", with: @sale_report.currency_name
    fill_in "Date from", with: @sale_report.date_from
    fill_in "Date to", with: @sale_report.date_to
    fill_in "Declaration number", with: @sale_report.declaration_number
    fill_in "Delivery amount", with: @sale_report.delivery_amount
    fill_in "Delivery rub", with: @sale_report.delivery_rub
    fill_in "Doc type name", with: @sale_report.doc_type_name
    fill_in "Gi box type name", with: @sale_report.gi_box_type_name
    fill_in "Gi", with: @sale_report.gi_id
    fill_in "Is kgvp v2", with: @sale_report.is_kgvp_v2
    fill_in "Kiz", with: @sale_report.kiz
    fill_in "Nm", with: @sale_report.nm_id
    fill_in "Office name", with: @sale_report.office_name
    fill_in "Order dt", with: @sale_report.order_dt
    fill_in "Penalty", with: @sale_report.penalty
    fill_in "Ppvz for pay", with: @sale_report.ppvz_for_pay
    fill_in "Ppvz inn", with: @sale_report.ppvz_inn
    fill_in "Ppvz kvw prc", with: @sale_report.ppvz_kvw_prc
    fill_in "Ppvz kvw prc base", with: @sale_report.ppvz_kvw_prc_base
    fill_in "Ppvz office", with: @sale_report.ppvz_office_id
    fill_in "Ppvz office name", with: @sale_report.ppvz_office_name
    fill_in "Ppvz reward", with: @sale_report.ppvz_reward
    fill_in "Ppvz sales commission", with: @sale_report.ppvz_sales_commission
    fill_in "Ppvz spp prc", with: @sale_report.ppvz_spp_prc
    fill_in "Ppvz supplier", with: @sale_report.ppvz_supplier_id
    fill_in "Ppvz supplier name", with: @sale_report.ppvz_supplier_name
    fill_in "Ppvz vw", with: @sale_report.ppvz_vw
    fill_in "Ppvz vw nds", with: @sale_report.ppvz_vw_nds
    fill_in "Product discount for report", with: @sale_report.product_discount_for_report
    fill_in "Quantity", with: @sale_report.quantity
    fill_in "Realization report", with: @sale_report.realization_report_id
    fill_in "Rebill logistic cost", with: @sale_report.rebill_logistic_cost
    fill_in "Rebill logistic org", with: @sale_report.rebill_logistic_org
    fill_in "Retail amount", with: @sale_report.retail_amount
    fill_in "Retail price", with: @sale_report.retail_price
    fill_in "Retail price withdisc rub", with: @sale_report.retail_price_withdisc_rub
    fill_in "Return amount", with: @sale_report.return_amount
    fill_in "Rid", with: @sale_report.rid
    fill_in "Rr dt", with: @sale_report.rr_dt
    fill_in "Rrd", with: @sale_report.rrd_id
    fill_in "Sa name", with: @sale_report.sa_name
    fill_in "Sale dt", with: @sale_report.sale_dt
    fill_in "Sale percent", with: @sale_report.sale_percent
    fill_in "Shk", with: @sale_report.shk_id
    fill_in "Site country", with: @sale_report.site_country
    fill_in "Srid", with: @sale_report.srid
    fill_in "Sticker", with: @sale_report.sticker_id
    fill_in "Subject name", with: @sale_report.subject_name
    fill_in "Sup rating prc up", with: @sale_report.sup_rating_prc_up
    fill_in "Supplier oper name", with: @sale_report.supplier_oper_name
    fill_in "Supplier promo", with: @sale_report.supplier_promo
    fill_in "Suppliercontract code", with: @sale_report.suppliercontract_code
    fill_in "Ts name", with: @sale_report.ts_name
    click_on "Create Sale report"

    assert_text "Sale report was successfully created"
    click_on "Back"
  end

  test "should update Sale report" do
    visit sale_report_url(@sale_report)
    click_on "Edit this sale report", match: :first

    fill_in "Acquiring bank", with: @sale_report.acquiring_bank
    fill_in "Acquiring fee", with: @sale_report.acquiring_fee
    fill_in "Additional payment", with: @sale_report.additional_payment
    fill_in "Barcode", with: @sale_report.barcode
    fill_in "Bonus type name", with: @sale_report.bonus_type_name
    fill_in "Brand name", with: @sale_report.brand_name
    fill_in "Commission percent", with: @sale_report.commission_percent
    fill_in "Create dt", with: @sale_report.create_dt
    fill_in "Currency name", with: @sale_report.currency_name
    fill_in "Date from", with: @sale_report.date_from
    fill_in "Date to", with: @sale_report.date_to
    fill_in "Declaration number", with: @sale_report.declaration_number
    fill_in "Delivery amount", with: @sale_report.delivery_amount
    fill_in "Delivery rub", with: @sale_report.delivery_rub
    fill_in "Doc type name", with: @sale_report.doc_type_name
    fill_in "Gi box type name", with: @sale_report.gi_box_type_name
    fill_in "Gi", with: @sale_report.gi_id
    fill_in "Is kgvp v2", with: @sale_report.is_kgvp_v2
    fill_in "Kiz", with: @sale_report.kiz
    fill_in "Nm", with: @sale_report.nm_id
    fill_in "Office name", with: @sale_report.office_name
    fill_in "Order dt", with: @sale_report.order_dt
    fill_in "Penalty", with: @sale_report.penalty
    fill_in "Ppvz for pay", with: @sale_report.ppvz_for_pay
    fill_in "Ppvz inn", with: @sale_report.ppvz_inn
    fill_in "Ppvz kvw prc", with: @sale_report.ppvz_kvw_prc
    fill_in "Ppvz kvw prc base", with: @sale_report.ppvz_kvw_prc_base
    fill_in "Ppvz office", with: @sale_report.ppvz_office_id
    fill_in "Ppvz office name", with: @sale_report.ppvz_office_name
    fill_in "Ppvz reward", with: @sale_report.ppvz_reward
    fill_in "Ppvz sales commission", with: @sale_report.ppvz_sales_commission
    fill_in "Ppvz spp prc", with: @sale_report.ppvz_spp_prc
    fill_in "Ppvz supplier", with: @sale_report.ppvz_supplier_id
    fill_in "Ppvz supplier name", with: @sale_report.ppvz_supplier_name
    fill_in "Ppvz vw", with: @sale_report.ppvz_vw
    fill_in "Ppvz vw nds", with: @sale_report.ppvz_vw_nds
    fill_in "Product discount for report", with: @sale_report.product_discount_for_report
    fill_in "Quantity", with: @sale_report.quantity
    fill_in "Realization report", with: @sale_report.realization_report_id
    fill_in "Rebill logistic cost", with: @sale_report.rebill_logistic_cost
    fill_in "Rebill logistic org", with: @sale_report.rebill_logistic_org
    fill_in "Retail amount", with: @sale_report.retail_amount
    fill_in "Retail price", with: @sale_report.retail_price
    fill_in "Retail price withdisc rub", with: @sale_report.retail_price_withdisc_rub
    fill_in "Return amount", with: @sale_report.return_amount
    fill_in "Rid", with: @sale_report.rid
    fill_in "Rr dt", with: @sale_report.rr_dt
    fill_in "Rrd", with: @sale_report.rrd_id
    fill_in "Sa name", with: @sale_report.sa_name
    fill_in "Sale dt", with: @sale_report.sale_dt
    fill_in "Sale percent", with: @sale_report.sale_percent
    fill_in "Shk", with: @sale_report.shk_id
    fill_in "Site country", with: @sale_report.site_country
    fill_in "Srid", with: @sale_report.srid
    fill_in "Sticker", with: @sale_report.sticker_id
    fill_in "Subject name", with: @sale_report.subject_name
    fill_in "Sup rating prc up", with: @sale_report.sup_rating_prc_up
    fill_in "Supplier oper name", with: @sale_report.supplier_oper_name
    fill_in "Supplier promo", with: @sale_report.supplier_promo
    fill_in "Suppliercontract code", with: @sale_report.suppliercontract_code
    fill_in "Ts name", with: @sale_report.ts_name
    click_on "Update Sale report"

    assert_text "Sale report was successfully updated"
    click_on "Back"
  end

  test "should destroy Sale report" do
    visit sale_report_url(@sale_report)
    click_on "Destroy this sale report", match: :first

    assert_text "Sale report was successfully destroyed"
  end
end
