require "application_system_test_case"

class Deliveries::ScheduledItemsTest < ApplicationSystemTestCase
  setup do
    @deliveries_scheduled_item = deliveries_scheduled_items(:one)
  end

  test "visiting the index" do
    visit deliveries_scheduled_items_url
    assert_selector "h1", text: "Scheduled items"
  end

  test "should create scheduled item" do
    visit deliveries_scheduled_items_url
    click_on "New scheduled item"

    fill_in "Deliveries sheduled", with: @deliveries_scheduled_item.deliveries_sheduled_id
    fill_in "Nm", with: @deliveries_scheduled_item.nm_id
    fill_in "Qty", with: @deliveries_scheduled_item.qty
    click_on "Create Scheduled item"

    assert_text "Scheduled item was successfully created"
    click_on "Back"
  end

  test "should update Scheduled item" do
    visit deliveries_scheduled_item_url(@deliveries_scheduled_item)
    click_on "Edit this scheduled item", match: :first

    fill_in "Deliveries sheduled", with: @deliveries_scheduled_item.deliveries_sheduled_id
    fill_in "Nm", with: @deliveries_scheduled_item.nm_id
    fill_in "Qty", with: @deliveries_scheduled_item.qty
    click_on "Update Scheduled item"

    assert_text "Scheduled item was successfully updated"
    click_on "Back"
  end

  test "should destroy Scheduled item" do
    visit deliveries_scheduled_item_url(@deliveries_scheduled_item)
    click_on "Destroy this scheduled item", match: :first

    assert_text "Scheduled item was successfully destroyed"
  end
end
