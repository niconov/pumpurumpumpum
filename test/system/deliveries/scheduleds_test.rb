require "application_system_test_case"

class Deliveries::ScheduledsTest < ApplicationSystemTestCase
  setup do
    @deliveries_scheduled = deliveries_scheduleds(:one)
  end

  test "visiting the index" do
    visit deliveries_scheduleds_url
    assert_selector "h1", text: "Scheduleds"
  end

  test "should create scheduled" do
    visit deliveries_scheduleds_url
    click_on "New scheduled"

    fill_in "Period", with: @deliveries_scheduled.period
    fill_in "Warehouse", with: @deliveries_scheduled.warehouse_id
    click_on "Create Scheduled"

    assert_text "Scheduled was successfully created"
    click_on "Back"
  end

  test "should update Scheduled" do
    visit deliveries_scheduled_url(@deliveries_scheduled)
    click_on "Edit this scheduled", match: :first

    fill_in "Period", with: @deliveries_scheduled.period
    fill_in "Warehouse", with: @deliveries_scheduled.warehouse_id
    click_on "Update Scheduled"

    assert_text "Scheduled was successfully updated"
    click_on "Back"
  end

  test "should destroy Scheduled" do
    visit deliveries_scheduled_url(@deliveries_scheduled)
    click_on "Destroy this scheduled", match: :first

    assert_text "Scheduled was successfully destroyed"
  end
end
