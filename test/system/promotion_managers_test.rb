require "application_system_test_case"

class PromotionManagersTest < ApplicationSystemTestCase
  setup do
    @promotion_manager = promotion_managers(:one)
  end

  test "visiting the index" do
    visit promotion_managers_url
    assert_selector "h1", text: "Promotion managers"
  end

  test "should create promotion manager" do
    visit promotion_managers_url
    click_on "New promotion manager"

    fill_in "Advert", with: @promotion_manager.advert_id
    fill_in "Budget daily", with: @promotion_manager.budget_daily
    check "Enabled" if @promotion_manager.enabled
    fill_in "Nm", with: @promotion_manager.nm_id
    click_on "Create Promotion manager"

    assert_text "Promotion manager was successfully created"
    click_on "Back"
  end

  test "should update Promotion manager" do
    visit promotion_manager_url(@promotion_manager)
    click_on "Edit this promotion manager", match: :first

    fill_in "Advert", with: @promotion_manager.advert_id
    fill_in "Budget daily", with: @promotion_manager.budget_daily
    check "Enabled" if @promotion_manager.enabled
    fill_in "Nm", with: @promotion_manager.nm_id
    click_on "Update Promotion manager"

    assert_text "Promotion manager was successfully updated"
    click_on "Back"
  end

  test "should destroy Promotion manager" do
    visit promotion_manager_url(@promotion_manager)
    click_on "Destroy this promotion manager", match: :first

    assert_text "Promotion manager was successfully destroyed"
  end
end
