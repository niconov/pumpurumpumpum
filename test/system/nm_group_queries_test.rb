require "application_system_test_case"

class NmGroupQueriesTest < ApplicationSystemTestCase
  setup do
    @nm_group_query = nm_group_queries(:one)
  end

  test "visiting the index" do
    visit nm_group_queries_url
    assert_selector "h1", text: "Nm group queries"
  end

  test "should create nm group query" do
    visit nm_group_queries_url
    click_on "New nm group query"

    fill_in "Name", with: @nm_group_query.name
    fill_in "Project", with: @nm_group_query.project_id
    fill_in "Query", with: @nm_group_query.query
    click_on "Create Nm group query"

    assert_text "Nm group query was successfully created"
    click_on "Back"
  end

  test "should update Nm group query" do
    visit nm_group_query_url(@nm_group_query)
    click_on "Edit this nm group query", match: :first

    fill_in "Name", with: @nm_group_query.name
    fill_in "Project", with: @nm_group_query.project_id
    fill_in "Query", with: @nm_group_query.query
    click_on "Update Nm group query"

    assert_text "Nm group query was successfully updated"
    click_on "Back"
  end

  test "should destroy Nm group query" do
    visit nm_group_query_url(@nm_group_query)
    click_on "Destroy this nm group query", match: :first

    assert_text "Nm group query was successfully destroyed"
  end
end
