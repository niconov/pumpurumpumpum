require "application_system_test_case"

class DeliveryMethodsTest < ApplicationSystemTestCase
  setup do
    @delivery_method = delivery_methods(:one)
  end

  test "visiting the index" do
    visit delivery_methods_url
    assert_selector "h1", text: "Delivery methods"
  end

  test "should create delivery method" do
    visit delivery_methods_url
    click_on "New delivery method"

    fill_in "Name", with: @delivery_method.name
    fill_in "Period", with: @delivery_method.period
    fill_in "Price", with: @delivery_method.price
    fill_in "Tariff", with: @delivery_method.tariff_id
    click_on "Create Delivery method"

    assert_text "Delivery method was successfully created"
    click_on "Back"
  end

  test "should update Delivery method" do
    visit delivery_method_url(@delivery_method)
    click_on "Edit this delivery method", match: :first

    fill_in "Name", with: @delivery_method.name
    fill_in "Period", with: @delivery_method.period
    fill_in "Price", with: @delivery_method.price
    fill_in "Tariff", with: @delivery_method.tariff_id
    click_on "Update Delivery method"

    assert_text "Delivery method was successfully updated"
    click_on "Back"
  end

  test "should destroy Delivery method" do
    visit delivery_method_url(@delivery_method)
    click_on "Destroy this delivery method", match: :first

    assert_text "Delivery method was successfully destroyed"
  end
end
