require "application_system_test_case"

class SalesOperationsTest < ApplicationSystemTestCase
  setup do
    @sales_operation = sales_operations(:one)
  end

  test "visiting the index" do
    visit sales_operations_url
    assert_selector "h1", text: "Sales operations"
  end

  test "should create sales operation" do
    visit sales_operations_url
    click_on "New sales operation"

    fill_in "Factor", with: @sales_operation.factor
    fill_in "Name", with: @sales_operation.name
    click_on "Create Sales operation"

    assert_text "Sales operation was successfully created"
    click_on "Back"
  end

  test "should update Sales operation" do
    visit sales_operation_url(@sales_operation)
    click_on "Edit this sales operation", match: :first

    fill_in "Factor", with: @sales_operation.factor
    fill_in "Name", with: @sales_operation.name
    click_on "Update Sales operation"

    assert_text "Sales operation was successfully updated"
    click_on "Back"
  end

  test "should destroy Sales operation" do
    visit sales_operation_url(@sales_operation)
    click_on "Destroy this sales operation", match: :first

    assert_text "Sales operation was successfully destroyed"
  end
end
