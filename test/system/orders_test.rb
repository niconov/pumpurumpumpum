require "application_system_test_case"

class OrdersTest < ApplicationSystemTestCase
  setup do
    @order = orders(:one)
  end

  test "visiting the index" do
    visit orders_url
    assert_selector "h1", text: "Orders"
  end

  test "should create order" do
    visit orders_url
    click_on "New order"

    fill_in "Article", with: @order.article
    fill_in "Barcode", with: @order.barcode
    check "Canceled" if @order.canceled
    fill_in "Comission", with: @order.comission_id
    fill_in "Datetime", with: @order.datetime
    fill_in "Discount", with: @order.discount
    fill_in "Gnumber", with: @order.gnumber
    fill_in "Income", with: @order.income_id
    fill_in "Last change date", with: @order.last_change_date
    fill_in "Nm", with: @order.nm_id
    fill_in "Odid", with: @order.odid
    fill_in "Orde typer", with: @order.orde_typer
    fill_in "Price", with: @order.price
    fill_in "Region", with: @order.region
    fill_in "Region", with: @order.region_id
    fill_in "Srid", with: @order.srid
    fill_in "User", with: @order.user_id
    fill_in "Warehouse", with: @order.warehouse
    fill_in "Warehouse", with: @order.warehouse_id
    click_on "Create Order"

    assert_text "Order was successfully created"
    click_on "Back"
  end

  test "should update Order" do
    visit order_url(@order)
    click_on "Edit this order", match: :first

    fill_in "Article", with: @order.article
    fill_in "Barcode", with: @order.barcode
    check "Canceled" if @order.canceled
    fill_in "Comission", with: @order.comission_id
    fill_in "Datetime", with: @order.datetime
    fill_in "Discount", with: @order.discount
    fill_in "Gnumber", with: @order.gnumber
    fill_in "Income", with: @order.income_id
    fill_in "Last change date", with: @order.last_change_date
    fill_in "Nm", with: @order.nm_id
    fill_in "Odid", with: @order.odid
    fill_in "Orde typer", with: @order.orde_typer
    fill_in "Price", with: @order.price
    fill_in "Region", with: @order.region
    fill_in "Region", with: @order.region_id
    fill_in "Srid", with: @order.srid
    fill_in "User", with: @order.user_id
    fill_in "Warehouse", with: @order.warehouse
    fill_in "Warehouse", with: @order.warehouse_id
    click_on "Update Order"

    assert_text "Order was successfully updated"
    click_on "Back"
  end

  test "should destroy Order" do
    visit order_url(@order)
    click_on "Destroy this order", match: :first

    assert_text "Order was successfully destroyed"
  end
end
