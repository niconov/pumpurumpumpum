require "application_system_test_case"

class ProjectRolesTest < ApplicationSystemTestCase
  setup do
    @project_role = project_roles(:one)
  end

  test "visiting the index" do
    visit project_roles_url
    assert_selector "h1", text: "Project roles"
  end

  test "should create project role" do
    visit project_roles_url
    click_on "New project role"

    fill_in "User", with: @project_role.user_id
    fill_in "User role", with: @project_role.user_role
    click_on "Create Project role"

    assert_text "Project role was successfully created"
    click_on "Back"
  end

  test "should update Project role" do
    visit project_role_url(@project_role)
    click_on "Edit this project role", match: :first

    fill_in "User", with: @project_role.user_id
    fill_in "User role", with: @project_role.user_role
    click_on "Update Project role"

    assert_text "Project role was successfully updated"
    click_on "Back"
  end

  test "should destroy Project role" do
    visit project_role_url(@project_role)
    click_on "Destroy this project role", match: :first

    assert_text "Project role was successfully destroyed"
  end
end
