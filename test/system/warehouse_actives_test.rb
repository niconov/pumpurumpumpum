require "application_system_test_case"

class WarehouseActivesTest < ApplicationSystemTestCase
  setup do
    @warehouse_active = warehouse_actives(:one)
  end

  test "visiting the index" do
    visit warehouse_actives_url
    assert_selector "h1", text: "Warehouse actives"
  end

  test "should create warehouse active" do
    visit warehouse_actives_url
    click_on "New warehouse active"

    check "Active" if @warehouse_active.active
    fill_in "User", with: @warehouse_active.user_id
    fill_in "Warehouse", with: @warehouse_active.warehouse_id
    click_on "Create Warehouse active"

    assert_text "Warehouse active was successfully created"
    click_on "Back"
  end

  test "should update Warehouse active" do
    visit warehouse_active_url(@warehouse_active)
    click_on "Edit this warehouse active", match: :first

    check "Active" if @warehouse_active.active
    fill_in "User", with: @warehouse_active.user_id
    fill_in "Warehouse", with: @warehouse_active.warehouse_id
    click_on "Update Warehouse active"

    assert_text "Warehouse active was successfully updated"
    click_on "Back"
  end

  test "should destroy Warehouse active" do
    visit warehouse_active_url(@warehouse_active)
    click_on "Destroy this warehouse active", match: :first

    assert_text "Warehouse active was successfully destroyed"
  end
end
