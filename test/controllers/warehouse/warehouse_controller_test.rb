require "test_helper"

class Warehouse::WarehouseControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get warehouse_warehouse_index_url
    assert_response :success
  end
end
