require "test_helper"

class AutoresponsersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @autoresponser = autoresponsers(:one)
  end

  test "should get index" do
    get autoresponsers_url
    assert_response :success
  end

  test "should get new" do
    get new_autoresponser_url
    assert_response :success
  end

  test "should create autoresponser" do
    assert_difference("Autoresponser.count") do
      post autoresponsers_url, params: { autoresponser: { project_id: @autoresponser.project_id, rating: @autoresponser.rating, text: @autoresponser.text } }
    end

    assert_redirected_to autoresponser_url(Autoresponser.last)
  end

  test "should show autoresponser" do
    get autoresponser_url(@autoresponser)
    assert_response :success
  end

  test "should get edit" do
    get edit_autoresponser_url(@autoresponser)
    assert_response :success
  end

  test "should update autoresponser" do
    patch autoresponser_url(@autoresponser), params: { autoresponser: { project_id: @autoresponser.project_id, rating: @autoresponser.rating, text: @autoresponser.text } }
    assert_redirected_to autoresponser_url(@autoresponser)
  end

  test "should destroy autoresponser" do
    assert_difference("Autoresponser.count", -1) do
      delete autoresponser_url(@autoresponser)
    end

    assert_redirected_to autoresponsers_url
  end
end
