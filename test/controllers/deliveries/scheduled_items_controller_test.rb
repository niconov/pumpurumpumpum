require "test_helper"

class Deliveries::ScheduledItemsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @deliveries_scheduled_item = deliveries_scheduled_items(:one)
  end

  test "should get index" do
    get deliveries_scheduled_items_url
    assert_response :success
  end

  test "should get new" do
    get new_deliveries_scheduled_item_url
    assert_response :success
  end

  test "should create deliveries_scheduled_item" do
    assert_difference("Deliveries::ScheduledItem.count") do
      post deliveries_scheduled_items_url, params: { deliveries_scheduled_item: { deliveries_sheduled_id: @deliveries_scheduled_item.deliveries_sheduled_id, nm_id: @deliveries_scheduled_item.nm_id, qty: @deliveries_scheduled_item.qty } }
    end

    assert_redirected_to deliveries_scheduled_item_url(Deliveries::ScheduledItem.last)
  end

  test "should show deliveries_scheduled_item" do
    get deliveries_scheduled_item_url(@deliveries_scheduled_item)
    assert_response :success
  end

  test "should get edit" do
    get edit_deliveries_scheduled_item_url(@deliveries_scheduled_item)
    assert_response :success
  end

  test "should update deliveries_scheduled_item" do
    patch deliveries_scheduled_item_url(@deliveries_scheduled_item), params: { deliveries_scheduled_item: { deliveries_sheduled_id: @deliveries_scheduled_item.deliveries_sheduled_id, nm_id: @deliveries_scheduled_item.nm_id, qty: @deliveries_scheduled_item.qty } }
    assert_redirected_to deliveries_scheduled_item_url(@deliveries_scheduled_item)
  end

  test "should destroy deliveries_scheduled_item" do
    assert_difference("Deliveries::ScheduledItem.count", -1) do
      delete deliveries_scheduled_item_url(@deliveries_scheduled_item)
    end

    assert_redirected_to deliveries_scheduled_items_url
  end
end
