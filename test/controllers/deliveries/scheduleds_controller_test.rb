require "test_helper"

class Deliveries::ScheduledsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @deliveries_scheduled = deliveries_scheduleds(:one)
  end

  test "should get index" do
    get deliveries_scheduleds_url
    assert_response :success
  end

  test "should get new" do
    get new_deliveries_scheduled_url
    assert_response :success
  end

  test "should create deliveries_scheduled" do
    assert_difference("Deliveries::Scheduled.count") do
      post deliveries_scheduleds_url, params: { deliveries_scheduled: { period: @deliveries_scheduled.period, warehouse_id: @deliveries_scheduled.warehouse_id } }
    end

    assert_redirected_to deliveries_scheduled_url(Deliveries::Scheduled.last)
  end

  test "should show deliveries_scheduled" do
    get deliveries_scheduled_url(@deliveries_scheduled)
    assert_response :success
  end

  test "should get edit" do
    get edit_deliveries_scheduled_url(@deliveries_scheduled)
    assert_response :success
  end

  test "should update deliveries_scheduled" do
    patch deliveries_scheduled_url(@deliveries_scheduled), params: { deliveries_scheduled: { period: @deliveries_scheduled.period, warehouse_id: @deliveries_scheduled.warehouse_id } }
    assert_redirected_to deliveries_scheduled_url(@deliveries_scheduled)
  end

  test "should destroy deliveries_scheduled" do
    assert_difference("Deliveries::Scheduled.count", -1) do
      delete deliveries_scheduled_url(@deliveries_scheduled)
    end

    assert_redirected_to deliveries_scheduleds_url
  end
end
