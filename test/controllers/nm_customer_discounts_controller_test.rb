require "test_helper"

class NmCustomerDiscountsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @nm_customer_discount = nm_customer_discounts(:one)
  end

  test "should get index" do
    get nm_customer_discounts_url
    assert_response :success
  end

  test "should get new" do
    get new_nm_customer_discount_url
    assert_response :success
  end

  test "should create nm_customer_discount" do
    assert_difference("NmCustomerDiscount.count") do
      post nm_customer_discounts_url, params: { nm_customer_discount: { bound_left: @nm_customer_discount.bound_left, bound_right: @nm_customer_discount.bound_right, category_id: @nm_customer_discount.category_id, nm_group_query_id: @nm_customer_discount.nm_group_query_id, spp: @nm_customer_discount.spp } }
    end

    assert_redirected_to nm_customer_discount_url(NmCustomerDiscount.last)
  end

  test "should show nm_customer_discount" do
    get nm_customer_discount_url(@nm_customer_discount)
    assert_response :success
  end

  test "should get edit" do
    get edit_nm_customer_discount_url(@nm_customer_discount)
    assert_response :success
  end

  test "should update nm_customer_discount" do
    patch nm_customer_discount_url(@nm_customer_discount), params: { nm_customer_discount: { bound_left: @nm_customer_discount.bound_left, bound_right: @nm_customer_discount.bound_right, category_id: @nm_customer_discount.category_id, nm_group_query_id: @nm_customer_discount.nm_group_query_id, spp: @nm_customer_discount.spp } }
    assert_redirected_to nm_customer_discount_url(@nm_customer_discount)
  end

  test "should destroy nm_customer_discount" do
    assert_difference("NmCustomerDiscount.count", -1) do
      delete nm_customer_discount_url(@nm_customer_discount)
    end

    assert_redirected_to nm_customer_discounts_url
  end
end
