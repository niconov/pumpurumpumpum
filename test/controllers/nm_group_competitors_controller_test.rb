require "test_helper"

class NmGroupCompetitorsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @nm_group_competitor = nm_group_competitors(:one)
  end

  test "should get index" do
    get nm_group_competitors_url
    assert_response :success
  end

  test "should get new" do
    get new_nm_group_competitor_url
    assert_response :success
  end

  test "should create nm_group_competitor" do
    assert_difference("NmGroupCompetitor.count") do
      post nm_group_competitors_url, params: { nm_group_competitor: { name: @nm_group_competitor.name, nm_id: @nm_group_competitor.nm_id } }
    end

    assert_redirected_to nm_group_competitor_url(NmGroupCompetitor.last)
  end

  test "should show nm_group_competitor" do
    get nm_group_competitor_url(@nm_group_competitor)
    assert_response :success
  end

  test "should get edit" do
    get edit_nm_group_competitor_url(@nm_group_competitor)
    assert_response :success
  end

  test "should update nm_group_competitor" do
    patch nm_group_competitor_url(@nm_group_competitor), params: { nm_group_competitor: { name: @nm_group_competitor.name, nm_id: @nm_group_competitor.nm_id } }
    assert_redirected_to nm_group_competitor_url(@nm_group_competitor)
  end

  test "should destroy nm_group_competitor" do
    assert_difference("NmGroupCompetitor.count", -1) do
      delete nm_group_competitor_url(@nm_group_competitor)
    end

    assert_redirected_to nm_group_competitors_url
  end
end
