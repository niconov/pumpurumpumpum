require "test_helper"

class AdvertsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @advert = adverts(:one)
  end

  test "should get index" do
    get adverts_url
    assert_response :success
  end

  test "should get new" do
    get new_advert_url
    assert_response :success
  end

  test "should create advert" do
    assert_difference("Advert.count") do
      post adverts_url, params: { advert: { advert_id: @advert.advert_id, bid_current: @advert.bid_current, max_cpm: @advert.max_cpm, min_place: @advert.min_place, name: @advert.name, param_name: @advert.param_name, param_value: @advert.param_value, search_query: @advert.search_query, status: @advert.status, timestamps: @advert.timestamps, type_id: @advert.type_id, user_id: @advert.user_id } }
    end

    assert_redirected_to advert_url(Advert::Advert.last)
  end

  test "should show advert" do
    get advert_url(@advert)
    assert_response :success
  end

  test "should get edit" do
    get edit_advert_url(@advert)
    assert_response :success
  end

  test "should update advert" do
    patch advert_url(@advert), params: { advert: { advert_id: @advert.advert_id, bid_current: @advert.bid_current, max_cpm: @advert.max_cpm, min_place: @advert.min_place, name: @advert.name, param_name: @advert.param_name, param_value: @advert.param_value, search_query: @advert.search_query, status: @advert.status, timestamps: @advert.timestamps, type_id: @advert.type_id, user_id: @advert.user_id } }
    assert_redirected_to advert_url(@advert)
  end

  test "should destroy advert" do
    assert_difference("Advert.count", -1) do
      delete advert_url(@advert)
    end

    assert_redirected_to adverts_url
  end
end
