require "test_helper"

class TaskManager::CategoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @task_manager_category = task_manager_categories(:one)
  end

  test "should get index" do
    get task_manager_categories_url
    assert_response :success
  end

  test "should get new" do
    get new_task_manager_category_url
    assert_response :success
  end

  test "should create task_manager_category" do
    assert_difference("TaskManager::Category.count") do
      post task_manager_categories_url, params: { task_manager_category: { project_id: @task_manager_category.project_id, title: @task_manager_category.title } }
    end

    assert_redirected_to task_manager_category_url(TaskManager::Category.last)
  end

  test "should show task_manager_category" do
    get task_manager_category_url(@task_manager_category)
    assert_response :success
  end

  test "should get edit" do
    get edit_task_manager_category_url(@task_manager_category)
    assert_response :success
  end

  test "should update task_manager_category" do
    patch task_manager_category_url(@task_manager_category), params: { task_manager_category: { project_id: @task_manager_category.project_id, title: @task_manager_category.title } }
    assert_redirected_to task_manager_category_url(@task_manager_category)
  end

  test "should destroy task_manager_category" do
    assert_difference("TaskManager::Category.count", -1) do
      delete task_manager_category_url(@task_manager_category)
    end

    assert_redirected_to task_manager_categories_url
  end
end
