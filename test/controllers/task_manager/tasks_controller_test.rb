require "test_helper"

class TaskManager::TasksControllerTest < ActionDispatch::IntegrationTest
  setup do
    @task_manager_task = task_manager_tasks(:one)
  end

  test "should get index" do
    get task_manager_tasks_url
    assert_response :success
  end

  test "should get new" do
    get new_task_manager_task_url
    assert_response :success
  end

  test "should create task_manager_task" do
    assert_difference("TaskManager::Task.count") do
      post task_manager_tasks_url, params: { task_manager_task: { category_id: @task_manager_task.category_id, project_id: @task_manager_task.project_id, status_id: @task_manager_task.status_id, text: @task_manager_task.text, title: @task_manager_task.title } }
    end

    assert_redirected_to task_manager_task_url(TaskManager::Task.last)
  end

  test "should show task_manager_task" do
    get task_manager_task_url(@task_manager_task)
    assert_response :success
  end

  test "should get edit" do
    get edit_task_manager_task_url(@task_manager_task)
    assert_response :success
  end

  test "should update task_manager_task" do
    patch task_manager_task_url(@task_manager_task), params: { task_manager_task: { category_id: @task_manager_task.category_id, project_id: @task_manager_task.project_id, status_id: @task_manager_task.status_id, text: @task_manager_task.text, title: @task_manager_task.title } }
    assert_redirected_to task_manager_task_url(@task_manager_task)
  end

  test "should destroy task_manager_task" do
    assert_difference("TaskManager::Task.count", -1) do
      delete task_manager_task_url(@task_manager_task)
    end

    assert_redirected_to task_manager_tasks_url
  end
end
