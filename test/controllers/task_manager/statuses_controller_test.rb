require "test_helper"

class TaskManager::StatusesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @task_manager_status = task_manager_statuses(:one)
  end

  test "should get index" do
    get task_manager_statuses_url
    assert_response :success
  end

  test "should get new" do
    get new_task_manager_status_url
    assert_response :success
  end

  test "should create task_manager_status" do
    assert_difference("TaskManager::Status.count") do
      post task_manager_statuses_url, params: { task_manager_status: { color: @task_manager_status.color, title: @task_manager_status.title } }
    end

    assert_redirected_to task_manager_status_url(TaskManager::Status.last)
  end

  test "should show task_manager_status" do
    get task_manager_status_url(@task_manager_status)
    assert_response :success
  end

  test "should get edit" do
    get edit_task_manager_status_url(@task_manager_status)
    assert_response :success
  end

  test "should update task_manager_status" do
    patch task_manager_status_url(@task_manager_status), params: { task_manager_status: { color: @task_manager_status.color, title: @task_manager_status.title } }
    assert_redirected_to task_manager_status_url(@task_manager_status)
  end

  test "should destroy task_manager_status" do
    assert_difference("TaskManager::Status.count", -1) do
      delete task_manager_status_url(@task_manager_status)
    end

    assert_redirected_to task_manager_statuses_url
  end
end
