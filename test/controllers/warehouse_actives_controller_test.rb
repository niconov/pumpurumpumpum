require "test_helper"

class WarehouseActivesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @warehouse_active = warehouse_actives(:one)
  end

  test "should get index" do
    get warehouse_actives_url
    assert_response :success
  end

  test "should get new" do
    get new_warehouse_active_url
    assert_response :success
  end

  test "should create warehouse_active" do
    assert_difference("WarehouseActive.count") do
      post warehouse_actives_url, params: { warehouse_active: { active: @warehouse_active.active, user_id: @warehouse_active.user_id, warehouse_id: @warehouse_active.warehouse_id } }
    end

    assert_redirected_to warehouse_active_url(WarehouseActive.last)
  end

  test "should show warehouse_active" do
    get warehouse_active_url(@warehouse_active)
    assert_response :success
  end

  test "should get edit" do
    get edit_warehouse_active_url(@warehouse_active)
    assert_response :success
  end

  test "should update warehouse_active" do
    patch warehouse_active_url(@warehouse_active), params: { warehouse_active: { active: @warehouse_active.active, user_id: @warehouse_active.user_id, warehouse_id: @warehouse_active.warehouse_id } }
    assert_redirected_to warehouse_active_url(@warehouse_active)
  end

  test "should destroy warehouse_active" do
    assert_difference("WarehouseActive.count", -1) do
      delete warehouse_active_url(@warehouse_active)
    end

    assert_redirected_to warehouse_actives_url
  end
end
