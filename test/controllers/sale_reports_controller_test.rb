require "test_helper"

class SaleReportsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @sale_report = sale_reports(:one)
  end

  test "should get index" do
    get sale_reports_url
    assert_response :success
  end

  test "should get new" do
    get new_sale_report_url
    assert_response :success
  end

  test "should create sale_report" do
    assert_difference("SaleReport.count") do
      post sale_reports_url, params: { sale_report: { acquiring_bank: @sale_report.acquiring_bank, acquiring_fee: @sale_report.acquiring_fee, additional_payment: @sale_report.additional_payment, barcode: @sale_report.barcode, bonus_type_name: @sale_report.bonus_type_name, brand_name: @sale_report.brand_name, commission_percent: @sale_report.commission_percent, create_dt: @sale_report.create_dt, currency_name: @sale_report.currency_name, date_from: @sale_report.date_from, date_to: @sale_report.date_to, declaration_number: @sale_report.declaration_number, delivery_amount: @sale_report.delivery_amount, delivery_rub: @sale_report.delivery_rub, doc_type_name: @sale_report.doc_type_name, gi_box_type_name: @sale_report.gi_box_type_name, gi_id: @sale_report.gi_id, is_kgvp_v2: @sale_report.is_kgvp_v2, kiz: @sale_report.kiz, nm_id: @sale_report.nm_id, office_name: @sale_report.office_name, order_dt: @sale_report.order_dt, penalty: @sale_report.penalty, ppvz_for_pay: @sale_report.ppvz_for_pay, ppvz_inn: @sale_report.ppvz_inn, ppvz_kvw_prc: @sale_report.ppvz_kvw_prc, ppvz_kvw_prc_base: @sale_report.ppvz_kvw_prc_base, ppvz_office_id: @sale_report.ppvz_office_id, ppvz_office_name: @sale_report.ppvz_office_name, ppvz_reward: @sale_report.ppvz_reward, ppvz_sales_commission: @sale_report.ppvz_sales_commission, ppvz_spp_prc: @sale_report.ppvz_spp_prc, ppvz_supplier_id: @sale_report.ppvz_supplier_id, ppvz_supplier_name: @sale_report.ppvz_supplier_name, ppvz_vw: @sale_report.ppvz_vw, ppvz_vw_nds: @sale_report.ppvz_vw_nds, product_discount_for_report: @sale_report.product_discount_for_report, quantity: @sale_report.quantity, realization_report_id: @sale_report.realization_report_id, rebill_logistic_cost: @sale_report.rebill_logistic_cost, rebill_logistic_org: @sale_report.rebill_logistic_org, retail_amount: @sale_report.retail_amount, retail_price: @sale_report.retail_price, retail_price_withdisc_rub: @sale_report.retail_price_withdisc_rub, return_amount: @sale_report.return_amount, rid: @sale_report.rid, rr_dt: @sale_report.rr_dt, rrd_id: @sale_report.rrd_id, sa_name: @sale_report.sa_name, sale_dt: @sale_report.sale_dt, sale_percent: @sale_report.sale_percent, shk_id: @sale_report.shk_id, site_country: @sale_report.site_country, srid: @sale_report.srid, sticker_id: @sale_report.sticker_id, subject_name: @sale_report.subject_name, sup_rating_prc_up: @sale_report.sup_rating_prc_up, supplier_oper_name: @sale_report.supplier_oper_name, supplier_promo: @sale_report.supplier_promo, suppliercontract_code: @sale_report.suppliercontract_code, ts_name: @sale_report.ts_name } }
    end

    assert_redirected_to sale_report_url(Sale::Report.last)
  end

  test "should show sale_report" do
    get sale_report_url(@sale_report)
    assert_response :success
  end

  test "should get edit" do
    get edit_sale_report_url(@sale_report)
    assert_response :success
  end

  test "should update sale_report" do
    patch sale_report_url(@sale_report), params: { sale_report: { acquiring_bank: @sale_report.acquiring_bank, acquiring_fee: @sale_report.acquiring_fee, additional_payment: @sale_report.additional_payment, barcode: @sale_report.barcode, bonus_type_name: @sale_report.bonus_type_name, brand_name: @sale_report.brand_name, commission_percent: @sale_report.commission_percent, create_dt: @sale_report.create_dt, currency_name: @sale_report.currency_name, date_from: @sale_report.date_from, date_to: @sale_report.date_to, declaration_number: @sale_report.declaration_number, delivery_amount: @sale_report.delivery_amount, delivery_rub: @sale_report.delivery_rub, doc_type_name: @sale_report.doc_type_name, gi_box_type_name: @sale_report.gi_box_type_name, gi_id: @sale_report.gi_id, is_kgvp_v2: @sale_report.is_kgvp_v2, kiz: @sale_report.kiz, nm_id: @sale_report.nm_id, office_name: @sale_report.office_name, order_dt: @sale_report.order_dt, penalty: @sale_report.penalty, ppvz_for_pay: @sale_report.ppvz_for_pay, ppvz_inn: @sale_report.ppvz_inn, ppvz_kvw_prc: @sale_report.ppvz_kvw_prc, ppvz_kvw_prc_base: @sale_report.ppvz_kvw_prc_base, ppvz_office_id: @sale_report.ppvz_office_id, ppvz_office_name: @sale_report.ppvz_office_name, ppvz_reward: @sale_report.ppvz_reward, ppvz_sales_commission: @sale_report.ppvz_sales_commission, ppvz_spp_prc: @sale_report.ppvz_spp_prc, ppvz_supplier_id: @sale_report.ppvz_supplier_id, ppvz_supplier_name: @sale_report.ppvz_supplier_name, ppvz_vw: @sale_report.ppvz_vw, ppvz_vw_nds: @sale_report.ppvz_vw_nds, product_discount_for_report: @sale_report.product_discount_for_report, quantity: @sale_report.quantity, realization_report_id: @sale_report.realization_report_id, rebill_logistic_cost: @sale_report.rebill_logistic_cost, rebill_logistic_org: @sale_report.rebill_logistic_org, retail_amount: @sale_report.retail_amount, retail_price: @sale_report.retail_price, retail_price_withdisc_rub: @sale_report.retail_price_withdisc_rub, return_amount: @sale_report.return_amount, rid: @sale_report.rid, rr_dt: @sale_report.rr_dt, rrd_id: @sale_report.rrd_id, sa_name: @sale_report.sa_name, sale_dt: @sale_report.sale_dt, sale_percent: @sale_report.sale_percent, shk_id: @sale_report.shk_id, site_country: @sale_report.site_country, srid: @sale_report.srid, sticker_id: @sale_report.sticker_id, subject_name: @sale_report.subject_name, sup_rating_prc_up: @sale_report.sup_rating_prc_up, supplier_oper_name: @sale_report.supplier_oper_name, supplier_promo: @sale_report.supplier_promo, suppliercontract_code: @sale_report.suppliercontract_code, ts_name: @sale_report.ts_name } }
    assert_redirected_to sale_report_url(@sale_report)
  end

  test "should destroy sale_report" do
    assert_difference("Sale::Report.count", -1) do
      delete sale_report_url(@sale_report)
    end

    assert_redirected_to sale_reports_url
  end
end
