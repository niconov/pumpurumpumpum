require "test_helper"

class NmsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @nm = nms(:one)
  end

  test "should get index" do
    get nms_url
    assert_response :success
  end

  test "should get new" do
    get new_nm_url
    assert_response :success
  end

  test "should create nm" do
    assert_difference("Nm::Nm.count") do
      post nms_url, params: { nm: { barcode: @nm.barcode, imt_id: @nm.imt_id, nm_id: @nm.nm_id, prohibited: @nm.prohibited, supplier_article: @nm.supplier_article, user_id: @nm.user_id } }
    end

    assert_redirected_to nm_url(Nm::Nm.last)
  end

  test "should show nm" do
    get nm_url(@nm)
    assert_response :success
  end

  test "should get edit" do
    get edit_nm_url(@nm)
    assert_response :success
  end

  test "should update nm" do
    patch nm_url(@nm), params: { nm: { barcode: @nm.barcode, imt_id: @nm.imt_id, nm_id: @nm.nm_id, prohibited: @nm.prohibited, supplier_article: @nm.supplier_article, user_id: @nm.user_id } }
    assert_redirected_to nm_url(@nm)
  end

  test "should destroy nm" do
    assert_difference("Nm::Nm.count", -1) do
      delete nm_url(@nm)
    end

    assert_redirected_to nms_url
  end
end
