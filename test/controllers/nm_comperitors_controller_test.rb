require "test_helper"

class NmComperitorsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @nm_comperitor = nm_comperitors(:one)
  end

  test "should get index" do
    get nm_comperitors_url
    assert_response :success
  end

  test "should get new" do
    get new_nm_comperitor_url
    assert_response :success
  end

  test "should create nm_comperitor" do
    assert_difference("NmComperitor.count") do
      post nm_comperitors_url, params: { nm_comperitor: { nm_group_competitor_id: @nm_comperitor.nm_group_competitor_id } }
    end

    assert_redirected_to nm_comperitor_url(NmComperitor.last)
  end

  test "should show nm_comperitor" do
    get nm_comperitor_url(@nm_comperitor)
    assert_response :success
  end

  test "should get edit" do
    get edit_nm_comperitor_url(@nm_comperitor)
    assert_response :success
  end

  test "should update nm_comperitor" do
    patch nm_comperitor_url(@nm_comperitor), params: { nm_comperitor: { nm_group_competitor_id: @nm_comperitor.nm_group_competitor_id } }
    assert_redirected_to nm_comperitor_url(@nm_comperitor)
  end

  test "should destroy nm_comperitor" do
    assert_difference("NmComperitor.count", -1) do
      delete nm_comperitor_url(@nm_comperitor)
    end

    assert_redirected_to nm_comperitors_url
  end
end
