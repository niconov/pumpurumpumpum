require "test_helper"

class NmGroupQueriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @nm_group_query = nm_group_queries(:one)
  end

  test "should get index" do
    get nm_group_queries_url
    assert_response :success
  end

  test "should get new" do
    get new_nm_group_query_url
    assert_response :success
  end

  test "should create nm_group_query" do
    assert_difference("NmGroupQuery.count") do
      post nm_group_queries_url, params: { nm_group_query: { name: @nm_group_query.name, project_id: @nm_group_query.project_id, query: @nm_group_query.query } }
    end

    assert_redirected_to nm_group_query_url(NmGroupQuery.last)
  end

  test "should show nm_group_query" do
    get nm_group_query_url(@nm_group_query)
    assert_response :success
  end

  test "should get edit" do
    get edit_nm_group_query_url(@nm_group_query)
    assert_response :success
  end

  test "should update nm_group_query" do
    patch nm_group_query_url(@nm_group_query), params: { nm_group_query: { name: @nm_group_query.name, project_id: @nm_group_query.project_id, query: @nm_group_query.query } }
    assert_redirected_to nm_group_query_url(@nm_group_query)
  end

  test "should destroy nm_group_query" do
    assert_difference("NmGroupQuery.count", -1) do
      delete nm_group_query_url(@nm_group_query)
    end

    assert_redirected_to nm_group_queries_url
  end
end
