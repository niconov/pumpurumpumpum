require "test_helper"

class UserWorkersControllerTest < ActionDispatch::IntegrationTest
  test "should get klass:string" do
    get user_workers_klass:string_url
    assert_response :success
  end

  test "should get enabled:boolean" do
    get user_workers_enabled:boolean_url
    assert_response :success
  end
end
