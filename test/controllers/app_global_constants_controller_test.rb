require "test_helper"

class AppGlobalConstantsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @app_global_constant = app_global_constants(:one)
  end

  test "should get index" do
    get app_global_constants_url
    assert_response :success
  end

  test "should get new" do
    get new_app_global_constant_url
    assert_response :success
  end

  test "should create app_global_constant" do
    assert_difference("AppGlobalConstant.count") do
      post app_global_constants_url, params: { app_global_constant: { name: @app_global_constant.name, value: @app_global_constant.value, value_type: @app_global_constant.value_type } }
    end

    assert_redirected_to app_global_constant_url(AppGlobalConstant.last)
  end

  test "should show app_global_constant" do
    get app_global_constant_url(@app_global_constant)
    assert_response :success
  end

  test "should get edit" do
    get edit_app_global_constant_url(@app_global_constant)
    assert_response :success
  end

  test "should update app_global_constant" do
    patch app_global_constant_url(@app_global_constant), params: { app_global_constant: { name: @app_global_constant.name, value: @app_global_constant.value, value_type: @app_global_constant.value_type } }
    assert_redirected_to app_global_constant_url(@app_global_constant)
  end

  test "should destroy app_global_constant" do
    assert_difference("AppGlobalConstant.count", -1) do
      delete app_global_constant_url(@app_global_constant)
    end

    assert_redirected_to app_global_constants_url
  end
end
