require "test_helper"

class SalesOperationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @sales_operation = sales_operations(:one)
  end

  test "should get index" do
    get sales_operations_url
    assert_response :success
  end

  test "should get new" do
    get new_sales_operation_url
    assert_response :success
  end

  test "should create sales_operation" do
    assert_difference("SalesOperation.count") do
      post sales_operations_url, params: { sales_operation: { factor: @sales_operation.factor, name: @sales_operation.name } }
    end

    assert_redirected_to sales_operation_url(SalesOperation.last)
  end

  test "should show sales_operation" do
    get sales_operation_url(@sales_operation)
    assert_response :success
  end

  test "should get edit" do
    get edit_sales_operation_url(@sales_operation)
    assert_response :success
  end

  test "should update sales_operation" do
    patch sales_operation_url(@sales_operation), params: { sales_operation: { factor: @sales_operation.factor, name: @sales_operation.name } }
    assert_redirected_to sales_operation_url(@sales_operation)
  end

  test "should destroy sales_operation" do
    assert_difference("SalesOperation.count", -1) do
      delete sales_operation_url(@sales_operation)
    end

    assert_redirected_to sales_operations_url
  end
end
