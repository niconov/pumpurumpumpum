require "test_helper"

class IncomesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @income = incomes(:one)
  end

  test "should get index" do
    get incomes_url
    assert_response :success
  end

  test "should get new" do
    get new_income_url
    assert_response :success
  end

  test "should create income" do
    assert_difference("Income.count") do
      post incomes_url, params: { income: { barcode: @income.barcode, date: @income.date, date_close: @income.date_close, income_id: @income.income_id, last_change_date: @income.last_change_date, nm_id: @income.nm_id, number: @income.number, quantity: @income.quantity, status: @income.status, supplier_article: @income.supplier_article, tech_size: @income.tech_size, total_price: @income.total_price, warehouse_name: @income.warehouse_name } }
    end

    assert_redirected_to income_url(Income::Income.last)
  end

  test "should show income" do
    get income_url(@income)
    assert_response :success
  end

  test "should get edit" do
    get edit_income_url(@income)
    assert_response :success
  end

  test "should update income" do
    patch income_url(@income), params: { income: { barcode: @income.barcode, date: @income.date, date_close: @income.date_close, income_id: @income.income_id, last_change_date: @income.last_change_date, nm_id: @income.nm_id, number: @income.number, quantity: @income.quantity, status: @income.status, supplier_article: @income.supplier_article, tech_size: @income.tech_size, total_price: @income.total_price, warehouse_name: @income.warehouse_name } }
    assert_redirected_to income_url(@income)
  end

  test "should destroy income" do
    assert_difference("Income.count", -1) do
      delete income_url(@income)
    end

    assert_redirected_to incomes_url
  end
end
