require "test_helper"

class PromotionManagersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @promotion_manager = promotion_managers(:one)
  end

  test "should get index" do
    get promotion_managers_url
    assert_response :success
  end

  test "should get new" do
    get new_promotion_manager_url
    assert_response :success
  end

  test "should create promotion_manager" do
    assert_difference("PromotionManager.count") do
      post promotion_managers_url, params: { promotion_manager: { advert_id: @promotion_manager.advert_id, budget_daily: @promotion_manager.budget_daily, enabled: @promotion_manager.enabled, nm_id: @promotion_manager.nm_id } }
    end

    assert_redirected_to promotion_manager_url(PromotionManager.last)
  end

  test "should show promotion_manager" do
    get promotion_manager_url(@promotion_manager)
    assert_response :success
  end

  test "should get edit" do
    get edit_promotion_manager_url(@promotion_manager)
    assert_response :success
  end

  test "should update promotion_manager" do
    patch promotion_manager_url(@promotion_manager), params: { promotion_manager: { advert_id: @promotion_manager.advert_id, budget_daily: @promotion_manager.budget_daily, enabled: @promotion_manager.enabled, nm_id: @promotion_manager.nm_id } }
    assert_redirected_to promotion_manager_url(@promotion_manager)
  end

  test "should destroy promotion_manager" do
    assert_difference("PromotionManager.count", -1) do
      delete promotion_manager_url(@promotion_manager)
    end

    assert_redirected_to promotion_managers_url
  end
end
